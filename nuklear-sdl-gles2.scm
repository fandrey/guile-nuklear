;;; Copyright (C) 2019 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (nuklear-sdl-gles2)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (nuklear bindings)
  #:use-module (nuklear-sdl-gles2 bindings))

(define-public init nk_sdl_init)
(define-public shutdown nk_sdl_shutdown)
(define-public handle-event nk_sdl_handle_event)

(define-public (render aa max-vertex-buffer max-element-buffer)
  (nk_sdl_render (nk-anti-aliasing->integer aa)
                 max-vertex-buffer
                 max-element-buffer))

(define-public init-font nk_sdl_init_font)
(define-public set-font nk_sdl_set_font)
