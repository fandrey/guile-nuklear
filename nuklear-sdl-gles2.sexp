;; nuklear-sdl-gles2.c:13:29
(function "nk_sdl_init" ((win (:pointer :void))) (:pointer (:struct nk_context)))

;; nuklear-sdl-gles2.c:14:29
(function "nk_sdl_font_stash_begin" ((atlas (:pointer (:pointer ;; nuklear-sdl-gles2.c:14:60
(struct nk_font_atlas
        ))))) :void)

;; nuklear-sdl-gles2.c:15:29
(function "nk_sdl_font_stash_end" () :void)

;; nuklear-sdl-gles2.c:16:29
(function "nk_sdl_handle_event" ((evt (:pointer :void))) :int)

;; nuklear-sdl-gles2.c:17:29
(function "nk_sdl_render" (((:enum nk_anti_aliasing)) (max_vertex_buffer :int) (max_element_buffer :int)) :void)

;; nuklear-sdl-gles2.c:18:29
(function "nk_sdl_shutdown" () :void)

;; nuklear-sdl-gles2.c:19:29
(function "nk_sdl_device_destroy" () :void)

;; nuklear-sdl-gles2.c:20:29
(function "nk_sdl_device_create" () :void)
