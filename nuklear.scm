;;; Copyright (C) 2019 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (nuklear)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 match)
  #:use-module (nuklear bindings))

(define (make-vec2 vec2)
  (make-c-struct nk_vec2_types vec2))

(define (make-vec2* . vec2)
  (make-vec2 vec2))

(define (make-rect rect)
  (make-c-struct nk_rect_types rect))

(define (make-rect* . rect)
  (make-rect rect))

(define (make-color color)
  (make-c-struct nk_color_types color))

(define (make-color* . color)
  (make-color color))

(define (make-colorf colorf)
  (make-c-struct nk_colorf_types colorf))

(define (make-colorf* . colorf)
  (make-colorf colorf))

(define-public (input-begin ctx)
  (nk_input_begin ctx))

(define-public (input-end ctx)
  (nk_input_end ctx))

(define-public (call-with-input ctx proc)
  (input-begin ctx)
  (call-with-values
      (lambda () (proc))
    (lambda vals
      (input-end ctx)
      (apply values vals))))

(define*-public (window-begin ctx name bounds flags #:optional title)
  (= nk_true
     (if title
         (nk_begin_titled ctx
                          (string->pointer name)
                          (string->pointer title)
                          (make-rect bounds)
                          (nk-panel-flags->integer flags))
         (nk_begin ctx
                   (string->pointer name)
                   (make-rect bounds)
                   (nk-panel-flags->integer flags)))))

(define-public window-end nk_end)

(define-public (call-with-window ctx name bounds flags . title-and-proc)
  (let ((title #f))
    (match-let (((or (title proc) (proc)) title-and-proc))
      (when (window-begin ctx name bounds flags title)
        (proc))
      (window-end ctx))))

(define-public (window-find ctx name)
  (nk_window_find ctx (string->pointer name)))

(define-public (window-get-bounds ctx)
  (parse-c-struct (nk_window_get_bounds ctx) nk_rect))

(define-public layout-row-static nk_layout_row_static)
(define-public layout-row-dynamic nk_layout_row_dynamic)

(define-public (layout-row ctx fmt height ratio)
  (nk_layout_row ctx
                 (nk-layout-format->integer fmt)
                 height
                 (length ratio)
                 (bytevector->pointer (list->f32vector ratio))))

(define-public (layout-row* ctx fmt height . ratio)
  (layout-row ctx fmt height ratio))

(define-public (layout-space-begin ctx fmt height count)
  (nk_layout_space_begin ctx (nk-layout-format->integer fmt) height count))

(define-public (layout-space-push ctx x y w h)
  (nk_layout_space_push ctx (make-rect* x y w h)))

(define-public layout-space-end nk_layout_space_end)

(define-public (call-with-layout-space ctx fmt height count proc)
  (layout-space-begin ctx fmt height count)
  (proc)
  (layout-space-end ctx))

(define*-public (group-begin ctx name flags #:optional title)
  (= nk_true
     (if title
         (nk_group_begin_titled ctx
                                (string->pointer name)
                                (string->pointer title)
                                (nk-panel-flags->integer flags))
         (nk_group_begin ctx
                         (string->pointer name)
                         (nk-panel-flags->integer flags)))))

(define-public group-end nk_group_end)

(define-public (call-with-group ctx name flags . title-and-proc)
  (let ((title #f))
    (match-let (((or (title proc) (proc)) title-and-proc))
      (when (group-begin ctx name flags title)
        (proc)
        (group-end ctx)))))

(define-public widget-width nk_widget_width)

(define*-public (label ctx str #:key color wrap (align 'left))
  (cond
   ((and color wrap)
    (nk_label_colored_wrap ctx
                           (string->pointer str)
                           (make-color color)))
   (color
    (nk_label_colored ctx
                      (string->pointer str)
                      (nk-text-alignment->integer align)
                      (make-color color)))
   (wrap
    (nk_label_wrap ctx (string->pointer str)))
   (else
    (nk_label ctx
              (string->pointer str)
              (nk-text-alignment->integer align)))))

(define*-public (button ctx #:key label color symbol image style align)
  (= nk_true
     (cond
      ((and symbol image)
       (error "No button procedure with symbol and image"))
      ((and symbol label style)
       (nk_button_symbol_label_styled ctx
                                      style
                                      (nk-symbol-type->integer symbol)
                                      (string->pointer label)
                                      (nk-text-alignment->integer align)))
      ((and image label style)
       (nk_button_image_label_styled ctx
                                     style
                                     image
                                     (string->pointer label)
                                     (nk-text-alignment->integer align)))
      ((and symbol style)
       (nk_button_symbol_styled ctx
                                style
                                (nk-symbol-type->integer symbol)))
      ((and image style)
       (nk_button_image_styled ctx
                               style
                               image))
      ((and label style)
       (nk_button_label_styled ctx
                               style
                               (string->pointer label)))
      ((and symbol label)
       (nk_button_symbol_label ctx
                               (nk-symbol-type->integer symbol)
                               (string->pointer label)
                               (nk-text-alignment->integer align)))
      ((and image label)
       (nk_button_image_label ctx
                              image
                              (string->pointer label)
                              (nk-text-alignment->integer align)))
      (symbol
       (nk_button_symbol ctx (nk-symbol-type->integer symbol)))
      (image
       (nk_button_image ctx image))
      (color
       (nk_button_color ctx (make-color color)))
      (label
       (nk_button_label ctx (string->pointer label)))
      (else
       (error "No button procedure for such arguments combination")))))

(define-public (option ctx label active)
  (= nk_true
     (nk_option_label ctx
                      (string->pointer label)
                      (if active nk_true nk_false))))

(define*-public (select-label ctx label align value #:key image symbol)
  (= nk_true
     (let ((label (string->pointer label))
           (align (nk-text-alignment->integer align))
           (value (if value nk_true nk_false)))
       (cond
        ((and image symbol)
         (error "Both image and symbol arguments are specified"))
        (image
         (nk_select_image_label ctx image label align value))
        (symbol
         (nk_select_symbol_label ctx (nk-symbol-type->integer symbol)
                                 label align value))
        (else
         (nk_select_label ctx label align value))))))

(define-public (color-picker ctx colorf color-format)
  (parse-c-struct (nk_color_picker ctx
                                   (make-colorf colorf)
                                   (nk-color-format->integer color-format))
                  nk_colorf_types))

(define-public (property ctx name min val max step inc-per-pixel)
  ((if (and (exact-integer? min)
            (exact-integer? val)
            (exact-integer? max)
            (exact-integer? step)
            (exact-integer? inc-per-pixel))
       nk_propertyi
       nk_propertyd)
   ctx (string->pointer name) min val max step inc-per-pixel))

(define*-public (combo-begin ctx #:key label color symbol image size)
  (= nk_true
     (cond
      ((and symbol image)
       (error "No combobox procedure with symbol and image"))
      ((and symbol label)
       (nk_combo_begin_symbol_label ctx
                               (string->pointer label)
                               (nk-symbol-type->integer symbol)
                               (make-vec2 size)))
      ((and image label)
       (nk_combo_begin_image_label ctx
                                   (string->pointer label)
                                   image
                                   (make-vec2 size)))
      (symbol
       (nk_combo_begin_symbol ctx
                              (nk-symbol-type->integer symbol)
                              (make-vec2 size)))
      (image
       (nk_combo_begin_image ctx
                             image
                             (make-vec2 size)))
      (color
       (nk_combo_begin_color ctx
                             color
                             (make-vec2 size)))
      (label
       (nk_combo_begin_label ctx
                             (string->pointer label)
                             (make-vec2 size)))
      (else
       (error "No combobox procedure for such arguments combination")))))

(define-public combo-end nk_combo_end)

(define*-public (call-with-combo ctx
                                 #:key label color symbol image size
                                 #:rest args)
  (let ((proc (let find-proc ((args args))
                (if (keyword? (car args))
                    (find-proc (cddr args))
                    (car args)))))
    (when (combo-begin ctx #:label label #:color color #:symbol symbol
                       #:image image #:size size)
      (proc)
      (combo-end ctx))))

(define-public context-style nk_context_style)
(define-public style-button nk_style_button)
(define-public style-button-text-background nk_style_button_text_background)
(define-public style-button-text-normal nk_style_button_text_normal)
(define-public style-button-text-hover nk_style_button_text_hover)
(define-public style-button-text-active nk_style_button_text_active)
(define-public style-selectable nk_style_selectable)
(define-public style-selectable-text-normal nk_style_selectable_text_normal)
(define-public style-selectable-text-hover nk_style_selectable_text_hover)
(define-public style-selectable-text-pressed nk_style_selectable_text_pressed)
(define-public style-selectable-text-normal-active nk_style_selectable_text_normal_active)
(define-public style-selectable-text-hover-active nk_style_selectable_text_hover_active)
(define-public style-selectable-text-pressed-active nk_style_selectable_text_pressed_active)
(define-public style-selectable-text-hover-background nk_style_selectable_text_background)

(define-public (style-push-font ctx font)
  (= 1 (nk_style_push_font ctx font)))

(define-public (style-push-float ctx address value)
  (= 1 (nk_style_push_float ctx address value)))

(define-public (style-push-vec2 ctx address value)
  (= 1
     (nk_style_push_vec2 ctx address (if (pointer? value)
                                         value
                                         (make-vec2 value)))))

(define-public (style-push-color ctx address value)
  (= 1
     (nk_style_push_color ctx address (if (pointer? value)
                                          value
                                          (make-color value)))))

(define-public style-pop-font nk_style_pop_font)
(define-public style-pop-float nk_style_pop_float)
(define-public style-pop-vec2 nk_style_pop_vec2)
(define-public style-pop-color nk_style_pop_color)

(define-public (call-with-style ctx type address value proc)
  (match-let (((push-proc pop-proc)
               (case type
                 ((float) (list style-push-float style-pop-float))
                 ((vec2) (list style-push-float style-pop-vec2))
                 ((color) (list style-push-color style-pop-color))
                 (else (error "Wrong style type: ~a" type)))))
    (dynamic-wind (lambda ()
                    (unless (push-proc ctx address value)
                      (error "Failed to push a style: ~a" type)))
                  (lambda ()
                    (proc))
                  (lambda ()
                    (pop-proc ctx)))))

(define-public (call-with-styles ctx styles proc)
  (let cws ((styles styles))
    (if (null? styles)
        (proc)
        (match-let (((type address value) (car styles)))
          (call-with-style ctx type address value
            (lambda ()
              (cws (cdr styles))))))))

(define*-public (rgba #:key r g b a rf gf bf af color colorf hex)
  (cond
   ((and r g b)
    (if a
        (nk_rgba r g b a)
        (nk_rgb r g b)))
   ((and rf gf bf)
    (if af
        (nk_rgba_f rf gf bf af)
        (nk_rgb_f rf gf bf)))
   (color
    (apply (if (= (length color) 3) nk_rgb nk_rgba) color))
   (colorf
    (apply (if (= (length colorf) 3) nk_rgb_f nk_rgba_f) colorf))
   (hex
    ((if (< (string-length hex) 8) nk_rgb_hex nk_rgba_hex)
     (string->pointer hex)))
   (else
    (error "No rgb{a} procedure for such arguments combination"))))

(define-public make-font-config nk_font_config)

(define-public font-config-pixel-snap nk_font_config_pixel_snap)
(define-public set-font-config-pixel-snap! nk_font_config_set_pixel_snap)
(define-public font-config-range nk_font_config_range)
(define-public set-font-config-range! nk_font_config_set_range)

(define-public font-default-glyph-ranges nk_font_default_glyph_ranges)
(define-public font-chinese-glyph-ranges nk_font_chinese_glyph_ranges)
(define-public font-cyrillic-glyph-ranges nk_font_cyrillic_glyph_ranges)
(define-public font-korean-glyph-ranges nk_font_korean_glyph_ranges)
