;;; Copyright (C) 2019 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(use-modules (system foreign)
             (gl)
             (sdl2)
             (sdl2 video)
             (sdl2 events)
             ((nuklear) #:prefix nk:)
             ((nuklear-sdl-gles2) #:prefix backend:))

(define font-filename (if (not (current-filename))
                          "Inconsolata-LGC.ttf"
                          (string-append (dirname (current-filename))
                                         "/Inconsolata-LGC.ttf")))

(define window-width 800)
(define window-height 600)

(define max-vertex-memory (* 512 1024))
(define max-element-memory (* 128 1024))

(define option-value 'easy)
(define property-value 20)
(define background-value (list 0.1 0.18 0.24 1.0))

(define (run-loop ctx win)
  (let ((running? #t)
        (event (poll-event)))
    (nk:input-begin ctx)
    (while event
      (if (quit-event? event)
          (set! running? #f))
      (backend:handle-event poll-event-ptr)
      (set! event (poll-event)))
    (nk:input-end ctx)

    (nk:call-with-window ctx "Demo" (list 50 50 200 200)
                         '(border movable scalable minimizable title)
      (lambda ()
        (nk:layout-row-static ctx 30 80 1)
        (when (nk:button ctx #:label "button")
          (display "button pressed!") (newline))
        (nk:layout-row-dynamic ctx 30 2)
        (if (nk:option ctx "easy" (eq? option-value 'easy)) (set! option-value 'easy))
        (if (nk:option ctx "hard" (eq? option-value 'hard)) (set! option-value 'hard))
        (nk:layout-row-dynamic ctx 22 1)
        (set! property-value (nk:property ctx "Compression" 0 property-value 100 10 1))

        (nk:layout-row-dynamic ctx 20 1)
        (nk:label ctx "background:" #:align 'left)
        (nk:layout-row-dynamic ctx 25 1)
        (nk:call-with-combo ctx #:color (nk:rgba #:colorf background-value)
                            #:size (list (nk:widget-width ctx) 400)
          (lambda ()
            (nk:layout-row-dynamic ctx 120 1)
            (set! background-value (nk:color-picker ctx background-value 'rgba))
            (nk:layout-row-dynamic ctx 25 1)
            (set! background-value
                  (list
                   (nk:property ctx "#R:" 0.0 (car background-value) 1.0 0.01 0.005)
                   (nk:property ctx "#G:" 0.0 (cadr background-value) 1.0 0.01 0.005)
                   (nk:property ctx "#B:" 0.0 (caddr background-value) 1.0 0.01 0.005)
                   (nk:property ctx "#A:" 0.0 (cadddr background-value) 1.0 0.01 0.005)))))))

    (apply gl-viewport 0 0 (window-size win))
    (gl-clear (clear-buffer-mask color-buffer))
    (apply set-gl-clear-color background-value)
    (backend:render 'n max-vertex-memory max-element-memory)
    (swap-gl-window win)
    running?))

(define (run)
  (call-with-window (make-window #:opengl? #t #:show? #t)
    (lambda (window)
      (call-with-gl-context window
        (lambda (gl-context)
          (gl-viewport 0 0 window-width window-height)
          (let* ((context (backend:init (unwrap-window window)))
                 (fonts (backend:init-font `((,font-filename 16)))))
            (backend:set-font context (car fonts))
            (let loop ((running? #t))
              (if running?
                  (loop (run-loop context window))))
            (backend:shutdown)))))))

(unless (resolve-module '(geiser) #f #:ensure #f)
  (run))
