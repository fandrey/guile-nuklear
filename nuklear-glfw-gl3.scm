;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (nuklear-glfw-gl3)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (nuklear bindings)
  #:use-module (nuklear-glfw-gl3 bindings))

(define-public (init win init-state)
  (nk_glfw3_init win
                 (nk-glfw-init-state->integer init-state)))

(define-public shutdown nk_glfw3_shutdown)
(define-public new-frame nk_glfw3_new_frame)

(define-public (render aa max-vertex-buffer max-element-buffer)
  (nk_glfw3_render (nk-anti-aliasing->integer aa)
                   max-vertex-buffer max-element-buffer))

(define-public init-font nk_glfw3_init_font)
(define-public set-font nk_glfw3_set_font)
