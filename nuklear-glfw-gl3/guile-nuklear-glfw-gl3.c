/* Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
 *
 * This file is part of Guile-Nuklear.
 *
 * Guile-Nuklear is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Guile-Nuklear is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Guile-Nuklear.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <string.h>
#include <libguile.h>
#include <GL/glew.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_KEYSTATE_BASED_INPUT
#define NK_GLFW_GL3_IMPLEMENTATION
#define NK_MEMSET memset

#include "nuklear.h"
#include "nuklear_glfw_gl3.h"

static struct nk_glfw glfw;

SCM nk_glfw3_init_wrapper(SCM win, SCM state)
{
  if (glewInit() != GLEW_OK)
  {
    scm_error(scm_from_utf8_symbol("nuklear-glfw-gl3-error"),
              "nk_glfw3_init_wrapper",
              "failed to init GLEW",
              SCM_EOL,
              SCM_BOOL_F);

    return SCM_UNSPECIFIED;
  }

  return scm_from_pointer(nk_glfw3_init(&glfw,
                                        scm_to_pointer(win),
                                        scm_to_int(state)),
                          NULL);
}

SCM nk_glfw3_shutdown_wrapper()
{
  nk_glfw3_shutdown(&glfw);

  return SCM_UNSPECIFIED;
}

SCM nk_glfw3_new_frame_wrapper()
{
  nk_glfw3_new_frame(&glfw);

  return SCM_UNSPECIFIED;
}

SCM nk_glfw3_render_wrapper(SCM aa,
                            SCM max_vertex_buffer,
                            SCM max_element_buffer)
{
  nk_glfw3_render(&glfw,
                  scm_to_int(aa),
                  scm_to_int(max_vertex_buffer),
                  scm_to_int(max_element_buffer));

  return SCM_UNSPECIFIED;
}

SCM nk_glfw3_init_font(SCM fonts)
{
  SCM f, lst = scm_list_n(SCM_UNDEFINED);
  struct nk_font_atlas *atlas;
  nk_glfw3_font_stash_begin(&glfw, &atlas);

  for(f = fonts; !scm_is_null(f); f = scm_cdr(f))
    {
      char * s = scm_to_locale_string(scm_caar(f));
      struct nk_font_config * cfg =
        scm_is_null(scm_cddar(f)) ? NULL : scm_to_pointer(scm_caddar(f));

      lst = scm_cons(scm_from_pointer
                     (nk_font_atlas_add_from_file(atlas, s,
                                                  scm_to_int(scm_cadar(f)),
                                                  cfg),
                      NULL),
                     lst);
      free(s);
    }

  nk_glfw3_font_stash_end(&glfw);

  return scm_reverse_x(lst, SCM_EOL);
}

SCM nk_glfw3_set_font(SCM ctx, SCM font)
{
  nk_style_set_font((struct nk_context *)scm_to_pointer(ctx),
                    &((struct nk_font *)(scm_to_pointer(font)))->handle);

  return SCM_UNSPECIFIED;
}

void init_guile_nuklear_glfw_gl3()
{
  scm_c_define_gsubr("nk_glfw3_init", 2, 0, 0, nk_glfw3_init_wrapper);
  scm_c_define_gsubr("nk_glfw3_shutdown", 0, 0, 0, nk_glfw3_shutdown_wrapper);
  scm_c_define_gsubr("nk_glfw3_new_frame", 0, 0, 0, nk_glfw3_new_frame_wrapper);
  scm_c_define_gsubr("nk_glfw3_render", 3, 0, 0, nk_glfw3_render_wrapper);
  scm_c_define_gsubr("nk_glfw3_init_font", 1, 0, 0, nk_glfw3_init_font);
  scm_c_define_gsubr("nk_glfw3_set_font", 2, 0, 0, nk_glfw3_set_font);
}
