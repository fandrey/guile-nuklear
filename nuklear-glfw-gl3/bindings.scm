;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (nuklear-glfw-gl3 bindings)
  #:use-module (system foreign)
  #:use-module (srfi srfi-1)
  #:use-module (nuklear-glfw-gl3 config))

(load-extension %libguile-nuklear-glfw-gl3 "init_guile_nuklear_glfw_gl3")

(define-public (nk-glfw-init-state->integer flags)
  (let ((flag->int (lambda (f)
                     (case f
                       ((default) 0)
                       ((install-callbacks) 1)))))
    (cond
     ((or (null? flags) (not flags)) 0)
     ((symbol? flags) (flag->int flags))
     (else (apply logior (map flag->int flags))))))

(export nk_glfw3_init
        nk_glfw3_shutdown
        nk_glfw3_new_frame
        nk_glfw3_render
        nk_glfw3_init_font
        nk_glfw3_set_font)
