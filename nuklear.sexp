
;; /usr/include/bits/types.h:31:23
(typedef __u_char :unsigned-char)

;; /usr/include/bits/types.h:32:28
(typedef __u_short :unsigned-short)

;; /usr/include/bits/types.h:33:22
(typedef __u_int :unsigned-int)

;; /usr/include/bits/types.h:34:27
(typedef __u_long :unsigned-long)

;; /usr/include/bits/types.h:37:21
(typedef __int8_t :signed-char)

;; /usr/include/bits/types.h:38:23
(typedef __uint8_t :unsigned-char)

;; /usr/include/bits/types.h:39:26
(typedef __int16_t :short)

;; /usr/include/bits/types.h:40:28
(typedef __uint16_t :unsigned-short)

;; /usr/include/bits/types.h:41:20
(typedef __int32_t :int)

;; /usr/include/bits/types.h:42:22
(typedef __uint32_t :unsigned-int)

;; /usr/include/bits/types.h:44:25
(typedef __int64_t :long)

;; /usr/include/bits/types.h:45:27
(typedef __uint64_t :unsigned-long)

;; /usr/include/bits/types.h:52:18
(typedef __int_least8_t __int8_t)

;; /usr/include/bits/types.h:53:19
(typedef __uint_least8_t __uint8_t)

;; /usr/include/bits/types.h:54:19
(typedef __int_least16_t __int16_t)

;; /usr/include/bits/types.h:55:20
(typedef __uint_least16_t __uint16_t)

;; /usr/include/bits/types.h:56:19
(typedef __int_least32_t __int32_t)

;; /usr/include/bits/types.h:57:20
(typedef __uint_least32_t __uint32_t)

;; /usr/include/bits/types.h:58:19
(typedef __int_least64_t __int64_t)

;; /usr/include/bits/types.h:59:20
(typedef __uint_least64_t __uint64_t)

;; /usr/include/bits/types.h:63:18
(typedef __quad_t :long)

;; /usr/include/bits/types.h:64:27
(typedef __u_quad_t :unsigned-long)

;; /usr/include/bits/types.h:72:18
(typedef __intmax_t :long)

;; /usr/include/bits/types.h:73:27
(typedef __uintmax_t :unsigned-long)

;; /usr/include/bits/types.h:145:25
(typedef __dev_t :unsigned-long)

;; /usr/include/bits/types.h:146:25
(typedef __uid_t :unsigned-int)

;; /usr/include/bits/types.h:147:25
(typedef __gid_t :unsigned-int)

;; /usr/include/bits/types.h:148:25
(typedef __ino_t :unsigned-long)

;; /usr/include/bits/types.h:149:27
(typedef __ino64_t :unsigned-long)

;; /usr/include/bits/types.h:150:26
(typedef __mode_t :unsigned-int)

;; /usr/include/bits/types.h:151:27
(typedef __nlink_t :unsigned-long)

;; /usr/include/bits/types.h:152:25
(typedef __off_t :long)

;; /usr/include/bits/types.h:153:27
(typedef __off64_t :long)

;; /usr/include/bits/types.h:154:25
(typedef __pid_t :int)

;; /usr/include/bits/types.h:155:26
(typedef __fsid_t ;; /usr/include/bits/types.h:155:12 <Spelling=/usr/include/bits/typesizes.h:72:24>
(struct :id 1
    (__val (:array :int 2))))

;; /usr/include/bits/types.h:156:27
(typedef __clock_t :long)

;; /usr/include/bits/types.h:157:26
(typedef __rlim_t :unsigned-long)

;; /usr/include/bits/types.h:158:28
(typedef __rlim64_t :unsigned-long)

;; /usr/include/bits/types.h:159:24
(typedef __id_t :unsigned-int)

;; /usr/include/bits/types.h:160:26
(typedef __time_t :long)

;; /usr/include/bits/types.h:161:30
(typedef __useconds_t :unsigned-int)

;; /usr/include/bits/types.h:162:31
(typedef __suseconds_t :long)

;; /usr/include/bits/types.h:164:27
(typedef __daddr_t :int)

;; /usr/include/bits/types.h:165:25
(typedef __key_t :int)

;; /usr/include/bits/types.h:168:29
(typedef __clockid_t :int)

;; /usr/include/bits/types.h:171:27
(typedef __timer_t (:pointer :void))

;; /usr/include/bits/types.h:174:29
(typedef __blksize_t :long)

;; /usr/include/bits/types.h:179:28
(typedef __blkcnt_t :long)

;; /usr/include/bits/types.h:180:30
(typedef __blkcnt64_t :long)

;; /usr/include/bits/types.h:183:30
(typedef __fsblkcnt_t :unsigned-long)

;; /usr/include/bits/types.h:184:32
(typedef __fsblkcnt64_t :unsigned-long)

;; /usr/include/bits/types.h:187:30
(typedef __fsfilcnt_t :unsigned-long)

;; /usr/include/bits/types.h:188:32
(typedef __fsfilcnt64_t :unsigned-long)

;; /usr/include/bits/types.h:191:28
(typedef __fsword_t :long)

;; /usr/include/bits/types.h:193:27
(typedef __ssize_t :long)

;; /usr/include/bits/types.h:196:33
(typedef __syscall_slong_t :long)

;; /usr/include/bits/types.h:198:33
(typedef __syscall_ulong_t :unsigned-long)

;; /usr/include/bits/types.h:202:19
(typedef __loff_t __off64_t)

;; /usr/include/bits/types.h:203:15
(typedef __caddr_t (:pointer :char))

;; /usr/include/bits/types.h:206:25
(typedef __intptr_t :long)

;; /usr/include/bits/types.h:209:23
(typedef __socklen_t :unsigned-int)

;; /usr/include/bits/types.h:214:13
(typedef __sig_atomic_t :int)

;; /usr/include/bits/stdint-intn.h:24:18
(typedef int8_t __int8_t)

;; /usr/include/bits/stdint-intn.h:25:19
(typedef int16_t __int16_t)

;; /usr/include/bits/stdint-intn.h:26:19
(typedef int32_t __int32_t)

;; /usr/include/bits/stdint-intn.h:27:19
(typedef int64_t __int64_t)

;; /usr/include/bits/stdint-uintn.h:24:19
(typedef uint8_t __uint8_t)

;; /usr/include/bits/stdint-uintn.h:25:20
(typedef uint16_t __uint16_t)

;; /usr/include/bits/stdint-uintn.h:26:20
(typedef uint32_t __uint32_t)

;; /usr/include/bits/stdint-uintn.h:27:20
(typedef uint64_t __uint64_t)

;; /usr/include/stdint.h:43:24
(typedef int_least8_t __int_least8_t)

;; /usr/include/stdint.h:44:25
(typedef int_least16_t __int_least16_t)

;; /usr/include/stdint.h:45:25
(typedef int_least32_t __int_least32_t)

;; /usr/include/stdint.h:46:25
(typedef int_least64_t __int_least64_t)

;; /usr/include/stdint.h:49:25
(typedef uint_least8_t __uint_least8_t)

;; /usr/include/stdint.h:50:26
(typedef uint_least16_t __uint_least16_t)

;; /usr/include/stdint.h:51:26
(typedef uint_least32_t __uint_least32_t)

;; /usr/include/stdint.h:52:26
(typedef uint_least64_t __uint_least64_t)

;; /usr/include/stdint.h:58:22
(typedef int_fast8_t :signed-char)

;; /usr/include/stdint.h:60:19
(typedef int_fast16_t :long)

;; /usr/include/stdint.h:61:19
(typedef int_fast32_t :long)

;; /usr/include/stdint.h:62:19
(typedef int_fast64_t :long)

;; /usr/include/stdint.h:71:24
(typedef uint_fast8_t :unsigned-char)

;; /usr/include/stdint.h:73:27
(typedef uint_fast16_t :unsigned-long)

;; /usr/include/stdint.h:74:27
(typedef uint_fast32_t :unsigned-long)

;; /usr/include/stdint.h:75:27
(typedef uint_fast64_t :unsigned-long)

;; /usr/include/stdint.h:87:19
(typedef intptr_t :long)

;; /usr/include/stdint.h:90:27
(typedef uintptr_t :unsigned-long)

;; /usr/include/stdint.h:101:21
(typedef intmax_t __intmax_t)

;; /usr/include/stdint.h:102:22
(typedef uintmax_t __uintmax_t)

;; /home/alf/src/Nuklear/src/nuklear.h:179:17
(typedef nk_char int8_t)

;; /home/alf/src/Nuklear/src/nuklear.h:180:18
(typedef nk_uchar uint8_t)

;; /home/alf/src/Nuklear/src/nuklear.h:181:18
(typedef nk_byte uint8_t)

;; /home/alf/src/Nuklear/src/nuklear.h:182:18
(typedef nk_short int16_t)

;; /home/alf/src/Nuklear/src/nuklear.h:183:19
(typedef nk_ushort uint16_t)

;; /home/alf/src/Nuklear/src/nuklear.h:184:18
(typedef nk_int int32_t)

;; /home/alf/src/Nuklear/src/nuklear.h:185:19
(typedef nk_uint uint32_t)

;; /home/alf/src/Nuklear/src/nuklear.h:186:22
(typedef nk_size uintptr_t)

;; /home/alf/src/Nuklear/src/nuklear.h:187:25
(typedef nk_ptr uintptr_t)

;; /home/alf/src/Nuklear/src/nuklear.h:189:17
(typedef nk_hash nk_uint)

;; /home/alf/src/Nuklear/src/nuklear.h:190:17
(typedef nk_flags nk_uint)

;; /home/alf/src/Nuklear/src/nuklear.h:191:17
(typedef nk_rune nk_uint)

;; /home/alf/src/Nuklear/src/nuklear.h:196:1 <Spelling=<scratch space>:7:1>
(typedef _dummy_array196 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:197:1 <Spelling=<scratch space>:9:1>
(typedef _dummy_array197 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:198:1 <Spelling=<scratch space>:11:1>
(typedef _dummy_array198 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:199:1 <Spelling=<scratch space>:13:1>
(typedef _dummy_array199 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:200:1 <Spelling=<scratch space>:15:1>
(typedef _dummy_array200 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:201:1 <Spelling=<scratch space>:17:1>
(typedef _dummy_array201 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:202:1 <Spelling=<scratch space>:19:1>
(typedef _dummy_array202 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:203:1 <Spelling=<scratch space>:21:1>
(typedef _dummy_array203 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:204:1 <Spelling=<scratch space>:23:1>
(typedef _dummy_array204 (:array :char 1))

;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
  )

;; /home/alf/src/Nuklear/src/nuklear.h:212:8
(struct nk_allocator
  )

;; /home/alf/src/Nuklear/src/nuklear.h:213:8
(struct nk_command_buffer
  )

;; /home/alf/src/Nuklear/src/nuklear.h:214:8
(struct nk_draw_command
  )

;; /home/alf/src/Nuklear/src/nuklear.h:215:8
(struct nk_convert_config
  )

;; /home/alf/src/Nuklear/src/nuklear.h:216:8
(struct nk_style_item
  )

;; /home/alf/src/Nuklear/src/nuklear.h:217:8
(struct nk_text_edit
  )

;; /home/alf/src/Nuklear/src/nuklear.h:218:8
(struct nk_draw_list
  )

;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
  )

;; /home/alf/src/Nuklear/src/nuklear.h:220:8
(struct nk_panel
  )

;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
  )

;; /home/alf/src/Nuklear/src/nuklear.h:222:8
(struct nk_draw_vertex_layout_element
  )

;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
  )

;; /home/alf/src/Nuklear/src/nuklear.h:224:8
(struct nk_style_toggle
  )

;; /home/alf/src/Nuklear/src/nuklear.h:225:8
(struct nk_style_selectable
  )

;; /home/alf/src/Nuklear/src/nuklear.h:226:8
(struct nk_style_slide
  )

;; /home/alf/src/Nuklear/src/nuklear.h:227:8
(struct nk_style_progress
  )

;; /home/alf/src/Nuklear/src/nuklear.h:228:8
(struct nk_style_scrollbar
  )

;; /home/alf/src/Nuklear/src/nuklear.h:229:8
(struct nk_style_edit
  )

;; /home/alf/src/Nuklear/src/nuklear.h:230:8
(struct nk_style_property
  )

;; /home/alf/src/Nuklear/src/nuklear.h:231:8
(struct nk_style_chart
  )

;; /home/alf/src/Nuklear/src/nuklear.h:232:8
(struct nk_style_combo
  )

;; /home/alf/src/Nuklear/src/nuklear.h:233:8
(struct nk_style_tab
  )

;; /home/alf/src/Nuklear/src/nuklear.h:234:8
(struct nk_style_window_header
  )

;; /home/alf/src/Nuklear/src/nuklear.h:235:8
(struct nk_style_window
  )

;; /home/alf/src/Nuklear/src/nuklear.h:237:1
(enum :id 2
    (nk_false 0)
    (nk_true 1))

;; /home/alf/src/Nuklear/src/nuklear.h:238:8
(struct nk_color
  (r nk_byte)
  (g nk_byte)
  (b nk_byte)
  (a nk_byte))

;; /home/alf/src/Nuklear/src/nuklear.h:239:8
(struct nk_colorf
  (r :float)
  (g :float)
  (b :float)
  (a :float))

;; /home/alf/src/Nuklear/src/nuklear.h:240:8
(struct nk_vec2
  (x :float)
  (y :float))

;; /home/alf/src/Nuklear/src/nuklear.h:241:8
(struct nk_vec2i
  (x :short)
  (y :short))

;; /home/alf/src/Nuklear/src/nuklear.h:242:8
(struct nk_rect
  (x :float)
  (y :float)
  (w :float)
  (h :float))

;; /home/alf/src/Nuklear/src/nuklear.h:243:8
(struct nk_recti
  (x :short)
  (y :short)
  (w :short)
  (h :short))

;; /home/alf/src/Nuklear/src/nuklear.h:244:14
(typedef nk_glyph (:array :char 4))

;; /home/alf/src/Nuklear/src/nuklear.h:245:36
(typedef nk_handle ;; /home/alf/src/Nuklear/src/nuklear.h:245:9
(union :id 3
    (ptr (:pointer :void))
    (id :int)))

;; /home/alf/src/Nuklear/src/nuklear.h:246:8
(struct nk_image
  (handle nk_handle)
  (w :unsigned-short)
  (h :unsigned-short)
  (region (:array :unsigned-short 4)))

;; /home/alf/src/Nuklear/src/nuklear.h:247:8
(struct nk_cursor
  (img (:struct nk_image))
  (size (:struct nk_vec2))
  (offset (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:248:8
(struct nk_scroll
  (x nk_uint)
  (y nk_uint))

;; /home/alf/src/Nuklear/src/nuklear.h:250:6
(enum nk_heading
    (NK_UP 0)
    (NK_RIGHT 1)
    (NK_DOWN 2)
    (NK_LEFT 3))

;; /home/alf/src/Nuklear/src/nuklear.h:251:6
(enum nk_button_behavior
    (NK_BUTTON_DEFAULT 0)
    (NK_BUTTON_REPEATER 1))

;; /home/alf/src/Nuklear/src/nuklear.h:252:6
(enum nk_modify
    (NK_FIXED 0)
    (NK_MODIFIABLE 1))

;; /home/alf/src/Nuklear/src/nuklear.h:253:6
(enum nk_orientation
    (NK_VERTICAL 0)
    (NK_HORIZONTAL 1))

;; /home/alf/src/Nuklear/src/nuklear.h:254:6
(enum nk_collapse_states
    (NK_MINIMIZED 0)
    (NK_MAXIMIZED 1))

;; /home/alf/src/Nuklear/src/nuklear.h:255:6
(enum nk_show_states
    (NK_HIDDEN 0)
    (NK_SHOWN 1))

;; /home/alf/src/Nuklear/src/nuklear.h:256:6
(enum nk_chart_type
    (NK_CHART_LINES 0)
    (NK_CHART_COLUMN 1)
    (NK_CHART_MAX 2))

;; /home/alf/src/Nuklear/src/nuklear.h:257:6
(enum nk_chart_event
    (NK_CHART_HOVERING 1)
    (NK_CHART_CLICKED 2))

;; /home/alf/src/Nuklear/src/nuklear.h:258:6
(enum nk_color_format
    (NK_RGB 0)
    (NK_RGBA 1))

;; /home/alf/src/Nuklear/src/nuklear.h:259:6
(enum nk_popup_type
    (NK_POPUP_STATIC 0)
    (NK_POPUP_DYNAMIC 1))

;; /home/alf/src/Nuklear/src/nuklear.h:260:6
(enum nk_layout_format
    (NK_DYNAMIC 0)
    (NK_STATIC 1))

;; /home/alf/src/Nuklear/src/nuklear.h:261:6
(enum nk_tree_type
    (NK_TREE_NODE 0)
    (NK_TREE_TAB 1))

;; /home/alf/src/Nuklear/src/nuklear.h:263:16
(typedef nk_plugin_alloc :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:264:16
(typedef nk_plugin_free :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:265:14
(typedef nk_plugin_filter :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:266:15
(typedef nk_plugin_paste :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:267:15
(typedef nk_plugin_copy :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:269:8
(struct nk_allocator
  (userdata nk_handle)
  (alloc nk_plugin_alloc)
  (free nk_plugin_free))

;; /home/alf/src/Nuklear/src/nuklear.h:274:6
(enum nk_symbol_type
    (NK_SYMBOL_NONE 0)
    (NK_SYMBOL_X 1)
    (NK_SYMBOL_UNDERSCORE 2)
    (NK_SYMBOL_CIRCLE_SOLID 3)
    (NK_SYMBOL_CIRCLE_OUTLINE 4)
    (NK_SYMBOL_RECT_SOLID 5)
    (NK_SYMBOL_RECT_OUTLINE 6)
    (NK_SYMBOL_TRIANGLE_UP 7)
    (NK_SYMBOL_TRIANGLE_DOWN 8)
    (NK_SYMBOL_TRIANGLE_LEFT 9)
    (NK_SYMBOL_TRIANGLE_RIGHT 10)
    (NK_SYMBOL_PLUS 11)
    (NK_SYMBOL_MINUS 12)
    (NK_SYMBOL_MAX 13))

;; /home/alf/src/Nuklear/src/nuklear.h:345:12
(function "nk_init_default" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:370:12
(function "nk_init_fixed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (memory (:pointer :void)) (size nk_size) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:388:12
(function "nk_init" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_allocator))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:407:12
(function "nk_init_custom" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (cmds (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) (pool (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:421:13
(function "nk_clear" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:434:13
(function "nk_free" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:516:6
(enum nk_keys
    (NK_KEY_NONE 0)
    (NK_KEY_SHIFT 1)
    (NK_KEY_CTRL 2)
    (NK_KEY_DEL 3)
    (NK_KEY_ENTER 4)
    (NK_KEY_TAB 5)
    (NK_KEY_BACKSPACE 6)
    (NK_KEY_COPY 7)
    (NK_KEY_CUT 8)
    (NK_KEY_PASTE 9)
    (NK_KEY_UP 10)
    (NK_KEY_DOWN 11)
    (NK_KEY_LEFT 12)
    (NK_KEY_RIGHT 13)
    (NK_KEY_TEXT_INSERT_MODE 14)
    (NK_KEY_TEXT_REPLACE_MODE 15)
    (NK_KEY_TEXT_RESET_MODE 16)
    (NK_KEY_TEXT_LINE_START 17)
    (NK_KEY_TEXT_LINE_END 18)
    (NK_KEY_TEXT_START 19)
    (NK_KEY_TEXT_END 20)
    (NK_KEY_TEXT_UNDO 21)
    (NK_KEY_TEXT_REDO 22)
    (NK_KEY_TEXT_SELECT_ALL 23)
    (NK_KEY_TEXT_WORD_LEFT 24)
    (NK_KEY_TEXT_WORD_RIGHT 25)
    (NK_KEY_SCROLL_START 26)
    (NK_KEY_SCROLL_END 27)
    (NK_KEY_SCROLL_DOWN 28)
    (NK_KEY_SCROLL_UP 29)
    (NK_KEY_MAX 30))

;; /home/alf/src/Nuklear/src/nuklear.h:551:6
(enum nk_buttons
    (NK_BUTTON_LEFT 0)
    (NK_BUTTON_MIDDLE 1)
    (NK_BUTTON_RIGHT 2)
    (NK_BUTTON_DOUBLE 3)
    (NK_BUTTON_MAX 4))

;; /home/alf/src/Nuklear/src/nuklear.h:570:13
(function "nk_input_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:584:13
(function "nk_input_motion" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (x :int) (y :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:598:13
(function "nk_input_key" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_keys)) (down :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:614:13
(function "nk_input_button" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_buttons)) (x :int) (y :int) (down :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:629:13
(function "nk_input_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (val (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:647:13
(function "nk_input_char" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (:char)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:664:13
(function "nk_input_glyph" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_glyph)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:680:13
(function "nk_input_unicode" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_rune)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:693:13
(function "nk_input_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:923:6
(enum nk_anti_aliasing
    (NK_ANTI_ALIASING_OFF 0)
    (NK_ANTI_ALIASING_ON 1))

;; /home/alf/src/Nuklear/src/nuklear.h:924:6
(enum nk_convert_result
    (NK_CONVERT_SUCCESS 0)
    (NK_CONVERT_INVALID_PARAM 1)
    (NK_CONVERT_COMMAND_BUFFER_FULL 2)
    (NK_CONVERT_VERTEX_BUFFER_FULL 4)
    (NK_CONVERT_ELEMENT_BUFFER_FULL 8))

;; /home/alf/src/Nuklear/src/nuklear.h:931:8
(struct nk_draw_null_texture
  (texture nk_handle)
  (uv (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:935:8
(struct nk_convert_config
  (global_alpha :float)
  (line_AA (:enum nk_anti_aliasing))
  (shape_AA (:enum nk_anti_aliasing))
  (circle_segment_count :unsigned-int)
  (arc_segment_count :unsigned-int)
  (curve_segment_count :unsigned-int)
  (null (:struct nk_draw_null_texture))
  (vertex_layout (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:222:8
(struct nk_draw_vertex_layout_element
      )))
  (vertex_size nk_size)
  (vertex_alignment nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:961:21
(struct nk_command
  )

;; /home/alf/src/Nuklear/src/nuklear.h:961:33
(function "nk__begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:961:21
(struct nk_command
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:976:33
(function "nk__next" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:961:21
(struct nk_command
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:961:21
(struct nk_command
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1022:17
(function "nk_convert" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (cmds (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) (vertices (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) (elements (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) ((:pointer (:struct nk_convert_config)))) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:1037:38
(function "nk__draw_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:214:8
(struct nk_draw_command
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1052:38
(function "nk__draw_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:214:8
(struct nk_draw_command
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1068:38
(function "nk__draw_next" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:214:8
(struct nk_draw_command
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:211:8
(struct nk_buffer
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:214:8
(struct nk_draw_command
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1231:6
(enum nk_panel_flags
    (NK_WINDOW_BORDER 1)
    (NK_WINDOW_MOVABLE 2)
    (NK_WINDOW_SCALABLE 4)
    (NK_WINDOW_CLOSABLE 8)
    (NK_WINDOW_MINIMIZABLE 16)
    (NK_WINDOW_NO_SCROLLBAR 32)
    (NK_WINDOW_TITLE 64)
    (NK_WINDOW_SCROLL_AUTO_HIDE 128)
    (NK_WINDOW_BACKGROUND 256)
    (NK_WINDOW_SCALE_LEFT 512)
    (NK_WINDOW_NO_INPUT 1024))

;; /home/alf/src/Nuklear/src/nuklear.h:1262:12
(function "nk_begin" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (title (:pointer :char)) (bounds (:struct nk_rect)) (flags nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1282:12
(function "nk_begin_titled" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (title (:pointer :char)) (bounds (:struct nk_rect)) (flags nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1295:13
(function "nk_end" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1311:15
(struct nk_window
  )

;; /home/alf/src/Nuklear/src/nuklear.h:1311:26
(function "nk_window_find" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:1311:15
(struct nk_window
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1327:23
(function "nk_window_get_bounds" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:1343:23
(function "nk_window_get_position" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:1359:23
(function "nk_window_get_size" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:1375:14
(function "nk_window_get_width" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:1391:14
(function "nk_window_get_height" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:1409:25
(function "nk_window_get_panel" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:220:8
(struct nk_panel
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1428:23
(function "nk_window_get_content_region" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:1447:23
(function "nk_window_get_content_region_min" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:1466:23
(function "nk_window_get_content_region_max" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:1484:23
(function "nk_window_get_content_region_size" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:1503:34
(function "nk_window_get_canvas" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:213:8
(struct nk_command_buffer
      )))

;; /home/alf/src/Nuklear/src/nuklear.h:1519:13
(function "nk_window_get_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (offset_x (:pointer nk_uint)) (offset_y (:pointer nk_uint))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1534:12
(function "nk_window_has_focus" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1549:12
(function "nk_window_is_hovered" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1564:12
(function "nk_window_is_collapsed" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1578:12
(function "nk_window_is_closed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1592:12
(function "nk_window_is_hidden" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1606:12
(function "nk_window_is_active" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1619:12
(function "nk_window_is_any_hovered" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1634:12
(function "nk_item_is_any_active" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:1647:13
(function "nk_window_set_bounds" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (bounds (:struct nk_rect))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1660:13
(function "nk_window_set_position" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (pos (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1673:13
(function "nk_window_set_size" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) ((:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1685:13
(function "nk_window_set_focus" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1701:13
(function "nk_window_set_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (offset_x nk_uint) (offset_y nk_uint)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1713:13
(function "nk_window_close" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1726:13
(function "nk_window_collapse" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (state (:enum nk_collapse_states))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1740:13
(function "nk_window_collapse_if" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) ((:enum nk_collapse_states)) (cond :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1753:13
(function "nk_window_show" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) ((:enum nk_show_states))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:1767:13
(function "nk_window_show_if" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) ((:enum nk_show_states)) (cond :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2056:13
(function "nk_layout_set_min_row_height" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (height :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2067:13
(function "nk_layout_reset_min_row_height" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2080:23
(function "nk_layout_widget_bounds" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:2094:14
(function "nk_layout_ratio_from_pixel" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (pixel_width :float)) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:2109:13
(function "nk_layout_row_dynamic" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (height :float) (cols :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2125:13
(function "nk_layout_row_static" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (height :float) (item_width :int) (cols :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2139:13
(function "nk_layout_row_begin" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (fmt (:enum nk_layout_format)) (row_height :float) (cols :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2151:13
(function "nk_layout_row_push" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (value :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2162:13
(function "nk_layout_row_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2176:13
(function "nk_layout_row" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_layout_format)) (height :float) (cols :int) (ratio (:pointer :float))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2188:13
(function "nk_layout_row_template_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (row_height :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2200:13
(function "nk_layout_row_template_push_dynamic" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2212:13
(function "nk_layout_row_template_push_variable" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (min_width :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2224:13
(function "nk_layout_row_template_push_static" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (width :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2235:13
(function "nk_layout_row_template_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2249:13
(function "nk_layout_space_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_layout_format)) (height :float) (widget_count :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2261:13
(function "nk_layout_space_push" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (bounds (:struct nk_rect))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2272:13
(function "nk_layout_space_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2285:23
(function "nk_layout_space_bounds" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:2299:23
(function "nk_layout_space_to_screen" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_vec2))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:2313:23
(function "nk_layout_space_to_local" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_vec2))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:2327:23
(function "nk_layout_space_rect_to_screen" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_rect))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:2341:23
(function "nk_layout_space_rect_to_local" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_rect))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:2444:12
(function "nk_group_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (title (:pointer :char)) (nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2460:12
(function "nk_group_begin_titled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (title (:pointer :char)) (nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2471:13
(function "nk_group_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2489:12
(function "nk_group_scrolled_offset_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (x_offset (:pointer nk_uint)) (y_offset (:pointer nk_uint)) (title (:pointer :char)) (flags nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2506:12
(function "nk_group_scrolled_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (off (:pointer (:struct nk_scroll))) (title (:pointer :char)) (nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2517:13
(function "nk_group_scrolled_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2531:13
(function "nk_group_get_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (id (:pointer :char)) (x_offset (:pointer nk_uint)) (y_offset (:pointer nk_uint))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2545:13
(function "nk_group_set_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (id (:pointer :char)) (x_offset nk_uint) (y_offset nk_uint)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2674:12
(function "nk_tree_push_hashed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) (title (:pointer :char)) (initial_state (:enum nk_collapse_states)) (hash (:pointer :char)) (len :int) (seed :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2738:12
(function "nk_tree_image_push_hashed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) ((:struct nk_image)) (title (:pointer :char)) (initial_state (:enum nk_collapse_states)) (hash (:pointer :char)) (len :int) (seed :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2749:13
(function "nk_tree_pop" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2765:12
(function "nk_tree_state_push" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) (title (:pointer :char)) (state (:pointer (:enum nk_collapse_states)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2782:12
(function "nk_tree_state_image_push" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) ((:struct nk_image)) (title (:pointer :char)) (state (:pointer (:enum nk_collapse_states)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2793:13
(function "nk_tree_state_pop" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2797:12
(function "nk_tree_element_push_hashed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) (title (:pointer :char)) (initial_state (:enum nk_collapse_states)) (selected (:pointer :int)) (hash (:pointer :char)) (len :int) (seed :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2798:12
(function "nk_tree_element_image_push_hashed" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_tree_type)) ((:struct nk_image)) (title (:pointer :char)) (initial_state (:enum nk_collapse_states)) (selected (:pointer :int)) (hash (:pointer :char)) (len :int) (seed :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2799:13
(function "nk_tree_element_pop" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2806:8
(struct nk_list_view
  (begin :int)
  (end :int)
  (count :int)
  (total_height :int)
  (ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))
  (scroll_pointer (:pointer nk_uint))
  (scroll_value nk_uint))

;; /home/alf/src/Nuklear/src/nuklear.h:2815:12
(function "nk_list_view_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (out (:pointer (:struct nk_list_view))) (id (:pointer :char)) (nk_flags) (row_height :int) (row_count :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2816:13
(function "nk_list_view_end" (((:pointer (:struct nk_list_view)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2822:6
(enum nk_widget_layout_states
    (NK_WIDGET_INVALID 0)
    (NK_WIDGET_VALID 1)
    (NK_WIDGET_ROM 2))

;; /home/alf/src/Nuklear/src/nuklear.h:2827:6
(enum nk_widget_states
    (NK_WIDGET_STATE_MODIFIED 2)
    (NK_WIDGET_STATE_INACTIVE 4)
    (NK_WIDGET_STATE_ENTERED 8)
    (NK_WIDGET_STATE_HOVER 16)
    (NK_WIDGET_STATE_ACTIVED 32)
    (NK_WIDGET_STATE_LEFT 64)
    (NK_WIDGET_STATE_HOVERED 18)
    (NK_WIDGET_STATE_ACTIVE 34))

;; /home/alf/src/Nuklear/src/nuklear.h:2837:37
(function "nk_widget" (((:pointer (:struct nk_rect))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:enum nk_widget_layout_states))

;; /home/alf/src/Nuklear/src/nuklear.h:2838:37
(function "nk_widget_fitting" (((:pointer (:struct nk_rect))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_vec2))) (:enum nk_widget_layout_states))

;; /home/alf/src/Nuklear/src/nuklear.h:2839:23
(function "nk_widget_bounds" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:2840:23
(function "nk_widget_position" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:2841:23
(function "nk_widget_size" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:2842:14
(function "nk_widget_width" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:2843:14
(function "nk_widget_height" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:2844:12
(function "nk_widget_is_hovered" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2845:12
(function "nk_widget_is_mouse_clicked" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_buttons))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2846:12
(function "nk_widget_has_mouse_click_down" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_buttons)) (down :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2847:13
(function "nk_spacing" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (cols :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2853:6
(enum nk_text_align
    (NK_TEXT_ALIGN_LEFT 1)
    (NK_TEXT_ALIGN_CENTERED 2)
    (NK_TEXT_ALIGN_RIGHT 4)
    (NK_TEXT_ALIGN_TOP 8)
    (NK_TEXT_ALIGN_MIDDLE 16)
    (NK_TEXT_ALIGN_BOTTOM 32))

;; /home/alf/src/Nuklear/src/nuklear.h:2861:6
(enum nk_text_alignment
    (NK_TEXT_LEFT 17)
    (NK_TEXT_CENTERED 18)
    (NK_TEXT_RIGHT 20))

;; /home/alf/src/Nuklear/src/nuklear.h:2866:13
(function "nk_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (nk_flags)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2867:13
(function "nk_text_colored" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (nk_flags) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2868:13
(function "nk_text_wrap" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2869:13
(function "nk_text_wrap_colored" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2870:13
(function "nk_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2871:13
(function "nk_label_colored" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2872:13
(function "nk_label_wrap" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2873:13
(function "nk_label_colored_wrap" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2874:13
(function "nk_image" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2875:13
(function "nk_image_color" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2898:12
(function "nk_button_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (title (:pointer :char)) (len :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2899:12
(function "nk_button_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (title (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2900:12
(function "nk_button_color" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_color))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2901:12
(function "nk_button_symbol" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2902:12
(function "nk_button_image" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (img (:struct nk_image))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2903:12
(function "nk_button_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (text_alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2904:12
(function "nk_button_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2905:12
(function "nk_button_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (img (:struct nk_image)) ((:pointer :char)) (text_alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2906:12
(function "nk_button_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (img (:struct nk_image)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2907:12
(function "nk_button_text_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (title (:pointer :char)) (len :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2908:12
(function "nk_button_label_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (title (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2909:12
(function "nk_button_symbol_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) ((:enum nk_symbol_type))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2910:12
(function "nk_button_image_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (img (:struct nk_image))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2911:12
(function "nk_button_symbol_text_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2912:12
(function "nk_button_symbol_label_styled" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (style (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (symbol (:enum nk_symbol_type)) (title (:pointer :char)) (align nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2913:12
(function "nk_button_image_label_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (img (:struct nk_image)) ((:pointer :char)) (text_alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2914:12
(function "nk_button_image_text_styled" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:223:8
(struct nk_style_button
      ))) (img (:struct nk_image)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2915:13
(function "nk_button_set_behavior" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_button_behavior))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:2916:12
(function "nk_button_push_behavior" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_button_behavior))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2917:12
(function "nk_button_pop_behavior" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2923:12
(function "nk_check_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (active :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2924:12
(function "nk_check_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (active :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2925:17
(function "nk_check_flags_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (flags :unsigned-int) (value :unsigned-int)) :unsigned-int)

;; /home/alf/src/Nuklear/src/nuklear.h:2926:17
(function "nk_check_flags_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (flags :unsigned-int) (value :unsigned-int)) :unsigned-int)

;; /home/alf/src/Nuklear/src/nuklear.h:2927:12
(function "nk_checkbox_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (active (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2928:12
(function "nk_checkbox_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (active (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2929:12
(function "nk_checkbox_flags_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (flags (:pointer :unsigned-int)) (value :unsigned-int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2930:12
(function "nk_checkbox_flags_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (flags (:pointer :unsigned-int)) (value :unsigned-int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2936:12
(function "nk_radio_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (active (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2937:12
(function "nk_radio_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (active (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2938:12
(function "nk_option_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (active :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2939:12
(function "nk_option_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (active :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2945:12
(function "nk_selectable_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2946:12
(function "nk_selectable_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2947:12
(function "nk_selectable_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2948:12
(function "nk_selectable_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (:int) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2949:12
(function "nk_selectable_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2950:12
(function "nk_selectable_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (align nk_flags) (value (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2952:12
(function "nk_select_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2953:12
(function "nk_select_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2954:12
(function "nk_select_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2955:12
(function "nk_select_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (:int) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2956:12
(function "nk_select_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2957:12
(function "nk_select_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (align nk_flags) (value :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2964:14
(function "nk_slide_float" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (min :float) (val :float) (max :float) (step :float)) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:2965:12
(function "nk_slide_int" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (min :int) (val :int) (max :int) (step :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2966:12
(function "nk_slider_float" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (min :float) (val (:pointer :float)) (max :float) (step :float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2967:12
(function "nk_slider_int" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (min :int) (val (:pointer :int)) (max :int) (step :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2973:12
(function "nk_progress" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (cur (:pointer nk_size)) (max nk_size) (modifyable :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:2974:16
(function "nk_prog" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (cur nk_size) (max nk_size) (modifyable :int)) nk_size)

;; /home/alf/src/Nuklear/src/nuklear.h:2981:25
(function "nk_color_picker" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_colorf)) ((:enum nk_color_format))) (:struct nk_colorf))

;; /home/alf/src/Nuklear/src/nuklear.h:2982:12
(function "nk_color_pick" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_colorf))) ((:enum nk_color_format))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3079:13
(function "nk_property_int" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :int) (val (:pointer :int)) (max :int) (step :int) (inc_per_pixel :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3100:13
(function "nk_property_float" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :float) (val (:pointer :float)) (max :float) (step :float) (inc_per_pixel :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3121:13
(function "nk_property_double" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :double) (val (:pointer :double)) (max :double) (step :double) (inc_per_pixel :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3144:12
(function "nk_propertyi" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :int) (val :int) (max :int) (step :int) (inc_per_pixel :float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3167:14
(function "nk_propertyf" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :float) (val :float) (max :float) (step :float) (inc_per_pixel :float)) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:3190:15
(function "nk_propertyd" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (name (:pointer :char)) (min :double) (val :double) (max :double) (step :double) (inc_per_pixel :float)) :double)

;; /home/alf/src/Nuklear/src/nuklear.h:3196:6
(enum nk_edit_flags
    (NK_EDIT_DEFAULT 0)
    (NK_EDIT_READ_ONLY 1)
    (NK_EDIT_AUTO_SELECT 2)
    (NK_EDIT_SIG_ENTER 4)
    (NK_EDIT_ALLOW_TAB 8)
    (NK_EDIT_NO_CURSOR 16)
    (NK_EDIT_SELECTABLE 32)
    (NK_EDIT_CLIPBOARD 64)
    (NK_EDIT_CTRL_ENTER_NEWLINE 128)
    (NK_EDIT_NO_HORIZONTAL_SCROLL 256)
    (NK_EDIT_ALWAYS_INSERT_MODE 512)
    (NK_EDIT_MULTILINE 1024)
    (NK_EDIT_GOTO_END_ON_ACTIVATE 2048))

;; /home/alf/src/Nuklear/src/nuklear.h:3211:6
(enum nk_edit_types
    (NK_EDIT_SIMPLE 512)
    (NK_EDIT_FIELD 608)
    (NK_EDIT_BOX 1640)
    (NK_EDIT_EDITOR 1128))

;; /home/alf/src/Nuklear/src/nuklear.h:3217:6
(enum nk_edit_events
    (NK_EDIT_ACTIVE 1)
    (NK_EDIT_INACTIVE 2)
    (NK_EDIT_ACTIVATED 4)
    (NK_EDIT_DEACTIVATED 8)
    (NK_EDIT_COMMITED 16))

;; /home/alf/src/Nuklear/src/nuklear.h:3224:17
(function "nk_edit_string" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_flags) (buffer (:pointer :char)) (len (:pointer :int)) (max :int) (nk_plugin_filter)) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:3225:17
(function "nk_edit_string_zero_terminated" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_flags) (buffer (:pointer :char)) (max :int) (nk_plugin_filter)) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:3226:17
(function "nk_edit_buffer" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_flags) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:217:8
(struct nk_text_edit
      ))) (nk_plugin_filter)) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:3227:13
(function "nk_edit_focus" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (flags nk_flags)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3228:13
(function "nk_edit_unfocus" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3234:12
(function "nk_chart_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) (num :int) (min :float) (max :float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3235:12
(function "nk_chart_begin_colored" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) ((:struct nk_color)) (active (:struct nk_color)) (num :int) (min :float) (max :float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3236:13
(function "nk_chart_add_slot" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) (count :int) (min_value :float) (max_value :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3237:13
(function "nk_chart_add_slot_colored" ((ctx (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) ((:struct nk_color)) (active (:struct nk_color)) (count :int) (min_value :float) (max_value :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3238:17
(function "nk_chart_push" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (:float)) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:3239:17
(function "nk_chart_push_slot" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (:float) (:int)) nk_flags)

;; /home/alf/src/Nuklear/src/nuklear.h:3240:13
(function "nk_chart_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3241:13
(function "nk_plot" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) (values (:pointer :float)) (count :int) (offset :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3242:13
(function "nk_plot_function" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_chart_type)) (userdata (:pointer :void)) (value_getter :function-pointer) (count :int) (offset :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3248:12
(function "nk_popup_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_popup_type)) ((:pointer :char)) (nk_flags) (bounds (:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3249:13
(function "nk_popup_close" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3250:13
(function "nk_popup_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3251:13
(function "nk_popup_get_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (offset_x (:pointer nk_uint)) (offset_y (:pointer nk_uint))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3252:13
(function "nk_popup_set_scroll" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (offset_x nk_uint) (offset_y nk_uint)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3258:12
(function "nk_combo" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items (:pointer (:pointer :char))) (count :int) (selected :int) (item_height :int) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3259:12
(function "nk_combo_separator" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items_separated_by_separator (:pointer :char)) (separator :int) (selected :int) (count :int) (item_height :int) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3260:12
(function "nk_combo_string" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items_separated_by_zeros (:pointer :char)) (selected :int) (count :int) (item_height :int) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3261:12
(function "nk_combo_callback" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (item_getter :function-pointer) (userdata (:pointer :void)) (selected :int) (count :int) (item_height :int) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3262:13
(function "nk_combobox" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items (:pointer (:pointer :char))) (count :int) (selected (:pointer :int)) (item_height :int) (size (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3263:13
(function "nk_combobox_string" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items_separated_by_zeros (:pointer :char)) (selected (:pointer :int)) (count :int) (item_height :int) (size (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3264:13
(function "nk_combobox_separator" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (items_separated_by_separator (:pointer :char)) (separator :int) (selected (:pointer :int)) (count :int) (item_height :int) (size (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3265:13
(function "nk_combobox_callback" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (item_getter :function-pointer) ((:pointer :void)) (selected (:pointer :int)) (count :int) (item_height :int) (size (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3271:12
(function "nk_combo_begin_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) (:int) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3272:12
(function "nk_combo_begin_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3273:12
(function "nk_combo_begin_color" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (color (:struct nk_color)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3274:12
(function "nk_combo_begin_symbol" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3275:12
(function "nk_combo_begin_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3276:12
(function "nk_combo_begin_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) (:int) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3277:12
(function "nk_combo_begin_image" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (img (:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3278:12
(function "nk_combo_begin_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) ((:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3279:12
(function "nk_combo_begin_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (selected (:pointer :char)) (:int) ((:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3280:12
(function "nk_combo_item_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3281:12
(function "nk_combo_item_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3282:12
(function "nk_combo_item_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3283:12
(function "nk_combo_item_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3284:12
(function "nk_combo_item_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3285:12
(function "nk_combo_item_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3286:13
(function "nk_combo_close" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3287:13
(function "nk_combo_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3293:12
(function "nk_contextual_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (nk_flags) ((:struct nk_vec2)) (trigger_bounds (:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3294:12
(function "nk_contextual_item_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3295:12
(function "nk_contextual_item_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3296:12
(function "nk_contextual_item_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3297:12
(function "nk_contextual_item_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (len :int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3298:12
(function "nk_contextual_item_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3299:12
(function "nk_contextual_item_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3300:13
(function "nk_contextual_close" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3301:13
(function "nk_contextual_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3307:13
(function "nk_tooltip" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3312:12
(function "nk_tooltip_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (width :float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3313:13
(function "nk_tooltip_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3319:13
(function "nk_menubar_begin" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3320:13
(function "nk_menubar_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3321:12
(function "nk_menu_begin_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) (title (:pointer :char)) (title_len :int) (align nk_flags) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3322:12
(function "nk_menu_begin_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3323:12
(function "nk_menu_begin_image" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) ((:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3324:12
(function "nk_menu_begin_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags) ((:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3325:12
(function "nk_menu_begin_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) ((:struct nk_image)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3326:12
(function "nk_menu_begin_symbol" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3327:12
(function "nk_menu_begin_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3328:12
(function "nk_menu_begin_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (align nk_flags) ((:enum nk_symbol_type)) (size (:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3329:12
(function "nk_menu_item_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (:int) (align nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3330:12
(function "nk_menu_item_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3331:12
(function "nk_menu_item_image_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3332:12
(function "nk_menu_item_image_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:struct nk_image)) ((:pointer :char)) (len :int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3333:12
(function "nk_menu_item_symbol_text" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (:int) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3334:12
(function "nk_menu_item_symbol_label" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_symbol_type)) ((:pointer :char)) (alignment nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3335:13
(function "nk_menu_close" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3336:13
(function "nk_menu_end" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3342:6
(enum nk_style_colors
    (NK_COLOR_TEXT 0)
    (NK_COLOR_WINDOW 1)
    (NK_COLOR_HEADER 2)
    (NK_COLOR_BORDER 3)
    (NK_COLOR_BUTTON 4)
    (NK_COLOR_BUTTON_HOVER 5)
    (NK_COLOR_BUTTON_ACTIVE 6)
    (NK_COLOR_TOGGLE 7)
    (NK_COLOR_TOGGLE_HOVER 8)
    (NK_COLOR_TOGGLE_CURSOR 9)
    (NK_COLOR_SELECT 10)
    (NK_COLOR_SELECT_ACTIVE 11)
    (NK_COLOR_SLIDER 12)
    (NK_COLOR_SLIDER_CURSOR 13)
    (NK_COLOR_SLIDER_CURSOR_HOVER 14)
    (NK_COLOR_SLIDER_CURSOR_ACTIVE 15)
    (NK_COLOR_PROPERTY 16)
    (NK_COLOR_EDIT 17)
    (NK_COLOR_EDIT_CURSOR 18)
    (NK_COLOR_COMBO 19)
    (NK_COLOR_CHART 20)
    (NK_COLOR_CHART_COLOR 21)
    (NK_COLOR_CHART_COLOR_HIGHLIGHT 22)
    (NK_COLOR_SCROLLBAR 23)
    (NK_COLOR_SCROLLBAR_CURSOR 24)
    (NK_COLOR_SCROLLBAR_CURSOR_HOVER 25)
    (NK_COLOR_SCROLLBAR_CURSOR_ACTIVE 26)
    (NK_COLOR_TAB_HEADER 27)
    (NK_COLOR_COUNT 28))

;; /home/alf/src/Nuklear/src/nuklear.h:3373:6
(enum nk_style_cursor
    (NK_CURSOR_ARROW 0)
    (NK_CURSOR_TEXT 1)
    (NK_CURSOR_MOVE 2)
    (NK_CURSOR_RESIZE_VERTICAL 3)
    (NK_CURSOR_RESIZE_HORIZONTAL 4)
    (NK_CURSOR_RESIZE_TOP_LEFT_DOWN_RIGHT 5)
    (NK_CURSOR_RESIZE_TOP_RIGHT_DOWN_LEFT 6)
    (NK_CURSOR_COUNT 7))

;; /home/alf/src/Nuklear/src/nuklear.h:3383:13
(function "nk_style_default" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3384:13
(function "nk_style_from_table" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_color)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3385:13
(function "nk_style_load_cursor" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_style_cursor)) ((:pointer (:struct nk_cursor)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3386:13
(function "nk_style_load_all_cursors" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_cursor)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3387:20
(function "nk_style_get_color_by_name" (((:enum nk_style_colors))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3388:13
(function "nk_style_set_font" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3389:12
(function "nk_style_set_cursor" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:enum nk_style_cursor))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3390:13
(function "nk_style_show_cursor" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3391:13
(function "nk_style_hide_cursor" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3393:12
(function "nk_style_push_font" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:219:8
(struct nk_user_font
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3394:12
(function "nk_style_push_float" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer :float)) (:float)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3395:12
(function "nk_style_push_vec2" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_vec2))) ((:struct nk_vec2))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3396:12
(function "nk_style_push_style_item" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:216:8
(struct nk_style_item
      ))) (;; /home/alf/src/Nuklear/src/nuklear.h:216:8
(struct nk_style_item
    ))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3397:12
(function "nk_style_push_flags" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer nk_flags)) (nk_flags)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3398:12
(function "nk_style_push_color" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      ))) ((:pointer (:struct nk_color))) ((:struct nk_color))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3400:12
(function "nk_style_pop_font" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3401:12
(function "nk_style_pop_float" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3402:12
(function "nk_style_pop_vec2" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3403:12
(function "nk_style_pop_style_item" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3404:12
(function "nk_style_pop_flags" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3405:12
(function "nk_style_pop_color" (((:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:221:8
(struct nk_context
      )))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3411:24
(function "nk_rgb" ((r :int) (g :int) (b :int)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3412:24
(function "nk_rgb_iv" ((rgb (:pointer :int))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3413:24
(function "nk_rgb_bv" ((rgb (:pointer nk_byte))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3414:24
(function "nk_rgb_f" ((r :float) (g :float) (b :float)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3415:24
(function "nk_rgb_fv" ((rgb (:pointer :float))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3416:24
(function "nk_rgb_cf" ((c (:struct nk_colorf))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3417:24
(function "nk_rgb_hex" ((rgb (:pointer :char))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3419:24
(function "nk_rgba" ((r :int) (g :int) (b :int) (a :int)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3420:24
(function "nk_rgba_u32" ((nk_uint)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3421:24
(function "nk_rgba_iv" ((rgba (:pointer :int))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3422:24
(function "nk_rgba_bv" ((rgba (:pointer nk_byte))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3423:24
(function "nk_rgba_f" ((r :float) (g :float) (b :float) (a :float)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3424:24
(function "nk_rgba_fv" ((rgba (:pointer :float))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3425:24
(function "nk_rgba_cf" ((c (:struct nk_colorf))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3426:24
(function "nk_rgba_hex" ((rgb (:pointer :char))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3428:25
(function "nk_hsva_colorf" ((h :float) (s :float) (v :float) (a :float)) (:struct nk_colorf))

;; /home/alf/src/Nuklear/src/nuklear.h:3429:25
(function "nk_hsva_colorfv" ((c (:pointer :float))) (:struct nk_colorf))

;; /home/alf/src/Nuklear/src/nuklear.h:3430:13
(function "nk_colorf_hsva_f" ((out_h (:pointer :float)) (out_s (:pointer :float)) (out_v (:pointer :float)) (out_a (:pointer :float)) (in (:struct nk_colorf))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3431:13
(function "nk_colorf_hsva_fv" ((hsva (:pointer :float)) (in (:struct nk_colorf))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3433:24
(function "nk_hsv" ((h :int) (s :int) (v :int)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3434:24
(function "nk_hsv_iv" ((hsv (:pointer :int))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3435:24
(function "nk_hsv_bv" ((hsv (:pointer nk_byte))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3436:24
(function "nk_hsv_f" ((h :float) (s :float) (v :float)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3437:24
(function "nk_hsv_fv" ((hsv (:pointer :float))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3439:24
(function "nk_hsva" ((h :int) (s :int) (v :int) (a :int)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3440:24
(function "nk_hsva_iv" ((hsva (:pointer :int))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3441:24
(function "nk_hsva_bv" ((hsva (:pointer nk_byte))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3442:24
(function "nk_hsva_f" ((h :float) (s :float) (v :float) (a :float)) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3443:24
(function "nk_hsva_fv" ((hsva (:pointer :float))) (:struct nk_color))

;; /home/alf/src/Nuklear/src/nuklear.h:3446:13
(function "nk_color_f" ((r (:pointer :float)) (g (:pointer :float)) (b (:pointer :float)) (a (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3447:13
(function "nk_color_fv" ((rgba_out (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3448:25
(function "nk_color_cf" (((:struct nk_color))) (:struct nk_colorf))

;; /home/alf/src/Nuklear/src/nuklear.h:3449:13
(function "nk_color_d" ((r (:pointer :double)) (g (:pointer :double)) (b (:pointer :double)) (a (:pointer :double)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3450:13
(function "nk_color_dv" ((rgba_out (:pointer :double)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3452:16
(function "nk_color_u32" (((:struct nk_color))) nk_uint)

;; /home/alf/src/Nuklear/src/nuklear.h:3453:13
(function "nk_color_hex_rgba" ((output (:pointer :char)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3454:13
(function "nk_color_hex_rgb" ((output (:pointer :char)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3456:13
(function "nk_color_hsv_i" ((out_h (:pointer :int)) (out_s (:pointer :int)) (out_v (:pointer :int)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3457:13
(function "nk_color_hsv_b" ((out_h (:pointer nk_byte)) (out_s (:pointer nk_byte)) (out_v (:pointer nk_byte)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3458:13
(function "nk_color_hsv_iv" ((hsv_out (:pointer :int)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3459:13
(function "nk_color_hsv_bv" ((hsv_out (:pointer nk_byte)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3460:13
(function "nk_color_hsv_f" ((out_h (:pointer :float)) (out_s (:pointer :float)) (out_v (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3461:13
(function "nk_color_hsv_fv" ((hsv_out (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3463:13
(function "nk_color_hsva_i" ((h (:pointer :int)) (s (:pointer :int)) (v (:pointer :int)) (a (:pointer :int)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3464:13
(function "nk_color_hsva_b" ((h (:pointer nk_byte)) (s (:pointer nk_byte)) (v (:pointer nk_byte)) (a (:pointer nk_byte)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3465:13
(function "nk_color_hsva_iv" ((hsva_out (:pointer :int)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3466:13
(function "nk_color_hsva_bv" ((hsva_out (:pointer nk_byte)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3467:13
(function "nk_color_hsva_f" ((out_h (:pointer :float)) (out_s (:pointer :float)) (out_v (:pointer :float)) (out_a (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3468:13
(function "nk_color_hsva_fv" ((hsva_out (:pointer :float)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3474:18
(function "nk_handle_ptr" (((:pointer :void))) nk_handle)

;; /home/alf/src/Nuklear/src/nuklear.h:3475:18
(function "nk_handle_id" ((:int)) nk_handle)

;; /home/alf/src/Nuklear/src/nuklear.h:3476:24
(function "nk_image_handle" ((nk_handle)) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3477:24
(function "nk_image_ptr" (((:pointer :void))) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3478:24
(function "nk_image_id" ((:int)) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3479:12
(function "nk_image_is_subimage" ((img (:pointer (:struct nk_image)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3480:24
(function "nk_subimage_ptr" (((:pointer :void)) (w :unsigned-short) (h :unsigned-short) (sub_region (:struct nk_rect))) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3481:24
(function "nk_subimage_id" ((:int) (w :unsigned-short) (h :unsigned-short) (sub_region (:struct nk_rect))) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3482:24
(function "nk_subimage_handle" ((nk_handle) (w :unsigned-short) (h :unsigned-short) (sub_region (:struct nk_rect))) (:struct nk_image))

;; /home/alf/src/Nuklear/src/nuklear.h:3488:16
(function "nk_murmur_hash" ((key (:pointer :void)) (len :int) (seed nk_hash)) nk_hash)

;; /home/alf/src/Nuklear/src/nuklear.h:3489:13
(function "nk_triangle_from_direction" ((result (:pointer (:struct nk_vec2))) (r (:struct nk_rect)) (pad_x :float) (pad_y :float) ((:enum nk_heading))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3491:23
(function "nk_vec2" ((x :float) (y :float)) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3492:23
(function "nk_vec2i" ((x :int) (y :int)) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3493:23
(function "nk_vec2v" ((xy (:pointer :float))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3494:23
(function "nk_vec2iv" ((xy (:pointer :int))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3496:23
(function "nk_get_null_rect" () (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3497:23
(function "nk_rect" ((x :float) (y :float) (w :float) (h :float)) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3498:23
(function "nk_recti" ((x :int) (y :int) (w :int) (h :int)) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3499:23
(function "nk_recta" ((pos (:struct nk_vec2)) (size (:struct nk_vec2))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3500:23
(function "nk_rectv" ((xywh (:pointer :float))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3501:23
(function "nk_rectiv" ((xywh (:pointer :int))) (:struct nk_rect))

;; /home/alf/src/Nuklear/src/nuklear.h:3502:23
(function "nk_rect_pos" (((:struct nk_rect))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3503:23
(function "nk_rect_size" (((:struct nk_rect))) (:struct nk_vec2))

;; /home/alf/src/Nuklear/src/nuklear.h:3509:12
(function "nk_strlen" ((str (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3510:12
(function "nk_stricmp" ((s1 (:pointer :char)) (s2 (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3511:12
(function "nk_stricmpn" ((s1 (:pointer :char)) (s2 (:pointer :char)) (n :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3512:12
(function "nk_strtoi" ((str (:pointer :char)) (endptr (:pointer (:pointer :char)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3513:14
(function "nk_strtof" ((str (:pointer :char)) (endptr (:pointer (:pointer :char)))) :float)

;; /home/alf/src/Nuklear/src/nuklear.h:3514:15
(function "nk_strtod" ((str (:pointer :char)) (endptr (:pointer (:pointer :char)))) :double)

;; /home/alf/src/Nuklear/src/nuklear.h:3515:12
(function "nk_strfilter" ((text (:pointer :char)) (regexp (:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3516:12
(function "nk_strmatch_fuzzy_string" ((str (:pointer :char)) (pattern (:pointer :char)) (out_score (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3517:12
(function "nk_strmatch_fuzzy_text" ((txt (:pointer :char)) (txt_len :int) (pattern (:pointer :char)) (out_score (:pointer :int))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3523:12
(function "nk_utf_decode" (((:pointer :char)) ((:pointer nk_rune)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3524:12
(function "nk_utf_encode" ((nk_rune) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3525:12
(function "nk_utf_len" (((:pointer :char)) (byte_len :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3526:20
(function "nk_utf_at" ((buffer (:pointer :char)) (length :int) (index :int) (unicode (:pointer nk_rune)) (len (:pointer :int))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3676:8
(struct nk_user_font_glyph
  )

;; /home/alf/src/Nuklear/src/nuklear.h:3677:16
(typedef nk_text_width_f :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:3678:15
(typedef nk_query_font_glyph_f :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:3683:8
(struct nk_user_font_glyph
  (uv (:array (:struct nk_vec2) 2))
  (offset (:struct nk_vec2))
  (width :float)
  (height :float)
  (xadvance :float))

;; /home/alf/src/Nuklear/src/nuklear.h:3695:8
(struct nk_user_font
  (userdata nk_handle)
  (height :float)
  (width nk_text_width_f)
  (query nk_query_font_glyph_f)
  (texture nk_handle))

;; /home/alf/src/Nuklear/src/nuklear.h:3711:6
(enum nk_font_coord_type
    (NK_COORD_UV 0)
    (NK_COORD_PIXEL 1))

;; /home/alf/src/Nuklear/src/nuklear.h:3716:8
(struct nk_font
  )

;; /home/alf/src/Nuklear/src/nuklear.h:3717:8
(struct nk_baked_font
  (height :float)
  (ascent :float)
  (descent :float)
  (glyph_offset nk_rune)
  (glyph_count nk_rune)
  (ranges (:pointer nk_rune)))

;; /home/alf/src/Nuklear/src/nuklear.h:3730:8
(struct nk_font_config
  (next (:pointer (:struct nk_font_config)))
  (ttf_blob (:pointer :void))
  (ttf_size nk_size)
  (ttf_data_owned_by_atlas :unsigned-char)
  (merge_mode :unsigned-char)
  (pixel_snap :unsigned-char)
  (oversample_v :unsigned-char)
  (oversample_h :unsigned-char)
  (padding (:array :unsigned-char 3))
  (size :float)
  (coord_type (:enum nk_font_coord_type))
  (spacing (:struct nk_vec2))
  (range (:pointer nk_rune))
  (font (:pointer (:struct nk_baked_font)))
  (fallback_glyph nk_rune)
  (n (:pointer (:struct nk_font_config)))
  (p (:pointer (:struct nk_font_config))))

;; /home/alf/src/Nuklear/src/nuklear.h:3766:8
(struct nk_font_glyph
  (codepoint nk_rune)
  (xadvance :float)
  (x0 :float)
  (y0 :float)
  (x1 :float)
  (y1 :float)
  (w :float)
  (h :float)
  (u0 :float)
  (v0 :float)
  (u1 :float)
  (v1 :float))

;; /home/alf/src/Nuklear/src/nuklear.h:3773:8
(struct nk_font
  (next (:pointer (:struct nk_font)))
  (handle (:struct nk_user_font))
  (info (:struct nk_baked_font))
  (scale :float)
  (glyphs (:pointer (:struct nk_font_glyph)))
  (fallback (:pointer (:struct nk_font_glyph)))
  (fallback_codepoint nk_rune)
  (texture nk_handle)
  (config (:pointer (:struct nk_font_config))))

;; /home/alf/src/Nuklear/src/nuklear.h:3785:6
(enum nk_font_atlas_format
    (NK_FONT_ATLAS_ALPHA8 0)
    (NK_FONT_ATLAS_RGBA32 1))

;; /home/alf/src/Nuklear/src/nuklear.h:3790:8
(struct nk_font_atlas
  (pixel (:pointer :void))
  (tex_width :int)
  (tex_height :int)
  (permanent (:struct nk_allocator))
  (temporary (:struct nk_allocator))
  (custom (:struct nk_recti))
  (cursors (:array (:struct nk_cursor) 7))
  (glyph_count :int)
  (glyphs (:pointer (:struct nk_font_glyph)))
  (default_font (:pointer (:struct nk_font)))
  (fonts (:pointer (:struct nk_font)))
  (config (:pointer (:struct nk_font_config)))
  (font_num :int))

;; /home/alf/src/Nuklear/src/nuklear.h:3810:23
(function "nk_font_default_glyph_ranges" () (:pointer nk_rune))

;; /home/alf/src/Nuklear/src/nuklear.h:3811:23
(function "nk_font_chinese_glyph_ranges" () (:pointer nk_rune))

;; /home/alf/src/Nuklear/src/nuklear.h:3812:23
(function "nk_font_cyrillic_glyph_ranges" () (:pointer nk_rune))

;; /home/alf/src/Nuklear/src/nuklear.h:3813:23
(function "nk_font_korean_glyph_ranges" () (:pointer nk_rune))

;; /home/alf/src/Nuklear/src/nuklear.h:3816:13
(function "nk_font_atlas_init_default" (((:pointer (:struct nk_font_atlas)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3818:13
(function "nk_font_atlas_init" (((:pointer (:struct nk_font_atlas))) ((:pointer (:struct nk_allocator)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3819:13
(function "nk_font_atlas_init_custom" (((:pointer (:struct nk_font_atlas))) (persistent (:pointer (:struct nk_allocator))) (transient (:pointer (:struct nk_allocator)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3820:13
(function "nk_font_atlas_begin" (((:pointer (:struct nk_font_atlas)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3821:30
(function "nk_font_config" ((pixel_height :float)) (:struct nk_font_config))

;; /home/alf/src/Nuklear/src/nuklear.h:3822:24
(function "nk_font_atlas_add" (((:pointer (:struct nk_font_atlas))) ((:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3824:24
(function "nk_font_atlas_add_default" (((:pointer (:struct nk_font_atlas))) (height :float) ((:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3826:24
(function "nk_font_atlas_add_from_memory" ((atlas (:pointer (:struct nk_font_atlas))) (memory (:pointer :void)) (size nk_size) (height :float) (config (:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3828:24
(function "nk_font_atlas_add_from_file" ((atlas (:pointer (:struct nk_font_atlas))) (file_path (:pointer :char)) (height :float) ((:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3830:24
(function "nk_font_atlas_add_compressed" (((:pointer (:struct nk_font_atlas))) (memory (:pointer :void)) (size nk_size) (height :float) ((:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3831:24
(function "nk_font_atlas_add_compressed_base85" (((:pointer (:struct nk_font_atlas))) (data (:pointer :char)) (height :float) (config (:pointer (:struct nk_font_config)))) (:pointer (:struct nk_font)))

;; /home/alf/src/Nuklear/src/nuklear.h:3832:20
(function "nk_font_atlas_bake" (((:pointer (:struct nk_font_atlas))) (width (:pointer :int)) (height (:pointer :int)) ((:enum nk_font_atlas_format))) (:pointer :void))

;; /home/alf/src/Nuklear/src/nuklear.h:3833:13
(function "nk_font_atlas_end" (((:pointer (:struct nk_font_atlas))) (tex nk_handle) ((:pointer (:struct nk_draw_null_texture)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3834:36
(function "nk_font_find_glyph" (((:pointer (:struct nk_font))) (unicode nk_rune)) (:pointer (:struct nk_font_glyph)))

;; /home/alf/src/Nuklear/src/nuklear.h:3835:13
(function "nk_font_atlas_cleanup" ((atlas (:pointer (:struct nk_font_atlas)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3836:13
(function "nk_font_atlas_clear" (((:pointer (:struct nk_font_atlas)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3873:8
(struct nk_memory_status
  (memory (:pointer :void))
  (type :unsigned-int)
  (size nk_size)
  (allocated nk_size)
  (needed nk_size)
  (calls nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:3882:6
(enum nk_allocation_type
    (NK_BUFFER_FIXED 0)
    (NK_BUFFER_DYNAMIC 1))

;; /home/alf/src/Nuklear/src/nuklear.h:3887:6
(enum nk_buffer_allocation_type
    (NK_BUFFER_FRONT 0)
    (NK_BUFFER_BACK 1)
    (NK_BUFFER_MAX 2))

;; /home/alf/src/Nuklear/src/nuklear.h:3893:8
(struct nk_buffer_marker
  (active :int)
  (offset nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:3898:8
(struct nk_memory
  (ptr (:pointer :void))
  (size nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:3899:8
(struct nk_buffer
  (marker (:array (:struct nk_buffer_marker) 2))
  (pool (:struct nk_allocator))
  (type (:enum nk_allocation_type))
  (memory (:struct nk_memory))
  (grow_factor :float)
  (allocated nk_size)
  (needed nk_size)
  (calls nk_size)
  (size nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:3921:13
(function "nk_buffer_init_default" (((:pointer (:struct nk_buffer)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3923:13
(function "nk_buffer_init" (((:pointer (:struct nk_buffer))) ((:pointer (:struct nk_allocator))) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3924:13
(function "nk_buffer_init_fixed" (((:pointer (:struct nk_buffer))) (memory (:pointer :void)) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3925:13
(function "nk_buffer_info" (((:pointer (:struct nk_memory_status))) ((:pointer (:struct nk_buffer)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3926:13
(function "nk_buffer_push" (((:pointer (:struct nk_buffer))) (type (:enum nk_buffer_allocation_type)) (memory (:pointer :void)) (size nk_size) (align nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3927:13
(function "nk_buffer_mark" (((:pointer (:struct nk_buffer))) (type (:enum nk_buffer_allocation_type))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3928:13
(function "nk_buffer_reset" (((:pointer (:struct nk_buffer))) (type (:enum nk_buffer_allocation_type))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3929:13
(function "nk_buffer_clear" (((:pointer (:struct nk_buffer)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3930:13
(function "nk_buffer_free" (((:pointer (:struct nk_buffer)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3931:14
(function "nk_buffer_memory" (((:pointer (:struct nk_buffer)))) (:pointer :void))

;; /home/alf/src/Nuklear/src/nuklear.h:3932:20
(function "nk_buffer_memory_const" (((:pointer (:struct nk_buffer)))) (:pointer :void))

;; /home/alf/src/Nuklear/src/nuklear.h:3933:16
(function "nk_buffer_total" (((:pointer (:struct nk_buffer)))) nk_size)

;; /home/alf/src/Nuklear/src/nuklear.h:3945:8
(struct nk_str
  (buffer (:struct nk_buffer))
  (len :int))

;; /home/alf/src/Nuklear/src/nuklear.h:3951:13
(function "nk_str_init_default" (((:pointer (:struct nk_str)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3953:13
(function "nk_str_init" (((:pointer (:struct nk_str))) ((:pointer (:struct nk_allocator))) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3954:13
(function "nk_str_init_fixed" (((:pointer (:struct nk_str))) (memory (:pointer :void)) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3955:13
(function "nk_str_clear" (((:pointer (:struct nk_str)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3956:13
(function "nk_str_free" (((:pointer (:struct nk_str)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3958:12
(function "nk_str_append_text_char" (((:pointer (:struct nk_str))) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3959:12
(function "nk_str_append_str_char" (((:pointer (:struct nk_str))) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3960:12
(function "nk_str_append_text_utf8" (((:pointer (:struct nk_str))) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3961:12
(function "nk_str_append_str_utf8" (((:pointer (:struct nk_str))) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3962:12
(function "nk_str_append_text_runes" (((:pointer (:struct nk_str))) ((:pointer nk_rune)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3963:12
(function "nk_str_append_str_runes" (((:pointer (:struct nk_str))) ((:pointer nk_rune))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3965:12
(function "nk_str_insert_at_char" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3966:12
(function "nk_str_insert_at_rune" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3968:12
(function "nk_str_insert_text_char" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3969:12
(function "nk_str_insert_str_char" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3970:12
(function "nk_str_insert_text_utf8" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3971:12
(function "nk_str_insert_str_utf8" (((:pointer (:struct nk_str))) (pos :int) ((:pointer :char))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3972:12
(function "nk_str_insert_text_runes" (((:pointer (:struct nk_str))) (pos :int) ((:pointer nk_rune)) (:int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3973:12
(function "nk_str_insert_str_runes" (((:pointer (:struct nk_str))) (pos :int) ((:pointer nk_rune))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3975:13
(function "nk_str_remove_chars" (((:pointer (:struct nk_str))) (len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3976:13
(function "nk_str_remove_runes" ((str (:pointer (:struct nk_str))) (len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3977:13
(function "nk_str_delete_chars" (((:pointer (:struct nk_str))) (pos :int) (len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3978:13
(function "nk_str_delete_runes" (((:pointer (:struct nk_str))) (pos :int) (len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:3980:14
(function "nk_str_at_char" (((:pointer (:struct nk_str))) (pos :int)) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3981:14
(function "nk_str_at_rune" (((:pointer (:struct nk_str))) (pos :int) (unicode (:pointer nk_rune)) (len (:pointer :int))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3982:16
(function "nk_str_rune_at" (((:pointer (:struct nk_str))) (pos :int)) nk_rune)

;; /home/alf/src/Nuklear/src/nuklear.h:3983:20
(function "nk_str_at_char_const" (((:pointer (:struct nk_str))) (pos :int)) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3984:20
(function "nk_str_at_const" (((:pointer (:struct nk_str))) (pos :int) (unicode (:pointer nk_rune)) (len (:pointer :int))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3986:14
(function "nk_str_get" (((:pointer (:struct nk_str)))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3987:20
(function "nk_str_get_const" (((:pointer (:struct nk_str)))) (:pointer :char))

;; /home/alf/src/Nuklear/src/nuklear.h:3988:12
(function "nk_str_len" (((:pointer (:struct nk_str)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:3989:12
(function "nk_str_len_char" (((:pointer (:struct nk_str)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4027:8
(struct nk_text_edit
  )

;; /home/alf/src/Nuklear/src/nuklear.h:4028:8
(struct nk_clipboard
  (userdata nk_handle)
  (paste nk_plugin_paste)
  (copy nk_plugin_copy))

;; /home/alf/src/Nuklear/src/nuklear.h:4034:8
(struct nk_text_undo_record
  (where :int)
  (insert_length :short)
  (delete_length :short)
  (char_storage :short))

;; /home/alf/src/Nuklear/src/nuklear.h:4041:8
(struct nk_text_undo_state
  (undo_rec (:array (:struct nk_text_undo_record) 99))
  (undo_char (:array nk_rune 999))
  (undo_point :short)
  (redo_point :short)
  (undo_char_point :short)
  (redo_char_point :short))

;; /home/alf/src/Nuklear/src/nuklear.h:4050:6
(enum nk_text_edit_type
    (NK_TEXT_EDIT_SINGLE_LINE 0)
    (NK_TEXT_EDIT_MULTI_LINE 1))

;; /home/alf/src/Nuklear/src/nuklear.h:4055:6
(enum nk_text_edit_mode
    (NK_TEXT_EDIT_MODE_VIEW 0)
    (NK_TEXT_EDIT_MODE_INSERT 1)
    (NK_TEXT_EDIT_MODE_REPLACE 2))

;; /home/alf/src/Nuklear/src/nuklear.h:4061:8
(struct nk_text_edit
  (clip (:struct nk_clipboard))
  (string (:struct nk_str))
  (filter nk_plugin_filter)
  (scrollbar (:struct nk_vec2))
  (cursor :int)
  (select_start :int)
  (select_end :int)
  (mode :unsigned-char)
  (cursor_at_end_of_line :unsigned-char)
  (initialized :unsigned-char)
  (has_preferred_x :unsigned-char)
  (single_line :unsigned-char)
  (active :unsigned-char)
  (padding1 :unsigned-char)
  (preferred_x :float)
  (undo (:struct nk_text_undo_state)))

;; /home/alf/src/Nuklear/src/nuklear.h:4082:12
(function "nk_filter_default" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4083:12
(function "nk_filter_ascii" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4084:12
(function "nk_filter_float" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4085:12
(function "nk_filter_decimal" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4086:12
(function "nk_filter_hex" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4087:12
(function "nk_filter_oct" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4088:12
(function "nk_filter_binary" (((:pointer (:struct nk_text_edit))) (unicode nk_rune)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4092:13
(function "nk_textedit_init_default" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4094:13
(function "nk_textedit_init" (((:pointer (:struct nk_text_edit))) ((:pointer (:struct nk_allocator))) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4095:13
(function "nk_textedit_init_fixed" (((:pointer (:struct nk_text_edit))) (memory (:pointer :void)) (size nk_size)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4096:13
(function "nk_textedit_free" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4097:13
(function "nk_textedit_text" (((:pointer (:struct nk_text_edit))) ((:pointer :char)) (total_len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4098:13
(function "nk_textedit_delete" (((:pointer (:struct nk_text_edit))) (where :int) (len :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4099:13
(function "nk_textedit_delete_selection" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4100:13
(function "nk_textedit_select_all" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4101:12
(function "nk_textedit_cut" (((:pointer (:struct nk_text_edit)))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4102:12
(function "nk_textedit_paste" (((:pointer (:struct nk_text_edit))) ((:pointer :char)) (len :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4103:13
(function "nk_textedit_undo" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4104:13
(function "nk_textedit_redo" (((:pointer (:struct nk_text_edit)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4155:6
(enum nk_command_type
    (NK_COMMAND_NOP 0)
    (NK_COMMAND_SCISSOR 1)
    (NK_COMMAND_LINE 2)
    (NK_COMMAND_CURVE 3)
    (NK_COMMAND_RECT 4)
    (NK_COMMAND_RECT_FILLED 5)
    (NK_COMMAND_RECT_MULTI_COLOR 6)
    (NK_COMMAND_CIRCLE 7)
    (NK_COMMAND_CIRCLE_FILLED 8)
    (NK_COMMAND_ARC 9)
    (NK_COMMAND_ARC_FILLED 10)
    (NK_COMMAND_TRIANGLE 11)
    (NK_COMMAND_TRIANGLE_FILLED 12)
    (NK_COMMAND_POLYGON 13)
    (NK_COMMAND_POLYGON_FILLED 14)
    (NK_COMMAND_POLYLINE 15)
    (NK_COMMAND_TEXT 16)
    (NK_COMMAND_IMAGE 17)
    (NK_COMMAND_CUSTOM 18))

;; /home/alf/src/Nuklear/src/nuklear.h:4178:8
(struct nk_command
  (type (:enum nk_command_type))
  (next nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:4186:8
(struct nk_command_scissor
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short))

;; /home/alf/src/Nuklear/src/nuklear.h:4192:8
(struct nk_command_line
  (header (:struct nk_command))
  (line_thickness :unsigned-short)
  (begin (:struct nk_vec2i))
  (end (:struct nk_vec2i))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4200:8
(struct nk_command_curve
  (header (:struct nk_command))
  (line_thickness :unsigned-short)
  (begin (:struct nk_vec2i))
  (end (:struct nk_vec2i))
  (ctrl (:array (:struct nk_vec2i) 2))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4209:8
(struct nk_command_rect
  (header (:struct nk_command))
  (rounding :unsigned-short)
  (line_thickness :unsigned-short)
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4218:8
(struct nk_command_rect_filled
  (header (:struct nk_command))
  (rounding :unsigned-short)
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4226:8
(struct nk_command_rect_multi_color
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (left (:struct nk_color))
  (top (:struct nk_color))
  (bottom (:struct nk_color))
  (right (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4236:8
(struct nk_command_triangle
  (header (:struct nk_command))
  (line_thickness :unsigned-short)
  (a (:struct nk_vec2i))
  (b (:struct nk_vec2i))
  (c (:struct nk_vec2i))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4245:8
(struct nk_command_triangle_filled
  (header (:struct nk_command))
  (a (:struct nk_vec2i))
  (b (:struct nk_vec2i))
  (c (:struct nk_vec2i))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4253:8
(struct nk_command_circle
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (line_thickness :unsigned-short)
  (w :unsigned-short)
  (h :unsigned-short)
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4261:8
(struct nk_command_circle_filled
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4268:8
(struct nk_command_arc
  (header (:struct nk_command))
  (cx :short)
  (cy :short)
  (r :unsigned-short)
  (line_thickness :unsigned-short)
  (a (:array :float 2))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4277:8
(struct nk_command_arc_filled
  (header (:struct nk_command))
  (cx :short)
  (cy :short)
  (r :unsigned-short)
  (a (:array :float 2))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4285:8
(struct nk_command_polygon
  (header (:struct nk_command))
  (color (:struct nk_color))
  (line_thickness :unsigned-short)
  (point_count :unsigned-short)
  (points (:array (:struct nk_vec2i) 1)))

;; /home/alf/src/Nuklear/src/nuklear.h:4293:8
(struct nk_command_polygon_filled
  (header (:struct nk_command))
  (color (:struct nk_color))
  (point_count :unsigned-short)
  (points (:array (:struct nk_vec2i) 1)))

;; /home/alf/src/Nuklear/src/nuklear.h:4300:8
(struct nk_command_polyline
  (header (:struct nk_command))
  (color (:struct nk_color))
  (line_thickness :unsigned-short)
  (point_count :unsigned-short)
  (points (:array (:struct nk_vec2i) 1)))

;; /home/alf/src/Nuklear/src/nuklear.h:4308:8
(struct nk_command_image
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (img (:struct nk_image))
  (col (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4316:16
(typedef nk_command_custom_callback :function-pointer)

;; /home/alf/src/Nuklear/src/nuklear.h:4318:8
(struct nk_command_custom
  (header (:struct nk_command))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (callback_data nk_handle)
  (callback nk_command_custom_callback))

;; /home/alf/src/Nuklear/src/nuklear.h:4326:8
(struct nk_command_text
  (header (:struct nk_command))
  (font (:pointer (:struct nk_user_font)))
  (background (:struct nk_color))
  (foreground (:struct nk_color))
  (x :short)
  (y :short)
  (w :unsigned-short)
  (h :unsigned-short)
  (height :float)
  (length :int)
  (string (:array :char 1)))

;; /home/alf/src/Nuklear/src/nuklear.h:4338:6
(enum nk_command_clipping
    (NK_CLIPPING_OFF 0)
    (NK_CLIPPING_ON 1))

;; /home/alf/src/Nuklear/src/nuklear.h:4343:8
(struct nk_command_buffer
  (base (:pointer (:struct nk_buffer)))
  (clip (:struct nk_rect))
  (use_clipping :int)
  (userdata nk_handle)
  (begin nk_size)
  (end nk_size)
  (last nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:4352:13
(function "nk_stroke_line" ((b (:pointer (:struct nk_command_buffer))) (x0 :float) (y0 :float) (x1 :float) (y1 :float) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4353:13
(function "nk_stroke_curve" (((:pointer (:struct nk_command_buffer))) (:float) (:float) (:float) (:float) (:float) (:float) (:float) (:float) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4354:13
(function "nk_stroke_rect" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (rounding :float) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4355:13
(function "nk_stroke_circle" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4356:13
(function "nk_stroke_arc" (((:pointer (:struct nk_command_buffer))) (cx :float) (cy :float) (radius :float) (a_min :float) (a_max :float) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4357:13
(function "nk_stroke_triangle" (((:pointer (:struct nk_command_buffer))) (:float) (:float) (:float) (:float) (:float) (:float) (line_thichness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4358:13
(function "nk_stroke_polyline" (((:pointer (:struct nk_command_buffer))) (points (:pointer :float)) (point_count :int) (line_thickness :float) (col (:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4359:13
(function "nk_stroke_polygon" (((:pointer (:struct nk_command_buffer))) ((:pointer :float)) (point_count :int) (line_thickness :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4362:13
(function "nk_fill_rect" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (rounding :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4363:13
(function "nk_fill_rect_multi_color" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (left (:struct nk_color)) (top (:struct nk_color)) (right (:struct nk_color)) (bottom (:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4364:13
(function "nk_fill_circle" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4365:13
(function "nk_fill_arc" (((:pointer (:struct nk_command_buffer))) (cx :float) (cy :float) (radius :float) (a_min :float) (a_max :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4366:13
(function "nk_fill_triangle" (((:pointer (:struct nk_command_buffer))) (x0 :float) (y0 :float) (x1 :float) (y1 :float) (x2 :float) (y2 :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4367:13
(function "nk_fill_polygon" (((:pointer (:struct nk_command_buffer))) ((:pointer :float)) (point_count :int) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4370:13
(function "nk_draw_image" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) ((:pointer (:struct nk_image))) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4371:13
(function "nk_draw_text" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (text (:pointer :char)) (len :int) ((:pointer (:struct nk_user_font))) ((:struct nk_color)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4372:13
(function "nk_push_scissor" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4373:13
(function "nk_push_custom" (((:pointer (:struct nk_command_buffer))) ((:struct nk_rect)) (nk_command_custom_callback) (usr nk_handle)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4380:8
(struct nk_mouse_button
  (down :int)
  (clicked :unsigned-int)
  (clicked_pos (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4385:8
(struct nk_mouse
  (buttons (:array (:struct nk_mouse_button) 4))
  (pos (:struct nk_vec2))
  (prev (:struct nk_vec2))
  (delta (:struct nk_vec2))
  (scroll_delta (:struct nk_vec2))
  (grab :unsigned-char)
  (grabbed :unsigned-char)
  (ungrab :unsigned-char))

;; /home/alf/src/Nuklear/src/nuklear.h:4396:8
(struct nk_key
  (down :int)
  (clicked :unsigned-int))

;; /home/alf/src/Nuklear/src/nuklear.h:4400:8
(struct nk_keyboard
  (keys (:array (:struct nk_key) 30))
  (text (:array :char 16))
  (text_len :int))

;; /home/alf/src/Nuklear/src/nuklear.h:4406:8
(struct nk_input
  (keyboard (:struct nk_keyboard))
  (mouse (:struct nk_mouse)))

;; /home/alf/src/Nuklear/src/nuklear.h:4411:12
(function "nk_input_has_mouse_click" (((:pointer (:struct nk_input))) ((:enum nk_buttons))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4412:12
(function "nk_input_has_mouse_click_in_rect" (((:pointer (:struct nk_input))) ((:enum nk_buttons)) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4413:12
(function "nk_input_has_mouse_click_down_in_rect" (((:pointer (:struct nk_input))) ((:enum nk_buttons)) ((:struct nk_rect)) (down :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4414:12
(function "nk_input_is_mouse_click_in_rect" (((:pointer (:struct nk_input))) ((:enum nk_buttons)) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4415:12
(function "nk_input_is_mouse_click_down_in_rect" ((i (:pointer (:struct nk_input))) (id (:enum nk_buttons)) (b (:struct nk_rect)) (down :int)) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4416:12
(function "nk_input_any_mouse_click_in_rect" (((:pointer (:struct nk_input))) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4417:12
(function "nk_input_is_mouse_prev_hovering_rect" (((:pointer (:struct nk_input))) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4418:12
(function "nk_input_is_mouse_hovering_rect" (((:pointer (:struct nk_input))) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4419:12
(function "nk_input_mouse_clicked" (((:pointer (:struct nk_input))) ((:enum nk_buttons)) ((:struct nk_rect))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4420:12
(function "nk_input_is_mouse_down" (((:pointer (:struct nk_input))) ((:enum nk_buttons))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4421:12
(function "nk_input_is_mouse_pressed" (((:pointer (:struct nk_input))) ((:enum nk_buttons))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4422:12
(function "nk_input_is_mouse_released" (((:pointer (:struct nk_input))) ((:enum nk_buttons))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4423:12
(function "nk_input_is_key_pressed" (((:pointer (:struct nk_input))) ((:enum nk_keys))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4424:12
(function "nk_input_is_key_released" (((:pointer (:struct nk_input))) ((:enum nk_keys))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4425:12
(function "nk_input_is_key_down" (((:pointer (:struct nk_input))) ((:enum nk_keys))) :int)

;; /home/alf/src/Nuklear/src/nuklear.h:4449:19
(typedef nk_draw_index nk_ushort)

;; /home/alf/src/Nuklear/src/nuklear.h:4451:6
(enum nk_draw_list_stroke
    (NK_STROKE_OPEN 0)
    (NK_STROKE_CLOSED 1))

;; /home/alf/src/Nuklear/src/nuklear.h:4458:6
(enum nk_draw_vertex_layout_attribute
    (NK_VERTEX_POSITION 0)
    (NK_VERTEX_COLOR 1)
    (NK_VERTEX_TEXCOORD 2)
    (NK_VERTEX_ATTRIBUTE_COUNT 3))

;; /home/alf/src/Nuklear/src/nuklear.h:4465:6
(enum nk_draw_vertex_layout_format
    (NK_FORMAT_SCHAR 0)
    (NK_FORMAT_SSHORT 1)
    (NK_FORMAT_SINT 2)
    (NK_FORMAT_UCHAR 3)
    (NK_FORMAT_USHORT 4)
    (NK_FORMAT_UINT 5)
    (NK_FORMAT_FLOAT 6)
    (NK_FORMAT_DOUBLE 7)
    (NK_FORMAT_COLOR_BEGIN 8)
    (NK_FORMAT_R8G8B8 8)
    (NK_FORMAT_R16G15B16 9)
    (NK_FORMAT_R32G32B32 10)
    (NK_FORMAT_R8G8B8A8 11)
    (NK_FORMAT_B8G8R8A8 12)
    (NK_FORMAT_R16G15B16A16 13)
    (NK_FORMAT_R32G32B32A32 14)
    (NK_FORMAT_R32G32B32A32_FLOAT 15)
    (NK_FORMAT_R32G32B32A32_DOUBLE 16)
    (NK_FORMAT_RGB32 17)
    (NK_FORMAT_RGBA32 18)
    (NK_FORMAT_COLOR_END 18)
    (NK_FORMAT_COUNT 19))

;; /home/alf/src/Nuklear/src/nuklear.h:4494:8
(struct nk_draw_vertex_layout_element
  (attribute (:enum nk_draw_vertex_layout_attribute))
  (format (:enum nk_draw_vertex_layout_format))
  (offset nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:4500:8
(struct nk_draw_command
  (elem_count :unsigned-int)
  (clip_rect (:struct nk_rect))
  (texture nk_handle))

;; /home/alf/src/Nuklear/src/nuklear.h:4512:8
(struct nk_draw_list
  (clip_rect (:struct nk_rect))
  (circle_vtx (:array (:struct nk_vec2) 12))
  (config (:struct nk_convert_config))
  (buffer (:pointer (:struct nk_buffer)))
  (vertices (:pointer (:struct nk_buffer)))
  (elements (:pointer (:struct nk_buffer)))
  (element_count :unsigned-int)
  (vertex_count :unsigned-int)
  (cmd_count :unsigned-int)
  (cmd_offset nk_size)
  (path_count :unsigned-int)
  (path_offset :unsigned-int)
  (line_AA (:enum nk_anti_aliasing))
  (shape_AA (:enum nk_anti_aliasing)))

;; /home/alf/src/Nuklear/src/nuklear.h:4538:13
(function "nk_draw_list_init" (((:pointer (:struct nk_draw_list)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4539:13
(function "nk_draw_list_setup" (((:pointer (:struct nk_draw_list))) ((:pointer (:struct nk_convert_config))) (cmds (:pointer (:struct nk_buffer))) (vertices (:pointer (:struct nk_buffer))) (elements (:pointer (:struct nk_buffer))) (line_aa (:enum nk_anti_aliasing)) (shape_aa (:enum nk_anti_aliasing))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4543:38
(function "nk__draw_list_begin" (((:pointer (:struct nk_draw_list))) ((:pointer (:struct nk_buffer)))) (:pointer (:struct nk_draw_command)))

;; /home/alf/src/Nuklear/src/nuklear.h:4544:38
(function "nk__draw_list_next" (((:pointer (:struct nk_draw_command))) ((:pointer (:struct nk_buffer))) ((:pointer (:struct nk_draw_list)))) (:pointer (:struct nk_draw_command)))

;; /home/alf/src/Nuklear/src/nuklear.h:4545:38
(function "nk__draw_list_end" (((:pointer (:struct nk_draw_list))) ((:pointer (:struct nk_buffer)))) (:pointer (:struct nk_draw_command)))

;; /home/alf/src/Nuklear/src/nuklear.h:4548:13
(function "nk_draw_list_path_clear" (((:pointer (:struct nk_draw_list)))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4549:13
(function "nk_draw_list_path_line_to" (((:pointer (:struct nk_draw_list))) (pos (:struct nk_vec2))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4550:13
(function "nk_draw_list_path_arc_to_fast" (((:pointer (:struct nk_draw_list))) (center (:struct nk_vec2)) (radius :float) (a_min :int) (a_max :int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4551:13
(function "nk_draw_list_path_arc_to" (((:pointer (:struct nk_draw_list))) (center (:struct nk_vec2)) (radius :float) (a_min :float) (a_max :float) (segments :unsigned-int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4552:13
(function "nk_draw_list_path_rect_to" (((:pointer (:struct nk_draw_list))) (a (:struct nk_vec2)) (b (:struct nk_vec2)) (rounding :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4553:13
(function "nk_draw_list_path_curve_to" (((:pointer (:struct nk_draw_list))) (p2 (:struct nk_vec2)) (p3 (:struct nk_vec2)) (p4 (:struct nk_vec2)) (num_segments :unsigned-int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4554:13
(function "nk_draw_list_path_fill" (((:pointer (:struct nk_draw_list))) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4555:13
(function "nk_draw_list_path_stroke" (((:pointer (:struct nk_draw_list))) ((:struct nk_color)) (closed (:enum nk_draw_list_stroke)) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4558:13
(function "nk_draw_list_stroke_line" (((:pointer (:struct nk_draw_list))) (a (:struct nk_vec2)) (b (:struct nk_vec2)) ((:struct nk_color)) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4559:13
(function "nk_draw_list_stroke_rect" (((:pointer (:struct nk_draw_list))) (rect (:struct nk_rect)) ((:struct nk_color)) (rounding :float) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4560:13
(function "nk_draw_list_stroke_triangle" (((:pointer (:struct nk_draw_list))) (a (:struct nk_vec2)) (b (:struct nk_vec2)) (c (:struct nk_vec2)) ((:struct nk_color)) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4561:13
(function "nk_draw_list_stroke_circle" (((:pointer (:struct nk_draw_list))) (center (:struct nk_vec2)) (radius :float) ((:struct nk_color)) (segs :unsigned-int) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4562:13
(function "nk_draw_list_stroke_curve" (((:pointer (:struct nk_draw_list))) (p0 (:struct nk_vec2)) (cp0 (:struct nk_vec2)) (cp1 (:struct nk_vec2)) (p1 (:struct nk_vec2)) ((:struct nk_color)) (segments :unsigned-int) (thickness :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4563:13
(function "nk_draw_list_stroke_poly_line" (((:pointer (:struct nk_draw_list))) (pnts (:pointer (:struct nk_vec2))) (cnt :unsigned-int) ((:struct nk_color)) ((:enum nk_draw_list_stroke)) (thickness :float) ((:enum nk_anti_aliasing))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4566:13
(function "nk_draw_list_fill_rect" (((:pointer (:struct nk_draw_list))) (rect (:struct nk_rect)) ((:struct nk_color)) (rounding :float)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4567:13
(function "nk_draw_list_fill_rect_multi_color" (((:pointer (:struct nk_draw_list))) (rect (:struct nk_rect)) (left (:struct nk_color)) (top (:struct nk_color)) (right (:struct nk_color)) (bottom (:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4568:13
(function "nk_draw_list_fill_triangle" (((:pointer (:struct nk_draw_list))) (a (:struct nk_vec2)) (b (:struct nk_vec2)) (c (:struct nk_vec2)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4569:13
(function "nk_draw_list_fill_circle" (((:pointer (:struct nk_draw_list))) (center (:struct nk_vec2)) (radius :float) (col (:struct nk_color)) (segs :unsigned-int)) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4570:13
(function "nk_draw_list_fill_poly_convex" (((:pointer (:struct nk_draw_list))) (points (:pointer (:struct nk_vec2))) (count :unsigned-int) ((:struct nk_color)) ((:enum nk_anti_aliasing))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4573:13
(function "nk_draw_list_add_image" (((:pointer (:struct nk_draw_list))) (texture (:struct nk_image)) (rect (:struct nk_rect)) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4574:13
(function "nk_draw_list_add_text" (((:pointer (:struct nk_draw_list))) ((:pointer (:struct nk_user_font))) ((:struct nk_rect)) (text (:pointer :char)) (len :int) (font_height :float) ((:struct nk_color))) :void)

;; /home/alf/src/Nuklear/src/nuklear.h:4586:6
(enum nk_style_item_type
    (NK_STYLE_ITEM_COLOR 0)
    (NK_STYLE_ITEM_IMAGE 1))

;; /home/alf/src/Nuklear/src/nuklear.h:4591:7
(union nk_style_item_data
  (image (:struct nk_image))
  (color (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:4596:8
(struct nk_style_item
  (type (:enum nk_style_item_type))
  (data (:union nk_style_item_data)))

;; /home/alf/src/Nuklear/src/nuklear.h:4601:8
(struct nk_style_text
  (color (:struct nk_color))
  (padding (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4606:8
(struct nk_style_button
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (text_background (:struct nk_color))
  (text_normal (:struct nk_color))
  (text_hover (:struct nk_color))
  (text_active (:struct nk_color))
  (text_alignment nk_flags)
  (border :float)
  (rounding :float)
  (padding (:struct nk_vec2))
  (image_padding (:struct nk_vec2))
  (touch_padding (:struct nk_vec2))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4633:8
(struct nk_style_toggle
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (cursor_normal (:struct nk_style_item))
  (cursor_hover (:struct nk_style_item))
  (text_normal (:struct nk_color))
  (text_hover (:struct nk_color))
  (text_active (:struct nk_color))
  (text_background (:struct nk_color))
  (text_alignment nk_flags)
  (padding (:struct nk_vec2))
  (touch_padding (:struct nk_vec2))
  (spacing :float)
  (border :float)
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4663:8
(struct nk_style_selectable
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (pressed (:struct nk_style_item))
  (normal_active (:struct nk_style_item))
  (hover_active (:struct nk_style_item))
  (pressed_active (:struct nk_style_item))
  (text_normal (:struct nk_color))
  (text_hover (:struct nk_color))
  (text_pressed (:struct nk_color))
  (text_normal_active (:struct nk_color))
  (text_hover_active (:struct nk_color))
  (text_pressed_active (:struct nk_color))
  (text_background (:struct nk_color))
  (text_alignment nk_flags)
  (rounding :float)
  (padding (:struct nk_vec2))
  (touch_padding (:struct nk_vec2))
  (image_padding (:struct nk_vec2))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4698:8
(struct nk_style_slider
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (bar_normal (:struct nk_color))
  (bar_hover (:struct nk_color))
  (bar_active (:struct nk_color))
  (bar_filled (:struct nk_color))
  (cursor_normal (:struct nk_style_item))
  (cursor_hover (:struct nk_style_item))
  (cursor_active (:struct nk_style_item))
  (border :float)
  (rounding :float)
  (bar_height :float)
  (padding (:struct nk_vec2))
  (spacing (:struct nk_vec2))
  (cursor_size (:struct nk_vec2))
  (show_buttons :int)
  (inc_button (:struct nk_style_button))
  (dec_button (:struct nk_style_button))
  (inc_symbol (:enum nk_symbol_type))
  (dec_symbol (:enum nk_symbol_type))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4737:8
(struct nk_style_progress
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (cursor_normal (:struct nk_style_item))
  (cursor_hover (:struct nk_style_item))
  (cursor_active (:struct nk_style_item))
  (cursor_border_color (:struct nk_color))
  (rounding :float)
  (border :float)
  (cursor_border :float)
  (cursor_rounding :float)
  (padding (:struct nk_vec2))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4763:8
(struct nk_style_scrollbar
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (cursor_normal (:struct nk_style_item))
  (cursor_hover (:struct nk_style_item))
  (cursor_active (:struct nk_style_item))
  (cursor_border_color (:struct nk_color))
  (border :float)
  (rounding :float)
  (border_cursor :float)
  (rounding_cursor :float)
  (padding (:struct nk_vec2))
  (show_buttons :int)
  (inc_button (:struct nk_style_button))
  (dec_button (:struct nk_style_button))
  (inc_symbol (:enum nk_symbol_type))
  (dec_symbol (:enum nk_symbol_type))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4796:8
(struct nk_style_edit
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (scrollbar (:struct nk_style_scrollbar))
  (cursor_normal (:struct nk_color))
  (cursor_hover (:struct nk_color))
  (cursor_text_normal (:struct nk_color))
  (cursor_text_hover (:struct nk_color))
  (text_normal (:struct nk_color))
  (text_hover (:struct nk_color))
  (text_active (:struct nk_color))
  (selected_normal (:struct nk_color))
  (selected_hover (:struct nk_color))
  (selected_text_normal (:struct nk_color))
  (selected_text_hover (:struct nk_color))
  (border :float)
  (rounding :float)
  (cursor_size :float)
  (scrollbar_size (:struct nk_vec2))
  (padding (:struct nk_vec2))
  (row_padding :float))

;; /home/alf/src/Nuklear/src/nuklear.h:4830:8
(struct nk_style_property
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (label_normal (:struct nk_color))
  (label_hover (:struct nk_color))
  (label_active (:struct nk_color))
  (sym_left (:enum nk_symbol_type))
  (sym_right (:enum nk_symbol_type))
  (border :float)
  (rounding :float)
  (padding (:struct nk_vec2))
  (edit (:struct nk_style_edit))
  (inc_button (:struct nk_style_button))
  (dec_button (:struct nk_style_button))
  (userdata nk_handle)
  (draw_begin :function-pointer)
  (draw_end :function-pointer))

;; /home/alf/src/Nuklear/src/nuklear.h:4861:8
(struct nk_style_chart
  (background (:struct nk_style_item))
  (border_color (:struct nk_color))
  (selected_color (:struct nk_color))
  (color (:struct nk_color))
  (border :float)
  (rounding :float)
  (padding (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4874:8
(struct nk_style_combo
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (border_color (:struct nk_color))
  (label_normal (:struct nk_color))
  (label_hover (:struct nk_color))
  (label_active (:struct nk_color))
  (symbol_normal (:struct nk_color))
  (symbol_hover (:struct nk_color))
  (symbol_active (:struct nk_color))
  (button (:struct nk_style_button))
  (sym_normal (:enum nk_symbol_type))
  (sym_hover (:enum nk_symbol_type))
  (sym_active (:enum nk_symbol_type))
  (border :float)
  (rounding :float)
  (content_padding (:struct nk_vec2))
  (button_padding (:struct nk_vec2))
  (spacing (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4905:8
(struct nk_style_tab
  (background (:struct nk_style_item))
  (border_color (:struct nk_color))
  (text (:struct nk_color))
  (tab_maximize_button (:struct nk_style_button))
  (tab_minimize_button (:struct nk_style_button))
  (node_maximize_button (:struct nk_style_button))
  (node_minimize_button (:struct nk_style_button))
  (sym_minimize (:enum nk_symbol_type))
  (sym_maximize (:enum nk_symbol_type))
  (border :float)
  (rounding :float)
  (indent :float)
  (padding (:struct nk_vec2))
  (spacing (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4927:6
(enum nk_style_header_align
    (NK_HEADER_LEFT 0)
    (NK_HEADER_RIGHT 1))

;; /home/alf/src/Nuklear/src/nuklear.h:4931:8
(struct nk_style_window_header
  (normal (:struct nk_style_item))
  (hover (:struct nk_style_item))
  (active (:struct nk_style_item))
  (close_button (:struct nk_style_button))
  (minimize_button (:struct nk_style_button))
  (close_symbol (:enum nk_symbol_type))
  (minimize_symbol (:enum nk_symbol_type))
  (maximize_symbol (:enum nk_symbol_type))
  (label_normal (:struct nk_color))
  (label_hover (:struct nk_color))
  (label_active (:struct nk_color))
  (align (:enum nk_style_header_align))
  (padding (:struct nk_vec2))
  (label_padding (:struct nk_vec2))
  (spacing (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4956:8
(struct nk_style_window
  (header (:struct nk_style_window_header))
  (fixed_background (:struct nk_style_item))
  (background (:struct nk_color))
  (border_color (:struct nk_color))
  (popup_border_color (:struct nk_color))
  (combo_border_color (:struct nk_color))
  (contextual_border_color (:struct nk_color))
  (menu_border_color (:struct nk_color))
  (group_border_color (:struct nk_color))
  (tooltip_border_color (:struct nk_color))
  (scaler (:struct nk_style_item))
  (border :float)
  (combo_border :float)
  (contextual_border :float)
  (menu_border :float)
  (group_border :float)
  (tooltip_border :float)
  (popup_border :float)
  (min_row_height_padding :float)
  (rounding :float)
  (spacing (:struct nk_vec2))
  (scrollbar_size (:struct nk_vec2))
  (min_size (:struct nk_vec2))
  (padding (:struct nk_vec2))
  (group_padding (:struct nk_vec2))
  (popup_padding (:struct nk_vec2))
  (combo_padding (:struct nk_vec2))
  (contextual_padding (:struct nk_vec2))
  (menu_padding (:struct nk_vec2))
  (tooltip_padding (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:4993:8
(struct nk_style
  (font (:pointer (:struct nk_user_font)))
  (cursors (:array (:pointer (:struct nk_cursor)) 7))
  (cursor_active (:pointer (:struct nk_cursor)))
  (cursor_last (:pointer (:struct nk_cursor)))
  (cursor_visible :int)
  (text (:struct nk_style_text))
  (button (:struct nk_style_button))
  (contextual_button (:struct nk_style_button))
  (menu_button (:struct nk_style_button))
  (option (:struct nk_style_toggle))
  (checkbox (:struct nk_style_toggle))
  (selectable (:struct nk_style_selectable))
  (slider (:struct nk_style_slider))
  (progress (:struct nk_style_progress))
  (property (:struct nk_style_property))
  (edit (:struct nk_style_edit))
  (chart (:struct nk_style_chart))
  (scrollh (:struct nk_style_scrollbar))
  (scrollv (:struct nk_style_scrollbar))
  (tab (:struct nk_style_tab))
  (combo (:struct nk_style_combo))
  (window (:struct nk_style_window)))

;; /home/alf/src/Nuklear/src/nuklear.h:5019:29
(function "nk_style_item_image" ((img (:struct nk_image))) (:struct nk_style_item))

;; /home/alf/src/Nuklear/src/nuklear.h:5020:29
(function "nk_style_item_color" (((:struct nk_color))) (:struct nk_style_item))

;; /home/alf/src/Nuklear/src/nuklear.h:5021:29
(function "nk_style_item_hide" () (:struct nk_style_item))

;; /home/alf/src/Nuklear/src/nuklear.h:5033:6
(enum nk_panel_type
    (NK_PANEL_NONE 0)
    (NK_PANEL_WINDOW 1)
    (NK_PANEL_GROUP 2)
    (NK_PANEL_POPUP 4)
    (NK_PANEL_CONTEXTUAL 16)
    (NK_PANEL_COMBO 32)
    (NK_PANEL_MENU 64)
    (NK_PANEL_TOOLTIP 128))

;; /home/alf/src/Nuklear/src/nuklear.h:5043:6
(enum nk_panel_set
    (NK_PANEL_SET_NONBLOCK 240)
    (NK_PANEL_SET_POPUP 244)
    (NK_PANEL_SET_SUB 246))

;; /home/alf/src/Nuklear/src/nuklear.h:5049:8
(struct nk_chart_slot
  (type (:enum nk_chart_type))
  (color (:struct nk_color))
  (highlight (:struct nk_color))
  (min :float)
  (max :float)
  (range :float)
  (count :int)
  (last (:struct nk_vec2))
  (index :int))

;; /home/alf/src/Nuklear/src/nuklear.h:5059:8
(struct nk_chart
  (slot :int)
  (x :float)
  (y :float)
  (w :float)
  (h :float)
  (slots (:array (:struct nk_chart_slot) 4)))

;; /home/alf/src/Nuklear/src/nuklear.h:5065:6
(enum nk_panel_row_layout_type
    (NK_LAYOUT_DYNAMIC_FIXED 0)
    (NK_LAYOUT_DYNAMIC_ROW 1)
    (NK_LAYOUT_DYNAMIC_FREE 2)
    (NK_LAYOUT_DYNAMIC 3)
    (NK_LAYOUT_STATIC_FIXED 4)
    (NK_LAYOUT_STATIC_ROW 5)
    (NK_LAYOUT_STATIC_FREE 6)
    (NK_LAYOUT_STATIC 7)
    (NK_LAYOUT_TEMPLATE 8)
    (NK_LAYOUT_COUNT 9))

;; /home/alf/src/Nuklear/src/nuklear.h:5077:8
(struct nk_row_layout
  (type (:enum nk_panel_row_layout_type))
  (index :int)
  (height :float)
  (min_height :float)
  (columns :int)
  (ratio (:pointer :float))
  (item_width :float)
  (item_height :float)
  (item_offset :float)
  (filled :float)
  (item (:struct nk_rect))
  (tree_depth :int)
  (templates (:array :float 16)))

;; /home/alf/src/Nuklear/src/nuklear.h:5093:8
(struct nk_popup_buffer
  (begin nk_size)
  (parent nk_size)
  (last nk_size)
  (end nk_size)
  (active :int))

;; /home/alf/src/Nuklear/src/nuklear.h:5101:8
(struct nk_menu_state
  (x :float)
  (y :float)
  (w :float)
  (h :float)
  (offset (:struct nk_scroll)))

;; /home/alf/src/Nuklear/src/nuklear.h:5106:8
(struct nk_panel
  (type (:enum nk_panel_type))
  (flags nk_flags)
  (bounds (:struct nk_rect))
  (offset_x (:pointer nk_uint))
  (offset_y (:pointer nk_uint))
  (at_x :float)
  (at_y :float)
  (max_x :float)
  (footer_height :float)
  (header_height :float)
  (border :float)
  (has_scrolling :unsigned-int)
  (clip (:struct nk_rect))
  (menu (:struct nk_menu_state))
  (row (:struct nk_row_layout))
  (chart (:struct nk_chart))
  (buffer (:pointer (:struct nk_command_buffer)))
  (parent (:pointer (:struct nk_panel))))

;; /home/alf/src/Nuklear/src/nuklear.h:5132:8
(struct nk_table
  )

;; /home/alf/src/Nuklear/src/nuklear.h:5133:6
(enum nk_window_flags
    (NK_WINDOW_PRIVATE 2048)
    (NK_WINDOW_DYNAMIC 2048)
    (NK_WINDOW_ROM 4096)
    (NK_WINDOW_NOT_INTERACTIVE 5120)
    (NK_WINDOW_HIDDEN 8192)
    (NK_WINDOW_CLOSED 16384)
    (NK_WINDOW_MINIMIZED 32768)
    (NK_WINDOW_REMOVE_ROM 65536))

;; /home/alf/src/Nuklear/src/nuklear.h:5151:8
(struct nk_popup_state
  (win (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:1311:15
(struct nk_window
      )))
  (type (:enum nk_panel_type))
  (buf (:struct nk_popup_buffer))
  (name nk_hash)
  (active :int)
  (combo_count :unsigned-int)
  (con_count :unsigned-int)
  (con_old :unsigned-int)
  (active_con :unsigned-int)
  (header (:struct nk_rect)))

;; /home/alf/src/Nuklear/src/nuklear.h:5163:8
(struct nk_edit_state
  (name nk_hash)
  (seq :unsigned-int)
  (old :unsigned-int)
  (active :int)
  (prev :int)
  (cursor :int)
  (sel_start :int)
  (sel_end :int)
  (scrollbar (:struct nk_scroll))
  (mode :unsigned-char)
  (single_line :unsigned-char))

;; /home/alf/src/Nuklear/src/nuklear.h:5176:8
(struct nk_property_state
  (active :int)
  (prev :int)
  (buffer (:array :char 64))
  (length :int)
  (cursor :int)
  (select_start :int)
  (select_end :int)
  (name nk_hash)
  (seq :unsigned-int)
  (old :unsigned-int)
  (state :int))

;; /home/alf/src/Nuklear/src/nuklear.h:5189:8
(struct nk_window
  (seq :unsigned-int)
  (name nk_hash)
  (name_string (:array :char 64))
  (flags nk_flags)
  (bounds (:struct nk_rect))
  (scrollbar (:struct nk_scroll))
  (buffer (:struct nk_command_buffer))
  (layout (:pointer (:struct nk_panel)))
  (scrollbar_hiding_timer :float)
  (property (:struct nk_property_state))
  (popup (:struct nk_popup_state))
  (edit (:struct nk_edit_state))
  (scrolled :unsigned-int)
  (tables (:pointer ;; /home/alf/src/Nuklear/src/nuklear.h:5132:8
(struct nk_table
      )))
  (table_count :unsigned-int)
  (next (:pointer (:struct nk_window)))
  (prev (:pointer (:struct nk_window)))
  (parent (:pointer (:struct nk_window))))

;; /home/alf/src/Nuklear/src/nuklear.h:5281:1 <Spelling=<scratch space>:25:1>
(struct nk_config_stack_style_item_element
  (address (:pointer (:struct nk_style_item)))
  (old_value (:struct nk_style_item)))

;; /home/alf/src/Nuklear/src/nuklear.h:5282:1 <Spelling=<scratch space>:31:1>
(struct nk_config_stack_float_element
  (address (:pointer :float))
  (old_value :float))

;; /home/alf/src/Nuklear/src/nuklear.h:5283:1 <Spelling=<scratch space>:37:1>
(struct nk_config_stack_vec2_element
  (address (:pointer (:struct nk_vec2)))
  (old_value (:struct nk_vec2)))

;; /home/alf/src/Nuklear/src/nuklear.h:5284:1 <Spelling=<scratch space>:43:1>
(struct nk_config_stack_flags_element
  (address (:pointer nk_flags))
  (old_value nk_flags))

;; /home/alf/src/Nuklear/src/nuklear.h:5285:1 <Spelling=<scratch space>:49:1>
(struct nk_config_stack_color_element
  (address (:pointer (:struct nk_color)))
  (old_value (:struct nk_color)))

;; /home/alf/src/Nuklear/src/nuklear.h:5286:1 <Spelling=<scratch space>:55:1>
(struct nk_config_stack_user_font_element
  (address (:pointer (:pointer (:struct nk_user_font))))
  (old_value (:pointer (:struct nk_user_font))))

;; /home/alf/src/Nuklear/src/nuklear.h:5287:1 <Spelling=<scratch space>:61:1>
(struct nk_config_stack_button_behavior_element
  (address (:pointer (:enum nk_button_behavior)))
  (old_value (:enum nk_button_behavior)))

;; /home/alf/src/Nuklear/src/nuklear.h:5289:1 <Spelling=<scratch space>:66:1>
(struct nk_config_stack_style_item
  (head :int)
  (elements (:array (:struct nk_config_stack_style_item_element) 16)))

;; /home/alf/src/Nuklear/src/nuklear.h:5290:1 <Spelling=<scratch space>:69:1>
(struct nk_config_stack_float
  (head :int)
  (elements (:array (:struct nk_config_stack_float_element) 32)))

;; /home/alf/src/Nuklear/src/nuklear.h:5291:1 <Spelling=<scratch space>:72:1>
(struct nk_config_stack_vec2
  (head :int)
  (elements (:array (:struct nk_config_stack_vec2_element) 16)))

;; /home/alf/src/Nuklear/src/nuklear.h:5292:1 <Spelling=<scratch space>:75:1>
(struct nk_config_stack_flags
  (head :int)
  (elements (:array (:struct nk_config_stack_flags_element) 32)))

;; /home/alf/src/Nuklear/src/nuklear.h:5293:1 <Spelling=<scratch space>:78:1>
(struct nk_config_stack_color
  (head :int)
  (elements (:array (:struct nk_config_stack_color_element) 32)))

;; /home/alf/src/Nuklear/src/nuklear.h:5294:1 <Spelling=<scratch space>:81:1>
(struct nk_config_stack_user_font
  (head :int)
  (elements (:array (:struct nk_config_stack_user_font_element) 8)))

;; /home/alf/src/Nuklear/src/nuklear.h:5295:1 <Spelling=<scratch space>:84:1>
(struct nk_config_stack_button_behavior
  (head :int)
  (elements (:array (:struct nk_config_stack_button_behavior_element) 8)))

;; /home/alf/src/Nuklear/src/nuklear.h:5297:8
(struct nk_configuration_stacks
  (style_items (:struct nk_config_stack_style_item))
  (floats (:struct nk_config_stack_float))
  (vectors (:struct nk_config_stack_vec2))
  (flags (:struct nk_config_stack_flags))
  (colors (:struct nk_config_stack_color))
  (fonts (:struct nk_config_stack_user_font))
  (button_behaviors (:struct nk_config_stack_button_behavior)))

;; /home/alf/src/Nuklear/src/nuklear.h:5313:8
(struct nk_table
  (seq :unsigned-int)
  (size :unsigned-int)
  (keys (:array nk_hash 59))
  (values (:array nk_uint 59))
  (next (:pointer (:struct nk_table)))
  (prev (:pointer (:struct nk_table))))

;; /home/alf/src/Nuklear/src/nuklear.h:5321:7
(union nk_page_data
  (tbl (:struct nk_table))
  (pan (:struct nk_panel))
  (win (:struct nk_window)))

;; /home/alf/src/Nuklear/src/nuklear.h:5327:8
(struct nk_page_element
  (data (:union nk_page_data))
  (next (:pointer (:struct nk_page_element)))
  (prev (:pointer (:struct nk_page_element))))

;; /home/alf/src/Nuklear/src/nuklear.h:5333:8
(struct nk_page
  (size :unsigned-int)
  (next (:pointer (:struct nk_page)))
  (win (:array (:struct nk_page_element) 1)))

;; /home/alf/src/Nuklear/src/nuklear.h:5339:8
(struct nk_pool
  (alloc (:struct nk_allocator))
  (type (:enum nk_allocation_type))
  (page_count :unsigned-int)
  (pages (:pointer (:struct nk_page)))
  (freelist (:pointer (:struct nk_page_element)))
  (capacity :unsigned-int)
  (size nk_size)
  (cap nk_size))

;; /home/alf/src/Nuklear/src/nuklear.h:5350:8
(struct nk_context
  (input (:struct nk_input))
  (style (:struct nk_style))
  (memory (:struct nk_buffer))
  (clip (:struct nk_clipboard))
  (last_widget_state nk_flags)
  (button_behavior (:enum nk_button_behavior))
  (stacks (:struct nk_configuration_stacks))
  (delta_time_seconds :float)
  (draw_list (:struct nk_draw_list))
  (text_edit (:struct nk_text_edit))
  (overlay (:struct nk_command_buffer))
  (build :int)
  (use_pool :int)
  (pool (:struct nk_pool))
  (begin (:pointer (:struct nk_window)))
  (end (:pointer (:struct nk_window)))
  (active (:pointer (:struct nk_window)))
  (current (:pointer (:struct nk_window)))
  (freelist (:pointer (:struct nk_page_element)))
  (count :unsigned-int)
  (seq :unsigned-int))
