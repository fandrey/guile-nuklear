#include <assert.h>
#include <libguile.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_MEMSET memset
#define NK_SDL_GLES2_IMPLEMENTATION

#include "nuklear.h"
#include "nuklear_sdl_gles2.h"

SCM nk_sdl_init_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_sdl_init((void *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_sdl_handle_event_wrapper(SCM arg_1)
{
  return scm_from_int(nk_sdl_handle_event((void *)scm_to_pointer(arg_1)));
}

SCM nk_sdl_render_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_sdl_render(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_sdl_shutdown_wrapper()
{
  nk_sdl_shutdown();
  return SCM_UNSPECIFIED;
}

SCM nk_sdl_device_destroy_wrapper()
{
  nk_sdl_device_destroy();
  return SCM_UNSPECIFIED;
}

SCM nk_sdl_device_create_wrapper()
{
  nk_sdl_device_create();
  return SCM_UNSPECIFIED;
}

SCM nk_sdl_init_font(SCM fonts)
{
  SCM f, lst = scm_list_n(SCM_UNDEFINED);
  struct nk_font_atlas *atlas;
  nk_sdl_font_stash_begin(&atlas);

  for(f = fonts; !scm_is_null(f); f = scm_cdr(f))
  {
    char * s = scm_to_locale_string(scm_caar(f));
    struct nk_font_config * cfg =
      scm_is_null(scm_cddar(f)) ? NULL : scm_to_pointer(scm_caddar(f));

    lst = scm_cons(scm_from_pointer
                   (nk_font_atlas_add_from_file(atlas, s,
                                                scm_to_int(scm_cadar(f)), cfg),
                    NULL),
                   lst);
    free(s);
  }

  nk_sdl_font_stash_end();
  return scm_reverse_x(lst, SCM_EOL);
}

SCM nk_sdl_set_font(SCM ctx, SCM font)
{
  nk_style_set_font((struct nk_context *)scm_to_pointer(ctx),
                    &((struct nk_font *)(scm_to_pointer(font)))->handle);

  return SCM_UNSPECIFIED;
}

void init_guile_nuklear_sdl_gles2()
{
  scm_c_define_gsubr("nk_sdl_init", 1, 0, 0, nk_sdl_init_wrapper);
  scm_c_define_gsubr("nk_sdl_handle_event", 1, 0, 0, nk_sdl_handle_event_wrapper);
  scm_c_define_gsubr("nk_sdl_render", 3, 0, 0, nk_sdl_render_wrapper);
  scm_c_define_gsubr("nk_sdl_shutdown", 0, 0, 0, nk_sdl_shutdown_wrapper);
  scm_c_define_gsubr("nk_sdl_device_destroy", 0, 0, 0, nk_sdl_device_destroy_wrapper);
  scm_c_define_gsubr("nk_sdl_device_create", 0, 0, 0, nk_sdl_device_create_wrapper);
  scm_c_define_gsubr("nk_sdl_init_font", 1, 0, 0, nk_sdl_init_font);
  scm_c_define_gsubr("nk_sdl_set_font", 2, 0, 0, nk_sdl_set_font);
}
