(define-module (nuklear-sdl-gles2 bindings)
  #:use-module (system foreign)
  #:use-module (srfi srfi-1)
  #:use-module (nuklear-sdl-gles2 config))

(load-extension %libguile-nuklear-sdl-gles2 "init_guile_nuklear_sdl_gles2")

(export nk_sdl_init nk_sdl_handle_event nk_sdl_render nk_sdl_shutdown nk_sdl_device_destroy nk_sdl_device_create nk_sdl_init_font nk_sdl_set_font)
