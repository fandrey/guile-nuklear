#!/bin/sh
# -*- Scheme -*-
exec guile "$0" "$@"
!#
;;; Copyright (C) 2019 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-Nuklear.
;;;
;;; Guile-Nuklear is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-Nuklear is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-Nuklear.  If not, see
;;; <https://www.gnu.org/licenses/>.

(use-modules (ice-9 format)
             (ice-9 match)
             (ice-9 getopt-long)
             (srfi srfi-1)
             (rnrs bytevectors)
             (system foreign))

(define (call-with-output-files filenames-and-options proc)
  (let ((ps (map (lambda (file)
                   (let ((file (if (pair? file) file (list file))))
                     (apply open-output-file file)))
                 filenames-and-options)))
    (call-with-values
        (lambda () (apply proc ps))
      (lambda vals
        (map close-output-port ps)
        (apply values vals)))))

(define* (mapa-b proc a b #:optional (step 1))
  "Apply PROC to the range [A; B] with the optional STEP."
  (let ((end? (if (< step 0)
                  (lambda (n) (< n b))
                  (lambda (n) (> n b)))))
    (letrec ((r (lambda (l n)
                  (if (end? n)
                      (reverse l)
                      (r (cons (proc n) l) (+ n step))))))
      (r (list) a))))

(define* (map0-n proc n #:optional (step 1))
  "Apply PROC to the range [0; N] with the optional STEP."
  (mapa-b proc 0 n step))

(define* (map1-n proc n #:optional (step 1))
  "Apply PROC to the range [1; N] with the optional STEP."
  (mapa-b proc 1 n step))

(define (find-default pred lst default)
  (let ((v (find pred lst)))
    (if v v default)))

(define (ensure-directory-exists path)
  (catch 'system-error
         (lambda () (stat path))
         (lambda (key . args) (mkdir path))))

(define (nk-type? type)
  (string=? "nk_" (substring (symbol->string type) 0 3)))

(define types-alist
  `((:char "char" int8)
    (:signed-char "schar" int8)
    (:unsigned-char "uchar" uint8)
    (:short "short" short)
    (:unsigned-short "ushort" unsigned-short)
    (:int "int" int)
    (:unsigned-int "uint" unsigned-int)
    (:long "long" long)
    (:unsigned-long "ulong" unsigned-long)
    (:double "double" double)
    (:float "double" float)
    ,@(map (lambda (s)
             (list (string->symbol (string-join (list s "_t") ""))
                   s (string->symbol s)))
           (list "int8" "uint8"
                 "int16" "uint16"
                 "int32" "uint32"
                 "int64" "uint64"))))

(define (conversion-type type)
  (let ((a (assq-ref types-alist type)))
    (if a (car a) a)))

(define (scm-type type structs-alist)
  (let ((a (assq-ref types-alist type)))
    (if a
        (cadr a)
        (or (assq-ref structs-alist type) type))))

(define (make-union-type types structs-alist)
  `(reduce (lambda (a b)
             (if (> (sizeof a) (sizeof b)) a b))
           int
           (list ,@(map (lambda (type)
                          (match type
                            ((':pointer _) ''*)
                            ((':struct struct) (scm-type struct structs-alist))
                            (_ (scm-type type structs-alist))))
                        types))))

(define (make-typedef scmout typedef typedefs structs-alist)
  (if (nk-type? (cadr typedef))
      (begin
        (write
         (match typedef
           (('typedef name ':function-pointer)
            `(define-public ,name '*))
           (('typedef name (':array type num))
            `(define-public ,name (make-list ,num
                                             ,(scm-type type structs-alist))))
           (('typedef name ('union ':id _ (_ types) ..1))
            `(define-public ,name ,(make-union-type types structs-alist)))
           (('typedef name type)
            `(define-public ,name ,(scm-type type structs-alist))))
         scmout)
        (newline scmout)
        (cons (cdr typedef) typedefs))
      typedefs))

(define (rename-name name rename-alist)
  (let ((a (assoc-ref rename-alist name)))
    (if (pair? a) (car a) name)))

(define (make-struct-getter cout struct-name field rnm-funcs exc-defs typedefs)
  (match-let (((field-name field-type) field))
    (let* ((getter (rename-name (format #f "~a_~a" struct-name field-name)
                                  rnm-funcs))
           (field-value (format #f
                                "((struct ~a *)scm_to_pointer(ptr))->~a"
                                struct-name
                                field-name)))
      (if (member getter exc-defs string=)
          #f
          (begin
            (format cout
                    "SCM ~a(SCM ptr)~%{~%  return ~a;~%}~%"
                    getter
                    (match field-type
                      (((or ':struct ':union) _)
                       (scm<-c ':pointer
                               (string-append "&" field-value)
                               typedefs))
                      (_ (scm<-c field-type field-value typedefs))))
            (list getter getter (list ':pointer)))))))

(define (make-struct-setter cout struct-name field rnm-funcs exc-defs typedefs)
  (match-let (((field-name field-type) field))
    (let* ((setter (rename-name (format #f "~a_set_~a" struct-name field-name)
                                  rnm-funcs))
           (field-value (format #f
                                "((struct ~a *)scm_to_pointer(ptr))->~a"
                                struct-name
                                field-name)))
      (if (member setter exc-defs string=)
          #f
          (let* ((scm->c-field (scm->c field-type "value" typedefs))
                 (set-exp (match field-type
                           ((':array _ ...)
                            (format #f "nk_memcopy(~a, ~a, sizeof(~a))"
                                    field-value scm->c-field field-value))
                           (_ (format #f "~a = ~a"
                                      field-value scm->c-field)))))
            (format cout
                    "SCM ~a(SCM ptr, SCM value)~%~
                     {~%  ~a;~%  return SCM_UNSPECIFIED;~%}~%"
                    setter
                    set-exp)
            (list setter setter (list ':pointer field-type)))))))

(define (make-struct cout scmout struct
                     rnm-structs rnm-funcs exc-defs
                     typedefs functions structs-alist)
  (let* ((namestr (symbol->string (cadr struct)))
         (a (assoc-ref rnm-structs namestr))
         (name (string->symbol (if (pair? a)
                                   (car a)
                                   ;; Append a suffix to structs names
                                   ;; in Scheme.  This resolves name
                                   ;; conflicts, e.g. nk_style_button:
                                   ;; the nk_style_button struct and
                                   ;; the getter nk_style.button.
                                   (string-append namestr "_types"))))
         (fields (cddr struct)))
    (if (not (pair? fields))
        (list functions typedefs structs-alist)
        (begin
          (write
           `(define-public ,name
              (list ,@(map (lambda (field)
                             (match field
                               ((_ (or (':pointer _) ':function-pointer)) ''*)
                               ((_ (':array (':pointer _) num))
                                `(make-list ,num '*))
                               ((_ (':array (or (':struct type) type) num))
                                `(make-list ,num ,(scm-type type structs-alist)))
                               ((_ (':enum _)) 'int)
                               ((_ (or (':struct type) (':union type) type))
                                (scm-type type structs-alist))))
                           fields)))
           scmout)
          (newline scmout)
          (list
           (append functions
                   (filter
                    identity
                    (append-map (lambda (field)
                                  ;; Use the original struct name
                                  (list (make-struct-getter cout namestr field
                                                            rnm-funcs exc-defs
                                                            typedefs)
                                        (make-struct-setter cout namestr field
                                                            rnm-funcs exc-defs
                                                            typedefs)))
                                fields)))
           typedefs
           (acons (cadr struct) name structs-alist))))))

(define (make-union scmout union structs-alist)
  (match-let ((('union name (_ types) ..1) union))
    (write `(define ,name
              ,(make-union-type types structs-alist))
           scmout))
  (newline scmout))

(define (csym->scmstr sym)
  (list->string (map (lambda (c)
                       (if (char=? c #\_)
                           #\-
                           (char-downcase c)))
                     (string->list (if (symbol? sym)
                                       (symbol->string sym)
                                       sym)))))

(define (csym->scmsym sym)
  (string->symbol (csym->scmstr sym)))

(define (scmstr->cstr str)
  (string-map (lambda (c) (if (char=? c #\-) #\_ c)) str))

(define (make-enum scmout enum)
  (let ((name #f))
    (match-let (((or (_ name (? pair? pairs) ..1)
                     (_ _ _ (? pair? pairs) ..1)) enum))
      (for-each (lambda (e)
                  (write `(,(if name 'define 'define-public) ,@e) scmout)
                  (newline scmout))
                pairs)
      (when name
        (let* ((strs (map (lambda (p) (csym->scmstr (car p))) pairs))
               (len (cdr (fold (lambda (s p)
                                 (let ((l (string-prefix-length (car p) s)))
                                   (if (< l (cdr p))
                                       (cons (substring s 0 l) l)
                                       p)))
                               (cons (car strs)
                                     (string-length (car strs)))
                               (cdr strs))))
               (preflen (if (every (lambda (s) (< len (string-length s))) strs)
                            len
                            3)))
          (write
           `(define-public (,(string->symbol
                              (string-append (csym->scmstr name) "->integer"))
                            flags)
              (let ((flag->int (lambda (f)
                                 (case f
                                   ,@(map (lambda (s p)
                                            `((,(string->symbol
                                                 (substring s preflen)))
                                              ,(car p)))
                                          strs pairs)))))
                (cond
                 ((or (null? flags) (not flags)) 0)
                 ((symbol? flags) (flag->int flags))
                 (else (apply logior (map flag->int flags))))))
           scmout))
        (newline scmout)))))

(define scm<-c #f)
(define scm->c #f)

(let ((make-conversion
       (lambda (dir)
         (letrec
             ((fn
               (lambda (type arg nk-types)
                 (if (and (symbol? type) (nk-type? type)
                          (not (eq? type 'nk_handle)))
                     (fn (cadr (assoc type nk-types)) arg nk-types)
                     (let ((a (conversion-type type)))
                       (cond
                        (a (format #f "scm_~a_~a(~a)" dir a arg))
                        ((or (eq? type ':pointer)
                             (eq? type ':function-pointer))
                         (if (string=? dir "to")
                             (format #f "scm_to_pointer(~a)" arg)
                             (format #f
                                     "scm_from_pointer((void *)~a, NULL)" arg)))
                        ((eq? type 'nk_handle)
                         (if (string=? dir "to")
                             (format #f
                                     "nk_handle_ptr(scm_to_pointer(~a))" arg)
                             (format #f "scm_from_pointer(~a.ptr, NULL)" arg)))
                        ((pair? type)
                         (cond
                          ((eq? (car type) ':enum) (fn ':int arg nk-types))
                          ((eq? (car type) ':array)
                           (fn `(:pointer ,(cadr type)) arg nk-types))
                          ((eq? (car type) ':pointer)
                           (if (string=? dir "to")
                               (format #f "~ascm_to_pointer(~a)"
                                       (pointer-cast (cadr type)) arg)
                               (format #f "scm_from_pointer((void *)~a, NULL)"
                                       arg)))
                          ((or (eq? (car type) ':struct)
                               (eq? (car type) 'struct))
                           (format #f "*(struct ~a *)scm_to_pointer(~a)"
                                   (cadr type) arg))
                          ((or (eq? (car type) ':union)
                               (eq? (car type) 'union))
                           (format #f "*(union ~a *)scm_to_pointer(~a)"
                                   (cadr type) arg))
                          (else (format #f "scm_~a_CONVERT_ME_~a_(~a)"
                                        dir type arg))))
                        (else (format #f "scm_~a_~a(~a)" dir type arg))))))))
           fn))))
  (set! scm<-c (make-conversion "from"))
  (set! scm->c (make-conversion "to")))

(define (type->ctype sym)
  (let ((str (symbol->string sym)))
    (string-join (string-split (if (string-prefix? ":" str)
                                   (substring str 1)
                                   str)
                               #\-)
                 " ")))

(define (pointer-cast type)
  (apply format #f
         (if (not (pair? type))
             (list "(~a *)" (type->ctype type))
             (if (eq? (car type) ':pointer)
                 (if (not (pair? (cadr type)))
                     (list "(const ~a **)" (type->ctype (cadr type)))
                     (list "(~a ~a **)"
                           (type->ctype (caadr type)) (type->ctype (cadadr type))))
                 (list "(~a ~a *)"
                       (type->ctype (car type)) (type->ctype (cadr type)))))))

(define (args-string arg-types args typedefs)
  (string-join (map (lambda (type arg)
                      (scm->c type arg typedefs))
                    arg-types args)
               ", "))

(define (make-function cout func typedefs functions
                       include-definitions exclude-definitions
                       rename-definitions)
  (match-let ((('function name ((or (argtypes) (_ argtypes)) ...) retype)
               func))
    (let ((inc (find (lambda (i) (string= name (car i))) include-definitions)))
      (cond
       (inc
        (match inc
          ((name wrapper-name args code)
           (format cout code)
           (cons (list name wrapper-name args) functions))))
       ((member name exclude-definitions)
        functions)
       (else
        (match-let ((((targs args) ...)
                     (map1-n (lambda (n)
                               (let ((arg (format #f "arg_~d" n)))
                                 (list (string-join (list "SCM" arg)) arg)))
                             (length argtypes))))
          (format cout "SCM ~a_wrapper(~a)~%{~%"
                  name (string-join targs ", "))
          (cond
           ((eq? retype ':void)
            (format cout "  ~a(~a);~%  return SCM_UNSPECIFIED;~%}~%~%"
                    name
                    (args-string argtypes args typedefs)))
           ((and (pair? retype)
                 (or (eq? (car retype) ':struct)
                     (eq? (car retype) 'struct)))
            (format cout
                    "  struct ~a * s = scm_gc_malloc_pointerless~
              (sizeof(struct ~a), \"struct ~a\");~%  ~
              *s = ~a(~a);~%  return scm_from_pointer(s, NULL);~%}~%~%"
                    (cadr retype) (cadr retype) (cadr retype)
                    name
                    (args-string argtypes args typedefs)))
           (else
            (format cout "  return ~a;~%}~%~%"
                    (scm<-c retype
                            (format #f "~a(~a)"
                                    name (args-string argtypes args typedefs))
                            typedefs))))
          (cons (list name (string-append name "_wrapper") argtypes)
                functions)))))))

(define (include-definitions cout functions defs)
  (let incdef ((functions functions)
               (defs defs))
    (if (null? defs)
        functions
        (match-let (((name wrapper-name args code) (car defs)))
          (if (find (lambda (f) (string= name (car f))) functions)
              (incdef functions (cdr defs))
              (begin
                (format cout code)
                (incdef (cons (list name wrapper-name args) functions)
                        (cdr defs))))))))

(define (make-init cout extension-name functions)
  (format cout "void init_guile_~a()~%{~%" (scmstr->cstr extension-name))
  (for-each (lambda (func)
              (match func
                ((name wrapper-name args)
                 (format cout
                         "  scm_c_define_gsubr(\"~a\", ~d, 0, 0, ~a);~%"
                         name (length args) wrapper-name))))
            functions)
  (format cout "}~%"))

(define (export-functions scmout functions)
  (write `(export ,@(map (lambda (f) (string->symbol (car f))) functions))
         scmout)
  (newline scmout))

(define (call-with-extension extension proc)
  (let ((inc-defs (list))
        (exc-defs (list))
        (rnm-defs (list))
        (fnd (lambda (l) (find-default pair? l (list)))))
    (match extension
      ((':extension name
         (':sexp sexp-file)
         (':prologue (':c prologue-c)
                     (':scm prologue-scm))
         .
         (or ((':definitions (or (':include inc-defs ..1)
                                 (':exclude exc-defs ..1)
                                 (':rename  rnm-defs ..1)) ..1))
             ()))
       (let ((rnm-structs (list))
             (rnm-funcs (list)))
         (match (fnd rnm-defs)
           (((or (':struct rnm-structs) (':function rnm-funcs)) ...)
            (proc name sexp-file prologue-c prologue-scm
                  (fnd inc-defs) (fnd exc-defs)
                  (filter pair? rnm-structs) (filter pair? rnm-funcs)))))))))

(define (make-extension extension)
  (call-with-extension extension
    (lambda (name sexp-file prologue-c prologue-scm inc-defs exc-defs
                  rnm-structs rnm-funcs)
      (ensure-directory-exists name)
      (call-with-output-files (list (format #f "~a/guile-~a.c" name name)
                                    (format #f "~a/bindings.scm" name))
        (lambda (cout scmout)
          (letrec
              ((make-ext
                (lambda (input functions typedefs structs-alist)
                  (if (null? input)
                      (let ((funcs (reverse
                                    (include-definitions cout
                                                         functions
                                                         inc-defs))))
                        (make-init cout name funcs)
                        (export-functions scmout funcs))
                      (let ((obj (car input))
                            (l (list functions typedefs structs-alist)))
                        (apply make-ext
                               (cdr input)
                               (case (car obj)
                                 ((typedef)
                                  (list functions
                                        (make-typedef scmout obj
                                                      typedefs structs-alist)
                                        structs-alist))
                                 ((struct)
                                  (make-struct cout
                                               scmout
                                               obj
                                               rnm-structs
                                               rnm-funcs
                                               exc-defs
                                               typedefs
                                               functions
                                               structs-alist))
                                 ((union)
                                  (make-union scmout obj structs-alist) l)
                                 ((enum) (make-enum scmout obj) l)
                                 ((function)
                                  (list (make-function cout obj
                                                       typedefs functions
                                                       inc-defs exc-defs
                                                       rnm-funcs)
                                        typedefs
                                        structs-alist))
                                 (else l))))))))
            (format cout prologue-c)
            (format scmout prologue-scm)
            (make-ext (call-with-input-file sexp-file
                        (lambda (input)
                          (let read-sexps ((sexps (list)))
                            (let ((s (read input)))
                              (if (eof-object? s)
                                  (reverse sexps)
                                  (read-sexps (cons s sexps)))))))
                      (list) (list) (list)))))
      (call-with-output-file (format #f "~a/config.scm.in" name)
        (lambda (conf)
          (format conf
                  "(define-module (~a config)~%  ~
                     #:export (%libguile-~a))~%~%~
                   (define %libguile-~a \"@LIBGUILE_~a@\")~%"
                  name name name
                  (string-upcase (scmstr->cstr name))))))))

(define (make-extensions . file-names)
  (letrec ((read-input
            (lambda (input extensions filename)
              (let ((s (read input)))
                (if (eof-object? s)
                    (begin
                      (format #t "Warning: the extension file is empty: ~a"
                              (car filename))
                      extensions)
                    (cons s extensions)))))
           (read-inputs
            (lambda (extensions input-files)
              (if (null? input-files)
                  (reverse extensions)
                  (read-inputs (call-with-input-file (car input-files)
                                 (lambda (input)
                                   (read-input input
                                               extensions
                                               (car input-files))))
                               (cdr input-files))))))
    (for-each make-extension
              (read-inputs (list) file-names))))

(unless (resolve-module '(geiser) #f #:ensure #f)
  (if (null? (cdr (command-line)))
      (begin
        (format #t
                "Usage: ~a FILE...~%~
                 FILE is an extension description file.~%"
                (car (command-line)))
        (exit #t))
      (catch 'system-error
        (lambda ()
          (apply make-extensions (cdr (command-line))))
        (lambda (key . args)
          (apply format (current-error-port) (cons (cadr args) (caddr args)))
          (exit #f)))))
