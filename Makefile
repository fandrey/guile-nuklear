-include config.mk

ifndef GUILE_NUKLEAR_PREFIX
  GUILE_NUKLEAR_PREFIX = $(CURDIR)
endif

SCM = \
	nuklear.scm nuklear/bindings.scm nuklear/config.scm \
	nuklear-sdl-gles2.scm nuklear-sdl-gles2/bindings.scm nuklear-sdl-gles2/config.scm \
	nuklear-glfw-gl3.scm nuklear-glfw-gl3/bindings.scm nuklear-glfw-gl3/config.scm

all: $(SCM:%.scm=%.go) nuklear/libguile-nuklear.so nuklear-sdl-gles2/libguile-nuklear-sdl-gles2.so nuklear-glfw-gl3/libguile-nuklear-glfw-gl3.so

%.go: %.scm
	GUILE_LOAD_PATH=$(GUILE_LOAD_PATH):$(CURDIR) GUILE_LOAD_COMPILED_PATH=$(GUILE_LOAD_COMPILED_PATH) guild compile $(GUILE_WARNINGS) -o "$@" "$<"

nuklear.sexp: nuklear.c
	c2ffi -x c -D sexp -I $(NUKLEAR_SRC) -o $@ $<

nuklear-sdl-gles2.sexp: nuklear-sdl-gles2.c
	c2ffi -x c -D sexp -I $(NUKLEAR_SRC) -o temp.sexp $<
	tail -n +`grep -n nuklear-sdl-gles2.c temp.sexp | head -n 1 | cut -d : -f 1` temp.sexp >$@
	rm temp.sexp

nuklear/guile-nuklear.c: nuklear.ext nuklear.sexp
	./make-extension.scm $<

nuklear-sdl-gles2/guile-nuklear-sdl-gles2.c: nuklear-sdl-gles2.ext nuklear-sdl-gles2.sexp
	./make-extension.scm $<

nuklear/libguile-nuklear.so: nuklear/guile-nuklear.c
	gcc -Wall -O2 -fPIC -shared -o $@ $^ `pkg-config --cflags --libs guile-2.2` -I $(NUKLEAR_SRC)/.. -I $(NUKLEAR_SDL_GLES2_PATH)

nuklear-sdl-gles2/libguile-nuklear-sdl-gles2.so: nuklear-sdl-gles2/guile-nuklear-sdl-gles2.c
	gcc -Wall -O2 -fPIC -shared -o $@ $^ `pkg-config --cflags --libs guile-2.2 sdl2` -Wl,-rpath,$(GUILE_NUKLEAR_PREFIX)/nuklear -L$(GUILE_NUKLEAR_PREFIX)/nuklear -lguile-nuklear -I $(NUKLEAR_SRC)/.. -I $(NUKLEAR_SDL_GLES2_PATH)

nuklear-glfw-gl3/libguile-nuklear-glfw-gl3.so: nuklear-glfw-gl3/guile-nuklear-glfw-gl3.c
	gcc -Wall -O0 -g -fPIC -shared -o $@ $^ `pkg-config --cflags --libs guile-2.2 glfw3 glew` -Wl,-rpath,$(GUILE_NUKLEAR_PREFIX)/nuklear -L$(GUILE_NUKLEAR_PREFIX)/nuklear -lguile-nuklear -I $(NUKLEAR_SRC)/.. -I $(NUKLEAR_GLFW_GL3_PATH)
