#include <libguile.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#include "nuklear.h"

SCM nk_color_r(SCM ptr)
{
  return scm_from_uint8(((struct nk_color *)scm_to_pointer(ptr))->r);
}
SCM nk_color_set_r(SCM ptr, SCM value)
{
  ((struct nk_color *)scm_to_pointer(ptr))->r = scm_to_uint8(value);
  return SCM_UNSPECIFIED;
}
SCM nk_color_g(SCM ptr)
{
  return scm_from_uint8(((struct nk_color *)scm_to_pointer(ptr))->g);
}
SCM nk_color_set_g(SCM ptr, SCM value)
{
  ((struct nk_color *)scm_to_pointer(ptr))->g = scm_to_uint8(value);
  return SCM_UNSPECIFIED;
}
SCM nk_color_b(SCM ptr)
{
  return scm_from_uint8(((struct nk_color *)scm_to_pointer(ptr))->b);
}
SCM nk_color_set_b(SCM ptr, SCM value)
{
  ((struct nk_color *)scm_to_pointer(ptr))->b = scm_to_uint8(value);
  return SCM_UNSPECIFIED;
}
SCM nk_color_a(SCM ptr)
{
  return scm_from_uint8(((struct nk_color *)scm_to_pointer(ptr))->a);
}
SCM nk_color_set_a(SCM ptr, SCM value)
{
  ((struct nk_color *)scm_to_pointer(ptr))->a = scm_to_uint8(value);
  return SCM_UNSPECIFIED;
}
SCM nk_colorf_r(SCM ptr)
{
  return scm_from_double(((struct nk_colorf *)scm_to_pointer(ptr))->r);
}
SCM nk_colorf_set_r(SCM ptr, SCM value)
{
  ((struct nk_colorf *)scm_to_pointer(ptr))->r = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_colorf_g(SCM ptr)
{
  return scm_from_double(((struct nk_colorf *)scm_to_pointer(ptr))->g);
}
SCM nk_colorf_set_g(SCM ptr, SCM value)
{
  ((struct nk_colorf *)scm_to_pointer(ptr))->g = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_colorf_b(SCM ptr)
{
  return scm_from_double(((struct nk_colorf *)scm_to_pointer(ptr))->b);
}
SCM nk_colorf_set_b(SCM ptr, SCM value)
{
  ((struct nk_colorf *)scm_to_pointer(ptr))->b = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_colorf_a(SCM ptr)
{
  return scm_from_double(((struct nk_colorf *)scm_to_pointer(ptr))->a);
}
SCM nk_colorf_set_a(SCM ptr, SCM value)
{
  ((struct nk_colorf *)scm_to_pointer(ptr))->a = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_vec2_x(SCM ptr)
{
  return scm_from_double(((struct nk_vec2 *)scm_to_pointer(ptr))->x);
}
SCM nk_vec2_set_x(SCM ptr, SCM value)
{
  ((struct nk_vec2 *)scm_to_pointer(ptr))->x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_vec2_y(SCM ptr)
{
  return scm_from_double(((struct nk_vec2 *)scm_to_pointer(ptr))->y);
}
SCM nk_vec2_set_y(SCM ptr, SCM value)
{
  ((struct nk_vec2 *)scm_to_pointer(ptr))->y = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_vec2i_x(SCM ptr)
{
  return scm_from_short(((struct nk_vec2i *)scm_to_pointer(ptr))->x);
}
SCM nk_vec2i_set_x(SCM ptr, SCM value)
{
  ((struct nk_vec2i *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_vec2i_y(SCM ptr)
{
  return scm_from_short(((struct nk_vec2i *)scm_to_pointer(ptr))->y);
}
SCM nk_vec2i_set_y(SCM ptr, SCM value)
{
  ((struct nk_vec2i *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_rect_x(SCM ptr)
{
  return scm_from_double(((struct nk_rect *)scm_to_pointer(ptr))->x);
}
SCM nk_rect_set_x(SCM ptr, SCM value)
{
  ((struct nk_rect *)scm_to_pointer(ptr))->x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_rect_y(SCM ptr)
{
  return scm_from_double(((struct nk_rect *)scm_to_pointer(ptr))->y);
}
SCM nk_rect_set_y(SCM ptr, SCM value)
{
  ((struct nk_rect *)scm_to_pointer(ptr))->y = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_rect_w(SCM ptr)
{
  return scm_from_double(((struct nk_rect *)scm_to_pointer(ptr))->w);
}
SCM nk_rect_set_w(SCM ptr, SCM value)
{
  ((struct nk_rect *)scm_to_pointer(ptr))->w = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_rect_h(SCM ptr)
{
  return scm_from_double(((struct nk_rect *)scm_to_pointer(ptr))->h);
}
SCM nk_rect_set_h(SCM ptr, SCM value)
{
  ((struct nk_rect *)scm_to_pointer(ptr))->h = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_recti_x(SCM ptr)
{
  return scm_from_short(((struct nk_recti *)scm_to_pointer(ptr))->x);
}
SCM nk_recti_set_x(SCM ptr, SCM value)
{
  ((struct nk_recti *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_recti_y(SCM ptr)
{
  return scm_from_short(((struct nk_recti *)scm_to_pointer(ptr))->y);
}
SCM nk_recti_set_y(SCM ptr, SCM value)
{
  ((struct nk_recti *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_recti_w(SCM ptr)
{
  return scm_from_short(((struct nk_recti *)scm_to_pointer(ptr))->w);
}
SCM nk_recti_set_w(SCM ptr, SCM value)
{
  ((struct nk_recti *)scm_to_pointer(ptr))->w = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_recti_h(SCM ptr)
{
  return scm_from_short(((struct nk_recti *)scm_to_pointer(ptr))->h);
}
SCM nk_recti_set_h(SCM ptr, SCM value)
{
  ((struct nk_recti *)scm_to_pointer(ptr))->h = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_image_set_handle(SCM ptr, SCM value)
{
  ((struct nk_image *)scm_to_pointer(ptr))->handle = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_image_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_image *)scm_to_pointer(ptr))->w);
}
SCM nk_image_set_w(SCM ptr, SCM value)
{
  ((struct nk_image *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_image_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_image *)scm_to_pointer(ptr))->h);
}
SCM nk_image_set_h(SCM ptr, SCM value)
{
  ((struct nk_image *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_image_region(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_image *)scm_to_pointer(ptr))->region, NULL);
}
SCM nk_image_set_region(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_image *)scm_to_pointer(ptr))->region, (unsigned short *)scm_to_pointer(value), sizeof(((struct nk_image *)scm_to_pointer(ptr))->region));
  return SCM_UNSPECIFIED;
}
SCM nk_cursor_img(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_cursor *)scm_to_pointer(ptr))->img, NULL);
}
SCM nk_cursor_set_img(SCM ptr, SCM value)
{
  ((struct nk_cursor *)scm_to_pointer(ptr))->img = *(struct nk_image *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_cursor_size(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_cursor *)scm_to_pointer(ptr))->size, NULL);
}
SCM nk_cursor_set_size(SCM ptr, SCM value)
{
  ((struct nk_cursor *)scm_to_pointer(ptr))->size = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_cursor_offset(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_cursor *)scm_to_pointer(ptr))->offset, NULL);
}
SCM nk_cursor_set_offset(SCM ptr, SCM value)
{
  ((struct nk_cursor *)scm_to_pointer(ptr))->offset = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_scroll_x(SCM ptr)
{
  return scm_from_uint32(((struct nk_scroll *)scm_to_pointer(ptr))->x);
}
SCM nk_scroll_set_x(SCM ptr, SCM value)
{
  ((struct nk_scroll *)scm_to_pointer(ptr))->x = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_scroll_y(SCM ptr)
{
  return scm_from_uint32(((struct nk_scroll *)scm_to_pointer(ptr))->y);
}
SCM nk_scroll_set_y(SCM ptr, SCM value)
{
  ((struct nk_scroll *)scm_to_pointer(ptr))->y = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_allocator_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_allocator *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_allocator_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_allocator *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_allocator_alloc(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_allocator *)scm_to_pointer(ptr))->alloc, NULL);
}
SCM nk_allocator_set_alloc(SCM ptr, SCM value)
{
  ((struct nk_allocator *)scm_to_pointer(ptr))->alloc = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_allocator_free(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_allocator *)scm_to_pointer(ptr))->free, NULL);
}
SCM nk_allocator_set_free(SCM ptr, SCM value)
{
  ((struct nk_allocator *)scm_to_pointer(ptr))->free = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_init_default_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_init_default((struct nk_context *)scm_to_pointer(arg_1), (struct nk_user_font *)scm_to_pointer(arg_2)));
}

SCM nk_init_fixed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_init_fixed((struct nk_context *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3), (struct nk_user_font *)scm_to_pointer(arg_4)));
}

SCM nk_init_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_init((struct nk_context *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2), (struct nk_user_font *)scm_to_pointer(arg_3)));
}

SCM nk_init_custom_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_init_custom((struct nk_context *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2), (struct nk_buffer *)scm_to_pointer(arg_3), (struct nk_user_font *)scm_to_pointer(arg_4)));
}

SCM nk_clear_wrapper(SCM arg_1)
{
  nk_clear((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_free_wrapper(SCM arg_1)
{
  nk_free((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_input_begin_wrapper(SCM arg_1)
{
  nk_input_begin((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_input_motion_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_input_motion((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_input_key_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_input_key((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_input_button_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_input_button((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_input_scroll_wrapper(SCM arg_1, SCM arg_2)
{
  nk_input_scroll((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_input_char_wrapper(SCM arg_1, SCM arg_2)
{
  nk_input_char((struct nk_context *)scm_to_pointer(arg_1), scm_to_char(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_input_glyph_wrapper(SCM arg_1, SCM arg_2)
{
  nk_input_glyph((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_input_unicode_wrapper(SCM arg_1, SCM arg_2)
{
  nk_input_unicode((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_input_end_wrapper(SCM arg_1)
{
  nk_input_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_null_texture_texture(SCM ptr)
{
  return scm_from_pointer(((struct nk_draw_null_texture *)scm_to_pointer(ptr))->texture.ptr, NULL);
}
SCM nk_draw_null_texture_set_texture(SCM ptr, SCM value)
{
  ((struct nk_draw_null_texture *)scm_to_pointer(ptr))->texture = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_draw_null_texture_uv(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_draw_null_texture *)scm_to_pointer(ptr))->uv, NULL);
}
SCM nk_draw_null_texture_set_uv(SCM ptr, SCM value)
{
  ((struct nk_draw_null_texture *)scm_to_pointer(ptr))->uv = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_global_alpha(SCM ptr)
{
  return scm_from_double(((struct nk_convert_config *)scm_to_pointer(ptr))->global_alpha);
}
SCM nk_convert_config_set_global_alpha(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->global_alpha = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_line_AA(SCM ptr)
{
  return scm_from_int(((struct nk_convert_config *)scm_to_pointer(ptr))->line_AA);
}
SCM nk_convert_config_set_line_AA(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->line_AA = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_shape_AA(SCM ptr)
{
  return scm_from_int(((struct nk_convert_config *)scm_to_pointer(ptr))->shape_AA);
}
SCM nk_convert_config_set_shape_AA(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->shape_AA = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_circle_segment_count(SCM ptr)
{
  return scm_from_uint(((struct nk_convert_config *)scm_to_pointer(ptr))->circle_segment_count);
}
SCM nk_convert_config_set_circle_segment_count(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->circle_segment_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_arc_segment_count(SCM ptr)
{
  return scm_from_uint(((struct nk_convert_config *)scm_to_pointer(ptr))->arc_segment_count);
}
SCM nk_convert_config_set_arc_segment_count(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->arc_segment_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_curve_segment_count(SCM ptr)
{
  return scm_from_uint(((struct nk_convert_config *)scm_to_pointer(ptr))->curve_segment_count);
}
SCM nk_convert_config_set_curve_segment_count(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->curve_segment_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_null(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_convert_config *)scm_to_pointer(ptr))->null, NULL);
}
SCM nk_convert_config_set_null(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->null = *(struct nk_draw_null_texture *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_vertex_layout(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_layout, NULL);
}
SCM nk_convert_config_set_vertex_layout(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_layout = (struct nk_draw_vertex_layout_element *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_vertex_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_size);
}
SCM nk_convert_config_set_vertex_size(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_convert_config_vertex_alignment(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_alignment);
}
SCM nk_convert_config_set_vertex_alignment(SCM ptr, SCM value)
{
  ((struct nk_convert_config *)scm_to_pointer(ptr))->vertex_alignment = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk__begin_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk__begin((struct nk_context *)scm_to_pointer(arg_1)), NULL);
}

SCM nk__next_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk__next((struct nk_context *)scm_to_pointer(arg_1), (struct nk_command *)scm_to_pointer(arg_2)), NULL);
}

SCM nk_convert_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_uint32(nk_convert((struct nk_context *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2), (struct nk_buffer *)scm_to_pointer(arg_3), (struct nk_buffer *)scm_to_pointer(arg_4), (struct nk_convert_config *)scm_to_pointer(arg_5)));
}

SCM nk__draw_begin_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk__draw_begin((struct nk_context *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2)), NULL);
}

SCM nk__draw_end_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk__draw_end((struct nk_context *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2)), NULL);
}

SCM nk__draw_next_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_pointer((void *)nk__draw_next((struct nk_draw_command *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2), (struct nk_context *)scm_to_pointer(arg_3)), NULL);
}

SCM nk_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_begin((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_begin_titled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_begin_titled((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), *(struct nk_rect *)scm_to_pointer(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_end_wrapper(SCM arg_1)
{
  nk_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_window_find_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk_window_find((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)), NULL);
}

SCM nk_window_get_bounds_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_window_get_bounds((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_position_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_window_get_position((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_size_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_window_get_size((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_width_wrapper(SCM arg_1)
{
  return scm_from_double(nk_window_get_width((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_window_get_height_wrapper(SCM arg_1)
{
  return scm_from_double(nk_window_get_height((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_window_get_panel_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_window_get_panel((struct nk_context *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_window_get_content_region_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_window_get_content_region((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_content_region_min_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_window_get_content_region_min((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_content_region_max_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_window_get_content_region_max((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_content_region_size_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_window_get_content_region_size((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_window_get_canvas_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_window_get_canvas((struct nk_context *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_window_get_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_get_scroll((struct nk_context *)scm_to_pointer(arg_1), (nk_uint *)scm_to_pointer(arg_2), (nk_uint *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_has_focus_wrapper(SCM arg_1)
{
  return scm_from_int(nk_window_has_focus((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_window_is_hovered_wrapper(SCM arg_1)
{
  return scm_from_int(nk_window_is_hovered((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_window_is_collapsed_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_window_is_collapsed((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_window_is_closed_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_window_is_closed((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_window_is_hidden_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_window_is_hidden((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_window_is_active_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_window_is_active((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_window_is_any_hovered_wrapper(SCM arg_1)
{
  return scm_from_int(nk_window_is_any_hovered((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_item_is_any_active_wrapper(SCM arg_1)
{
  return scm_from_int(nk_item_is_any_active((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_window_set_position_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_set_position((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_set_size_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_set_size((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_set_focus_wrapper(SCM arg_1, SCM arg_2)
{
  nk_window_set_focus((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_window_set_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_set_scroll((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), scm_to_uint32(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_close_wrapper(SCM arg_1, SCM arg_2)
{
  nk_window_close((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_window_collapse_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_collapse((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_collapse_if_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_window_collapse_if((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_window_show_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_window_show((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_window_show_if_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_window_show_if((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_set_min_row_height_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_set_min_row_height((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_reset_min_row_height_wrapper(SCM arg_1)
{
  nk_layout_reset_min_row_height((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_widget_bounds_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_layout_widget_bounds((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_layout_ratio_from_pixel_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_double(nk_layout_ratio_from_pixel((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2)));
}

SCM nk_layout_row_dynamic_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_layout_row_dynamic((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_static_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_layout_row_static((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_layout_row_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_double(arg_3), scm_to_int(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_push_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_row_push((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_end_wrapper(SCM arg_1)
{
  nk_layout_row_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_layout_row((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_double(arg_3), scm_to_int(arg_4), (float *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_template_begin_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_row_template_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_template_push_dynamic_wrapper(SCM arg_1)
{
  nk_layout_row_template_push_dynamic((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_template_push_variable_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_row_template_push_variable((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_template_push_static_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_row_template_push_static((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_row_template_end_wrapper(SCM arg_1)
{
  nk_layout_row_template_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_space_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_layout_space_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_double(arg_3), scm_to_int(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_space_push_wrapper(SCM arg_1, SCM arg_2)
{
  nk_layout_space_push((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_space_end_wrapper(SCM arg_1)
{
  nk_layout_space_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_layout_space_bounds_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_layout_space_bounds((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_layout_space_to_screen_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_layout_space_to_screen((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_layout_space_to_local_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_layout_space_to_local((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_layout_space_rect_to_screen_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_layout_space_rect_to_screen((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_layout_space_rect_to_local_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_layout_space_rect_to_local((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_group_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_group_begin((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_group_begin_titled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_group_begin_titled((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_group_end_wrapper(SCM arg_1)
{
  nk_group_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_group_scrolled_offset_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_group_scrolled_offset_begin((struct nk_context *)scm_to_pointer(arg_1), (nk_uint *)scm_to_pointer(arg_2), (nk_uint *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_group_scrolled_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_group_scrolled_begin((struct nk_context *)scm_to_pointer(arg_1), (struct nk_scroll *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_group_scrolled_end_wrapper(SCM arg_1)
{
  nk_group_scrolled_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_group_get_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_group_get_scroll((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (nk_uint *)scm_to_pointer(arg_3), (nk_uint *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_group_set_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_group_set_scroll((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), scm_to_uint32(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_tree_push_hashed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_int(nk_tree_push_hashed((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), (char *)scm_to_pointer(arg_5), scm_to_int(arg_6), scm_to_int(arg_7)));
}

SCM nk_tree_image_push_hashed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8)
{
  return scm_from_int(nk_tree_image_push_hashed((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_int(arg_5), (char *)scm_to_pointer(arg_6), scm_to_int(arg_7), scm_to_int(arg_8)));
}

SCM nk_tree_pop_wrapper(SCM arg_1)
{
  nk_tree_pop((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_tree_state_push_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_tree_state_push((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), (enum nk_collapse_states *)scm_to_pointer(arg_4)));
}

SCM nk_tree_state_image_push_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_tree_state_image_push((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), (enum nk_collapse_states *)scm_to_pointer(arg_5)));
}

SCM nk_tree_state_pop_wrapper(SCM arg_1)
{
  nk_tree_state_pop((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_tree_element_push_hashed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8)
{
  return scm_from_int(nk_tree_element_push_hashed((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), (int *)scm_to_pointer(arg_5), (char *)scm_to_pointer(arg_6), scm_to_int(arg_7), scm_to_int(arg_8)));
}

SCM nk_tree_element_image_push_hashed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8, SCM arg_9)
{
  return scm_from_int(nk_tree_element_image_push_hashed((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_int(arg_5), (int *)scm_to_pointer(arg_6), (char *)scm_to_pointer(arg_7), scm_to_int(arg_8), scm_to_int(arg_9)));
}

SCM nk_tree_element_pop_wrapper(SCM arg_1)
{
  nk_tree_element_pop((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_list_view_get_begin(SCM ptr)
{
  return scm_from_int(((struct nk_list_view *)scm_to_pointer(ptr))->begin);
}
SCM nk_list_view_set_begin(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->begin = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_get_end(SCM ptr)
{
  return scm_from_int(((struct nk_list_view *)scm_to_pointer(ptr))->end);
}
SCM nk_list_view_set_end(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->end = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_count(SCM ptr)
{
  return scm_from_int(((struct nk_list_view *)scm_to_pointer(ptr))->count);
}
SCM nk_list_view_set_count(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->count = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_total_height(SCM ptr)
{
  return scm_from_int(((struct nk_list_view *)scm_to_pointer(ptr))->total_height);
}
SCM nk_list_view_set_total_height(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->total_height = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_ctx(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_list_view *)scm_to_pointer(ptr))->ctx, NULL);
}
SCM nk_list_view_set_ctx(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->ctx = (struct nk_context *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_scroll_pointer(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_list_view *)scm_to_pointer(ptr))->scroll_pointer, NULL);
}
SCM nk_list_view_set_scroll_pointer(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->scroll_pointer = (nk_uint *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_scroll_value(SCM ptr)
{
  return scm_from_uint32(((struct nk_list_view *)scm_to_pointer(ptr))->scroll_value);
}
SCM nk_list_view_set_scroll_value(SCM ptr, SCM value)
{
  ((struct nk_list_view *)scm_to_pointer(ptr))->scroll_value = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_list_view_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_list_view_begin((struct nk_context *)scm_to_pointer(arg_1), (struct nk_list_view *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), scm_to_int(arg_5), scm_to_int(arg_6)));
}

SCM nk_list_view_end_wrapper(SCM arg_1)
{
  nk_list_view_end((struct nk_list_view *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_widget_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_widget((struct nk_rect *)scm_to_pointer(arg_1), (struct nk_context *)scm_to_pointer(arg_2)));
}

SCM nk_widget_fitting_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_widget_fitting((struct nk_rect *)scm_to_pointer(arg_1), (struct nk_context *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_widget_bounds_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_widget_bounds((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_widget_position_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_widget_position((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_widget_size_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_widget_size((struct nk_context *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_widget_width_wrapper(SCM arg_1)
{
  return scm_from_double(nk_widget_width((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_widget_height_wrapper(SCM arg_1)
{
  return scm_from_double(nk_widget_height((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_widget_is_hovered_wrapper(SCM arg_1)
{
  return scm_from_int(nk_widget_is_hovered((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_widget_is_mouse_clicked_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_widget_is_mouse_clicked((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_widget_has_mouse_click_down_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_widget_has_mouse_click_down((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3)));
}

SCM nk_spacing_wrapper(SCM arg_1, SCM arg_2)
{
  nk_spacing((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_text_colored_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_text_colored((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_text_wrap_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_text_wrap((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_text_wrap_colored_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_text_wrap_colored((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_label_colored_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_label_colored((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_label_wrap_wrapper(SCM arg_1, SCM arg_2)
{
  nk_label_wrap((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_label_colored_wrap_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_label_colored_wrap((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_image_wrapper(SCM arg_1, SCM arg_2)
{
  nk_image((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_image_color_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_image_color((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_button_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_button_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_button_label_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_button_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_button_color_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_button_color((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2)));
}

SCM nk_button_symbol_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_button_symbol((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_button_image_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_button_image((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2)));
}

SCM nk_button_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_button_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_button_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_button_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_button_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_button_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_button_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_button_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_button_text_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_button_text_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_button_label_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_button_label_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3)));
}

SCM nk_button_symbol_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_button_symbol_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_button_image_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_button_image_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), *(struct nk_image *)scm_to_pointer(arg_3)));
}

SCM nk_button_symbol_text_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_button_symbol_text_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), scm_to_int(arg_3), (char *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_uint32(arg_6)));
}

SCM nk_button_symbol_label_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_button_symbol_label_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), scm_to_int(arg_3), (char *)scm_to_pointer(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_button_image_label_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_button_image_label_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_button_image_text_styled_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_button_image_text_styled((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_button *)scm_to_pointer(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_uint32(arg_6)));
}

SCM nk_button_set_behavior_wrapper(SCM arg_1, SCM arg_2)
{
  nk_button_set_behavior((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_button_push_behavior_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_button_push_behavior((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_button_pop_behavior_wrapper(SCM arg_1)
{
  return scm_from_int(nk_button_pop_behavior((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_check_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_check_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_check_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_check_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4)));
}

SCM nk_check_flags_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_uint(nk_check_flags_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint(arg_3), scm_to_uint(arg_4)));
}

SCM nk_check_flags_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_uint(nk_check_flags_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint(arg_4), scm_to_uint(arg_5)));
}

SCM nk_checkbox_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_checkbox_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3)));
}

SCM nk_checkbox_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_checkbox_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), (int *)scm_to_pointer(arg_4)));
}

SCM nk_checkbox_flags_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_checkbox_flags_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (unsigned int *)scm_to_pointer(arg_3), scm_to_uint(arg_4)));
}

SCM nk_checkbox_flags_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_checkbox_flags_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), (unsigned int *)scm_to_pointer(arg_4), scm_to_uint(arg_5)));
}

SCM nk_radio_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_radio_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3)));
}

SCM nk_radio_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_radio_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), (int *)scm_to_pointer(arg_4)));
}

SCM nk_option_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_option_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_option_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_option_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4)));
}

SCM nk_selectable_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_selectable_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), (int *)scm_to_pointer(arg_4)));
}

SCM nk_selectable_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_selectable_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), (int *)scm_to_pointer(arg_5)));
}

SCM nk_selectable_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_selectable_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), (int *)scm_to_pointer(arg_5)));
}

SCM nk_selectable_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_selectable_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5), (int *)scm_to_pointer(arg_6)));
}

SCM nk_selectable_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_selectable_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), (int *)scm_to_pointer(arg_5)));
}

SCM nk_selectable_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_selectable_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5), (int *)scm_to_pointer(arg_6)));
}

SCM nk_select_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_select_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), scm_to_int(arg_4)));
}

SCM nk_select_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_select_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), scm_to_int(arg_5)));
}

SCM nk_select_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_select_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), scm_to_int(arg_5)));
}

SCM nk_select_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_select_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5), scm_to_int(arg_6)));
}

SCM nk_select_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_select_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), scm_to_int(arg_5)));
}

SCM nk_select_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_select_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5), scm_to_int(arg_6)));
}

SCM nk_slide_float_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_double(nk_slide_float((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5)));
}

SCM nk_slide_int_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_slide_int((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5)));
}

SCM nk_slider_float_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_slider_float((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2), (float *)scm_to_pointer(arg_3), scm_to_double(arg_4), scm_to_double(arg_5)));
}

SCM nk_slider_int_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_slider_int((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (int *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_int(arg_5)));
}

SCM nk_progress_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_progress((struct nk_context *)scm_to_pointer(arg_1), (nk_size *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3), scm_to_int(arg_4)));
}

SCM nk_prog_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_uintptr_t(nk_prog((struct nk_context *)scm_to_pointer(arg_1), scm_to_uintptr_t(arg_2), scm_to_uintptr_t(arg_3), scm_to_int(arg_4)));
}

SCM nk_color_picker_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  struct nk_colorf * s = scm_gc_malloc_pointerless(sizeof(struct nk_colorf), "struct nk_colorf");
  *s = nk_color_picker((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_colorf *)scm_to_pointer(arg_2), scm_to_int(arg_3));
  return scm_from_pointer(s, NULL);
}

SCM nk_color_pick_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_color_pick((struct nk_context *)scm_to_pointer(arg_1), (struct nk_colorf *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_property_int_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_property_int((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), (int *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), scm_to_double(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_property_float_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_property_float((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), (float *)scm_to_pointer(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_property_double_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_property_double((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), (double *)scm_to_pointer(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_propertyi_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_int(nk_propertyi((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), scm_to_double(arg_7)));
}

SCM nk_propertyf_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_double(nk_propertyf((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7)));
}

SCM nk_propertyd_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_double(nk_propertyd((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7)));
}

SCM nk_edit_string_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_uint32(nk_edit_string((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), (char *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_pointer(arg_6)));
}

SCM nk_edit_string_zero_terminated_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_uint32(nk_edit_string_zero_terminated((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_pointer(arg_5)));
}

SCM nk_edit_buffer_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_uint32(nk_edit_buffer((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), (struct nk_text_edit *)scm_to_pointer(arg_3), scm_to_pointer(arg_4)));
}

SCM nk_edit_focus_wrapper(SCM arg_1, SCM arg_2)
{
  nk_edit_focus((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_edit_unfocus_wrapper(SCM arg_1)
{
  nk_edit_unfocus((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_chart_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_chart_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_double(arg_4), scm_to_double(arg_5)));
}

SCM nk_chart_begin_colored_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_int(nk_chart_begin_colored((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_double(arg_6), scm_to_double(arg_7)));
}

SCM nk_chart_add_slot_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_chart_add_slot((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_double(arg_4), scm_to_double(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_chart_add_slot_colored_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_chart_add_slot_colored((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_double(arg_6), scm_to_double(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_chart_push_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_uint32(nk_chart_push((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2)));
}

SCM nk_chart_push_slot_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_uint32(nk_chart_push_slot((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_int(arg_3)));
}

SCM nk_chart_end_wrapper(SCM arg_1)
{
  nk_chart_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_plot_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_plot((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (float *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_int(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_plot_function_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_plot_function((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (void *)scm_to_pointer(arg_3), scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_int(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_popup_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_popup_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4), *(struct nk_rect *)scm_to_pointer(arg_5)));
}

SCM nk_popup_close_wrapper(SCM arg_1)
{
  nk_popup_close((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_popup_end_wrapper(SCM arg_1)
{
  nk_popup_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_popup_get_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_popup_get_scroll((struct nk_context *)scm_to_pointer(arg_1), (nk_uint *)scm_to_pointer(arg_2), (nk_uint *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_popup_set_scroll_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_popup_set_scroll((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), scm_to_uint32(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_combo_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_combo((struct nk_context *)scm_to_pointer(arg_1), (const char **)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6)));
}

SCM nk_combo_separator_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_int(nk_combo_separator((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), *(struct nk_vec2 *)scm_to_pointer(arg_7)));
}

SCM nk_combo_string_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_combo_string((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6)));
}

SCM nk_combo_callback_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  return scm_from_int(nk_combo_callback((struct nk_context *)scm_to_pointer(arg_1), scm_to_pointer(arg_2), (void *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), *(struct nk_vec2 *)scm_to_pointer(arg_7)));
}

SCM nk_combobox_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_combobox((struct nk_context *)scm_to_pointer(arg_1), (const char **)scm_to_pointer(arg_2), scm_to_int(arg_3), (int *)scm_to_pointer(arg_4), scm_to_int(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_combobox_string_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_combobox_string((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_int(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_combobox_separator_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_combobox_separator((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), (int *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), *(struct nk_vec2 *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_combobox_callback_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_combobox_callback((struct nk_context *)scm_to_pointer(arg_1), scm_to_pointer(arg_2), (void *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_int(arg_6), *(struct nk_vec2 *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_combo_begin_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_begin_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_combo_begin_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_combo_begin_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_combo_begin_color_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_combo_begin_color((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_combo_begin_symbol_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_combo_begin_symbol((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_combo_begin_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_begin_symbol_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_combo_begin_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_combo_begin_symbol_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_int(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5)));
}

SCM nk_combo_begin_image_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_combo_begin_image((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_combo_begin_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_begin_image_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_combo_begin_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_combo_begin_image_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_image *)scm_to_pointer(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5)));
}

SCM nk_combo_item_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_combo_item_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_combo_item_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_item_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_combo_item_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_item_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_combo_item_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_combo_item_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_combo_item_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_combo_item_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_combo_item_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_combo_item_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_combo_close_wrapper(SCM arg_1)
{
  nk_combo_close((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_combo_end_wrapper(SCM arg_1)
{
  nk_combo_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_contextual_begin_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_contextual_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_uint32(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_rect *)scm_to_pointer(arg_4)));
}

SCM nk_contextual_item_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_contextual_item_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_contextual_item_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_contextual_item_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_contextual_item_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_contextual_item_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_contextual_item_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_contextual_item_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_contextual_item_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_contextual_item_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_contextual_item_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_contextual_item_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_contextual_close_wrapper(SCM arg_1)
{
  nk_contextual_close((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_contextual_end_wrapper(SCM arg_1)
{
  nk_contextual_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_tooltip_wrapper(SCM arg_1, SCM arg_2)
{
  nk_tooltip((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_tooltip_begin_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_tooltip_begin((struct nk_context *)scm_to_pointer(arg_1), scm_to_double(arg_2)));
}

SCM nk_tooltip_end_wrapper(SCM arg_1)
{
  nk_tooltip_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_menubar_begin_wrapper(SCM arg_1)
{
  nk_menubar_begin((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_menubar_end_wrapper(SCM arg_1)
{
  nk_menubar_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_menu_begin_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_menu_begin_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5)));
}

SCM nk_menu_begin_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_begin_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_menu_begin_image_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_begin_image((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), *(struct nk_image *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_menu_begin_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_menu_begin_image_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), *(struct nk_image *)scm_to_pointer(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6)));
}

SCM nk_menu_begin_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_menu_begin_image_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), *(struct nk_image *)scm_to_pointer(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5)));
}

SCM nk_menu_begin_symbol_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_begin_symbol((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4)));
}

SCM nk_menu_begin_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  return scm_from_int(nk_menu_begin_symbol_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4), scm_to_int(arg_5), *(struct nk_vec2 *)scm_to_pointer(arg_6)));
}

SCM nk_menu_begin_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_menu_begin_symbol_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3), scm_to_int(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5)));
}

SCM nk_menu_item_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_item_text((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_menu_item_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_menu_item_label((struct nk_context *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_menu_item_image_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_item_image_label((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_menu_item_image_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_menu_item_image_text((struct nk_context *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_menu_item_symbol_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_int(nk_menu_item_symbol_text((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), scm_to_uint32(arg_5)));
}

SCM nk_menu_item_symbol_label_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_menu_item_symbol_label((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_uint32(arg_4)));
}

SCM nk_menu_close_wrapper(SCM arg_1)
{
  nk_menu_close((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_menu_end_wrapper(SCM arg_1)
{
  nk_menu_end((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_style_default_wrapper(SCM arg_1)
{
  nk_style_default((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_style_from_table_wrapper(SCM arg_1, SCM arg_2)
{
  nk_style_from_table((struct nk_context *)scm_to_pointer(arg_1), (struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_style_load_cursor_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_style_load_cursor((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2), (struct nk_cursor *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_style_load_all_cursors_wrapper(SCM arg_1, SCM arg_2)
{
  nk_style_load_all_cursors((struct nk_context *)scm_to_pointer(arg_1), (struct nk_cursor *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_style_get_color_by_name_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_style_get_color_by_name(scm_to_int(arg_1)), NULL);
}

SCM nk_style_set_cursor_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_style_set_cursor((struct nk_context *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_style_show_cursor_wrapper(SCM arg_1)
{
  nk_style_show_cursor((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_style_hide_cursor_wrapper(SCM arg_1)
{
  nk_style_hide_cursor((struct nk_context *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_style_push_font_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_style_push_font((struct nk_context *)scm_to_pointer(arg_1), (struct nk_user_font *)scm_to_pointer(arg_2)));
}

SCM nk_style_push_float_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_style_push_float((struct nk_context *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), scm_to_double(arg_3)));
}

SCM nk_style_push_vec2_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_style_push_vec2((struct nk_context *)scm_to_pointer(arg_1), (struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3)));
}

SCM nk_style_push_style_item_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_style_push_style_item((struct nk_context *)scm_to_pointer(arg_1), (struct nk_style_item *)scm_to_pointer(arg_2), *(struct nk_style_item *)scm_to_pointer(arg_3)));
}

SCM nk_style_push_flags_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_style_push_flags((struct nk_context *)scm_to_pointer(arg_1), (nk_flags *)scm_to_pointer(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_style_push_color_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_style_push_color((struct nk_context *)scm_to_pointer(arg_1), (struct nk_color *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3)));
}

SCM nk_style_pop_font_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_font((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_style_pop_float_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_float((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_style_pop_vec2_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_vec2((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_style_pop_style_item_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_style_item((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_style_pop_flags_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_flags((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_style_pop_color_wrapper(SCM arg_1)
{
  return scm_from_int(nk_style_pop_color((struct nk_context *)scm_to_pointer(arg_1)));
}

SCM nk_rgb_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_iv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_iv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_bv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_bv((nk_byte *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_f(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_fv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_fv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_cf_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_cf(*(struct nk_colorf *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgb_hex_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgb_hex((char *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_u32_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_u32(scm_to_uint32(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_iv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_iv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_bv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_bv((nk_byte *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_f(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_fv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_fv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_cf_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_cf(*(struct nk_colorf *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rgba_hex_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_rgba_hex((char *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_colorf_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_colorf * s = scm_gc_malloc_pointerless(sizeof(struct nk_colorf), "struct nk_colorf");
  *s = nk_hsva_colorf(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_colorfv_wrapper(SCM arg_1)
{
  struct nk_colorf * s = scm_gc_malloc_pointerless(sizeof(struct nk_colorf), "struct nk_colorf");
  *s = nk_hsva_colorfv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_colorf_hsva_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_colorf_hsva_f((float *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), (float *)scm_to_pointer(arg_3), (float *)scm_to_pointer(arg_4), *(struct nk_colorf *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_colorf_hsva_fv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_colorf_hsva_fv((float *)scm_to_pointer(arg_1), *(struct nk_colorf *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_hsv_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsv(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsv_iv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsv_iv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsv_bv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsv_bv((nk_byte *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsv_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsv_f(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsv_fv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsv_fv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsva(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_iv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsva_iv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_bv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsva_bv((nk_byte *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsva_f(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_hsva_fv_wrapper(SCM arg_1)
{
  struct nk_color * s = scm_gc_malloc_pointerless(sizeof(struct nk_color), "struct nk_color");
  *s = nk_hsva_fv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_color_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_color_f((float *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), (float *)scm_to_pointer(arg_3), (float *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_color_fv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_fv((float *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_cf_wrapper(SCM arg_1)
{
  struct nk_colorf * s = scm_gc_malloc_pointerless(sizeof(struct nk_colorf), "struct nk_colorf");
  *s = nk_color_cf(*(struct nk_color *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_color_d_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_color_d((double *)scm_to_pointer(arg_1), (double *)scm_to_pointer(arg_2), (double *)scm_to_pointer(arg_3), (double *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_color_dv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_dv((double *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_u32_wrapper(SCM arg_1)
{
  return scm_from_uint32(nk_color_u32(*(struct nk_color *)scm_to_pointer(arg_1)));
}

SCM nk_color_hex_rgba_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hex_rgba((char *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hex_rgb_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hex_rgb((char *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_i_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_color_hsv_i((int *)scm_to_pointer(arg_1), (int *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_b_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_color_hsv_b((nk_byte *)scm_to_pointer(arg_1), (nk_byte *)scm_to_pointer(arg_2), (nk_byte *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_iv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsv_iv((int *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_bv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsv_bv((nk_byte *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_color_hsv_f((float *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), (float *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsv_fv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsv_fv((float *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_i_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_color_hsva_i((int *)scm_to_pointer(arg_1), (int *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_b_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_color_hsva_b((nk_byte *)scm_to_pointer(arg_1), (nk_byte *)scm_to_pointer(arg_2), (nk_byte *)scm_to_pointer(arg_3), (nk_byte *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_iv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsva_iv((int *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_bv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsva_bv((nk_byte *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_f_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_color_hsva_f((float *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), (float *)scm_to_pointer(arg_3), (float *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_color_hsva_fv_wrapper(SCM arg_1, SCM arg_2)
{
  nk_color_hsva_fv((float *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_handle_ptr_wrapper(SCM arg_1)
{
  return scm_from_pointer(nk_handle_ptr((void *)scm_to_pointer(arg_1)).ptr, NULL);
}

SCM nk_handle_id_wrapper(SCM arg_1)
{
  return scm_from_pointer(nk_handle_id(scm_to_int(arg_1)).ptr, NULL);
}

SCM nk_image_ptr_wrapper(SCM arg_1)
{
  struct nk_image * s = scm_gc_malloc_pointerless(sizeof(struct nk_image), "struct nk_image");
  *s = nk_image_ptr((void *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_image_id_wrapper(SCM arg_1)
{
  struct nk_image * s = scm_gc_malloc_pointerless(sizeof(struct nk_image), "struct nk_image");
  *s = nk_image_id(scm_to_int(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_image_is_subimage_wrapper(SCM arg_1)
{
  return scm_from_int(nk_image_is_subimage((struct nk_image *)scm_to_pointer(arg_1)));
}

SCM nk_subimage_ptr_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_image * s = scm_gc_malloc_pointerless(sizeof(struct nk_image), "struct nk_image");
  *s = nk_subimage_ptr((void *)scm_to_pointer(arg_1), scm_to_ushort(arg_2), scm_to_ushort(arg_3), *(struct nk_rect *)scm_to_pointer(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_subimage_id_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_image * s = scm_gc_malloc_pointerless(sizeof(struct nk_image), "struct nk_image");
  *s = nk_subimage_id(scm_to_int(arg_1), scm_to_ushort(arg_2), scm_to_ushort(arg_3), *(struct nk_rect *)scm_to_pointer(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_subimage_handle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_image * s = scm_gc_malloc_pointerless(sizeof(struct nk_image), "struct nk_image");
  *s = nk_subimage_handle(nk_handle_ptr(scm_to_pointer(arg_1)), scm_to_ushort(arg_2), scm_to_ushort(arg_3), *(struct nk_rect *)scm_to_pointer(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_murmur_hash_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_uint32(nk_murmur_hash((void *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_uint32(arg_3)));
}

SCM nk_triangle_from_direction_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_triangle_from_direction((struct nk_vec2 *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_int(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_vec2_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_vec2(scm_to_double(arg_1), scm_to_double(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_vec2i_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_vec2i(scm_to_int(arg_1), scm_to_int(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_vec2v_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_vec2v((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_vec2iv_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_vec2iv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_get_null_rect_wrapper()
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_get_null_rect();
  return scm_from_pointer(s, NULL);
}

SCM nk_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_rect(scm_to_double(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_recti_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_recti(scm_to_int(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), scm_to_int(arg_4));
  return scm_from_pointer(s, NULL);
}

SCM nk_recta_wrapper(SCM arg_1, SCM arg_2)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_recta(*(struct nk_vec2 *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2));
  return scm_from_pointer(s, NULL);
}

SCM nk_rectv_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_rectv((float *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rectiv_wrapper(SCM arg_1)
{
  struct nk_rect * s = scm_gc_malloc_pointerless(sizeof(struct nk_rect), "struct nk_rect");
  *s = nk_rectiv((int *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rect_pos_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_rect_pos(*(struct nk_rect *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_rect_size_wrapper(SCM arg_1)
{
  struct nk_vec2 * s = scm_gc_malloc_pointerless(sizeof(struct nk_vec2), "struct nk_vec2");
  *s = nk_rect_size(*(struct nk_rect *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_strlen_wrapper(SCM arg_1)
{
  return scm_from_int(nk_strlen((char *)scm_to_pointer(arg_1)));
}

SCM nk_stricmp_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_stricmp((char *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_stricmpn_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_stricmpn((char *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_strtoi_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_strtoi((char *)scm_to_pointer(arg_1), (const char **)scm_to_pointer(arg_2)));
}

SCM nk_strtof_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_double(nk_strtof((char *)scm_to_pointer(arg_1), (const char **)scm_to_pointer(arg_2)));
}

SCM nk_strtod_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_double(nk_strtod((char *)scm_to_pointer(arg_1), (const char **)scm_to_pointer(arg_2)));
}

SCM nk_strfilter_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_strfilter((char *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_strmatch_fuzzy_string_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_strmatch_fuzzy_string((char *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3)));
}

SCM nk_strmatch_fuzzy_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_strmatch_fuzzy_text((char *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4)));
}

SCM nk_utf_decode_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_utf_decode((char *)scm_to_pointer(arg_1), (nk_rune *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_utf_encode_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_utf_encode(scm_to_uint32(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_utf_len_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_utf_len((char *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_utf_at_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_pointer((void *)nk_utf_at((char *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3), (nk_rune *)scm_to_pointer(arg_4), (int *)scm_to_pointer(arg_5)), NULL);
}

SCM nk_user_font_glyph_uv(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_user_font_glyph *)scm_to_pointer(ptr))->uv, NULL);
}
SCM nk_user_font_glyph_set_uv(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_user_font_glyph *)scm_to_pointer(ptr))->uv, (struct nk_vec2 *)scm_to_pointer(value), sizeof(((struct nk_user_font_glyph *)scm_to_pointer(ptr))->uv));
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_glyph_offset(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_user_font_glyph *)scm_to_pointer(ptr))->offset, NULL);
}
SCM nk_user_font_glyph_set_offset(SCM ptr, SCM value)
{
  ((struct nk_user_font_glyph *)scm_to_pointer(ptr))->offset = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_glyph_width(SCM ptr)
{
  return scm_from_double(((struct nk_user_font_glyph *)scm_to_pointer(ptr))->width);
}
SCM nk_user_font_glyph_set_width(SCM ptr, SCM value)
{
  ((struct nk_user_font_glyph *)scm_to_pointer(ptr))->width = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_glyph_height(SCM ptr)
{
  return scm_from_double(((struct nk_user_font_glyph *)scm_to_pointer(ptr))->height);
}
SCM nk_user_font_glyph_set_height(SCM ptr, SCM value)
{
  ((struct nk_user_font_glyph *)scm_to_pointer(ptr))->height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_glyph_xadvance(SCM ptr)
{
  return scm_from_double(((struct nk_user_font_glyph *)scm_to_pointer(ptr))->xadvance);
}
SCM nk_user_font_glyph_set_xadvance(SCM ptr, SCM value)
{
  ((struct nk_user_font_glyph *)scm_to_pointer(ptr))->xadvance = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_user_font *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_user_font_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_user_font *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_height(SCM ptr)
{
  return scm_from_double(((struct nk_user_font *)scm_to_pointer(ptr))->height);
}
SCM nk_user_font_set_height(SCM ptr, SCM value)
{
  ((struct nk_user_font *)scm_to_pointer(ptr))->height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_width(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_user_font *)scm_to_pointer(ptr))->width, NULL);
}
SCM nk_user_font_set_width(SCM ptr, SCM value)
{
  ((struct nk_user_font *)scm_to_pointer(ptr))->width = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_query(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_user_font *)scm_to_pointer(ptr))->query, NULL);
}
SCM nk_user_font_set_query(SCM ptr, SCM value)
{
  ((struct nk_user_font *)scm_to_pointer(ptr))->query = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_user_font_texture(SCM ptr)
{
  return scm_from_pointer(((struct nk_user_font *)scm_to_pointer(ptr))->texture.ptr, NULL);
}
SCM nk_user_font_set_texture(SCM ptr, SCM value)
{
  ((struct nk_user_font *)scm_to_pointer(ptr))->texture = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_height(SCM ptr)
{
  return scm_from_double(((struct nk_baked_font *)scm_to_pointer(ptr))->height);
}
SCM nk_baked_font_set_height(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_ascent(SCM ptr)
{
  return scm_from_double(((struct nk_baked_font *)scm_to_pointer(ptr))->ascent);
}
SCM nk_baked_font_set_ascent(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->ascent = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_descent(SCM ptr)
{
  return scm_from_double(((struct nk_baked_font *)scm_to_pointer(ptr))->descent);
}
SCM nk_baked_font_set_descent(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->descent = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_glyph_offset(SCM ptr)
{
  return scm_from_uint32(((struct nk_baked_font *)scm_to_pointer(ptr))->glyph_offset);
}
SCM nk_baked_font_set_glyph_offset(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->glyph_offset = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_glyph_count(SCM ptr)
{
  return scm_from_uint32(((struct nk_baked_font *)scm_to_pointer(ptr))->glyph_count);
}
SCM nk_baked_font_set_glyph_count(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->glyph_count = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_baked_font_ranges(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_baked_font *)scm_to_pointer(ptr))->ranges, NULL);
}
SCM nk_baked_font_set_ranges(SCM ptr, SCM value)
{
  ((struct nk_baked_font *)scm_to_pointer(ptr))->ranges = (nk_rune *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_font_config_set_next(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->next = (struct nk_font_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_ttf_blob(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->ttf_blob, NULL);
}
SCM nk_font_config_set_ttf_blob(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->ttf_blob = (void *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_ttf_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_font_config *)scm_to_pointer(ptr))->ttf_size);
}
SCM nk_font_config_set_ttf_size(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->ttf_size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_ttf_data_owned_by_atlas(SCM ptr)
{
  return scm_from_uchar(((struct nk_font_config *)scm_to_pointer(ptr))->ttf_data_owned_by_atlas);
}
SCM nk_font_config_set_ttf_data_owned_by_atlas(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->ttf_data_owned_by_atlas = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_merge_mode(SCM ptr)
{
  return scm_from_uchar(((struct nk_font_config *)scm_to_pointer(ptr))->merge_mode);
}
SCM nk_font_config_set_merge_mode(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->merge_mode = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_pixel_snap(SCM ptr)
{
  return scm_from_uchar(((struct nk_font_config *)scm_to_pointer(ptr))->pixel_snap);
}
SCM nk_font_config_set_pixel_snap(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->pixel_snap = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_oversample_v(SCM ptr)
{
  return scm_from_uchar(((struct nk_font_config *)scm_to_pointer(ptr))->oversample_v);
}
SCM nk_font_config_set_oversample_v(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->oversample_v = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_oversample_h(SCM ptr)
{
  return scm_from_uchar(((struct nk_font_config *)scm_to_pointer(ptr))->oversample_h);
}
SCM nk_font_config_set_oversample_h(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->oversample_h = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_padding(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_font_config_set_padding(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_font_config *)scm_to_pointer(ptr))->padding, (unsigned char *)scm_to_pointer(value), sizeof(((struct nk_font_config *)scm_to_pointer(ptr))->padding));
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_size(SCM ptr)
{
  return scm_from_double(((struct nk_font_config *)scm_to_pointer(ptr))->size);
}
SCM nk_font_config_set_size(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->size = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_coord_type(SCM ptr)
{
  return scm_from_int(((struct nk_font_config *)scm_to_pointer(ptr))->coord_type);
}
SCM nk_font_config_set_coord_type(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->coord_type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font_config *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_font_config_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_range(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->range, NULL);
}
SCM nk_font_config_set_range(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->range = (nk_rune *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_font(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->font, NULL);
}
SCM nk_font_config_set_font(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->font = (struct nk_baked_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_fallback_glyph(SCM ptr)
{
  return scm_from_uint32(((struct nk_font_config *)scm_to_pointer(ptr))->fallback_glyph);
}
SCM nk_font_config_set_fallback_glyph(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->fallback_glyph = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_n(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->n, NULL);
}
SCM nk_font_config_set_n(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->n = (struct nk_font_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_config_p(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_config *)scm_to_pointer(ptr))->p, NULL);
}
SCM nk_font_config_set_p(SCM ptr, SCM value)
{
  ((struct nk_font_config *)scm_to_pointer(ptr))->p = (struct nk_font_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_codepoint(SCM ptr)
{
  return scm_from_uint32(((struct nk_font_glyph *)scm_to_pointer(ptr))->codepoint);
}
SCM nk_font_glyph_set_codepoint(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->codepoint = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_xadvance(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->xadvance);
}
SCM nk_font_glyph_set_xadvance(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->xadvance = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_x0(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->x0);
}
SCM nk_font_glyph_set_x0(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->x0 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_y0(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->y0);
}
SCM nk_font_glyph_set_y0(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->y0 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_x1(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->x1);
}
SCM nk_font_glyph_set_x1(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->x1 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_y1(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->y1);
}
SCM nk_font_glyph_set_y1(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->y1 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_w(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->w);
}
SCM nk_font_glyph_set_w(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->w = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_h(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->h);
}
SCM nk_font_glyph_set_h(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->h = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_u0(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->u0);
}
SCM nk_font_glyph_set_u0(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->u0 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_v0(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->v0);
}
SCM nk_font_glyph_set_v0(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->v0 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_u1(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->u1);
}
SCM nk_font_glyph_set_u1(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->u1 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyph_v1(SCM ptr)
{
  return scm_from_double(((struct nk_font_glyph *)scm_to_pointer(ptr))->v1);
}
SCM nk_font_glyph_set_v1(SCM ptr, SCM value)
{
  ((struct nk_font_glyph *)scm_to_pointer(ptr))->v1 = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_font_set_next(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->next = (struct nk_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_handle(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font *)scm_to_pointer(ptr))->handle, NULL);
}
SCM nk_font_set_handle(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->handle = *(struct nk_user_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_info(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font *)scm_to_pointer(ptr))->info, NULL);
}
SCM nk_font_set_info(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->info = *(struct nk_baked_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_scale(SCM ptr)
{
  return scm_from_double(((struct nk_font *)scm_to_pointer(ptr))->scale);
}
SCM nk_font_set_scale(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->scale = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_glyphs(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font *)scm_to_pointer(ptr))->glyphs, NULL);
}
SCM nk_font_set_glyphs(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->glyphs = (struct nk_font_glyph *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_fallback(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font *)scm_to_pointer(ptr))->fallback, NULL);
}
SCM nk_font_set_fallback(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->fallback = (struct nk_font_glyph *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_fallback_codepoint(SCM ptr)
{
  return scm_from_uint32(((struct nk_font *)scm_to_pointer(ptr))->fallback_codepoint);
}
SCM nk_font_set_fallback_codepoint(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->fallback_codepoint = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_texture(SCM ptr)
{
  return scm_from_pointer(((struct nk_font *)scm_to_pointer(ptr))->texture.ptr, NULL);
}
SCM nk_font_set_texture(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->texture = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_font_get_config(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font *)scm_to_pointer(ptr))->config, NULL);
}
SCM nk_font_set_config(SCM ptr, SCM value)
{
  ((struct nk_font *)scm_to_pointer(ptr))->config = (struct nk_font_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_pixel(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->pixel, NULL);
}
SCM nk_font_atlas_set_pixel(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->pixel = (void *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_tex_width(SCM ptr)
{
  return scm_from_int(((struct nk_font_atlas *)scm_to_pointer(ptr))->tex_width);
}
SCM nk_font_atlas_set_tex_width(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->tex_width = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_tex_height(SCM ptr)
{
  return scm_from_int(((struct nk_font_atlas *)scm_to_pointer(ptr))->tex_height);
}
SCM nk_font_atlas_set_tex_height(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->tex_height = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_permanent(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font_atlas *)scm_to_pointer(ptr))->permanent, NULL);
}
SCM nk_font_atlas_set_permanent(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->permanent = *(struct nk_allocator *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_temporary(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font_atlas *)scm_to_pointer(ptr))->temporary, NULL);
}
SCM nk_font_atlas_set_temporary(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->temporary = *(struct nk_allocator *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_custom(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_font_atlas *)scm_to_pointer(ptr))->custom, NULL);
}
SCM nk_font_atlas_set_custom(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->custom = *(struct nk_recti *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_cursors(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->cursors, NULL);
}
SCM nk_font_atlas_set_cursors(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_font_atlas *)scm_to_pointer(ptr))->cursors, (struct nk_cursor *)scm_to_pointer(value), sizeof(((struct nk_font_atlas *)scm_to_pointer(ptr))->cursors));
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_glyph_count(SCM ptr)
{
  return scm_from_int(((struct nk_font_atlas *)scm_to_pointer(ptr))->glyph_count);
}
SCM nk_font_atlas_set_glyph_count(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->glyph_count = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_glyphs(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->glyphs, NULL);
}
SCM nk_font_atlas_set_glyphs(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->glyphs = (struct nk_font_glyph *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_default_font(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->default_font, NULL);
}
SCM nk_font_atlas_set_default_font(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->default_font = (struct nk_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_fonts(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->fonts, NULL);
}
SCM nk_font_atlas_set_fonts(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->fonts = (struct nk_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_config(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_font_atlas *)scm_to_pointer(ptr))->config, NULL);
}
SCM nk_font_atlas_set_config(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->config = (struct nk_font_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_atlas_font_num(SCM ptr)
{
  return scm_from_int(((struct nk_font_atlas *)scm_to_pointer(ptr))->font_num);
}
SCM nk_font_atlas_set_font_num(SCM ptr, SCM value)
{
  ((struct nk_font_atlas *)scm_to_pointer(ptr))->font_num = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_font_default_glyph_ranges_wrapper()
{
  return scm_from_pointer((void *)nk_font_default_glyph_ranges(), NULL);
}

SCM nk_font_chinese_glyph_ranges_wrapper()
{
  return scm_from_pointer((void *)nk_font_chinese_glyph_ranges(), NULL);
}

SCM nk_font_cyrillic_glyph_ranges_wrapper()
{
  return scm_from_pointer((void *)nk_font_cyrillic_glyph_ranges(), NULL);
}

SCM nk_font_korean_glyph_ranges_wrapper()
{
  return scm_from_pointer((void *)nk_font_korean_glyph_ranges(), NULL);
}

SCM nk_font_atlas_init_default_wrapper(SCM arg_1)
{
  nk_font_atlas_init_default((struct nk_font_atlas *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_font_atlas_init_wrapper(SCM arg_1, SCM arg_2)
{
  nk_font_atlas_init((struct nk_font_atlas *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_font_atlas_init_custom_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_font_atlas_init_custom((struct nk_font_atlas *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2), (struct nk_allocator *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_font_atlas_begin_wrapper(SCM arg_1)
{
  nk_font_atlas_begin((struct nk_font_atlas *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_font_config_wrapper(SCM arg_1)
{
  struct nk_font_config * s = scm_gc_malloc_pointerless(sizeof(struct nk_font_config), "struct nk_font_config");
  *s = nk_font_config(scm_to_double(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_font_atlas_add_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk_font_atlas_add((struct nk_font_atlas *)scm_to_pointer(arg_1), (struct nk_font_config *)scm_to_pointer(arg_2)), NULL);
}

SCM nk_font_atlas_add_default_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_pointer((void *)nk_font_atlas_add_default((struct nk_font_atlas *)scm_to_pointer(arg_1), scm_to_double(arg_2), (struct nk_font_config *)scm_to_pointer(arg_3)), NULL);
}

SCM nk_font_atlas_add_from_memory_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_pointer((void *)nk_font_atlas_add_from_memory((struct nk_font_atlas *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3), scm_to_double(arg_4), (struct nk_font_config *)scm_to_pointer(arg_5)), NULL);
}

SCM nk_font_atlas_add_from_file_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_pointer((void *)nk_font_atlas_add_from_file((struct nk_font_atlas *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), (struct nk_font_config *)scm_to_pointer(arg_4)), NULL);
}

SCM nk_font_atlas_add_compressed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  return scm_from_pointer((void *)nk_font_atlas_add_compressed((struct nk_font_atlas *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3), scm_to_double(arg_4), (struct nk_font_config *)scm_to_pointer(arg_5)), NULL);
}

SCM nk_font_atlas_add_compressed_base85_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_pointer((void *)nk_font_atlas_add_compressed_base85((struct nk_font_atlas *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_double(arg_3), (struct nk_font_config *)scm_to_pointer(arg_4)), NULL);
}

SCM nk_font_atlas_bake_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_pointer((void *)nk_font_atlas_bake((struct nk_font_atlas *)scm_to_pointer(arg_1), (int *)scm_to_pointer(arg_2), (int *)scm_to_pointer(arg_3), scm_to_int(arg_4)), NULL);
}

SCM nk_font_atlas_end_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_font_atlas_end((struct nk_font_atlas *)scm_to_pointer(arg_1), nk_handle_ptr(scm_to_pointer(arg_2)), (struct nk_draw_null_texture *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_font_find_glyph_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk_font_find_glyph((struct nk_font *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)), NULL);
}

SCM nk_font_atlas_cleanup_wrapper(SCM arg_1)
{
  nk_font_atlas_cleanup((struct nk_font_atlas *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_font_atlas_clear_wrapper(SCM arg_1)
{
  nk_font_atlas_clear((struct nk_font_atlas *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_memory_status_memory(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_memory_status *)scm_to_pointer(ptr))->memory, NULL);
}
SCM nk_memory_status_set_memory(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->memory = (void *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_status_type(SCM ptr)
{
  return scm_from_uint(((struct nk_memory_status *)scm_to_pointer(ptr))->type);
}
SCM nk_memory_status_set_type(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->type = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_status_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_memory_status *)scm_to_pointer(ptr))->size);
}
SCM nk_memory_status_set_size(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_status_allocated(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_memory_status *)scm_to_pointer(ptr))->allocated);
}
SCM nk_memory_status_set_allocated(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->allocated = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_status_needed(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_memory_status *)scm_to_pointer(ptr))->needed);
}
SCM nk_memory_status_set_needed(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->needed = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_status_calls(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_memory_status *)scm_to_pointer(ptr))->calls);
}
SCM nk_memory_status_set_calls(SCM ptr, SCM value)
{
  ((struct nk_memory_status *)scm_to_pointer(ptr))->calls = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_marker_active(SCM ptr)
{
  return scm_from_int(((struct nk_buffer_marker *)scm_to_pointer(ptr))->active);
}
SCM nk_buffer_marker_set_active(SCM ptr, SCM value)
{
  ((struct nk_buffer_marker *)scm_to_pointer(ptr))->active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_marker_offset(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_buffer_marker *)scm_to_pointer(ptr))->offset);
}
SCM nk_buffer_marker_set_offset(SCM ptr, SCM value)
{
  ((struct nk_buffer_marker *)scm_to_pointer(ptr))->offset = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_ptr(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_memory *)scm_to_pointer(ptr))->ptr, NULL);
}
SCM nk_memory_set_ptr(SCM ptr, SCM value)
{
  ((struct nk_memory *)scm_to_pointer(ptr))->ptr = (void *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_memory_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_memory *)scm_to_pointer(ptr))->size);
}
SCM nk_memory_set_size(SCM ptr, SCM value)
{
  ((struct nk_memory *)scm_to_pointer(ptr))->size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_marker(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_buffer *)scm_to_pointer(ptr))->marker, NULL);
}
SCM nk_buffer_set_marker(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_buffer *)scm_to_pointer(ptr))->marker, (struct nk_buffer_marker *)scm_to_pointer(value), sizeof(((struct nk_buffer *)scm_to_pointer(ptr))->marker));
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_pool(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_buffer *)scm_to_pointer(ptr))->pool, NULL);
}
SCM nk_buffer_set_pool(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->pool = *(struct nk_allocator *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_type(SCM ptr)
{
  return scm_from_int(((struct nk_buffer *)scm_to_pointer(ptr))->type);
}
SCM nk_buffer_set_type(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_get_memory(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_buffer *)scm_to_pointer(ptr))->memory, NULL);
}
SCM nk_buffer_set_memory(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->memory = *(struct nk_memory *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_grow_factor(SCM ptr)
{
  return scm_from_double(((struct nk_buffer *)scm_to_pointer(ptr))->grow_factor);
}
SCM nk_buffer_set_grow_factor(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->grow_factor = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_allocated(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_buffer *)scm_to_pointer(ptr))->allocated);
}
SCM nk_buffer_set_allocated(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->allocated = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_needed(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_buffer *)scm_to_pointer(ptr))->needed);
}
SCM nk_buffer_set_needed(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->needed = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_calls(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_buffer *)scm_to_pointer(ptr))->calls);
}
SCM nk_buffer_set_calls(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->calls = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_buffer *)scm_to_pointer(ptr))->size);
}
SCM nk_buffer_set_size(SCM ptr, SCM value)
{
  ((struct nk_buffer *)scm_to_pointer(ptr))->size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_buffer_init_default_wrapper(SCM arg_1)
{
  nk_buffer_init_default((struct nk_buffer *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_init_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_buffer_init((struct nk_buffer *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_init_fixed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_buffer_init_fixed((struct nk_buffer *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_info_wrapper(SCM arg_1, SCM arg_2)
{
  nk_buffer_info((struct nk_memory_status *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_push_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_buffer_push((struct nk_buffer *)scm_to_pointer(arg_1), scm_to_int(arg_2), (void *)scm_to_pointer(arg_3), scm_to_uintptr_t(arg_4), scm_to_uintptr_t(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_mark_wrapper(SCM arg_1, SCM arg_2)
{
  nk_buffer_mark((struct nk_buffer *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_reset_wrapper(SCM arg_1, SCM arg_2)
{
  nk_buffer_reset((struct nk_buffer *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_clear_wrapper(SCM arg_1)
{
  nk_buffer_clear((struct nk_buffer *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_free_wrapper(SCM arg_1)
{
  nk_buffer_free((struct nk_buffer *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_buffer_memory_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_buffer_memory((struct nk_buffer *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_buffer_memory_const_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_buffer_memory_const((struct nk_buffer *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_buffer_total_wrapper(SCM arg_1)
{
  return scm_from_uintptr_t(nk_buffer_total((struct nk_buffer *)scm_to_pointer(arg_1)));
}

SCM nk_str_buffer(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_str *)scm_to_pointer(ptr))->buffer, NULL);
}
SCM nk_str_set_buffer(SCM ptr, SCM value)
{
  ((struct nk_str *)scm_to_pointer(ptr))->buffer = *(struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_str_get_len(SCM ptr)
{
  return scm_from_int(((struct nk_str *)scm_to_pointer(ptr))->len);
}
SCM nk_str_set_len(SCM ptr, SCM value)
{
  ((struct nk_str *)scm_to_pointer(ptr))->len = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_str_init_default_wrapper(SCM arg_1)
{
  nk_str_init_default((struct nk_str *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_str_init_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_str_init((struct nk_str *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_str_init_fixed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_str_init_fixed((struct nk_str *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_str_clear_wrapper(SCM arg_1)
{
  nk_str_clear((struct nk_str *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_str_free_wrapper(SCM arg_1)
{
  nk_str_free((struct nk_str *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_str_append_text_char_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_append_text_char((struct nk_str *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_str_append_str_char_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_str_append_str_char((struct nk_str *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_str_append_text_utf8_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_append_text_utf8((struct nk_str *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_str_append_str_utf8_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_str_append_str_utf8((struct nk_str *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2)));
}

SCM nk_str_append_text_runes_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_append_text_runes((struct nk_str *)scm_to_pointer(arg_1), (nk_rune *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_str_append_str_runes_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_str_append_str_runes((struct nk_str *)scm_to_pointer(arg_1), (nk_rune *)scm_to_pointer(arg_2)));
}

SCM nk_str_insert_at_char_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_str_insert_at_char((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_str_insert_at_rune_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_str_insert_at_rune((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_str_insert_text_char_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_str_insert_text_char((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_str_insert_str_char_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_insert_str_char((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3)));
}

SCM nk_str_insert_text_utf8_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_str_insert_text_utf8((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_str_insert_str_utf8_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_insert_str_utf8((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (char *)scm_to_pointer(arg_3)));
}

SCM nk_str_insert_text_runes_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_str_insert_text_runes((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (nk_rune *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_str_insert_str_runes_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_str_insert_str_runes((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (nk_rune *)scm_to_pointer(arg_3)));
}

SCM nk_str_remove_chars_wrapper(SCM arg_1, SCM arg_2)
{
  nk_str_remove_chars((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_str_remove_runes_wrapper(SCM arg_1, SCM arg_2)
{
  nk_str_remove_runes((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_str_delete_chars_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_str_delete_chars((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_str_delete_runes_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_str_delete_runes((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_str_at_char_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk_str_at_char((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2)), NULL);
}

SCM nk_str_at_rune_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_pointer((void *)nk_str_at_rune((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (nk_rune *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4)), NULL);
}

SCM nk_str_rune_at_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_uint32(nk_str_rune_at((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_str_at_char_const_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk_str_at_char_const((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2)), NULL);
}

SCM nk_str_at_const_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_pointer((void *)nk_str_at_const((struct nk_str *)scm_to_pointer(arg_1), scm_to_int(arg_2), (nk_rune *)scm_to_pointer(arg_3), (int *)scm_to_pointer(arg_4)), NULL);
}

SCM nk_str_get_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_str_get((struct nk_str *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_str_get_const_wrapper(SCM arg_1)
{
  return scm_from_pointer((void *)nk_str_get_const((struct nk_str *)scm_to_pointer(arg_1)), NULL);
}

SCM nk_str_len_wrapper(SCM arg_1)
{
  return scm_from_int(nk_str_len((struct nk_str *)scm_to_pointer(arg_1)));
}

SCM nk_str_len_char_wrapper(SCM arg_1)
{
  return scm_from_int(nk_str_len_char((struct nk_str *)scm_to_pointer(arg_1)));
}

SCM nk_clipboard_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_clipboard *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_clipboard_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_clipboard *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_clipboard_paste(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_clipboard *)scm_to_pointer(ptr))->paste, NULL);
}
SCM nk_clipboard_set_paste(SCM ptr, SCM value)
{
  ((struct nk_clipboard *)scm_to_pointer(ptr))->paste = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_clipboard_copy(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_clipboard *)scm_to_pointer(ptr))->copy, NULL);
}
SCM nk_clipboard_set_copy(SCM ptr, SCM value)
{
  ((struct nk_clipboard *)scm_to_pointer(ptr))->copy = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_record_where(SCM ptr)
{
  return scm_from_int(((struct nk_text_undo_record *)scm_to_pointer(ptr))->where);
}
SCM nk_text_undo_record_set_where(SCM ptr, SCM value)
{
  ((struct nk_text_undo_record *)scm_to_pointer(ptr))->where = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_record_insert_length(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_record *)scm_to_pointer(ptr))->insert_length);
}
SCM nk_text_undo_record_set_insert_length(SCM ptr, SCM value)
{
  ((struct nk_text_undo_record *)scm_to_pointer(ptr))->insert_length = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_record_delete_length(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_record *)scm_to_pointer(ptr))->delete_length);
}
SCM nk_text_undo_record_set_delete_length(SCM ptr, SCM value)
{
  ((struct nk_text_undo_record *)scm_to_pointer(ptr))->delete_length = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_record_char_storage(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_record *)scm_to_pointer(ptr))->char_storage);
}
SCM nk_text_undo_record_set_char_storage(SCM ptr, SCM value)
{
  ((struct nk_text_undo_record *)scm_to_pointer(ptr))->char_storage = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_undo_rec(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_rec, NULL);
}
SCM nk_text_undo_state_set_undo_rec(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_rec, (struct nk_text_undo_record *)scm_to_pointer(value), sizeof(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_rec));
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_undo_char(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_char, NULL);
}
SCM nk_text_undo_state_set_undo_char(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_char, (nk_rune *)scm_to_pointer(value), sizeof(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_char));
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_undo_point(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_point);
}
SCM nk_text_undo_state_set_undo_point(SCM ptr, SCM value)
{
  ((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_point = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_redo_point(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_state *)scm_to_pointer(ptr))->redo_point);
}
SCM nk_text_undo_state_set_redo_point(SCM ptr, SCM value)
{
  ((struct nk_text_undo_state *)scm_to_pointer(ptr))->redo_point = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_undo_char_point(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_char_point);
}
SCM nk_text_undo_state_set_undo_char_point(SCM ptr, SCM value)
{
  ((struct nk_text_undo_state *)scm_to_pointer(ptr))->undo_char_point = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_undo_state_redo_char_point(SCM ptr)
{
  return scm_from_short(((struct nk_text_undo_state *)scm_to_pointer(ptr))->redo_char_point);
}
SCM nk_text_undo_state_set_redo_char_point(SCM ptr, SCM value)
{
  ((struct nk_text_undo_state *)scm_to_pointer(ptr))->redo_char_point = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_clip(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_text_edit *)scm_to_pointer(ptr))->clip, NULL);
}
SCM nk_text_edit_set_clip(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->clip = *(struct nk_clipboard *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_string(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_text_edit *)scm_to_pointer(ptr))->string, NULL);
}
SCM nk_text_edit_set_string(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->string = *(struct nk_str *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_filter(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_text_edit *)scm_to_pointer(ptr))->filter, NULL);
}
SCM nk_text_edit_set_filter(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->filter = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_scrollbar(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_text_edit *)scm_to_pointer(ptr))->scrollbar, NULL);
}
SCM nk_text_edit_set_scrollbar(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->scrollbar = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_cursor(SCM ptr)
{
  return scm_from_int(((struct nk_text_edit *)scm_to_pointer(ptr))->cursor);
}
SCM nk_text_edit_set_cursor(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->cursor = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_select_start(SCM ptr)
{
  return scm_from_int(((struct nk_text_edit *)scm_to_pointer(ptr))->select_start);
}
SCM nk_text_edit_set_select_start(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->select_start = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_select_end(SCM ptr)
{
  return scm_from_int(((struct nk_text_edit *)scm_to_pointer(ptr))->select_end);
}
SCM nk_text_edit_set_select_end(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->select_end = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_mode(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->mode);
}
SCM nk_text_edit_set_mode(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->mode = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_cursor_at_end_of_line(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->cursor_at_end_of_line);
}
SCM nk_text_edit_set_cursor_at_end_of_line(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->cursor_at_end_of_line = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_initialized(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->initialized);
}
SCM nk_text_edit_set_initialized(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->initialized = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_has_preferred_x(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->has_preferred_x);
}
SCM nk_text_edit_set_has_preferred_x(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->has_preferred_x = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_single_line(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->single_line);
}
SCM nk_text_edit_set_single_line(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->single_line = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_active(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->active);
}
SCM nk_text_edit_set_active(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->active = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_padding1(SCM ptr)
{
  return scm_from_uchar(((struct nk_text_edit *)scm_to_pointer(ptr))->padding1);
}
SCM nk_text_edit_set_padding1(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->padding1 = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_preferred_x(SCM ptr)
{
  return scm_from_double(((struct nk_text_edit *)scm_to_pointer(ptr))->preferred_x);
}
SCM nk_text_edit_set_preferred_x(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->preferred_x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_text_edit_undo(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_text_edit *)scm_to_pointer(ptr))->undo, NULL);
}
SCM nk_text_edit_set_undo(SCM ptr, SCM value)
{
  ((struct nk_text_edit *)scm_to_pointer(ptr))->undo = *(struct nk_text_undo_state *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_filter_default_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_default((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_ascii_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_ascii((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_float_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_float((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_decimal_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_decimal((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_hex_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_hex((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_oct_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_oct((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_filter_binary_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_filter_binary((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_uint32(arg_2)));
}

SCM nk_textedit_init_default_wrapper(SCM arg_1)
{
  nk_textedit_init_default((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_init_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_textedit_init((struct nk_text_edit *)scm_to_pointer(arg_1), (struct nk_allocator *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_init_fixed_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_textedit_init_fixed((struct nk_text_edit *)scm_to_pointer(arg_1), (void *)scm_to_pointer(arg_2), scm_to_uintptr_t(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_free_wrapper(SCM arg_1)
{
  nk_textedit_free((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_textedit_text((struct nk_text_edit *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_delete_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_textedit_delete((struct nk_text_edit *)scm_to_pointer(arg_1), scm_to_int(arg_2), scm_to_int(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_delete_selection_wrapper(SCM arg_1)
{
  nk_textedit_delete_selection((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_select_all_wrapper(SCM arg_1)
{
  nk_textedit_select_all((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_cut_wrapper(SCM arg_1)
{
  return scm_from_int(nk_textedit_cut((struct nk_text_edit *)scm_to_pointer(arg_1)));
}

SCM nk_textedit_paste_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_textedit_paste((struct nk_text_edit *)scm_to_pointer(arg_1), (char *)scm_to_pointer(arg_2), scm_to_int(arg_3)));
}

SCM nk_textedit_undo_wrapper(SCM arg_1)
{
  nk_textedit_undo((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_textedit_redo_wrapper(SCM arg_1)
{
  nk_textedit_redo((struct nk_text_edit *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_command_type(SCM ptr)
{
  return scm_from_int(((struct nk_command *)scm_to_pointer(ptr))->type);
}
SCM nk_command_set_type(SCM ptr, SCM value)
{
  ((struct nk_command *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_next(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_command *)scm_to_pointer(ptr))->next);
}
SCM nk_command_set_next(SCM ptr, SCM value)
{
  ((struct nk_command *)scm_to_pointer(ptr))->next = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_scissor_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_scissor *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_scissor_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_scissor *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_scissor_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_scissor *)scm_to_pointer(ptr))->x);
}
SCM nk_command_scissor_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_scissor *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_scissor_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_scissor *)scm_to_pointer(ptr))->y);
}
SCM nk_command_scissor_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_scissor *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_scissor_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_scissor *)scm_to_pointer(ptr))->w);
}
SCM nk_command_scissor_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_scissor *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_scissor_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_scissor *)scm_to_pointer(ptr))->h);
}
SCM nk_command_scissor_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_scissor *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_line_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_line *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_line_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_line *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_line_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_line *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_line_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_line *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_line_begin(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_line *)scm_to_pointer(ptr))->begin, NULL);
}
SCM nk_command_line_set_begin(SCM ptr, SCM value)
{
  ((struct nk_command_line *)scm_to_pointer(ptr))->begin = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_line_end(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_line *)scm_to_pointer(ptr))->end, NULL);
}
SCM nk_command_line_set_end(SCM ptr, SCM value)
{
  ((struct nk_command_line *)scm_to_pointer(ptr))->end = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_line_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_line *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_line_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_line *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_curve *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_curve_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_curve *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_curve *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_curve_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_curve *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_begin(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_curve *)scm_to_pointer(ptr))->begin, NULL);
}
SCM nk_command_curve_set_begin(SCM ptr, SCM value)
{
  ((struct nk_command_curve *)scm_to_pointer(ptr))->begin = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_end(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_curve *)scm_to_pointer(ptr))->end, NULL);
}
SCM nk_command_curve_set_end(SCM ptr, SCM value)
{
  ((struct nk_command_curve *)scm_to_pointer(ptr))->end = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_ctrl(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_curve *)scm_to_pointer(ptr))->ctrl, NULL);
}
SCM nk_command_curve_set_ctrl(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_curve *)scm_to_pointer(ptr))->ctrl, (struct nk_vec2i *)scm_to_pointer(value), sizeof(((struct nk_command_curve *)scm_to_pointer(ptr))->ctrl));
  return SCM_UNSPECIFIED;
}
SCM nk_command_curve_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_curve *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_curve_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_curve *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_rect_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_rounding(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect *)scm_to_pointer(ptr))->rounding);
}
SCM nk_command_rect_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->rounding = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_rect_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect *)scm_to_pointer(ptr))->x);
}
SCM nk_command_rect_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect *)scm_to_pointer(ptr))->y);
}
SCM nk_command_rect_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect *)scm_to_pointer(ptr))->w);
}
SCM nk_command_rect_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect *)scm_to_pointer(ptr))->h);
}
SCM nk_command_rect_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_rect_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_rect *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_filled *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_rect_filled_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_rounding(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect_filled *)scm_to_pointer(ptr))->rounding);
}
SCM nk_command_rect_filled_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->rounding = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect_filled *)scm_to_pointer(ptr))->x);
}
SCM nk_command_rect_filled_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect_filled *)scm_to_pointer(ptr))->y);
}
SCM nk_command_rect_filled_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect_filled *)scm_to_pointer(ptr))->w);
}
SCM nk_command_rect_filled_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect_filled *)scm_to_pointer(ptr))->h);
}
SCM nk_command_rect_filled_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_filled_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_filled *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_rect_filled_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_rect_filled *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_rect_multi_color_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->x);
}
SCM nk_command_rect_multi_color_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->y);
}
SCM nk_command_rect_multi_color_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->w);
}
SCM nk_command_rect_multi_color_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->h);
}
SCM nk_command_rect_multi_color_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_left(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->left, NULL);
}
SCM nk_command_rect_multi_color_set_left(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->left = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_top(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->top, NULL);
}
SCM nk_command_rect_multi_color_set_top(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->top = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_bottom(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->bottom, NULL);
}
SCM nk_command_rect_multi_color_set_bottom(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->bottom = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_rect_multi_color_right(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->right, NULL);
}
SCM nk_command_rect_multi_color_set_right(SCM ptr, SCM value)
{
  ((struct nk_command_rect_multi_color *)scm_to_pointer(ptr))->right = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_triangle_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_triangle *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_triangle_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_a(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle *)scm_to_pointer(ptr))->a, NULL);
}
SCM nk_command_triangle_set_a(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->a = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_b(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle *)scm_to_pointer(ptr))->b, NULL);
}
SCM nk_command_triangle_set_b(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->b = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_c(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle *)scm_to_pointer(ptr))->c, NULL);
}
SCM nk_command_triangle_set_c(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->c = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_triangle_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_triangle *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_filled_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_triangle_filled_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_filled_a(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->a, NULL);
}
SCM nk_command_triangle_filled_set_a(SCM ptr, SCM value)
{
  ((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->a = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_filled_b(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->b, NULL);
}
SCM nk_command_triangle_filled_set_b(SCM ptr, SCM value)
{
  ((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->b = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_filled_c(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->c, NULL);
}
SCM nk_command_triangle_filled_set_c(SCM ptr, SCM value)
{
  ((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->c = *(struct nk_vec2i *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_triangle_filled_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_triangle_filled_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_triangle_filled *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_circle *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_circle_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_circle *)scm_to_pointer(ptr))->x);
}
SCM nk_command_circle_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_circle *)scm_to_pointer(ptr))->y);
}
SCM nk_command_circle_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_circle *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_circle_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_circle *)scm_to_pointer(ptr))->w);
}
SCM nk_command_circle_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_circle *)scm_to_pointer(ptr))->h);
}
SCM nk_command_circle_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_circle *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_circle_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_circle *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_circle_filled *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_circle_filled_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_circle_filled *)scm_to_pointer(ptr))->x);
}
SCM nk_command_circle_filled_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_circle_filled *)scm_to_pointer(ptr))->y);
}
SCM nk_command_circle_filled_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_circle_filled *)scm_to_pointer(ptr))->w);
}
SCM nk_command_circle_filled_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_circle_filled *)scm_to_pointer(ptr))->h);
}
SCM nk_command_circle_filled_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_circle_filled_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_circle_filled *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_circle_filled_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_circle_filled *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_arc *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_arc_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_cx(SCM ptr)
{
  return scm_from_short(((struct nk_command_arc *)scm_to_pointer(ptr))->cx);
}
SCM nk_command_arc_set_cx(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->cx = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_cy(SCM ptr)
{
  return scm_from_short(((struct nk_command_arc *)scm_to_pointer(ptr))->cy);
}
SCM nk_command_arc_set_cy(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->cy = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_r(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_arc *)scm_to_pointer(ptr))->r);
}
SCM nk_command_arc_set_r(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->r = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_arc *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_arc_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_a(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_arc *)scm_to_pointer(ptr))->a, NULL);
}
SCM nk_command_arc_set_a(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_arc *)scm_to_pointer(ptr))->a, (float *)scm_to_pointer(value), sizeof(((struct nk_command_arc *)scm_to_pointer(ptr))->a));
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_arc *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_arc_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_arc *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_arc_filled *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_arc_filled_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_arc_filled *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_cx(SCM ptr)
{
  return scm_from_short(((struct nk_command_arc_filled *)scm_to_pointer(ptr))->cx);
}
SCM nk_command_arc_filled_set_cx(SCM ptr, SCM value)
{
  ((struct nk_command_arc_filled *)scm_to_pointer(ptr))->cx = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_cy(SCM ptr)
{
  return scm_from_short(((struct nk_command_arc_filled *)scm_to_pointer(ptr))->cy);
}
SCM nk_command_arc_filled_set_cy(SCM ptr, SCM value)
{
  ((struct nk_command_arc_filled *)scm_to_pointer(ptr))->cy = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_r(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_arc_filled *)scm_to_pointer(ptr))->r);
}
SCM nk_command_arc_filled_set_r(SCM ptr, SCM value)
{
  ((struct nk_command_arc_filled *)scm_to_pointer(ptr))->r = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_a(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_arc_filled *)scm_to_pointer(ptr))->a, NULL);
}
SCM nk_command_arc_filled_set_a(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_arc_filled *)scm_to_pointer(ptr))->a, (float *)scm_to_pointer(value), sizeof(((struct nk_command_arc_filled *)scm_to_pointer(ptr))->a));
  return SCM_UNSPECIFIED;
}
SCM nk_command_arc_filled_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_arc_filled *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_arc_filled_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_arc_filled *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polygon *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_polygon_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_polygon *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polygon *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_polygon_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_polygon *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_polygon *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_polygon_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_polygon *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_point_count(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_polygon *)scm_to_pointer(ptr))->point_count);
}
SCM nk_command_polygon_set_point_count(SCM ptr, SCM value)
{
  ((struct nk_command_polygon *)scm_to_pointer(ptr))->point_count = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_points(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_polygon *)scm_to_pointer(ptr))->points, NULL);
}
SCM nk_command_polygon_set_points(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_polygon *)scm_to_pointer(ptr))->points, (struct nk_vec2i *)scm_to_pointer(value), sizeof(((struct nk_command_polygon *)scm_to_pointer(ptr))->points));
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_filled_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_polygon_filled_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_filled_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_polygon_filled_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_filled_point_count(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->point_count);
}
SCM nk_command_polygon_filled_set_point_count(SCM ptr, SCM value)
{
  ((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->point_count = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polygon_filled_points(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->points, NULL);
}
SCM nk_command_polygon_filled_set_points(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->points, (struct nk_vec2i *)scm_to_pointer(value), sizeof(((struct nk_command_polygon_filled *)scm_to_pointer(ptr))->points));
  return SCM_UNSPECIFIED;
}
SCM nk_command_polyline_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polyline *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_polyline_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_polyline *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polyline_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_polyline *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_command_polyline_set_color(SCM ptr, SCM value)
{
  ((struct nk_command_polyline *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polyline_line_thickness(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_polyline *)scm_to_pointer(ptr))->line_thickness);
}
SCM nk_command_polyline_set_line_thickness(SCM ptr, SCM value)
{
  ((struct nk_command_polyline *)scm_to_pointer(ptr))->line_thickness = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polyline_point_count(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_polyline *)scm_to_pointer(ptr))->point_count);
}
SCM nk_command_polyline_set_point_count(SCM ptr, SCM value)
{
  ((struct nk_command_polyline *)scm_to_pointer(ptr))->point_count = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_polyline_points(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_polyline *)scm_to_pointer(ptr))->points, NULL);
}
SCM nk_command_polyline_set_points(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_polyline *)scm_to_pointer(ptr))->points, (struct nk_vec2i *)scm_to_pointer(value), sizeof(((struct nk_command_polyline *)scm_to_pointer(ptr))->points));
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_image *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_image_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_image *)scm_to_pointer(ptr))->x);
}
SCM nk_command_image_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_image *)scm_to_pointer(ptr))->y);
}
SCM nk_command_image_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_image *)scm_to_pointer(ptr))->w);
}
SCM nk_command_image_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_image *)scm_to_pointer(ptr))->h);
}
SCM nk_command_image_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_img(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_image *)scm_to_pointer(ptr))->img, NULL);
}
SCM nk_command_image_set_img(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->img = *(struct nk_image *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_image_col(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_image *)scm_to_pointer(ptr))->col, NULL);
}
SCM nk_command_image_set_col(SCM ptr, SCM value)
{
  ((struct nk_command_image *)scm_to_pointer(ptr))->col = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_custom *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_custom_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_custom *)scm_to_pointer(ptr))->x);
}
SCM nk_command_custom_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_custom *)scm_to_pointer(ptr))->y);
}
SCM nk_command_custom_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_custom *)scm_to_pointer(ptr))->w);
}
SCM nk_command_custom_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_custom *)scm_to_pointer(ptr))->h);
}
SCM nk_command_custom_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_callback_data(SCM ptr)
{
  return scm_from_pointer(((struct nk_command_custom *)scm_to_pointer(ptr))->callback_data.ptr, NULL);
}
SCM nk_command_custom_set_callback_data(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->callback_data = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_command_custom_get_callback(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_custom *)scm_to_pointer(ptr))->callback, NULL);
}
SCM nk_command_custom_set_callback(SCM ptr, SCM value)
{
  ((struct nk_command_custom *)scm_to_pointer(ptr))->callback = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_text *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_command_text_set_header(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->header = *(struct nk_command *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_font(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_text *)scm_to_pointer(ptr))->font, NULL);
}
SCM nk_command_text_set_font(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->font = (struct nk_user_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_text *)scm_to_pointer(ptr))->background, NULL);
}
SCM nk_command_text_set_background(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->background = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_foreground(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_text *)scm_to_pointer(ptr))->foreground, NULL);
}
SCM nk_command_text_set_foreground(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->foreground = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_x(SCM ptr)
{
  return scm_from_short(((struct nk_command_text *)scm_to_pointer(ptr))->x);
}
SCM nk_command_text_set_x(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->x = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_y(SCM ptr)
{
  return scm_from_short(((struct nk_command_text *)scm_to_pointer(ptr))->y);
}
SCM nk_command_text_set_y(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->y = scm_to_short(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_w(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_text *)scm_to_pointer(ptr))->w);
}
SCM nk_command_text_set_w(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->w = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_h(SCM ptr)
{
  return scm_from_ushort(((struct nk_command_text *)scm_to_pointer(ptr))->h);
}
SCM nk_command_text_set_h(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->h = scm_to_ushort(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_height(SCM ptr)
{
  return scm_from_double(((struct nk_command_text *)scm_to_pointer(ptr))->height);
}
SCM nk_command_text_set_height(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_length(SCM ptr)
{
  return scm_from_int(((struct nk_command_text *)scm_to_pointer(ptr))->length);
}
SCM nk_command_text_set_length(SCM ptr, SCM value)
{
  ((struct nk_command_text *)scm_to_pointer(ptr))->length = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_text_string(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_text *)scm_to_pointer(ptr))->string, NULL);
}
SCM nk_command_text_set_string(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_command_text *)scm_to_pointer(ptr))->string, (char *)scm_to_pointer(value), sizeof(((struct nk_command_text *)scm_to_pointer(ptr))->string));
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_base(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_command_buffer *)scm_to_pointer(ptr))->base, NULL);
}
SCM nk_command_buffer_set_base(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->base = (struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_clip(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_command_buffer *)scm_to_pointer(ptr))->clip, NULL);
}
SCM nk_command_buffer_set_clip(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->clip = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_use_clipping(SCM ptr)
{
  return scm_from_int(((struct nk_command_buffer *)scm_to_pointer(ptr))->use_clipping);
}
SCM nk_command_buffer_set_use_clipping(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->use_clipping = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_command_buffer *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_command_buffer_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_begin(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_command_buffer *)scm_to_pointer(ptr))->begin);
}
SCM nk_command_buffer_set_begin(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->begin = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_end(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_command_buffer *)scm_to_pointer(ptr))->end);
}
SCM nk_command_buffer_set_end(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->end = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_command_buffer_last(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_command_buffer *)scm_to_pointer(ptr))->last);
}
SCM nk_command_buffer_set_last(SCM ptr, SCM value)
{
  ((struct nk_command_buffer *)scm_to_pointer(ptr))->last = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_stroke_line_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_stroke_line((struct nk_command_buffer *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), *(struct nk_color *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_curve_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_stroke_curve((struct nk_command_buffer *)scm_to_pointer(arg_1),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(0))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(1))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(2))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(3))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(4))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(5))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(6))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(7))),
                  scm_to_double(scm_list_ref(arg_2, scm_from_int(8))),
                  *(struct nk_color *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_stroke_rect((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_circle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_stroke_circle((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), scm_to_double(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_arc_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8)
{
  nk_stroke_arc((struct nk_command_buffer *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7), *(struct nk_color *)scm_to_pointer(arg_8));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_triangle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8, SCM arg_9)
{
  nk_stroke_triangle((struct nk_command_buffer *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7), scm_to_double(arg_8), *(struct nk_color *)scm_to_pointer(arg_9));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_polyline_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_stroke_polyline((struct nk_command_buffer *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_double(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_stroke_polygon_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_stroke_polygon((struct nk_command_buffer *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_double(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_fill_rect((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), scm_to_double(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_rect_multi_color_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_fill_rect_multi_color((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5), *(struct nk_color *)scm_to_pointer(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_circle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  nk_fill_circle((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_arc_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_fill_arc((struct nk_command_buffer *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), *(struct nk_color *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_triangle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8)
{
  nk_fill_triangle((struct nk_command_buffer *)scm_to_pointer(arg_1), scm_to_double(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_double(arg_6), scm_to_double(arg_7), *(struct nk_color *)scm_to_pointer(arg_8));
  return SCM_UNSPECIFIED;
}

SCM nk_fill_polygon_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_fill_polygon((struct nk_command_buffer *)scm_to_pointer(arg_1), (float *)scm_to_pointer(arg_2), scm_to_int(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_image_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_draw_image((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), (struct nk_image *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_draw_text((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), (char *)scm_to_pointer(arg_3), scm_to_int(arg_4), (struct nk_user_font *)scm_to_pointer(arg_5), *(struct nk_color *)scm_to_pointer(arg_6), *(struct nk_color *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_push_scissor_wrapper(SCM arg_1, SCM arg_2)
{
  nk_push_scissor((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_push_custom_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_push_custom((struct nk_command_buffer *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), scm_to_pointer(arg_3), nk_handle_ptr(scm_to_pointer(arg_4)));
  return SCM_UNSPECIFIED;
}

SCM nk_mouse_button_down(SCM ptr)
{
  return scm_from_int(((struct nk_mouse_button *)scm_to_pointer(ptr))->down);
}
SCM nk_mouse_button_set_down(SCM ptr, SCM value)
{
  ((struct nk_mouse_button *)scm_to_pointer(ptr))->down = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_button_clicked(SCM ptr)
{
  return scm_from_uint(((struct nk_mouse_button *)scm_to_pointer(ptr))->clicked);
}
SCM nk_mouse_button_set_clicked(SCM ptr, SCM value)
{
  ((struct nk_mouse_button *)scm_to_pointer(ptr))->clicked = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_button_clicked_pos(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_mouse_button *)scm_to_pointer(ptr))->clicked_pos, NULL);
}
SCM nk_mouse_button_set_clicked_pos(SCM ptr, SCM value)
{
  ((struct nk_mouse_button *)scm_to_pointer(ptr))->clicked_pos = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_buttons(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_mouse *)scm_to_pointer(ptr))->buttons, NULL);
}
SCM nk_mouse_set_buttons(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_mouse *)scm_to_pointer(ptr))->buttons, (struct nk_mouse_button *)scm_to_pointer(value), sizeof(((struct nk_mouse *)scm_to_pointer(ptr))->buttons));
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_pos(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_mouse *)scm_to_pointer(ptr))->pos, NULL);
}
SCM nk_mouse_set_pos(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->pos = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_prev(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_mouse *)scm_to_pointer(ptr))->prev, NULL);
}
SCM nk_mouse_set_prev(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->prev = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_delta(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_mouse *)scm_to_pointer(ptr))->delta, NULL);
}
SCM nk_mouse_set_delta(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->delta = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_scroll_delta(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_mouse *)scm_to_pointer(ptr))->scroll_delta, NULL);
}
SCM nk_mouse_set_scroll_delta(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->scroll_delta = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_grab(SCM ptr)
{
  return scm_from_uchar(((struct nk_mouse *)scm_to_pointer(ptr))->grab);
}
SCM nk_mouse_set_grab(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->grab = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_grabbed(SCM ptr)
{
  return scm_from_uchar(((struct nk_mouse *)scm_to_pointer(ptr))->grabbed);
}
SCM nk_mouse_set_grabbed(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->grabbed = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_mouse_ungrab(SCM ptr)
{
  return scm_from_uchar(((struct nk_mouse *)scm_to_pointer(ptr))->ungrab);
}
SCM nk_mouse_set_ungrab(SCM ptr, SCM value)
{
  ((struct nk_mouse *)scm_to_pointer(ptr))->ungrab = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_key_down(SCM ptr)
{
  return scm_from_int(((struct nk_key *)scm_to_pointer(ptr))->down);
}
SCM nk_key_set_down(SCM ptr, SCM value)
{
  ((struct nk_key *)scm_to_pointer(ptr))->down = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_key_clicked(SCM ptr)
{
  return scm_from_uint(((struct nk_key *)scm_to_pointer(ptr))->clicked);
}
SCM nk_key_set_clicked(SCM ptr, SCM value)
{
  ((struct nk_key *)scm_to_pointer(ptr))->clicked = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_keyboard_keys(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_keyboard *)scm_to_pointer(ptr))->keys, NULL);
}
SCM nk_keyboard_set_keys(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_keyboard *)scm_to_pointer(ptr))->keys, (struct nk_key *)scm_to_pointer(value), sizeof(((struct nk_keyboard *)scm_to_pointer(ptr))->keys));
  return SCM_UNSPECIFIED;
}
SCM nk_keyboard_text(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_keyboard *)scm_to_pointer(ptr))->text, NULL);
}
SCM nk_keyboard_set_text(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_keyboard *)scm_to_pointer(ptr))->text, (char *)scm_to_pointer(value), sizeof(((struct nk_keyboard *)scm_to_pointer(ptr))->text));
  return SCM_UNSPECIFIED;
}
SCM nk_keyboard_text_len(SCM ptr)
{
  return scm_from_int(((struct nk_keyboard *)scm_to_pointer(ptr))->text_len);
}
SCM nk_keyboard_set_text_len(SCM ptr, SCM value)
{
  ((struct nk_keyboard *)scm_to_pointer(ptr))->text_len = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_input_keyboard(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_input *)scm_to_pointer(ptr))->keyboard, NULL);
}
SCM nk_input_set_keyboard(SCM ptr, SCM value)
{
  ((struct nk_input *)scm_to_pointer(ptr))->keyboard = *(struct nk_keyboard *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_input_mouse(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_input *)scm_to_pointer(ptr))->mouse, NULL);
}
SCM nk_input_set_mouse(SCM ptr, SCM value)
{
  ((struct nk_input *)scm_to_pointer(ptr))->mouse = *(struct nk_mouse *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_input_has_mouse_click_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_has_mouse_click((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_has_mouse_click_in_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_input_has_mouse_click_in_rect((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3)));
}

SCM nk_input_has_mouse_click_down_in_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_input_has_mouse_click_down_in_rect((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_input_is_mouse_click_in_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_input_is_mouse_click_in_rect((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3)));
}

SCM nk_input_is_mouse_click_down_in_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  return scm_from_int(nk_input_is_mouse_click_down_in_rect((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3), scm_to_int(arg_4)));
}

SCM nk_input_any_mouse_click_in_rect_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_any_mouse_click_in_rect((struct nk_input *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2)));
}

SCM nk_input_is_mouse_prev_hovering_rect_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_mouse_prev_hovering_rect((struct nk_input *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2)));
}

SCM nk_input_is_mouse_hovering_rect_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_mouse_hovering_rect((struct nk_input *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2)));
}

SCM nk_input_mouse_clicked_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_int(nk_input_mouse_clicked((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3)));
}

SCM nk_input_is_mouse_down_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_mouse_down((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_is_mouse_pressed_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_mouse_pressed((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_is_mouse_released_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_mouse_released((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_is_key_pressed_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_key_pressed((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_is_key_released_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_key_released((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_input_is_key_down_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_int(nk_input_is_key_down((struct nk_input *)scm_to_pointer(arg_1), scm_to_int(arg_2)));
}

SCM nk_draw_vertex_layout_element_attribute(SCM ptr)
{
  return scm_from_int(((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->attribute);
}
SCM nk_draw_vertex_layout_element_set_attribute(SCM ptr, SCM value)
{
  ((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->attribute = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_vertex_layout_element_format(SCM ptr)
{
  return scm_from_int(((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->format);
}
SCM nk_draw_vertex_layout_element_set_format(SCM ptr, SCM value)
{
  ((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->format = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_vertex_layout_element_offset(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->offset);
}
SCM nk_draw_vertex_layout_element_set_offset(SCM ptr, SCM value)
{
  ((struct nk_draw_vertex_layout_element *)scm_to_pointer(ptr))->offset = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_command_elem_count(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_command *)scm_to_pointer(ptr))->elem_count);
}
SCM nk_draw_command_set_elem_count(SCM ptr, SCM value)
{
  ((struct nk_draw_command *)scm_to_pointer(ptr))->elem_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_command_clip_rect(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_draw_command *)scm_to_pointer(ptr))->clip_rect, NULL);
}
SCM nk_draw_command_set_clip_rect(SCM ptr, SCM value)
{
  ((struct nk_draw_command *)scm_to_pointer(ptr))->clip_rect = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_command_texture(SCM ptr)
{
  return scm_from_pointer(((struct nk_draw_command *)scm_to_pointer(ptr))->texture.ptr, NULL);
}
SCM nk_draw_command_set_texture(SCM ptr, SCM value)
{
  ((struct nk_draw_command *)scm_to_pointer(ptr))->texture = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_clip_rect(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_draw_list *)scm_to_pointer(ptr))->clip_rect, NULL);
}
SCM nk_draw_list_set_clip_rect(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->clip_rect = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_circle_vtx(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_draw_list *)scm_to_pointer(ptr))->circle_vtx, NULL);
}
SCM nk_draw_list_set_circle_vtx(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_draw_list *)scm_to_pointer(ptr))->circle_vtx, (struct nk_vec2 *)scm_to_pointer(value), sizeof(((struct nk_draw_list *)scm_to_pointer(ptr))->circle_vtx));
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_config(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_draw_list *)scm_to_pointer(ptr))->config, NULL);
}
SCM nk_draw_list_set_config(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->config = *(struct nk_convert_config *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_buffer(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_draw_list *)scm_to_pointer(ptr))->buffer, NULL);
}
SCM nk_draw_list_set_buffer(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->buffer = (struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_vertices(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_draw_list *)scm_to_pointer(ptr))->vertices, NULL);
}
SCM nk_draw_list_set_vertices(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->vertices = (struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_draw_list *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_draw_list_set_elements(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->elements = (struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_element_count(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_list *)scm_to_pointer(ptr))->element_count);
}
SCM nk_draw_list_set_element_count(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->element_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_vertex_count(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_list *)scm_to_pointer(ptr))->vertex_count);
}
SCM nk_draw_list_set_vertex_count(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->vertex_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_cmd_count(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_list *)scm_to_pointer(ptr))->cmd_count);
}
SCM nk_draw_list_set_cmd_count(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->cmd_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_cmd_offset(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_draw_list *)scm_to_pointer(ptr))->cmd_offset);
}
SCM nk_draw_list_set_cmd_offset(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->cmd_offset = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_path_count(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_list *)scm_to_pointer(ptr))->path_count);
}
SCM nk_draw_list_set_path_count(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->path_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_path_offset(SCM ptr)
{
  return scm_from_uint(((struct nk_draw_list *)scm_to_pointer(ptr))->path_offset);
}
SCM nk_draw_list_set_path_offset(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->path_offset = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_line_AA(SCM ptr)
{
  return scm_from_int(((struct nk_draw_list *)scm_to_pointer(ptr))->line_AA);
}
SCM nk_draw_list_set_line_AA(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->line_AA = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_shape_AA(SCM ptr)
{
  return scm_from_int(((struct nk_draw_list *)scm_to_pointer(ptr))->shape_AA);
}
SCM nk_draw_list_set_shape_AA(SCM ptr, SCM value)
{
  ((struct nk_draw_list *)scm_to_pointer(ptr))->shape_AA = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_draw_list_init_wrapper(SCM arg_1)
{
  nk_draw_list_init((struct nk_draw_list *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_setup_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_draw_list_setup((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_convert_config *)scm_to_pointer(arg_2), (struct nk_buffer *)scm_to_pointer(arg_3), (struct nk_buffer *)scm_to_pointer(arg_4), (struct nk_buffer *)scm_to_pointer(arg_5), scm_to_int(arg_6), scm_to_int(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk__draw_list_begin_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk__draw_list_begin((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2)), NULL);
}

SCM nk__draw_list_next_wrapper(SCM arg_1, SCM arg_2, SCM arg_3)
{
  return scm_from_pointer((void *)nk__draw_list_next((struct nk_draw_command *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2), (struct nk_draw_list *)scm_to_pointer(arg_3)), NULL);
}

SCM nk__draw_list_end_wrapper(SCM arg_1, SCM arg_2)
{
  return scm_from_pointer((void *)nk__draw_list_end((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_buffer *)scm_to_pointer(arg_2)), NULL);
}

SCM nk_draw_list_path_clear_wrapper(SCM arg_1)
{
  nk_draw_list_path_clear((struct nk_draw_list *)scm_to_pointer(arg_1));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_line_to_wrapper(SCM arg_1, SCM arg_2)
{
  nk_draw_list_path_line_to((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_arc_to_fast_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_path_arc_to_fast((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_int(arg_4), scm_to_int(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_arc_to_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_draw_list_path_arc_to((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_double(arg_3), scm_to_double(arg_4), scm_to_double(arg_5), scm_to_uint(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_rect_to_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_draw_list_path_rect_to((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), scm_to_double(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_curve_to_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_path_curve_to((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4), scm_to_uint(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_fill_wrapper(SCM arg_1, SCM arg_2)
{
  nk_draw_list_path_fill((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_path_stroke_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_draw_list_path_stroke((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_color *)scm_to_pointer(arg_2), scm_to_int(arg_3), scm_to_double(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_line_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_stroke_line((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_double(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_stroke_rect((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), scm_to_double(arg_4), scm_to_double(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_triangle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_draw_list_stroke_triangle((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5), scm_to_double(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_circle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_draw_list_stroke_circle((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_double(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_uint(arg_5), scm_to_double(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_curve_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7, SCM arg_8)
{
  nk_draw_list_stroke_curve((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4), *(struct nk_vec2 *)scm_to_pointer(arg_5), *(struct nk_color *)scm_to_pointer(arg_6), scm_to_uint(arg_7), scm_to_double(arg_8));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_stroke_poly_line_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_draw_list_stroke_poly_line((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_uint(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_double(arg_6), scm_to_int(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_fill_rect_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_draw_list_fill_rect((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), scm_to_double(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_fill_rect_multi_color_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6)
{
  nk_draw_list_fill_rect_multi_color((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_rect *)scm_to_pointer(arg_2), *(struct nk_color *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5), *(struct nk_color *)scm_to_pointer(arg_6));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_fill_triangle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_fill_triangle((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), *(struct nk_vec2 *)scm_to_pointer(arg_3), *(struct nk_vec2 *)scm_to_pointer(arg_4), *(struct nk_color *)scm_to_pointer(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_fill_circle_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_fill_circle((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_double(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_uint(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_fill_poly_convex_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5)
{
  nk_draw_list_fill_poly_convex((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_vec2 *)scm_to_pointer(arg_2), scm_to_uint(arg_3), *(struct nk_color *)scm_to_pointer(arg_4), scm_to_int(arg_5));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_add_image_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4)
{
  nk_draw_list_add_image((struct nk_draw_list *)scm_to_pointer(arg_1), *(struct nk_image *)scm_to_pointer(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3), *(struct nk_color *)scm_to_pointer(arg_4));
  return SCM_UNSPECIFIED;
}

SCM nk_draw_list_add_text_wrapper(SCM arg_1, SCM arg_2, SCM arg_3, SCM arg_4, SCM arg_5, SCM arg_6, SCM arg_7)
{
  nk_draw_list_add_text((struct nk_draw_list *)scm_to_pointer(arg_1), (struct nk_user_font *)scm_to_pointer(arg_2), *(struct nk_rect *)scm_to_pointer(arg_3), (char *)scm_to_pointer(arg_4), scm_to_int(arg_5), scm_to_double(arg_6), *(struct nk_color *)scm_to_pointer(arg_7));
  return SCM_UNSPECIFIED;
}

SCM nk_style_item_type(SCM ptr)
{
  return scm_from_int(((struct nk_style_item *)scm_to_pointer(ptr))->type);
}
SCM nk_style_item_set_type(SCM ptr, SCM value)
{
  ((struct nk_style_item *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_item_data(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_item *)scm_to_pointer(ptr))->data, NULL);
}
SCM nk_style_item_set_data(SCM ptr, SCM value)
{
  ((struct nk_style_item *)scm_to_pointer(ptr))->data = *(union nk_style_item_data *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_text_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_text *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_style_text_set_color(SCM ptr, SCM value)
{
  ((struct nk_style_text *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_text_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_text *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_text_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_text *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_button_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_button_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_button_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_button_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_text_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->text_background, NULL);
}
SCM nk_style_button_set_text_background(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->text_background = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->text_normal, NULL);
}
SCM nk_style_button_set_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->text_hover, NULL);
}
SCM nk_style_button_set_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_text_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->text_active, NULL);
}
SCM nk_style_button_set_text_active(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->text_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_text_alignment(SCM ptr)
{
  return scm_from_uint32(((struct nk_style_button *)scm_to_pointer(ptr))->text_alignment);
}
SCM nk_style_button_set_text_alignment(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->text_alignment = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_button *)scm_to_pointer(ptr))->border);
}
SCM nk_style_button_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_button *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_button_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_button_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_image_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->image_padding, NULL);
}
SCM nk_style_button_set_image_padding(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->image_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_touch_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_button *)scm_to_pointer(ptr))->touch_padding, NULL);
}
SCM nk_style_button_set_touch_padding(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->touch_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_button *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_button_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_button *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_button_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_button *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_button_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_button *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_toggle_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_toggle_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_toggle_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_toggle_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_cursor_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->cursor_normal, NULL);
}
SCM nk_style_toggle_set_cursor_normal(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->cursor_normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_cursor_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->cursor_hover, NULL);
}
SCM nk_style_toggle_set_cursor_hover(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->cursor_hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->text_normal, NULL);
}
SCM nk_style_toggle_set_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->text_hover, NULL);
}
SCM nk_style_toggle_set_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_text_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->text_active, NULL);
}
SCM nk_style_toggle_set_text_active(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->text_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_text_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->text_background, NULL);
}
SCM nk_style_toggle_set_text_background(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->text_background = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_text_alignment(SCM ptr)
{
  return scm_from_uint32(((struct nk_style_toggle *)scm_to_pointer(ptr))->text_alignment);
}
SCM nk_style_toggle_set_text_alignment(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->text_alignment = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_toggle_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_touch_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_toggle *)scm_to_pointer(ptr))->touch_padding, NULL);
}
SCM nk_style_toggle_set_touch_padding(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->touch_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_spacing(SCM ptr)
{
  return scm_from_double(((struct nk_style_toggle *)scm_to_pointer(ptr))->spacing);
}
SCM nk_style_toggle_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->spacing = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_toggle *)scm_to_pointer(ptr))->border);
}
SCM nk_style_toggle_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_toggle *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_toggle_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_toggle *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_toggle_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_toggle_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_toggle *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_toggle_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_toggle *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_selectable_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_selectable_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_pressed(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->pressed, NULL);
}
SCM nk_style_selectable_set_pressed(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->pressed = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_normal_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->normal_active, NULL);
}
SCM nk_style_selectable_set_normal_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->normal_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_hover_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->hover_active, NULL);
}
SCM nk_style_selectable_set_hover_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->hover_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_pressed_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->pressed_active, NULL);
}
SCM nk_style_selectable_set_pressed_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->pressed_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_normal, NULL);
}
SCM nk_style_selectable_set_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_hover, NULL);
}
SCM nk_style_selectable_set_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_pressed(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_pressed, NULL);
}
SCM nk_style_selectable_set_text_pressed(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_pressed = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_normal_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_normal_active, NULL);
}
SCM nk_style_selectable_set_text_normal_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_normal_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_hover_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_hover_active, NULL);
}
SCM nk_style_selectable_set_text_hover_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_hover_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_pressed_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_pressed_active, NULL);
}
SCM nk_style_selectable_set_text_pressed_active(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_pressed_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->text_background, NULL);
}
SCM nk_style_selectable_set_text_background(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_background = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_text_alignment(SCM ptr)
{
  return scm_from_uint32(((struct nk_style_selectable *)scm_to_pointer(ptr))->text_alignment);
}
SCM nk_style_selectable_set_text_alignment(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->text_alignment = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_selectable *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_selectable_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_selectable_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_touch_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->touch_padding, NULL);
}
SCM nk_style_selectable_set_touch_padding(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->touch_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_image_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_selectable *)scm_to_pointer(ptr))->image_padding, NULL);
}
SCM nk_style_selectable_set_image_padding(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->image_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_selectable *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_selectable_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_selectable *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_selectable_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_selectable *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_selectable_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_selectable *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_slider_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_slider_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_slider_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_slider_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_bar_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->bar_normal, NULL);
}
SCM nk_style_slider_set_bar_normal(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->bar_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_bar_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->bar_hover, NULL);
}
SCM nk_style_slider_set_bar_hover(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->bar_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_bar_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->bar_active, NULL);
}
SCM nk_style_slider_set_bar_active(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->bar_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_bar_filled(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->bar_filled, NULL);
}
SCM nk_style_slider_set_bar_filled(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->bar_filled = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_cursor_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_normal, NULL);
}
SCM nk_style_slider_set_cursor_normal(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_cursor_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_hover, NULL);
}
SCM nk_style_slider_set_cursor_hover(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_cursor_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_active, NULL);
}
SCM nk_style_slider_set_cursor_active(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_slider *)scm_to_pointer(ptr))->border);
}
SCM nk_style_slider_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_slider *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_slider_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_bar_height(SCM ptr)
{
  return scm_from_double(((struct nk_style_slider *)scm_to_pointer(ptr))->bar_height);
}
SCM nk_style_slider_set_bar_height(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->bar_height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_slider_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_style_slider_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_cursor_size(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_size, NULL);
}
SCM nk_style_slider_set_cursor_size(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->cursor_size = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_show_buttons(SCM ptr)
{
  return scm_from_int(((struct nk_style_slider *)scm_to_pointer(ptr))->show_buttons);
}
SCM nk_style_slider_set_show_buttons(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->show_buttons = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_inc_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->inc_button, NULL);
}
SCM nk_style_slider_set_inc_button(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->inc_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_dec_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_slider *)scm_to_pointer(ptr))->dec_button, NULL);
}
SCM nk_style_slider_set_dec_button(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->dec_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_inc_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_slider *)scm_to_pointer(ptr))->inc_symbol);
}
SCM nk_style_slider_set_inc_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->inc_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_dec_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_slider *)scm_to_pointer(ptr))->dec_symbol);
}
SCM nk_style_slider_set_dec_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->dec_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_slider *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_slider_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_slider *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_slider_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_slider *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_slider_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_slider *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_progress_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_progress_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_progress_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_progress_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_normal, NULL);
}
SCM nk_style_progress_set_cursor_normal(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_hover, NULL);
}
SCM nk_style_progress_set_cursor_hover(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_active, NULL);
}
SCM nk_style_progress_set_cursor_active(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_border_color, NULL);
}
SCM nk_style_progress_set_cursor_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_progress *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_progress_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_progress *)scm_to_pointer(ptr))->border);
}
SCM nk_style_progress_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_border);
}
SCM nk_style_progress_set_cursor_border(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_cursor_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_rounding);
}
SCM nk_style_progress_set_cursor_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->cursor_rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_progress *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_progress_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_progress *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_progress_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_progress *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_progress_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_progress *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_progress_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_progress *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_scrollbar_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_scrollbar_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_scrollbar_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_scrollbar_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_cursor_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_normal, NULL);
}
SCM nk_style_scrollbar_set_cursor_normal(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_cursor_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_hover, NULL);
}
SCM nk_style_scrollbar_set_cursor_hover(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_cursor_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_active, NULL);
}
SCM nk_style_scrollbar_set_cursor_active(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_cursor_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_border_color, NULL);
}
SCM nk_style_scrollbar_set_cursor_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->cursor_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border);
}
SCM nk_style_scrollbar_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_scrollbar_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_border_cursor(SCM ptr)
{
  return scm_from_double(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border_cursor);
}
SCM nk_style_scrollbar_set_border_cursor(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->border_cursor = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_rounding_cursor(SCM ptr)
{
  return scm_from_double(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->rounding_cursor);
}
SCM nk_style_scrollbar_set_rounding_cursor(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->rounding_cursor = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_scrollbar_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_show_buttons(SCM ptr)
{
  return scm_from_int(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->show_buttons);
}
SCM nk_style_scrollbar_set_show_buttons(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->show_buttons = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_inc_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->inc_button, NULL);
}
SCM nk_style_scrollbar_set_inc_button(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->inc_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_dec_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_scrollbar *)scm_to_pointer(ptr))->dec_button, NULL);
}
SCM nk_style_scrollbar_set_dec_button(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->dec_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_inc_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->inc_symbol);
}
SCM nk_style_scrollbar_set_inc_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->inc_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_dec_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->dec_symbol);
}
SCM nk_style_scrollbar_set_dec_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->dec_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_scrollbar *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_scrollbar_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_scrollbar *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_scrollbar_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollbar_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_scrollbar *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_scrollbar_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_scrollbar *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_edit_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_edit_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_edit_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_edit_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_scrollbar(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->scrollbar, NULL);
}
SCM nk_style_edit_set_scrollbar(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->scrollbar = *(struct nk_style_scrollbar *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_cursor_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_normal, NULL);
}
SCM nk_style_edit_set_cursor_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_cursor_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_hover, NULL);
}
SCM nk_style_edit_set_cursor_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_cursor_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_text_normal, NULL);
}
SCM nk_style_edit_set_cursor_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_cursor_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_text_hover, NULL);
}
SCM nk_style_edit_set_cursor_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->text_normal, NULL);
}
SCM nk_style_edit_set_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->text_hover, NULL);
}
SCM nk_style_edit_set_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_text_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->text_active, NULL);
}
SCM nk_style_edit_set_text_active(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->text_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_selected_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->selected_normal, NULL);
}
SCM nk_style_edit_set_selected_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->selected_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_selected_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->selected_hover, NULL);
}
SCM nk_style_edit_set_selected_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->selected_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_selected_text_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->selected_text_normal, NULL);
}
SCM nk_style_edit_set_selected_text_normal(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->selected_text_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_selected_text_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->selected_text_hover, NULL);
}
SCM nk_style_edit_set_selected_text_hover(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->selected_text_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_edit *)scm_to_pointer(ptr))->border);
}
SCM nk_style_edit_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_edit *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_edit_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_cursor_size(SCM ptr)
{
  return scm_from_double(((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_size);
}
SCM nk_style_edit_set_cursor_size(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->cursor_size = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_scrollbar_size(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->scrollbar_size, NULL);
}
SCM nk_style_edit_set_scrollbar_size(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->scrollbar_size = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_edit *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_edit_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit_row_padding(SCM ptr)
{
  return scm_from_double(((struct nk_style_edit *)scm_to_pointer(ptr))->row_padding);
}
SCM nk_style_edit_set_row_padding(SCM ptr, SCM value)
{
  ((struct nk_style_edit *)scm_to_pointer(ptr))->row_padding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_property_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_property_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_property_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_property_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_label_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->label_normal, NULL);
}
SCM nk_style_property_set_label_normal(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->label_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_label_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->label_hover, NULL);
}
SCM nk_style_property_set_label_hover(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->label_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_label_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->label_active, NULL);
}
SCM nk_style_property_set_label_active(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->label_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_sym_left(SCM ptr)
{
  return scm_from_int(((struct nk_style_property *)scm_to_pointer(ptr))->sym_left);
}
SCM nk_style_property_set_sym_left(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->sym_left = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_sym_right(SCM ptr)
{
  return scm_from_int(((struct nk_style_property *)scm_to_pointer(ptr))->sym_right);
}
SCM nk_style_property_set_sym_right(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->sym_right = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_property *)scm_to_pointer(ptr))->border);
}
SCM nk_style_property_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_property *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_property_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_property_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_edit(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->edit, NULL);
}
SCM nk_style_property_set_edit(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->edit = *(struct nk_style_edit *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_inc_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->inc_button, NULL);
}
SCM nk_style_property_set_inc_button(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->inc_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_dec_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_property *)scm_to_pointer(ptr))->dec_button, NULL);
}
SCM nk_style_property_set_dec_button(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->dec_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_userdata(SCM ptr)
{
  return scm_from_pointer(((struct nk_style_property *)scm_to_pointer(ptr))->userdata.ptr, NULL);
}
SCM nk_style_property_set_userdata(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->userdata = nk_handle_ptr(scm_to_pointer(value));
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_draw_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_property *)scm_to_pointer(ptr))->draw_begin, NULL);
}
SCM nk_style_property_set_draw_begin(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->draw_begin = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property_draw_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style_property *)scm_to_pointer(ptr))->draw_end, NULL);
}
SCM nk_style_property_set_draw_end(SCM ptr, SCM value)
{
  ((struct nk_style_property *)scm_to_pointer(ptr))->draw_end = scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_chart *)scm_to_pointer(ptr))->background, NULL);
}
SCM nk_style_chart_set_background(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->background = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_chart *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_chart_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_selected_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_chart *)scm_to_pointer(ptr))->selected_color, NULL);
}
SCM nk_style_chart_set_selected_color(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->selected_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_chart *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_style_chart_set_color(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_chart *)scm_to_pointer(ptr))->border);
}
SCM nk_style_chart_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_chart *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_chart_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_chart *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_chart_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_chart *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_combo_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_combo_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_combo_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_combo_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_label_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->label_normal, NULL);
}
SCM nk_style_combo_set_label_normal(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->label_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_label_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->label_hover, NULL);
}
SCM nk_style_combo_set_label_hover(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->label_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_label_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->label_active, NULL);
}
SCM nk_style_combo_set_label_active(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->label_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_symbol_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_normal, NULL);
}
SCM nk_style_combo_set_symbol_normal(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_symbol_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_hover, NULL);
}
SCM nk_style_combo_set_symbol_hover(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_symbol_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_active, NULL);
}
SCM nk_style_combo_set_symbol_active(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->symbol_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->button, NULL);
}
SCM nk_style_combo_set_button(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_sym_normal(SCM ptr)
{
  return scm_from_int(((struct nk_style_combo *)scm_to_pointer(ptr))->sym_normal);
}
SCM nk_style_combo_set_sym_normal(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->sym_normal = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_sym_hover(SCM ptr)
{
  return scm_from_int(((struct nk_style_combo *)scm_to_pointer(ptr))->sym_hover);
}
SCM nk_style_combo_set_sym_hover(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->sym_hover = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_sym_active(SCM ptr)
{
  return scm_from_int(((struct nk_style_combo *)scm_to_pointer(ptr))->sym_active);
}
SCM nk_style_combo_set_sym_active(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->sym_active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_combo *)scm_to_pointer(ptr))->border);
}
SCM nk_style_combo_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_combo *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_combo_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_content_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->content_padding, NULL);
}
SCM nk_style_combo_set_content_padding(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->content_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_button_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->button_padding, NULL);
}
SCM nk_style_combo_set_button_padding(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->button_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_combo *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_style_combo_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_combo *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->background, NULL);
}
SCM nk_style_tab_set_background(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->background = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_tab_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_text(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->text, NULL);
}
SCM nk_style_tab_set_text(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->text = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_tab_maximize_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->tab_maximize_button, NULL);
}
SCM nk_style_tab_set_tab_maximize_button(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->tab_maximize_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_tab_minimize_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->tab_minimize_button, NULL);
}
SCM nk_style_tab_set_tab_minimize_button(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->tab_minimize_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_node_maximize_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->node_maximize_button, NULL);
}
SCM nk_style_tab_set_node_maximize_button(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->node_maximize_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_node_minimize_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->node_minimize_button, NULL);
}
SCM nk_style_tab_set_node_minimize_button(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->node_minimize_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_sym_minimize(SCM ptr)
{
  return scm_from_int(((struct nk_style_tab *)scm_to_pointer(ptr))->sym_minimize);
}
SCM nk_style_tab_set_sym_minimize(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->sym_minimize = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_sym_maximize(SCM ptr)
{
  return scm_from_int(((struct nk_style_tab *)scm_to_pointer(ptr))->sym_maximize);
}
SCM nk_style_tab_set_sym_maximize(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->sym_maximize = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_tab *)scm_to_pointer(ptr))->border);
}
SCM nk_style_tab_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_tab *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_tab_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_indent(SCM ptr)
{
  return scm_from_double(((struct nk_style_tab *)scm_to_pointer(ptr))->indent);
}
SCM nk_style_tab_set_indent(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->indent = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_tab_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_tab *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_style_tab_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_tab *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->normal, NULL);
}
SCM nk_style_window_header_set_normal(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->normal = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->hover, NULL);
}
SCM nk_style_window_header_set_hover(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->hover = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_style_window_header_set_active(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->active = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_close_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->close_button, NULL);
}
SCM nk_style_window_header_set_close_button(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->close_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_minimize_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->minimize_button, NULL);
}
SCM nk_style_window_header_set_minimize_button(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->minimize_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_close_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_window_header *)scm_to_pointer(ptr))->close_symbol);
}
SCM nk_style_window_header_set_close_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->close_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_minimize_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_window_header *)scm_to_pointer(ptr))->minimize_symbol);
}
SCM nk_style_window_header_set_minimize_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->minimize_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_maximize_symbol(SCM ptr)
{
  return scm_from_int(((struct nk_style_window_header *)scm_to_pointer(ptr))->maximize_symbol);
}
SCM nk_style_window_header_set_maximize_symbol(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->maximize_symbol = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_label_normal(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->label_normal, NULL);
}
SCM nk_style_window_header_set_label_normal(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->label_normal = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_label_hover(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->label_hover, NULL);
}
SCM nk_style_window_header_set_label_hover(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->label_hover = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_label_active(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->label_active, NULL);
}
SCM nk_style_window_header_set_label_active(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->label_active = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_align(SCM ptr)
{
  return scm_from_int(((struct nk_style_window_header *)scm_to_pointer(ptr))->align);
}
SCM nk_style_window_header_set_align(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->align = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_window_header_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_label_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->label_padding, NULL);
}
SCM nk_style_window_header_set_label_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->label_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window_header *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_style_window_header_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_window_header *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_style_window_set_header(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->header = *(struct nk_style_window_header *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_fixed_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->fixed_background, NULL);
}
SCM nk_style_window_set_fixed_background(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->fixed_background = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_background(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->background, NULL);
}
SCM nk_style_window_set_background(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->background = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->border_color, NULL);
}
SCM nk_style_window_set_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_popup_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->popup_border_color, NULL);
}
SCM nk_style_window_set_popup_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->popup_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_combo_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->combo_border_color, NULL);
}
SCM nk_style_window_set_combo_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->combo_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_contextual_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->contextual_border_color, NULL);
}
SCM nk_style_window_set_contextual_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->contextual_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_menu_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->menu_border_color, NULL);
}
SCM nk_style_window_set_menu_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->menu_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_group_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->group_border_color, NULL);
}
SCM nk_style_window_set_group_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->group_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_tooltip_border_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_border_color, NULL);
}
SCM nk_style_window_set_tooltip_border_color(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_border_color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_scaler(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->scaler, NULL);
}
SCM nk_style_window_set_scaler(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->scaler = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->border);
}
SCM nk_style_window_set_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_combo_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->combo_border);
}
SCM nk_style_window_set_combo_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->combo_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_contextual_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->contextual_border);
}
SCM nk_style_window_set_contextual_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->contextual_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_menu_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->menu_border);
}
SCM nk_style_window_set_menu_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->menu_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_group_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->group_border);
}
SCM nk_style_window_set_group_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->group_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_tooltip_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_border);
}
SCM nk_style_window_set_tooltip_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_popup_border(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->popup_border);
}
SCM nk_style_window_set_popup_border(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->popup_border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_min_row_height_padding(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->min_row_height_padding);
}
SCM nk_style_window_set_min_row_height_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->min_row_height_padding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_rounding(SCM ptr)
{
  return scm_from_double(((struct nk_style_window *)scm_to_pointer(ptr))->rounding);
}
SCM nk_style_window_set_rounding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->rounding = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_spacing(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->spacing, NULL);
}
SCM nk_style_window_set_spacing(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->spacing = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_scrollbar_size(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->scrollbar_size, NULL);
}
SCM nk_style_window_set_scrollbar_size(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->scrollbar_size = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_min_size(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->min_size, NULL);
}
SCM nk_style_window_set_min_size(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->min_size = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->padding, NULL);
}
SCM nk_style_window_set_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_group_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->group_padding, NULL);
}
SCM nk_style_window_set_group_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->group_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_popup_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->popup_padding, NULL);
}
SCM nk_style_window_set_popup_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->popup_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_combo_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->combo_padding, NULL);
}
SCM nk_style_window_set_combo_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->combo_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_contextual_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->contextual_padding, NULL);
}
SCM nk_style_window_set_contextual_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->contextual_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_menu_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->menu_padding, NULL);
}
SCM nk_style_window_set_menu_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->menu_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window_tooltip_padding(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_padding, NULL);
}
SCM nk_style_window_set_tooltip_padding(SCM ptr, SCM value)
{
  ((struct nk_style_window *)scm_to_pointer(ptr))->tooltip_padding = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_font(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style *)scm_to_pointer(ptr))->font, NULL);
}
SCM nk_style_cursors(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style *)scm_to_pointer(ptr))->cursors, NULL);
}
SCM nk_style_set_cursors(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_style *)scm_to_pointer(ptr))->cursors, (struct nk_cursor **)scm_to_pointer(value), sizeof(((struct nk_style *)scm_to_pointer(ptr))->cursors));
  return SCM_UNSPECIFIED;
}
SCM nk_style_cursor_active(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style *)scm_to_pointer(ptr))->cursor_active, NULL);
}
SCM nk_style_set_cursor_active(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->cursor_active = (struct nk_cursor *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_cursor_last(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_style *)scm_to_pointer(ptr))->cursor_last, NULL);
}
SCM nk_style_set_cursor_last(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->cursor_last = (struct nk_cursor *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_cursor_visible(SCM ptr)
{
  return scm_from_int(((struct nk_style *)scm_to_pointer(ptr))->cursor_visible);
}
SCM nk_style_set_cursor_visible(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->cursor_visible = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_text(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->text, NULL);
}
SCM nk_style_set_text(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->text = *(struct nk_style_text *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->button, NULL);
}
SCM nk_style_set_button(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_contextual_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->contextual_button, NULL);
}
SCM nk_style_set_contextual_button(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->contextual_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_menu_button(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->menu_button, NULL);
}
SCM nk_style_set_menu_button(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->menu_button = *(struct nk_style_button *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_option(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->option, NULL);
}
SCM nk_style_set_option(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->option = *(struct nk_style_toggle *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_checkbox(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->checkbox, NULL);
}
SCM nk_style_set_checkbox(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->checkbox = *(struct nk_style_toggle *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_selectable(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->selectable, NULL);
}
SCM nk_style_set_selectable(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->selectable = *(struct nk_style_selectable *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_slider(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->slider, NULL);
}
SCM nk_style_set_slider(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->slider = *(struct nk_style_slider *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_progress(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->progress, NULL);
}
SCM nk_style_set_progress(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->progress = *(struct nk_style_progress *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_property(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->property, NULL);
}
SCM nk_style_set_property(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->property = *(struct nk_style_property *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_edit(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->edit, NULL);
}
SCM nk_style_set_edit(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->edit = *(struct nk_style_edit *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_chart(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->chart, NULL);
}
SCM nk_style_set_chart(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->chart = *(struct nk_style_chart *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollh(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->scrollh, NULL);
}
SCM nk_style_set_scrollh(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->scrollh = *(struct nk_style_scrollbar *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_scrollv(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->scrollv, NULL);
}
SCM nk_style_set_scrollv(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->scrollv = *(struct nk_style_scrollbar *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_tab(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->tab, NULL);
}
SCM nk_style_set_tab(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->tab = *(struct nk_style_tab *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_combo(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->combo, NULL);
}
SCM nk_style_set_combo(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->combo = *(struct nk_style_combo *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_window(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_style *)scm_to_pointer(ptr))->window, NULL);
}
SCM nk_style_set_window(SCM ptr, SCM value)
{
  ((struct nk_style *)scm_to_pointer(ptr))->window = *(struct nk_style_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_style_item_image_wrapper(SCM arg_1)
{
  struct nk_style_item * s = scm_gc_malloc_pointerless(sizeof(struct nk_style_item), "struct nk_style_item");
  *s = nk_style_item_image(*(struct nk_image *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_style_item_color_wrapper(SCM arg_1)
{
  struct nk_style_item * s = scm_gc_malloc_pointerless(sizeof(struct nk_style_item), "struct nk_style_item");
  *s = nk_style_item_color(*(struct nk_color *)scm_to_pointer(arg_1));
  return scm_from_pointer(s, NULL);
}

SCM nk_style_item_hide_wrapper()
{
  struct nk_style_item * s = scm_gc_malloc_pointerless(sizeof(struct nk_style_item), "struct nk_style_item");
  *s = nk_style_item_hide();
  return scm_from_pointer(s, NULL);
}

SCM nk_chart_slot_type(SCM ptr)
{
  return scm_from_int(((struct nk_chart_slot *)scm_to_pointer(ptr))->type);
}
SCM nk_chart_slot_set_type(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_color(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_chart_slot *)scm_to_pointer(ptr))->color, NULL);
}
SCM nk_chart_slot_set_color(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->color = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_highlight(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_chart_slot *)scm_to_pointer(ptr))->highlight, NULL);
}
SCM nk_chart_slot_set_highlight(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->highlight = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_min(SCM ptr)
{
  return scm_from_double(((struct nk_chart_slot *)scm_to_pointer(ptr))->min);
}
SCM nk_chart_slot_set_min(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->min = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_max(SCM ptr)
{
  return scm_from_double(((struct nk_chart_slot *)scm_to_pointer(ptr))->max);
}
SCM nk_chart_slot_set_max(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->max = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_range(SCM ptr)
{
  return scm_from_double(((struct nk_chart_slot *)scm_to_pointer(ptr))->range);
}
SCM nk_chart_slot_set_range(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->range = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_count(SCM ptr)
{
  return scm_from_int(((struct nk_chart_slot *)scm_to_pointer(ptr))->count);
}
SCM nk_chart_slot_set_count(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->count = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_last(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_chart_slot *)scm_to_pointer(ptr))->last, NULL);
}
SCM nk_chart_slot_set_last(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->last = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot_index(SCM ptr)
{
  return scm_from_int(((struct nk_chart_slot *)scm_to_pointer(ptr))->index);
}
SCM nk_chart_slot_set_index(SCM ptr, SCM value)
{
  ((struct nk_chart_slot *)scm_to_pointer(ptr))->index = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slot(SCM ptr)
{
  return scm_from_int(((struct nk_chart *)scm_to_pointer(ptr))->slot);
}
SCM nk_chart_set_slot(SCM ptr, SCM value)
{
  ((struct nk_chart *)scm_to_pointer(ptr))->slot = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_x(SCM ptr)
{
  return scm_from_double(((struct nk_chart *)scm_to_pointer(ptr))->x);
}
SCM nk_chart_set_x(SCM ptr, SCM value)
{
  ((struct nk_chart *)scm_to_pointer(ptr))->x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_y(SCM ptr)
{
  return scm_from_double(((struct nk_chart *)scm_to_pointer(ptr))->y);
}
SCM nk_chart_set_y(SCM ptr, SCM value)
{
  ((struct nk_chart *)scm_to_pointer(ptr))->y = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_w(SCM ptr)
{
  return scm_from_double(((struct nk_chart *)scm_to_pointer(ptr))->w);
}
SCM nk_chart_set_w(SCM ptr, SCM value)
{
  ((struct nk_chart *)scm_to_pointer(ptr))->w = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_h(SCM ptr)
{
  return scm_from_double(((struct nk_chart *)scm_to_pointer(ptr))->h);
}
SCM nk_chart_set_h(SCM ptr, SCM value)
{
  ((struct nk_chart *)scm_to_pointer(ptr))->h = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_chart_slots(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_chart *)scm_to_pointer(ptr))->slots, NULL);
}
SCM nk_chart_set_slots(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_chart *)scm_to_pointer(ptr))->slots, (struct nk_chart_slot *)scm_to_pointer(value), sizeof(((struct nk_chart *)scm_to_pointer(ptr))->slots));
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_type(SCM ptr)
{
  return scm_from_int(((struct nk_row_layout *)scm_to_pointer(ptr))->type);
}
SCM nk_row_layout_set_type(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_index(SCM ptr)
{
  return scm_from_int(((struct nk_row_layout *)scm_to_pointer(ptr))->index);
}
SCM nk_row_layout_set_index(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->index = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_height(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->height);
}
SCM nk_row_layout_set_height(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_min_height(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->min_height);
}
SCM nk_row_layout_set_min_height(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->min_height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_columns(SCM ptr)
{
  return scm_from_int(((struct nk_row_layout *)scm_to_pointer(ptr))->columns);
}
SCM nk_row_layout_set_columns(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->columns = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_ratio(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_row_layout *)scm_to_pointer(ptr))->ratio, NULL);
}
SCM nk_row_layout_set_ratio(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->ratio = (float *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_item_width(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->item_width);
}
SCM nk_row_layout_set_item_width(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->item_width = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_item_height(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->item_height);
}
SCM nk_row_layout_set_item_height(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->item_height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_item_offset(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->item_offset);
}
SCM nk_row_layout_set_item_offset(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->item_offset = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_filled(SCM ptr)
{
  return scm_from_double(((struct nk_row_layout *)scm_to_pointer(ptr))->filled);
}
SCM nk_row_layout_set_filled(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->filled = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_item(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_row_layout *)scm_to_pointer(ptr))->item, NULL);
}
SCM nk_row_layout_set_item(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->item = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_tree_depth(SCM ptr)
{
  return scm_from_int(((struct nk_row_layout *)scm_to_pointer(ptr))->tree_depth);
}
SCM nk_row_layout_set_tree_depth(SCM ptr, SCM value)
{
  ((struct nk_row_layout *)scm_to_pointer(ptr))->tree_depth = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_row_layout_templates(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_row_layout *)scm_to_pointer(ptr))->templates, NULL);
}
SCM nk_row_layout_set_templates(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_row_layout *)scm_to_pointer(ptr))->templates, (float *)scm_to_pointer(value), sizeof(((struct nk_row_layout *)scm_to_pointer(ptr))->templates));
  return SCM_UNSPECIFIED;
}
SCM nk_popup_buffer_begin(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_popup_buffer *)scm_to_pointer(ptr))->begin);
}
SCM nk_popup_buffer_set_begin(SCM ptr, SCM value)
{
  ((struct nk_popup_buffer *)scm_to_pointer(ptr))->begin = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_buffer_parent(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_popup_buffer *)scm_to_pointer(ptr))->parent);
}
SCM nk_popup_buffer_set_parent(SCM ptr, SCM value)
{
  ((struct nk_popup_buffer *)scm_to_pointer(ptr))->parent = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_buffer_last(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_popup_buffer *)scm_to_pointer(ptr))->last);
}
SCM nk_popup_buffer_set_last(SCM ptr, SCM value)
{
  ((struct nk_popup_buffer *)scm_to_pointer(ptr))->last = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_buffer_end(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_popup_buffer *)scm_to_pointer(ptr))->end);
}
SCM nk_popup_buffer_set_end(SCM ptr, SCM value)
{
  ((struct nk_popup_buffer *)scm_to_pointer(ptr))->end = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_buffer_active(SCM ptr)
{
  return scm_from_int(((struct nk_popup_buffer *)scm_to_pointer(ptr))->active);
}
SCM nk_popup_buffer_set_active(SCM ptr, SCM value)
{
  ((struct nk_popup_buffer *)scm_to_pointer(ptr))->active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_menu_state_x(SCM ptr)
{
  return scm_from_double(((struct nk_menu_state *)scm_to_pointer(ptr))->x);
}
SCM nk_menu_state_set_x(SCM ptr, SCM value)
{
  ((struct nk_menu_state *)scm_to_pointer(ptr))->x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_menu_state_y(SCM ptr)
{
  return scm_from_double(((struct nk_menu_state *)scm_to_pointer(ptr))->y);
}
SCM nk_menu_state_set_y(SCM ptr, SCM value)
{
  ((struct nk_menu_state *)scm_to_pointer(ptr))->y = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_menu_state_w(SCM ptr)
{
  return scm_from_double(((struct nk_menu_state *)scm_to_pointer(ptr))->w);
}
SCM nk_menu_state_set_w(SCM ptr, SCM value)
{
  ((struct nk_menu_state *)scm_to_pointer(ptr))->w = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_menu_state_h(SCM ptr)
{
  return scm_from_double(((struct nk_menu_state *)scm_to_pointer(ptr))->h);
}
SCM nk_menu_state_set_h(SCM ptr, SCM value)
{
  ((struct nk_menu_state *)scm_to_pointer(ptr))->h = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_menu_state_offset(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_menu_state *)scm_to_pointer(ptr))->offset, NULL);
}
SCM nk_menu_state_set_offset(SCM ptr, SCM value)
{
  ((struct nk_menu_state *)scm_to_pointer(ptr))->offset = *(struct nk_scroll *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_type(SCM ptr)
{
  return scm_from_int(((struct nk_panel *)scm_to_pointer(ptr))->type);
}
SCM nk_panel_set_type(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_flags(SCM ptr)
{
  return scm_from_uint32(((struct nk_panel *)scm_to_pointer(ptr))->flags);
}
SCM nk_panel_set_flags(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->flags = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_bounds(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_panel *)scm_to_pointer(ptr))->bounds, NULL);
}
SCM nk_panel_set_bounds(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->bounds = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_offset_x(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_panel *)scm_to_pointer(ptr))->offset_x, NULL);
}
SCM nk_panel_set_offset_x(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->offset_x = (nk_uint *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_offset_y(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_panel *)scm_to_pointer(ptr))->offset_y, NULL);
}
SCM nk_panel_set_offset_y(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->offset_y = (nk_uint *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_at_x(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->at_x);
}
SCM nk_panel_set_at_x(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->at_x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_at_y(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->at_y);
}
SCM nk_panel_set_at_y(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->at_y = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_max_x(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->max_x);
}
SCM nk_panel_set_max_x(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->max_x = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_footer_height(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->footer_height);
}
SCM nk_panel_set_footer_height(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->footer_height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_header_height(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->header_height);
}
SCM nk_panel_set_header_height(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->header_height = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_border(SCM ptr)
{
  return scm_from_double(((struct nk_panel *)scm_to_pointer(ptr))->border);
}
SCM nk_panel_set_border(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->border = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_has_scrolling(SCM ptr)
{
  return scm_from_uint(((struct nk_panel *)scm_to_pointer(ptr))->has_scrolling);
}
SCM nk_panel_set_has_scrolling(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->has_scrolling = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_clip(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_panel *)scm_to_pointer(ptr))->clip, NULL);
}
SCM nk_panel_set_clip(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->clip = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_menu(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_panel *)scm_to_pointer(ptr))->menu, NULL);
}
SCM nk_panel_set_menu(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->menu = *(struct nk_menu_state *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_row(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_panel *)scm_to_pointer(ptr))->row, NULL);
}
SCM nk_panel_set_row(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->row = *(struct nk_row_layout *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_chart(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_panel *)scm_to_pointer(ptr))->chart, NULL);
}
SCM nk_panel_set_chart(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->chart = *(struct nk_chart *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_buffer(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_panel *)scm_to_pointer(ptr))->buffer, NULL);
}
SCM nk_panel_set_buffer(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->buffer = (struct nk_command_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_panel_parent(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_panel *)scm_to_pointer(ptr))->parent, NULL);
}
SCM nk_panel_set_parent(SCM ptr, SCM value)
{
  ((struct nk_panel *)scm_to_pointer(ptr))->parent = (struct nk_panel *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_win(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_popup_state *)scm_to_pointer(ptr))->win, NULL);
}
SCM nk_popup_state_set_win(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->win = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_type(SCM ptr)
{
  return scm_from_int(((struct nk_popup_state *)scm_to_pointer(ptr))->type);
}
SCM nk_popup_state_set_type(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_buf(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_popup_state *)scm_to_pointer(ptr))->buf, NULL);
}
SCM nk_popup_state_set_buf(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->buf = *(struct nk_popup_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_name(SCM ptr)
{
  return scm_from_uint32(((struct nk_popup_state *)scm_to_pointer(ptr))->name);
}
SCM nk_popup_state_set_name(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->name = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_active(SCM ptr)
{
  return scm_from_int(((struct nk_popup_state *)scm_to_pointer(ptr))->active);
}
SCM nk_popup_state_set_active(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_combo_count(SCM ptr)
{
  return scm_from_uint(((struct nk_popup_state *)scm_to_pointer(ptr))->combo_count);
}
SCM nk_popup_state_set_combo_count(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->combo_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_con_count(SCM ptr)
{
  return scm_from_uint(((struct nk_popup_state *)scm_to_pointer(ptr))->con_count);
}
SCM nk_popup_state_set_con_count(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->con_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_con_old(SCM ptr)
{
  return scm_from_uint(((struct nk_popup_state *)scm_to_pointer(ptr))->con_old);
}
SCM nk_popup_state_set_con_old(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->con_old = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_active_con(SCM ptr)
{
  return scm_from_uint(((struct nk_popup_state *)scm_to_pointer(ptr))->active_con);
}
SCM nk_popup_state_set_active_con(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->active_con = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_popup_state_header(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_popup_state *)scm_to_pointer(ptr))->header, NULL);
}
SCM nk_popup_state_set_header(SCM ptr, SCM value)
{
  ((struct nk_popup_state *)scm_to_pointer(ptr))->header = *(struct nk_rect *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_name(SCM ptr)
{
  return scm_from_uint32(((struct nk_edit_state *)scm_to_pointer(ptr))->name);
}
SCM nk_edit_state_set_name(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->name = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_seq(SCM ptr)
{
  return scm_from_uint(((struct nk_edit_state *)scm_to_pointer(ptr))->seq);
}
SCM nk_edit_state_set_seq(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->seq = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_old(SCM ptr)
{
  return scm_from_uint(((struct nk_edit_state *)scm_to_pointer(ptr))->old);
}
SCM nk_edit_state_set_old(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->old = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_active(SCM ptr)
{
  return scm_from_int(((struct nk_edit_state *)scm_to_pointer(ptr))->active);
}
SCM nk_edit_state_set_active(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_prev(SCM ptr)
{
  return scm_from_int(((struct nk_edit_state *)scm_to_pointer(ptr))->prev);
}
SCM nk_edit_state_set_prev(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->prev = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_cursor(SCM ptr)
{
  return scm_from_int(((struct nk_edit_state *)scm_to_pointer(ptr))->cursor);
}
SCM nk_edit_state_set_cursor(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->cursor = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_sel_start(SCM ptr)
{
  return scm_from_int(((struct nk_edit_state *)scm_to_pointer(ptr))->sel_start);
}
SCM nk_edit_state_set_sel_start(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->sel_start = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_sel_end(SCM ptr)
{
  return scm_from_int(((struct nk_edit_state *)scm_to_pointer(ptr))->sel_end);
}
SCM nk_edit_state_set_sel_end(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->sel_end = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_scrollbar(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_edit_state *)scm_to_pointer(ptr))->scrollbar, NULL);
}
SCM nk_edit_state_set_scrollbar(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->scrollbar = *(struct nk_scroll *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_mode(SCM ptr)
{
  return scm_from_uchar(((struct nk_edit_state *)scm_to_pointer(ptr))->mode);
}
SCM nk_edit_state_set_mode(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->mode = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_edit_state_single_line(SCM ptr)
{
  return scm_from_uchar(((struct nk_edit_state *)scm_to_pointer(ptr))->single_line);
}
SCM nk_edit_state_set_single_line(SCM ptr, SCM value)
{
  ((struct nk_edit_state *)scm_to_pointer(ptr))->single_line = scm_to_uchar(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_active(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->active);
}
SCM nk_property_state_set_active(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->active = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_prev(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->prev);
}
SCM nk_property_state_set_prev(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->prev = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_buffer(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_property_state *)scm_to_pointer(ptr))->buffer, NULL);
}
SCM nk_property_state_set_buffer(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_property_state *)scm_to_pointer(ptr))->buffer, (char *)scm_to_pointer(value), sizeof(((struct nk_property_state *)scm_to_pointer(ptr))->buffer));
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_length(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->length);
}
SCM nk_property_state_set_length(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->length = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_cursor(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->cursor);
}
SCM nk_property_state_set_cursor(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->cursor = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_select_start(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->select_start);
}
SCM nk_property_state_set_select_start(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->select_start = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_select_end(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->select_end);
}
SCM nk_property_state_set_select_end(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->select_end = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_name(SCM ptr)
{
  return scm_from_uint32(((struct nk_property_state *)scm_to_pointer(ptr))->name);
}
SCM nk_property_state_set_name(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->name = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_seq(SCM ptr)
{
  return scm_from_uint(((struct nk_property_state *)scm_to_pointer(ptr))->seq);
}
SCM nk_property_state_set_seq(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->seq = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_old(SCM ptr)
{
  return scm_from_uint(((struct nk_property_state *)scm_to_pointer(ptr))->old);
}
SCM nk_property_state_set_old(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->old = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_property_state_state(SCM ptr)
{
  return scm_from_int(((struct nk_property_state *)scm_to_pointer(ptr))->state);
}
SCM nk_property_state_set_state(SCM ptr, SCM value)
{
  ((struct nk_property_state *)scm_to_pointer(ptr))->state = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_seq(SCM ptr)
{
  return scm_from_uint(((struct nk_window *)scm_to_pointer(ptr))->seq);
}
SCM nk_window_set_seq(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->seq = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_name(SCM ptr)
{
  return scm_from_uint32(((struct nk_window *)scm_to_pointer(ptr))->name);
}
SCM nk_window_set_name(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->name = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_name_string(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->name_string, NULL);
}
SCM nk_window_set_name_string(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_window *)scm_to_pointer(ptr))->name_string, (char *)scm_to_pointer(value), sizeof(((struct nk_window *)scm_to_pointer(ptr))->name_string));
  return SCM_UNSPECIFIED;
}
SCM nk_window_flags(SCM ptr)
{
  return scm_from_uint32(((struct nk_window *)scm_to_pointer(ptr))->flags);
}
SCM nk_window_set_flags(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->flags = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_bounds(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->bounds, NULL);
}
SCM nk_window_scrollbar(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->scrollbar, NULL);
}
SCM nk_window_set_scrollbar(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->scrollbar = *(struct nk_scroll *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_buffer(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->buffer, NULL);
}
SCM nk_window_set_buffer(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->buffer = *(struct nk_command_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_layout(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->layout, NULL);
}
SCM nk_window_set_layout(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->layout = (struct nk_panel *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_scrollbar_hiding_timer(SCM ptr)
{
  return scm_from_double(((struct nk_window *)scm_to_pointer(ptr))->scrollbar_hiding_timer);
}
SCM nk_window_set_scrollbar_hiding_timer(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->scrollbar_hiding_timer = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_property(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->property, NULL);
}
SCM nk_window_set_property(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->property = *(struct nk_property_state *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_popup(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->popup, NULL);
}
SCM nk_window_set_popup(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->popup = *(struct nk_popup_state *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_edit(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_window *)scm_to_pointer(ptr))->edit, NULL);
}
SCM nk_window_set_edit(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->edit = *(struct nk_edit_state *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_scrolled(SCM ptr)
{
  return scm_from_uint(((struct nk_window *)scm_to_pointer(ptr))->scrolled);
}
SCM nk_window_set_scrolled(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->scrolled = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_tables(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->tables, NULL);
}
SCM nk_window_set_tables(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->tables = (struct nk_table *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_table_count(SCM ptr)
{
  return scm_from_uint(((struct nk_window *)scm_to_pointer(ptr))->table_count);
}
SCM nk_window_set_table_count(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->table_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_window_set_next(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->next = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_prev(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->prev, NULL);
}
SCM nk_window_set_prev(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->prev = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_window_parent(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_window *)scm_to_pointer(ptr))->parent, NULL);
}
SCM nk_window_set_parent(SCM ptr, SCM value)
{
  ((struct nk_window *)scm_to_pointer(ptr))->parent = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_style_item_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_style_item_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_style_item_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_style_item_element *)scm_to_pointer(ptr))->address = (struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_style_item_element_old_value(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_config_stack_style_item_element *)scm_to_pointer(ptr))->old_value, NULL);
}
SCM nk_config_stack_style_item_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_style_item_element *)scm_to_pointer(ptr))->old_value = *(struct nk_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_float_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_float_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_float_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_float_element *)scm_to_pointer(ptr))->address = (float *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_float_element_old_value(SCM ptr)
{
  return scm_from_double(((struct nk_config_stack_float_element *)scm_to_pointer(ptr))->old_value);
}
SCM nk_config_stack_float_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_float_element *)scm_to_pointer(ptr))->old_value = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_vec2_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_vec2_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_vec2_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_vec2_element *)scm_to_pointer(ptr))->address = (struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_vec2_element_old_value(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_config_stack_vec2_element *)scm_to_pointer(ptr))->old_value, NULL);
}
SCM nk_config_stack_vec2_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_vec2_element *)scm_to_pointer(ptr))->old_value = *(struct nk_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_flags_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_flags_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_flags_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_flags_element *)scm_to_pointer(ptr))->address = (nk_flags *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_flags_element_old_value(SCM ptr)
{
  return scm_from_uint32(((struct nk_config_stack_flags_element *)scm_to_pointer(ptr))->old_value);
}
SCM nk_config_stack_flags_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_flags_element *)scm_to_pointer(ptr))->old_value = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_color_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_color_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_color_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_color_element *)scm_to_pointer(ptr))->address = (struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_color_element_old_value(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_config_stack_color_element *)scm_to_pointer(ptr))->old_value, NULL);
}
SCM nk_config_stack_color_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_color_element *)scm_to_pointer(ptr))->old_value = *(struct nk_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_user_font_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_user_font_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_user_font_element_old_value(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_user_font_element *)scm_to_pointer(ptr))->old_value, NULL);
}
SCM nk_config_stack_user_font_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_user_font_element *)scm_to_pointer(ptr))->old_value = (struct nk_user_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_button_behavior_element_address(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_button_behavior_element *)scm_to_pointer(ptr))->address, NULL);
}
SCM nk_config_stack_button_behavior_element_set_address(SCM ptr, SCM value)
{
  ((struct nk_config_stack_button_behavior_element *)scm_to_pointer(ptr))->address = (enum nk_button_behavior *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_button_behavior_element_old_value(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_button_behavior_element *)scm_to_pointer(ptr))->old_value);
}
SCM nk_config_stack_button_behavior_element_set_old_value(SCM ptr, SCM value)
{
  ((struct nk_config_stack_button_behavior_element *)scm_to_pointer(ptr))->old_value = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_style_item_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_style_item *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_style_item_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_style_item *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_style_item_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_style_item *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_style_item_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_style_item *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_style_item_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_style_item *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_float_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_float *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_float_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_float *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_float_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_float *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_float_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_float *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_float_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_float *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_vec2_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_vec2 *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_vec2_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_vec2 *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_vec2_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_vec2 *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_vec2_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_vec2 *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_vec2_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_vec2 *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_flags_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_flags *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_flags_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_flags *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_flags_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_flags *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_flags_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_flags *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_flags_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_flags *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_color_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_color *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_color_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_color *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_color_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_color *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_color_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_color *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_color_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_color *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_user_font_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_user_font *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_user_font_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_user_font *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_user_font_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_user_font *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_user_font_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_user_font *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_user_font_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_user_font *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_button_behavior_head(SCM ptr)
{
  return scm_from_int(((struct nk_config_stack_button_behavior *)scm_to_pointer(ptr))->head);
}
SCM nk_config_stack_button_behavior_set_head(SCM ptr, SCM value)
{
  ((struct nk_config_stack_button_behavior *)scm_to_pointer(ptr))->head = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_config_stack_button_behavior_elements(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_config_stack_button_behavior *)scm_to_pointer(ptr))->elements, NULL);
}
SCM nk_config_stack_button_behavior_set_elements(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_config_stack_button_behavior *)scm_to_pointer(ptr))->elements, (struct nk_config_stack_button_behavior_element *)scm_to_pointer(value), sizeof(((struct nk_config_stack_button_behavior *)scm_to_pointer(ptr))->elements));
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_style_items(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->style_items, NULL);
}
SCM nk_configuration_stacks_set_style_items(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->style_items = *(struct nk_config_stack_style_item *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_floats(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->floats, NULL);
}
SCM nk_configuration_stacks_set_floats(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->floats = *(struct nk_config_stack_float *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_vectors(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->vectors, NULL);
}
SCM nk_configuration_stacks_set_vectors(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->vectors = *(struct nk_config_stack_vec2 *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_flags(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->flags, NULL);
}
SCM nk_configuration_stacks_set_flags(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->flags = *(struct nk_config_stack_flags *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_colors(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->colors, NULL);
}
SCM nk_configuration_stacks_set_colors(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->colors = *(struct nk_config_stack_color *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_fonts(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->fonts, NULL);
}
SCM nk_configuration_stacks_set_fonts(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->fonts = *(struct nk_config_stack_user_font *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_configuration_stacks_button_behaviors(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_configuration_stacks *)scm_to_pointer(ptr))->button_behaviors, NULL);
}
SCM nk_configuration_stacks_set_button_behaviors(SCM ptr, SCM value)
{
  ((struct nk_configuration_stacks *)scm_to_pointer(ptr))->button_behaviors = *(struct nk_config_stack_button_behavior *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_table_seq(SCM ptr)
{
  return scm_from_uint(((struct nk_table *)scm_to_pointer(ptr))->seq);
}
SCM nk_table_set_seq(SCM ptr, SCM value)
{
  ((struct nk_table *)scm_to_pointer(ptr))->seq = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_table_size(SCM ptr)
{
  return scm_from_uint(((struct nk_table *)scm_to_pointer(ptr))->size);
}
SCM nk_table_set_size(SCM ptr, SCM value)
{
  ((struct nk_table *)scm_to_pointer(ptr))->size = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_table_keys(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_table *)scm_to_pointer(ptr))->keys, NULL);
}
SCM nk_table_set_keys(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_table *)scm_to_pointer(ptr))->keys, (nk_hash *)scm_to_pointer(value), sizeof(((struct nk_table *)scm_to_pointer(ptr))->keys));
  return SCM_UNSPECIFIED;
}
SCM nk_table_values(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_table *)scm_to_pointer(ptr))->values, NULL);
}
SCM nk_table_set_values(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_table *)scm_to_pointer(ptr))->values, (nk_uint *)scm_to_pointer(value), sizeof(((struct nk_table *)scm_to_pointer(ptr))->values));
  return SCM_UNSPECIFIED;
}
SCM nk_table_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_table *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_table_set_next(SCM ptr, SCM value)
{
  ((struct nk_table *)scm_to_pointer(ptr))->next = (struct nk_table *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_table_prev(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_table *)scm_to_pointer(ptr))->prev, NULL);
}
SCM nk_table_set_prev(SCM ptr, SCM value)
{
  ((struct nk_table *)scm_to_pointer(ptr))->prev = (struct nk_table *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_element_data(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_page_element *)scm_to_pointer(ptr))->data, NULL);
}
SCM nk_page_element_set_data(SCM ptr, SCM value)
{
  ((struct nk_page_element *)scm_to_pointer(ptr))->data = *(union nk_page_data *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_element_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_page_element *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_page_element_set_next(SCM ptr, SCM value)
{
  ((struct nk_page_element *)scm_to_pointer(ptr))->next = (struct nk_page_element *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_element_prev(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_page_element *)scm_to_pointer(ptr))->prev, NULL);
}
SCM nk_page_element_set_prev(SCM ptr, SCM value)
{
  ((struct nk_page_element *)scm_to_pointer(ptr))->prev = (struct nk_page_element *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_size(SCM ptr)
{
  return scm_from_uint(((struct nk_page *)scm_to_pointer(ptr))->size);
}
SCM nk_page_set_size(SCM ptr, SCM value)
{
  ((struct nk_page *)scm_to_pointer(ptr))->size = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_next(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_page *)scm_to_pointer(ptr))->next, NULL);
}
SCM nk_page_set_next(SCM ptr, SCM value)
{
  ((struct nk_page *)scm_to_pointer(ptr))->next = (struct nk_page *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_page_win(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_page *)scm_to_pointer(ptr))->win, NULL);
}
SCM nk_page_set_win(SCM ptr, SCM value)
{
  nk_memcopy(((struct nk_page *)scm_to_pointer(ptr))->win, (struct nk_page_element *)scm_to_pointer(value), sizeof(((struct nk_page *)scm_to_pointer(ptr))->win));
  return SCM_UNSPECIFIED;
}
SCM nk_pool_get_alloc(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_pool *)scm_to_pointer(ptr))->alloc, NULL);
}
SCM nk_pool_set_alloc(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->alloc = *(struct nk_allocator *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_type(SCM ptr)
{
  return scm_from_int(((struct nk_pool *)scm_to_pointer(ptr))->type);
}
SCM nk_pool_set_type(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->type = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_page_count(SCM ptr)
{
  return scm_from_uint(((struct nk_pool *)scm_to_pointer(ptr))->page_count);
}
SCM nk_pool_set_page_count(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->page_count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_pages(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_pool *)scm_to_pointer(ptr))->pages, NULL);
}
SCM nk_pool_set_pages(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->pages = (struct nk_page *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_freelist(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_pool *)scm_to_pointer(ptr))->freelist, NULL);
}
SCM nk_pool_set_freelist(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->freelist = (struct nk_page_element *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_capacity(SCM ptr)
{
  return scm_from_uint(((struct nk_pool *)scm_to_pointer(ptr))->capacity);
}
SCM nk_pool_set_capacity(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->capacity = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_size(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_pool *)scm_to_pointer(ptr))->size);
}
SCM nk_pool_set_size(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->size = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_pool_cap(SCM ptr)
{
  return scm_from_uintptr_t(((struct nk_pool *)scm_to_pointer(ptr))->cap);
}
SCM nk_pool_set_cap(SCM ptr, SCM value)
{
  ((struct nk_pool *)scm_to_pointer(ptr))->cap = scm_to_uintptr_t(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_input(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->input, NULL);
}
SCM nk_context_set_input(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->input = *(struct nk_input *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_style(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->style, NULL);
}
SCM nk_context_set_style(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->style = *(struct nk_style *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_memory(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->memory, NULL);
}
SCM nk_context_set_memory(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->memory = *(struct nk_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_clip(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->clip, NULL);
}
SCM nk_context_set_clip(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->clip = *(struct nk_clipboard *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_last_widget_state(SCM ptr)
{
  return scm_from_uint32(((struct nk_context *)scm_to_pointer(ptr))->last_widget_state);
}
SCM nk_context_set_last_widget_state(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->last_widget_state = scm_to_uint32(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_button_behavior(SCM ptr)
{
  return scm_from_int(((struct nk_context *)scm_to_pointer(ptr))->button_behavior);
}
SCM nk_context_set_button_behavior(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->button_behavior = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_stacks(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->stacks, NULL);
}
SCM nk_context_set_stacks(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->stacks = *(struct nk_configuration_stacks *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_delta_time_seconds(SCM ptr)
{
  return scm_from_double(((struct nk_context *)scm_to_pointer(ptr))->delta_time_seconds);
}
SCM nk_context_set_delta_time_seconds(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->delta_time_seconds = scm_to_double(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_draw_list(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->draw_list, NULL);
}
SCM nk_context_set_draw_list(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->draw_list = *(struct nk_draw_list *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_text_edit(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->text_edit, NULL);
}
SCM nk_context_set_text_edit(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->text_edit = *(struct nk_text_edit *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_overlay(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->overlay, NULL);
}
SCM nk_context_set_overlay(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->overlay = *(struct nk_command_buffer *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_build(SCM ptr)
{
  return scm_from_int(((struct nk_context *)scm_to_pointer(ptr))->build);
}
SCM nk_context_set_build(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->build = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_use_pool(SCM ptr)
{
  return scm_from_int(((struct nk_context *)scm_to_pointer(ptr))->use_pool);
}
SCM nk_context_set_use_pool(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->use_pool = scm_to_int(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_pool(SCM ptr)
{
  return scm_from_pointer((void *)&((struct nk_context *)scm_to_pointer(ptr))->pool, NULL);
}
SCM nk_context_set_pool(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->pool = *(struct nk_pool *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_begin(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_context *)scm_to_pointer(ptr))->begin, NULL);
}
SCM nk_context_set_begin(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->begin = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_end(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_context *)scm_to_pointer(ptr))->end, NULL);
}
SCM nk_context_set_end(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->end = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_active(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_context *)scm_to_pointer(ptr))->active, NULL);
}
SCM nk_context_set_active(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->active = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_current(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_context *)scm_to_pointer(ptr))->current, NULL);
}
SCM nk_context_set_current(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->current = (struct nk_window *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_freelist(SCM ptr)
{
  return scm_from_pointer((void *)((struct nk_context *)scm_to_pointer(ptr))->freelist, NULL);
}
SCM nk_context_set_freelist(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->freelist = (struct nk_page_element *)scm_to_pointer(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_count(SCM ptr)
{
  return scm_from_uint(((struct nk_context *)scm_to_pointer(ptr))->count);
}
SCM nk_context_set_count(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->count = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
SCM nk_context_seq(SCM ptr)
{
  return scm_from_uint(((struct nk_context *)scm_to_pointer(ptr))->seq);
}
SCM nk_context_set_seq(SCM ptr, SCM value)
{
  ((struct nk_context *)scm_to_pointer(ptr))->seq = scm_to_uint(value);
  return SCM_UNSPECIFIED;
}
void init_guile_nuklear()
{
  scm_c_define_gsubr("nk_context_set_seq", 2, 0, 0, nk_context_set_seq);
  scm_c_define_gsubr("nk_context_seq", 1, 0, 0, nk_context_seq);
  scm_c_define_gsubr("nk_context_set_count", 2, 0, 0, nk_context_set_count);
  scm_c_define_gsubr("nk_context_count", 1, 0, 0, nk_context_count);
  scm_c_define_gsubr("nk_context_set_freelist", 2, 0, 0, nk_context_set_freelist);
  scm_c_define_gsubr("nk_context_freelist", 1, 0, 0, nk_context_freelist);
  scm_c_define_gsubr("nk_context_set_current", 2, 0, 0, nk_context_set_current);
  scm_c_define_gsubr("nk_context_current", 1, 0, 0, nk_context_current);
  scm_c_define_gsubr("nk_context_set_active", 2, 0, 0, nk_context_set_active);
  scm_c_define_gsubr("nk_context_active", 1, 0, 0, nk_context_active);
  scm_c_define_gsubr("nk_context_set_end", 2, 0, 0, nk_context_set_end);
  scm_c_define_gsubr("nk_context_end", 1, 0, 0, nk_context_end);
  scm_c_define_gsubr("nk_context_set_begin", 2, 0, 0, nk_context_set_begin);
  scm_c_define_gsubr("nk_context_begin", 1, 0, 0, nk_context_begin);
  scm_c_define_gsubr("nk_context_set_pool", 2, 0, 0, nk_context_set_pool);
  scm_c_define_gsubr("nk_context_pool", 1, 0, 0, nk_context_pool);
  scm_c_define_gsubr("nk_context_set_use_pool", 2, 0, 0, nk_context_set_use_pool);
  scm_c_define_gsubr("nk_context_use_pool", 1, 0, 0, nk_context_use_pool);
  scm_c_define_gsubr("nk_context_set_build", 2, 0, 0, nk_context_set_build);
  scm_c_define_gsubr("nk_context_build", 1, 0, 0, nk_context_build);
  scm_c_define_gsubr("nk_context_set_overlay", 2, 0, 0, nk_context_set_overlay);
  scm_c_define_gsubr("nk_context_overlay", 1, 0, 0, nk_context_overlay);
  scm_c_define_gsubr("nk_context_set_text_edit", 2, 0, 0, nk_context_set_text_edit);
  scm_c_define_gsubr("nk_context_text_edit", 1, 0, 0, nk_context_text_edit);
  scm_c_define_gsubr("nk_context_set_draw_list", 2, 0, 0, nk_context_set_draw_list);
  scm_c_define_gsubr("nk_context_draw_list", 1, 0, 0, nk_context_draw_list);
  scm_c_define_gsubr("nk_context_set_delta_time_seconds", 2, 0, 0, nk_context_set_delta_time_seconds);
  scm_c_define_gsubr("nk_context_delta_time_seconds", 1, 0, 0, nk_context_delta_time_seconds);
  scm_c_define_gsubr("nk_context_set_stacks", 2, 0, 0, nk_context_set_stacks);
  scm_c_define_gsubr("nk_context_stacks", 1, 0, 0, nk_context_stacks);
  scm_c_define_gsubr("nk_context_set_button_behavior", 2, 0, 0, nk_context_set_button_behavior);
  scm_c_define_gsubr("nk_context_button_behavior", 1, 0, 0, nk_context_button_behavior);
  scm_c_define_gsubr("nk_context_set_last_widget_state", 2, 0, 0, nk_context_set_last_widget_state);
  scm_c_define_gsubr("nk_context_last_widget_state", 1, 0, 0, nk_context_last_widget_state);
  scm_c_define_gsubr("nk_context_set_clip", 2, 0, 0, nk_context_set_clip);
  scm_c_define_gsubr("nk_context_clip", 1, 0, 0, nk_context_clip);
  scm_c_define_gsubr("nk_context_set_memory", 2, 0, 0, nk_context_set_memory);
  scm_c_define_gsubr("nk_context_memory", 1, 0, 0, nk_context_memory);
  scm_c_define_gsubr("nk_context_set_style", 2, 0, 0, nk_context_set_style);
  scm_c_define_gsubr("nk_context_style", 1, 0, 0, nk_context_style);
  scm_c_define_gsubr("nk_context_set_input", 2, 0, 0, nk_context_set_input);
  scm_c_define_gsubr("nk_context_input", 1, 0, 0, nk_context_input);
  scm_c_define_gsubr("nk_pool_set_cap", 2, 0, 0, nk_pool_set_cap);
  scm_c_define_gsubr("nk_pool_cap", 1, 0, 0, nk_pool_cap);
  scm_c_define_gsubr("nk_pool_set_size", 2, 0, 0, nk_pool_set_size);
  scm_c_define_gsubr("nk_pool_size", 1, 0, 0, nk_pool_size);
  scm_c_define_gsubr("nk_pool_set_capacity", 2, 0, 0, nk_pool_set_capacity);
  scm_c_define_gsubr("nk_pool_capacity", 1, 0, 0, nk_pool_capacity);
  scm_c_define_gsubr("nk_pool_set_freelist", 2, 0, 0, nk_pool_set_freelist);
  scm_c_define_gsubr("nk_pool_freelist", 1, 0, 0, nk_pool_freelist);
  scm_c_define_gsubr("nk_pool_set_pages", 2, 0, 0, nk_pool_set_pages);
  scm_c_define_gsubr("nk_pool_pages", 1, 0, 0, nk_pool_pages);
  scm_c_define_gsubr("nk_pool_set_page_count", 2, 0, 0, nk_pool_set_page_count);
  scm_c_define_gsubr("nk_pool_page_count", 1, 0, 0, nk_pool_page_count);
  scm_c_define_gsubr("nk_pool_set_type", 2, 0, 0, nk_pool_set_type);
  scm_c_define_gsubr("nk_pool_type", 1, 0, 0, nk_pool_type);
  scm_c_define_gsubr("nk_pool_set_alloc", 2, 0, 0, nk_pool_set_alloc);
  scm_c_define_gsubr("nk_pool_get_alloc", 1, 0, 0, nk_pool_get_alloc);
  scm_c_define_gsubr("nk_page_set_win", 2, 0, 0, nk_page_set_win);
  scm_c_define_gsubr("nk_page_win", 1, 0, 0, nk_page_win);
  scm_c_define_gsubr("nk_page_set_next", 2, 0, 0, nk_page_set_next);
  scm_c_define_gsubr("nk_page_next", 1, 0, 0, nk_page_next);
  scm_c_define_gsubr("nk_page_set_size", 2, 0, 0, nk_page_set_size);
  scm_c_define_gsubr("nk_page_size", 1, 0, 0, nk_page_size);
  scm_c_define_gsubr("nk_page_element_set_prev", 2, 0, 0, nk_page_element_set_prev);
  scm_c_define_gsubr("nk_page_element_prev", 1, 0, 0, nk_page_element_prev);
  scm_c_define_gsubr("nk_page_element_set_next", 2, 0, 0, nk_page_element_set_next);
  scm_c_define_gsubr("nk_page_element_next", 1, 0, 0, nk_page_element_next);
  scm_c_define_gsubr("nk_page_element_set_data", 2, 0, 0, nk_page_element_set_data);
  scm_c_define_gsubr("nk_page_element_data", 1, 0, 0, nk_page_element_data);
  scm_c_define_gsubr("nk_table_set_prev", 2, 0, 0, nk_table_set_prev);
  scm_c_define_gsubr("nk_table_prev", 1, 0, 0, nk_table_prev);
  scm_c_define_gsubr("nk_table_set_next", 2, 0, 0, nk_table_set_next);
  scm_c_define_gsubr("nk_table_next", 1, 0, 0, nk_table_next);
  scm_c_define_gsubr("nk_table_set_values", 2, 0, 0, nk_table_set_values);
  scm_c_define_gsubr("nk_table_values", 1, 0, 0, nk_table_values);
  scm_c_define_gsubr("nk_table_set_keys", 2, 0, 0, nk_table_set_keys);
  scm_c_define_gsubr("nk_table_keys", 1, 0, 0, nk_table_keys);
  scm_c_define_gsubr("nk_table_set_size", 2, 0, 0, nk_table_set_size);
  scm_c_define_gsubr("nk_table_size", 1, 0, 0, nk_table_size);
  scm_c_define_gsubr("nk_table_set_seq", 2, 0, 0, nk_table_set_seq);
  scm_c_define_gsubr("nk_table_seq", 1, 0, 0, nk_table_seq);
  scm_c_define_gsubr("nk_configuration_stacks_set_button_behaviors", 2, 0, 0, nk_configuration_stacks_set_button_behaviors);
  scm_c_define_gsubr("nk_configuration_stacks_button_behaviors", 1, 0, 0, nk_configuration_stacks_button_behaviors);
  scm_c_define_gsubr("nk_configuration_stacks_set_fonts", 2, 0, 0, nk_configuration_stacks_set_fonts);
  scm_c_define_gsubr("nk_configuration_stacks_fonts", 1, 0, 0, nk_configuration_stacks_fonts);
  scm_c_define_gsubr("nk_configuration_stacks_set_colors", 2, 0, 0, nk_configuration_stacks_set_colors);
  scm_c_define_gsubr("nk_configuration_stacks_colors", 1, 0, 0, nk_configuration_stacks_colors);
  scm_c_define_gsubr("nk_configuration_stacks_set_flags", 2, 0, 0, nk_configuration_stacks_set_flags);
  scm_c_define_gsubr("nk_configuration_stacks_flags", 1, 0, 0, nk_configuration_stacks_flags);
  scm_c_define_gsubr("nk_configuration_stacks_set_vectors", 2, 0, 0, nk_configuration_stacks_set_vectors);
  scm_c_define_gsubr("nk_configuration_stacks_vectors", 1, 0, 0, nk_configuration_stacks_vectors);
  scm_c_define_gsubr("nk_configuration_stacks_set_floats", 2, 0, 0, nk_configuration_stacks_set_floats);
  scm_c_define_gsubr("nk_configuration_stacks_floats", 1, 0, 0, nk_configuration_stacks_floats);
  scm_c_define_gsubr("nk_configuration_stacks_set_style_items", 2, 0, 0, nk_configuration_stacks_set_style_items);
  scm_c_define_gsubr("nk_configuration_stacks_style_items", 1, 0, 0, nk_configuration_stacks_style_items);
  scm_c_define_gsubr("nk_config_stack_button_behavior_set_elements", 2, 0, 0, nk_config_stack_button_behavior_set_elements);
  scm_c_define_gsubr("nk_config_stack_button_behavior_elements", 1, 0, 0, nk_config_stack_button_behavior_elements);
  scm_c_define_gsubr("nk_config_stack_button_behavior_set_head", 2, 0, 0, nk_config_stack_button_behavior_set_head);
  scm_c_define_gsubr("nk_config_stack_button_behavior_head", 1, 0, 0, nk_config_stack_button_behavior_head);
  scm_c_define_gsubr("nk_config_stack_user_font_set_elements", 2, 0, 0, nk_config_stack_user_font_set_elements);
  scm_c_define_gsubr("nk_config_stack_user_font_elements", 1, 0, 0, nk_config_stack_user_font_elements);
  scm_c_define_gsubr("nk_config_stack_user_font_set_head", 2, 0, 0, nk_config_stack_user_font_set_head);
  scm_c_define_gsubr("nk_config_stack_user_font_head", 1, 0, 0, nk_config_stack_user_font_head);
  scm_c_define_gsubr("nk_config_stack_color_set_elements", 2, 0, 0, nk_config_stack_color_set_elements);
  scm_c_define_gsubr("nk_config_stack_color_elements", 1, 0, 0, nk_config_stack_color_elements);
  scm_c_define_gsubr("nk_config_stack_color_set_head", 2, 0, 0, nk_config_stack_color_set_head);
  scm_c_define_gsubr("nk_config_stack_color_head", 1, 0, 0, nk_config_stack_color_head);
  scm_c_define_gsubr("nk_config_stack_flags_set_elements", 2, 0, 0, nk_config_stack_flags_set_elements);
  scm_c_define_gsubr("nk_config_stack_flags_elements", 1, 0, 0, nk_config_stack_flags_elements);
  scm_c_define_gsubr("nk_config_stack_flags_set_head", 2, 0, 0, nk_config_stack_flags_set_head);
  scm_c_define_gsubr("nk_config_stack_flags_head", 1, 0, 0, nk_config_stack_flags_head);
  scm_c_define_gsubr("nk_config_stack_vec2_set_elements", 2, 0, 0, nk_config_stack_vec2_set_elements);
  scm_c_define_gsubr("nk_config_stack_vec2_elements", 1, 0, 0, nk_config_stack_vec2_elements);
  scm_c_define_gsubr("nk_config_stack_vec2_set_head", 2, 0, 0, nk_config_stack_vec2_set_head);
  scm_c_define_gsubr("nk_config_stack_vec2_head", 1, 0, 0, nk_config_stack_vec2_head);
  scm_c_define_gsubr("nk_config_stack_float_set_elements", 2, 0, 0, nk_config_stack_float_set_elements);
  scm_c_define_gsubr("nk_config_stack_float_elements", 1, 0, 0, nk_config_stack_float_elements);
  scm_c_define_gsubr("nk_config_stack_float_set_head", 2, 0, 0, nk_config_stack_float_set_head);
  scm_c_define_gsubr("nk_config_stack_float_head", 1, 0, 0, nk_config_stack_float_head);
  scm_c_define_gsubr("nk_config_stack_style_item_set_elements", 2, 0, 0, nk_config_stack_style_item_set_elements);
  scm_c_define_gsubr("nk_config_stack_style_item_elements", 1, 0, 0, nk_config_stack_style_item_elements);
  scm_c_define_gsubr("nk_config_stack_style_item_set_head", 2, 0, 0, nk_config_stack_style_item_set_head);
  scm_c_define_gsubr("nk_config_stack_style_item_head", 1, 0, 0, nk_config_stack_style_item_head);
  scm_c_define_gsubr("nk_config_stack_button_behavior_element_set_old_value", 2, 0, 0, nk_config_stack_button_behavior_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_button_behavior_element_old_value", 1, 0, 0, nk_config_stack_button_behavior_element_old_value);
  scm_c_define_gsubr("nk_config_stack_button_behavior_element_set_address", 2, 0, 0, nk_config_stack_button_behavior_element_set_address);
  scm_c_define_gsubr("nk_config_stack_button_behavior_element_address", 1, 0, 0, nk_config_stack_button_behavior_element_address);
  scm_c_define_gsubr("nk_config_stack_user_font_element_set_old_value", 2, 0, 0, nk_config_stack_user_font_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_user_font_element_old_value", 1, 0, 0, nk_config_stack_user_font_element_old_value);
  scm_c_define_gsubr("nk_config_stack_user_font_element_address", 1, 0, 0, nk_config_stack_user_font_element_address);
  scm_c_define_gsubr("nk_config_stack_color_element_set_old_value", 2, 0, 0, nk_config_stack_color_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_color_element_old_value", 1, 0, 0, nk_config_stack_color_element_old_value);
  scm_c_define_gsubr("nk_config_stack_color_element_set_address", 2, 0, 0, nk_config_stack_color_element_set_address);
  scm_c_define_gsubr("nk_config_stack_color_element_address", 1, 0, 0, nk_config_stack_color_element_address);
  scm_c_define_gsubr("nk_config_stack_flags_element_set_old_value", 2, 0, 0, nk_config_stack_flags_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_flags_element_old_value", 1, 0, 0, nk_config_stack_flags_element_old_value);
  scm_c_define_gsubr("nk_config_stack_flags_element_set_address", 2, 0, 0, nk_config_stack_flags_element_set_address);
  scm_c_define_gsubr("nk_config_stack_flags_element_address", 1, 0, 0, nk_config_stack_flags_element_address);
  scm_c_define_gsubr("nk_config_stack_vec2_element_set_old_value", 2, 0, 0, nk_config_stack_vec2_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_vec2_element_old_value", 1, 0, 0, nk_config_stack_vec2_element_old_value);
  scm_c_define_gsubr("nk_config_stack_vec2_element_set_address", 2, 0, 0, nk_config_stack_vec2_element_set_address);
  scm_c_define_gsubr("nk_config_stack_vec2_element_address", 1, 0, 0, nk_config_stack_vec2_element_address);
  scm_c_define_gsubr("nk_config_stack_float_element_set_old_value", 2, 0, 0, nk_config_stack_float_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_float_element_old_value", 1, 0, 0, nk_config_stack_float_element_old_value);
  scm_c_define_gsubr("nk_config_stack_float_element_set_address", 2, 0, 0, nk_config_stack_float_element_set_address);
  scm_c_define_gsubr("nk_config_stack_float_element_address", 1, 0, 0, nk_config_stack_float_element_address);
  scm_c_define_gsubr("nk_config_stack_style_item_element_set_old_value", 2, 0, 0, nk_config_stack_style_item_element_set_old_value);
  scm_c_define_gsubr("nk_config_stack_style_item_element_old_value", 1, 0, 0, nk_config_stack_style_item_element_old_value);
  scm_c_define_gsubr("nk_config_stack_style_item_element_set_address", 2, 0, 0, nk_config_stack_style_item_element_set_address);
  scm_c_define_gsubr("nk_config_stack_style_item_element_address", 1, 0, 0, nk_config_stack_style_item_element_address);
  scm_c_define_gsubr("nk_window_set_parent", 2, 0, 0, nk_window_set_parent);
  scm_c_define_gsubr("nk_window_parent", 1, 0, 0, nk_window_parent);
  scm_c_define_gsubr("nk_window_set_prev", 2, 0, 0, nk_window_set_prev);
  scm_c_define_gsubr("nk_window_prev", 1, 0, 0, nk_window_prev);
  scm_c_define_gsubr("nk_window_set_next", 2, 0, 0, nk_window_set_next);
  scm_c_define_gsubr("nk_window_next", 1, 0, 0, nk_window_next);
  scm_c_define_gsubr("nk_window_set_table_count", 2, 0, 0, nk_window_set_table_count);
  scm_c_define_gsubr("nk_window_table_count", 1, 0, 0, nk_window_table_count);
  scm_c_define_gsubr("nk_window_set_tables", 2, 0, 0, nk_window_set_tables);
  scm_c_define_gsubr("nk_window_tables", 1, 0, 0, nk_window_tables);
  scm_c_define_gsubr("nk_window_set_scrolled", 2, 0, 0, nk_window_set_scrolled);
  scm_c_define_gsubr("nk_window_scrolled", 1, 0, 0, nk_window_scrolled);
  scm_c_define_gsubr("nk_window_set_edit", 2, 0, 0, nk_window_set_edit);
  scm_c_define_gsubr("nk_window_edit", 1, 0, 0, nk_window_edit);
  scm_c_define_gsubr("nk_window_set_popup", 2, 0, 0, nk_window_set_popup);
  scm_c_define_gsubr("nk_window_popup", 1, 0, 0, nk_window_popup);
  scm_c_define_gsubr("nk_window_set_property", 2, 0, 0, nk_window_set_property);
  scm_c_define_gsubr("nk_window_property", 1, 0, 0, nk_window_property);
  scm_c_define_gsubr("nk_window_set_scrollbar_hiding_timer", 2, 0, 0, nk_window_set_scrollbar_hiding_timer);
  scm_c_define_gsubr("nk_window_scrollbar_hiding_timer", 1, 0, 0, nk_window_scrollbar_hiding_timer);
  scm_c_define_gsubr("nk_window_set_layout", 2, 0, 0, nk_window_set_layout);
  scm_c_define_gsubr("nk_window_layout", 1, 0, 0, nk_window_layout);
  scm_c_define_gsubr("nk_window_set_buffer", 2, 0, 0, nk_window_set_buffer);
  scm_c_define_gsubr("nk_window_buffer", 1, 0, 0, nk_window_buffer);
  scm_c_define_gsubr("nk_window_set_scrollbar", 2, 0, 0, nk_window_set_scrollbar);
  scm_c_define_gsubr("nk_window_scrollbar", 1, 0, 0, nk_window_scrollbar);
  scm_c_define_gsubr("nk_window_bounds", 1, 0, 0, nk_window_bounds);
  scm_c_define_gsubr("nk_window_set_flags", 2, 0, 0, nk_window_set_flags);
  scm_c_define_gsubr("nk_window_flags", 1, 0, 0, nk_window_flags);
  scm_c_define_gsubr("nk_window_set_name_string", 2, 0, 0, nk_window_set_name_string);
  scm_c_define_gsubr("nk_window_name_string", 1, 0, 0, nk_window_name_string);
  scm_c_define_gsubr("nk_window_set_name", 2, 0, 0, nk_window_set_name);
  scm_c_define_gsubr("nk_window_name", 1, 0, 0, nk_window_name);
  scm_c_define_gsubr("nk_window_set_seq", 2, 0, 0, nk_window_set_seq);
  scm_c_define_gsubr("nk_window_seq", 1, 0, 0, nk_window_seq);
  scm_c_define_gsubr("nk_property_state_set_state", 2, 0, 0, nk_property_state_set_state);
  scm_c_define_gsubr("nk_property_state_state", 1, 0, 0, nk_property_state_state);
  scm_c_define_gsubr("nk_property_state_set_old", 2, 0, 0, nk_property_state_set_old);
  scm_c_define_gsubr("nk_property_state_old", 1, 0, 0, nk_property_state_old);
  scm_c_define_gsubr("nk_property_state_set_seq", 2, 0, 0, nk_property_state_set_seq);
  scm_c_define_gsubr("nk_property_state_seq", 1, 0, 0, nk_property_state_seq);
  scm_c_define_gsubr("nk_property_state_set_name", 2, 0, 0, nk_property_state_set_name);
  scm_c_define_gsubr("nk_property_state_name", 1, 0, 0, nk_property_state_name);
  scm_c_define_gsubr("nk_property_state_set_select_end", 2, 0, 0, nk_property_state_set_select_end);
  scm_c_define_gsubr("nk_property_state_select_end", 1, 0, 0, nk_property_state_select_end);
  scm_c_define_gsubr("nk_property_state_set_select_start", 2, 0, 0, nk_property_state_set_select_start);
  scm_c_define_gsubr("nk_property_state_select_start", 1, 0, 0, nk_property_state_select_start);
  scm_c_define_gsubr("nk_property_state_set_cursor", 2, 0, 0, nk_property_state_set_cursor);
  scm_c_define_gsubr("nk_property_state_cursor", 1, 0, 0, nk_property_state_cursor);
  scm_c_define_gsubr("nk_property_state_set_length", 2, 0, 0, nk_property_state_set_length);
  scm_c_define_gsubr("nk_property_state_length", 1, 0, 0, nk_property_state_length);
  scm_c_define_gsubr("nk_property_state_set_buffer", 2, 0, 0, nk_property_state_set_buffer);
  scm_c_define_gsubr("nk_property_state_buffer", 1, 0, 0, nk_property_state_buffer);
  scm_c_define_gsubr("nk_property_state_set_prev", 2, 0, 0, nk_property_state_set_prev);
  scm_c_define_gsubr("nk_property_state_prev", 1, 0, 0, nk_property_state_prev);
  scm_c_define_gsubr("nk_property_state_set_active", 2, 0, 0, nk_property_state_set_active);
  scm_c_define_gsubr("nk_property_state_active", 1, 0, 0, nk_property_state_active);
  scm_c_define_gsubr("nk_edit_state_set_single_line", 2, 0, 0, nk_edit_state_set_single_line);
  scm_c_define_gsubr("nk_edit_state_single_line", 1, 0, 0, nk_edit_state_single_line);
  scm_c_define_gsubr("nk_edit_state_set_mode", 2, 0, 0, nk_edit_state_set_mode);
  scm_c_define_gsubr("nk_edit_state_mode", 1, 0, 0, nk_edit_state_mode);
  scm_c_define_gsubr("nk_edit_state_set_scrollbar", 2, 0, 0, nk_edit_state_set_scrollbar);
  scm_c_define_gsubr("nk_edit_state_scrollbar", 1, 0, 0, nk_edit_state_scrollbar);
  scm_c_define_gsubr("nk_edit_state_set_sel_end", 2, 0, 0, nk_edit_state_set_sel_end);
  scm_c_define_gsubr("nk_edit_state_sel_end", 1, 0, 0, nk_edit_state_sel_end);
  scm_c_define_gsubr("nk_edit_state_set_sel_start", 2, 0, 0, nk_edit_state_set_sel_start);
  scm_c_define_gsubr("nk_edit_state_sel_start", 1, 0, 0, nk_edit_state_sel_start);
  scm_c_define_gsubr("nk_edit_state_set_cursor", 2, 0, 0, nk_edit_state_set_cursor);
  scm_c_define_gsubr("nk_edit_state_cursor", 1, 0, 0, nk_edit_state_cursor);
  scm_c_define_gsubr("nk_edit_state_set_prev", 2, 0, 0, nk_edit_state_set_prev);
  scm_c_define_gsubr("nk_edit_state_prev", 1, 0, 0, nk_edit_state_prev);
  scm_c_define_gsubr("nk_edit_state_set_active", 2, 0, 0, nk_edit_state_set_active);
  scm_c_define_gsubr("nk_edit_state_active", 1, 0, 0, nk_edit_state_active);
  scm_c_define_gsubr("nk_edit_state_set_old", 2, 0, 0, nk_edit_state_set_old);
  scm_c_define_gsubr("nk_edit_state_old", 1, 0, 0, nk_edit_state_old);
  scm_c_define_gsubr("nk_edit_state_set_seq", 2, 0, 0, nk_edit_state_set_seq);
  scm_c_define_gsubr("nk_edit_state_seq", 1, 0, 0, nk_edit_state_seq);
  scm_c_define_gsubr("nk_edit_state_set_name", 2, 0, 0, nk_edit_state_set_name);
  scm_c_define_gsubr("nk_edit_state_name", 1, 0, 0, nk_edit_state_name);
  scm_c_define_gsubr("nk_popup_state_set_header", 2, 0, 0, nk_popup_state_set_header);
  scm_c_define_gsubr("nk_popup_state_header", 1, 0, 0, nk_popup_state_header);
  scm_c_define_gsubr("nk_popup_state_set_active_con", 2, 0, 0, nk_popup_state_set_active_con);
  scm_c_define_gsubr("nk_popup_state_active_con", 1, 0, 0, nk_popup_state_active_con);
  scm_c_define_gsubr("nk_popup_state_set_con_old", 2, 0, 0, nk_popup_state_set_con_old);
  scm_c_define_gsubr("nk_popup_state_con_old", 1, 0, 0, nk_popup_state_con_old);
  scm_c_define_gsubr("nk_popup_state_set_con_count", 2, 0, 0, nk_popup_state_set_con_count);
  scm_c_define_gsubr("nk_popup_state_con_count", 1, 0, 0, nk_popup_state_con_count);
  scm_c_define_gsubr("nk_popup_state_set_combo_count", 2, 0, 0, nk_popup_state_set_combo_count);
  scm_c_define_gsubr("nk_popup_state_combo_count", 1, 0, 0, nk_popup_state_combo_count);
  scm_c_define_gsubr("nk_popup_state_set_active", 2, 0, 0, nk_popup_state_set_active);
  scm_c_define_gsubr("nk_popup_state_active", 1, 0, 0, nk_popup_state_active);
  scm_c_define_gsubr("nk_popup_state_set_name", 2, 0, 0, nk_popup_state_set_name);
  scm_c_define_gsubr("nk_popup_state_name", 1, 0, 0, nk_popup_state_name);
  scm_c_define_gsubr("nk_popup_state_set_buf", 2, 0, 0, nk_popup_state_set_buf);
  scm_c_define_gsubr("nk_popup_state_buf", 1, 0, 0, nk_popup_state_buf);
  scm_c_define_gsubr("nk_popup_state_set_type", 2, 0, 0, nk_popup_state_set_type);
  scm_c_define_gsubr("nk_popup_state_type", 1, 0, 0, nk_popup_state_type);
  scm_c_define_gsubr("nk_popup_state_set_win", 2, 0, 0, nk_popup_state_set_win);
  scm_c_define_gsubr("nk_popup_state_win", 1, 0, 0, nk_popup_state_win);
  scm_c_define_gsubr("nk_panel_set_parent", 2, 0, 0, nk_panel_set_parent);
  scm_c_define_gsubr("nk_panel_parent", 1, 0, 0, nk_panel_parent);
  scm_c_define_gsubr("nk_panel_set_buffer", 2, 0, 0, nk_panel_set_buffer);
  scm_c_define_gsubr("nk_panel_buffer", 1, 0, 0, nk_panel_buffer);
  scm_c_define_gsubr("nk_panel_set_chart", 2, 0, 0, nk_panel_set_chart);
  scm_c_define_gsubr("nk_panel_chart", 1, 0, 0, nk_panel_chart);
  scm_c_define_gsubr("nk_panel_set_row", 2, 0, 0, nk_panel_set_row);
  scm_c_define_gsubr("nk_panel_row", 1, 0, 0, nk_panel_row);
  scm_c_define_gsubr("nk_panel_set_menu", 2, 0, 0, nk_panel_set_menu);
  scm_c_define_gsubr("nk_panel_menu", 1, 0, 0, nk_panel_menu);
  scm_c_define_gsubr("nk_panel_set_clip", 2, 0, 0, nk_panel_set_clip);
  scm_c_define_gsubr("nk_panel_clip", 1, 0, 0, nk_panel_clip);
  scm_c_define_gsubr("nk_panel_set_has_scrolling", 2, 0, 0, nk_panel_set_has_scrolling);
  scm_c_define_gsubr("nk_panel_has_scrolling", 1, 0, 0, nk_panel_has_scrolling);
  scm_c_define_gsubr("nk_panel_set_border", 2, 0, 0, nk_panel_set_border);
  scm_c_define_gsubr("nk_panel_border", 1, 0, 0, nk_panel_border);
  scm_c_define_gsubr("nk_panel_set_header_height", 2, 0, 0, nk_panel_set_header_height);
  scm_c_define_gsubr("nk_panel_header_height", 1, 0, 0, nk_panel_header_height);
  scm_c_define_gsubr("nk_panel_set_footer_height", 2, 0, 0, nk_panel_set_footer_height);
  scm_c_define_gsubr("nk_panel_footer_height", 1, 0, 0, nk_panel_footer_height);
  scm_c_define_gsubr("nk_panel_set_max_x", 2, 0, 0, nk_panel_set_max_x);
  scm_c_define_gsubr("nk_panel_max_x", 1, 0, 0, nk_panel_max_x);
  scm_c_define_gsubr("nk_panel_set_at_y", 2, 0, 0, nk_panel_set_at_y);
  scm_c_define_gsubr("nk_panel_at_y", 1, 0, 0, nk_panel_at_y);
  scm_c_define_gsubr("nk_panel_set_at_x", 2, 0, 0, nk_panel_set_at_x);
  scm_c_define_gsubr("nk_panel_at_x", 1, 0, 0, nk_panel_at_x);
  scm_c_define_gsubr("nk_panel_set_offset_y", 2, 0, 0, nk_panel_set_offset_y);
  scm_c_define_gsubr("nk_panel_offset_y", 1, 0, 0, nk_panel_offset_y);
  scm_c_define_gsubr("nk_panel_set_offset_x", 2, 0, 0, nk_panel_set_offset_x);
  scm_c_define_gsubr("nk_panel_offset_x", 1, 0, 0, nk_panel_offset_x);
  scm_c_define_gsubr("nk_panel_set_bounds", 2, 0, 0, nk_panel_set_bounds);
  scm_c_define_gsubr("nk_panel_bounds", 1, 0, 0, nk_panel_bounds);
  scm_c_define_gsubr("nk_panel_set_flags", 2, 0, 0, nk_panel_set_flags);
  scm_c_define_gsubr("nk_panel_flags", 1, 0, 0, nk_panel_flags);
  scm_c_define_gsubr("nk_panel_set_type", 2, 0, 0, nk_panel_set_type);
  scm_c_define_gsubr("nk_panel_type", 1, 0, 0, nk_panel_type);
  scm_c_define_gsubr("nk_menu_state_set_offset", 2, 0, 0, nk_menu_state_set_offset);
  scm_c_define_gsubr("nk_menu_state_offset", 1, 0, 0, nk_menu_state_offset);
  scm_c_define_gsubr("nk_menu_state_set_h", 2, 0, 0, nk_menu_state_set_h);
  scm_c_define_gsubr("nk_menu_state_h", 1, 0, 0, nk_menu_state_h);
  scm_c_define_gsubr("nk_menu_state_set_w", 2, 0, 0, nk_menu_state_set_w);
  scm_c_define_gsubr("nk_menu_state_w", 1, 0, 0, nk_menu_state_w);
  scm_c_define_gsubr("nk_menu_state_set_y", 2, 0, 0, nk_menu_state_set_y);
  scm_c_define_gsubr("nk_menu_state_y", 1, 0, 0, nk_menu_state_y);
  scm_c_define_gsubr("nk_menu_state_set_x", 2, 0, 0, nk_menu_state_set_x);
  scm_c_define_gsubr("nk_menu_state_x", 1, 0, 0, nk_menu_state_x);
  scm_c_define_gsubr("nk_popup_buffer_set_active", 2, 0, 0, nk_popup_buffer_set_active);
  scm_c_define_gsubr("nk_popup_buffer_active", 1, 0, 0, nk_popup_buffer_active);
  scm_c_define_gsubr("nk_popup_buffer_set_end", 2, 0, 0, nk_popup_buffer_set_end);
  scm_c_define_gsubr("nk_popup_buffer_end", 1, 0, 0, nk_popup_buffer_end);
  scm_c_define_gsubr("nk_popup_buffer_set_last", 2, 0, 0, nk_popup_buffer_set_last);
  scm_c_define_gsubr("nk_popup_buffer_last", 1, 0, 0, nk_popup_buffer_last);
  scm_c_define_gsubr("nk_popup_buffer_set_parent", 2, 0, 0, nk_popup_buffer_set_parent);
  scm_c_define_gsubr("nk_popup_buffer_parent", 1, 0, 0, nk_popup_buffer_parent);
  scm_c_define_gsubr("nk_popup_buffer_set_begin", 2, 0, 0, nk_popup_buffer_set_begin);
  scm_c_define_gsubr("nk_popup_buffer_begin", 1, 0, 0, nk_popup_buffer_begin);
  scm_c_define_gsubr("nk_row_layout_set_templates", 2, 0, 0, nk_row_layout_set_templates);
  scm_c_define_gsubr("nk_row_layout_templates", 1, 0, 0, nk_row_layout_templates);
  scm_c_define_gsubr("nk_row_layout_set_tree_depth", 2, 0, 0, nk_row_layout_set_tree_depth);
  scm_c_define_gsubr("nk_row_layout_tree_depth", 1, 0, 0, nk_row_layout_tree_depth);
  scm_c_define_gsubr("nk_row_layout_set_item", 2, 0, 0, nk_row_layout_set_item);
  scm_c_define_gsubr("nk_row_layout_item", 1, 0, 0, nk_row_layout_item);
  scm_c_define_gsubr("nk_row_layout_set_filled", 2, 0, 0, nk_row_layout_set_filled);
  scm_c_define_gsubr("nk_row_layout_filled", 1, 0, 0, nk_row_layout_filled);
  scm_c_define_gsubr("nk_row_layout_set_item_offset", 2, 0, 0, nk_row_layout_set_item_offset);
  scm_c_define_gsubr("nk_row_layout_item_offset", 1, 0, 0, nk_row_layout_item_offset);
  scm_c_define_gsubr("nk_row_layout_set_item_height", 2, 0, 0, nk_row_layout_set_item_height);
  scm_c_define_gsubr("nk_row_layout_item_height", 1, 0, 0, nk_row_layout_item_height);
  scm_c_define_gsubr("nk_row_layout_set_item_width", 2, 0, 0, nk_row_layout_set_item_width);
  scm_c_define_gsubr("nk_row_layout_item_width", 1, 0, 0, nk_row_layout_item_width);
  scm_c_define_gsubr("nk_row_layout_set_ratio", 2, 0, 0, nk_row_layout_set_ratio);
  scm_c_define_gsubr("nk_row_layout_ratio", 1, 0, 0, nk_row_layout_ratio);
  scm_c_define_gsubr("nk_row_layout_set_columns", 2, 0, 0, nk_row_layout_set_columns);
  scm_c_define_gsubr("nk_row_layout_columns", 1, 0, 0, nk_row_layout_columns);
  scm_c_define_gsubr("nk_row_layout_set_min_height", 2, 0, 0, nk_row_layout_set_min_height);
  scm_c_define_gsubr("nk_row_layout_min_height", 1, 0, 0, nk_row_layout_min_height);
  scm_c_define_gsubr("nk_row_layout_set_height", 2, 0, 0, nk_row_layout_set_height);
  scm_c_define_gsubr("nk_row_layout_height", 1, 0, 0, nk_row_layout_height);
  scm_c_define_gsubr("nk_row_layout_set_index", 2, 0, 0, nk_row_layout_set_index);
  scm_c_define_gsubr("nk_row_layout_index", 1, 0, 0, nk_row_layout_index);
  scm_c_define_gsubr("nk_row_layout_set_type", 2, 0, 0, nk_row_layout_set_type);
  scm_c_define_gsubr("nk_row_layout_type", 1, 0, 0, nk_row_layout_type);
  scm_c_define_gsubr("nk_chart_set_slots", 2, 0, 0, nk_chart_set_slots);
  scm_c_define_gsubr("nk_chart_slots", 1, 0, 0, nk_chart_slots);
  scm_c_define_gsubr("nk_chart_set_h", 2, 0, 0, nk_chart_set_h);
  scm_c_define_gsubr("nk_chart_h", 1, 0, 0, nk_chart_h);
  scm_c_define_gsubr("nk_chart_set_w", 2, 0, 0, nk_chart_set_w);
  scm_c_define_gsubr("nk_chart_w", 1, 0, 0, nk_chart_w);
  scm_c_define_gsubr("nk_chart_set_y", 2, 0, 0, nk_chart_set_y);
  scm_c_define_gsubr("nk_chart_y", 1, 0, 0, nk_chart_y);
  scm_c_define_gsubr("nk_chart_set_x", 2, 0, 0, nk_chart_set_x);
  scm_c_define_gsubr("nk_chart_x", 1, 0, 0, nk_chart_x);
  scm_c_define_gsubr("nk_chart_set_slot", 2, 0, 0, nk_chart_set_slot);
  scm_c_define_gsubr("nk_chart_slot", 1, 0, 0, nk_chart_slot);
  scm_c_define_gsubr("nk_chart_slot_set_index", 2, 0, 0, nk_chart_slot_set_index);
  scm_c_define_gsubr("nk_chart_slot_index", 1, 0, 0, nk_chart_slot_index);
  scm_c_define_gsubr("nk_chart_slot_set_last", 2, 0, 0, nk_chart_slot_set_last);
  scm_c_define_gsubr("nk_chart_slot_last", 1, 0, 0, nk_chart_slot_last);
  scm_c_define_gsubr("nk_chart_slot_set_count", 2, 0, 0, nk_chart_slot_set_count);
  scm_c_define_gsubr("nk_chart_slot_count", 1, 0, 0, nk_chart_slot_count);
  scm_c_define_gsubr("nk_chart_slot_set_range", 2, 0, 0, nk_chart_slot_set_range);
  scm_c_define_gsubr("nk_chart_slot_range", 1, 0, 0, nk_chart_slot_range);
  scm_c_define_gsubr("nk_chart_slot_set_max", 2, 0, 0, nk_chart_slot_set_max);
  scm_c_define_gsubr("nk_chart_slot_max", 1, 0, 0, nk_chart_slot_max);
  scm_c_define_gsubr("nk_chart_slot_set_min", 2, 0, 0, nk_chart_slot_set_min);
  scm_c_define_gsubr("nk_chart_slot_min", 1, 0, 0, nk_chart_slot_min);
  scm_c_define_gsubr("nk_chart_slot_set_highlight", 2, 0, 0, nk_chart_slot_set_highlight);
  scm_c_define_gsubr("nk_chart_slot_highlight", 1, 0, 0, nk_chart_slot_highlight);
  scm_c_define_gsubr("nk_chart_slot_set_color", 2, 0, 0, nk_chart_slot_set_color);
  scm_c_define_gsubr("nk_chart_slot_color", 1, 0, 0, nk_chart_slot_color);
  scm_c_define_gsubr("nk_chart_slot_set_type", 2, 0, 0, nk_chart_slot_set_type);
  scm_c_define_gsubr("nk_chart_slot_type", 1, 0, 0, nk_chart_slot_type);
  scm_c_define_gsubr("nk_style_set_window", 2, 0, 0, nk_style_set_window);
  scm_c_define_gsubr("nk_style_window", 1, 0, 0, nk_style_window);
  scm_c_define_gsubr("nk_style_set_combo", 2, 0, 0, nk_style_set_combo);
  scm_c_define_gsubr("nk_style_combo", 1, 0, 0, nk_style_combo);
  scm_c_define_gsubr("nk_style_set_tab", 2, 0, 0, nk_style_set_tab);
  scm_c_define_gsubr("nk_style_tab", 1, 0, 0, nk_style_tab);
  scm_c_define_gsubr("nk_style_set_scrollv", 2, 0, 0, nk_style_set_scrollv);
  scm_c_define_gsubr("nk_style_scrollv", 1, 0, 0, nk_style_scrollv);
  scm_c_define_gsubr("nk_style_set_scrollh", 2, 0, 0, nk_style_set_scrollh);
  scm_c_define_gsubr("nk_style_scrollh", 1, 0, 0, nk_style_scrollh);
  scm_c_define_gsubr("nk_style_set_chart", 2, 0, 0, nk_style_set_chart);
  scm_c_define_gsubr("nk_style_chart", 1, 0, 0, nk_style_chart);
  scm_c_define_gsubr("nk_style_set_edit", 2, 0, 0, nk_style_set_edit);
  scm_c_define_gsubr("nk_style_edit", 1, 0, 0, nk_style_edit);
  scm_c_define_gsubr("nk_style_set_property", 2, 0, 0, nk_style_set_property);
  scm_c_define_gsubr("nk_style_property", 1, 0, 0, nk_style_property);
  scm_c_define_gsubr("nk_style_set_progress", 2, 0, 0, nk_style_set_progress);
  scm_c_define_gsubr("nk_style_progress", 1, 0, 0, nk_style_progress);
  scm_c_define_gsubr("nk_style_set_slider", 2, 0, 0, nk_style_set_slider);
  scm_c_define_gsubr("nk_style_slider", 1, 0, 0, nk_style_slider);
  scm_c_define_gsubr("nk_style_set_selectable", 2, 0, 0, nk_style_set_selectable);
  scm_c_define_gsubr("nk_style_selectable", 1, 0, 0, nk_style_selectable);
  scm_c_define_gsubr("nk_style_set_checkbox", 2, 0, 0, nk_style_set_checkbox);
  scm_c_define_gsubr("nk_style_checkbox", 1, 0, 0, nk_style_checkbox);
  scm_c_define_gsubr("nk_style_set_option", 2, 0, 0, nk_style_set_option);
  scm_c_define_gsubr("nk_style_option", 1, 0, 0, nk_style_option);
  scm_c_define_gsubr("nk_style_set_menu_button", 2, 0, 0, nk_style_set_menu_button);
  scm_c_define_gsubr("nk_style_menu_button", 1, 0, 0, nk_style_menu_button);
  scm_c_define_gsubr("nk_style_set_contextual_button", 2, 0, 0, nk_style_set_contextual_button);
  scm_c_define_gsubr("nk_style_contextual_button", 1, 0, 0, nk_style_contextual_button);
  scm_c_define_gsubr("nk_style_set_button", 2, 0, 0, nk_style_set_button);
  scm_c_define_gsubr("nk_style_button", 1, 0, 0, nk_style_button);
  scm_c_define_gsubr("nk_style_set_text", 2, 0, 0, nk_style_set_text);
  scm_c_define_gsubr("nk_style_text", 1, 0, 0, nk_style_text);
  scm_c_define_gsubr("nk_style_set_cursor_visible", 2, 0, 0, nk_style_set_cursor_visible);
  scm_c_define_gsubr("nk_style_cursor_visible", 1, 0, 0, nk_style_cursor_visible);
  scm_c_define_gsubr("nk_style_set_cursor_last", 2, 0, 0, nk_style_set_cursor_last);
  scm_c_define_gsubr("nk_style_cursor_last", 1, 0, 0, nk_style_cursor_last);
  scm_c_define_gsubr("nk_style_set_cursor_active", 2, 0, 0, nk_style_set_cursor_active);
  scm_c_define_gsubr("nk_style_cursor_active", 1, 0, 0, nk_style_cursor_active);
  scm_c_define_gsubr("nk_style_set_cursors", 2, 0, 0, nk_style_set_cursors);
  scm_c_define_gsubr("nk_style_cursors", 1, 0, 0, nk_style_cursors);
  scm_c_define_gsubr("nk_style_font", 1, 0, 0, nk_style_font);
  scm_c_define_gsubr("nk_style_window_set_tooltip_padding", 2, 0, 0, nk_style_window_set_tooltip_padding);
  scm_c_define_gsubr("nk_style_window_tooltip_padding", 1, 0, 0, nk_style_window_tooltip_padding);
  scm_c_define_gsubr("nk_style_window_set_menu_padding", 2, 0, 0, nk_style_window_set_menu_padding);
  scm_c_define_gsubr("nk_style_window_menu_padding", 1, 0, 0, nk_style_window_menu_padding);
  scm_c_define_gsubr("nk_style_window_set_contextual_padding", 2, 0, 0, nk_style_window_set_contextual_padding);
  scm_c_define_gsubr("nk_style_window_contextual_padding", 1, 0, 0, nk_style_window_contextual_padding);
  scm_c_define_gsubr("nk_style_window_set_combo_padding", 2, 0, 0, nk_style_window_set_combo_padding);
  scm_c_define_gsubr("nk_style_window_combo_padding", 1, 0, 0, nk_style_window_combo_padding);
  scm_c_define_gsubr("nk_style_window_set_popup_padding", 2, 0, 0, nk_style_window_set_popup_padding);
  scm_c_define_gsubr("nk_style_window_popup_padding", 1, 0, 0, nk_style_window_popup_padding);
  scm_c_define_gsubr("nk_style_window_set_group_padding", 2, 0, 0, nk_style_window_set_group_padding);
  scm_c_define_gsubr("nk_style_window_group_padding", 1, 0, 0, nk_style_window_group_padding);
  scm_c_define_gsubr("nk_style_window_set_padding", 2, 0, 0, nk_style_window_set_padding);
  scm_c_define_gsubr("nk_style_window_padding", 1, 0, 0, nk_style_window_padding);
  scm_c_define_gsubr("nk_style_window_set_min_size", 2, 0, 0, nk_style_window_set_min_size);
  scm_c_define_gsubr("nk_style_window_min_size", 1, 0, 0, nk_style_window_min_size);
  scm_c_define_gsubr("nk_style_window_set_scrollbar_size", 2, 0, 0, nk_style_window_set_scrollbar_size);
  scm_c_define_gsubr("nk_style_window_scrollbar_size", 1, 0, 0, nk_style_window_scrollbar_size);
  scm_c_define_gsubr("nk_style_window_set_spacing", 2, 0, 0, nk_style_window_set_spacing);
  scm_c_define_gsubr("nk_style_window_spacing", 1, 0, 0, nk_style_window_spacing);
  scm_c_define_gsubr("nk_style_window_set_rounding", 2, 0, 0, nk_style_window_set_rounding);
  scm_c_define_gsubr("nk_style_window_rounding", 1, 0, 0, nk_style_window_rounding);
  scm_c_define_gsubr("nk_style_window_set_min_row_height_padding", 2, 0, 0, nk_style_window_set_min_row_height_padding);
  scm_c_define_gsubr("nk_style_window_min_row_height_padding", 1, 0, 0, nk_style_window_min_row_height_padding);
  scm_c_define_gsubr("nk_style_window_set_popup_border", 2, 0, 0, nk_style_window_set_popup_border);
  scm_c_define_gsubr("nk_style_window_popup_border", 1, 0, 0, nk_style_window_popup_border);
  scm_c_define_gsubr("nk_style_window_set_tooltip_border", 2, 0, 0, nk_style_window_set_tooltip_border);
  scm_c_define_gsubr("nk_style_window_tooltip_border", 1, 0, 0, nk_style_window_tooltip_border);
  scm_c_define_gsubr("nk_style_window_set_group_border", 2, 0, 0, nk_style_window_set_group_border);
  scm_c_define_gsubr("nk_style_window_group_border", 1, 0, 0, nk_style_window_group_border);
  scm_c_define_gsubr("nk_style_window_set_menu_border", 2, 0, 0, nk_style_window_set_menu_border);
  scm_c_define_gsubr("nk_style_window_menu_border", 1, 0, 0, nk_style_window_menu_border);
  scm_c_define_gsubr("nk_style_window_set_contextual_border", 2, 0, 0, nk_style_window_set_contextual_border);
  scm_c_define_gsubr("nk_style_window_contextual_border", 1, 0, 0, nk_style_window_contextual_border);
  scm_c_define_gsubr("nk_style_window_set_combo_border", 2, 0, 0, nk_style_window_set_combo_border);
  scm_c_define_gsubr("nk_style_window_combo_border", 1, 0, 0, nk_style_window_combo_border);
  scm_c_define_gsubr("nk_style_window_set_border", 2, 0, 0, nk_style_window_set_border);
  scm_c_define_gsubr("nk_style_window_border", 1, 0, 0, nk_style_window_border);
  scm_c_define_gsubr("nk_style_window_set_scaler", 2, 0, 0, nk_style_window_set_scaler);
  scm_c_define_gsubr("nk_style_window_scaler", 1, 0, 0, nk_style_window_scaler);
  scm_c_define_gsubr("nk_style_window_set_tooltip_border_color", 2, 0, 0, nk_style_window_set_tooltip_border_color);
  scm_c_define_gsubr("nk_style_window_tooltip_border_color", 1, 0, 0, nk_style_window_tooltip_border_color);
  scm_c_define_gsubr("nk_style_window_set_group_border_color", 2, 0, 0, nk_style_window_set_group_border_color);
  scm_c_define_gsubr("nk_style_window_group_border_color", 1, 0, 0, nk_style_window_group_border_color);
  scm_c_define_gsubr("nk_style_window_set_menu_border_color", 2, 0, 0, nk_style_window_set_menu_border_color);
  scm_c_define_gsubr("nk_style_window_menu_border_color", 1, 0, 0, nk_style_window_menu_border_color);
  scm_c_define_gsubr("nk_style_window_set_contextual_border_color", 2, 0, 0, nk_style_window_set_contextual_border_color);
  scm_c_define_gsubr("nk_style_window_contextual_border_color", 1, 0, 0, nk_style_window_contextual_border_color);
  scm_c_define_gsubr("nk_style_window_set_combo_border_color", 2, 0, 0, nk_style_window_set_combo_border_color);
  scm_c_define_gsubr("nk_style_window_combo_border_color", 1, 0, 0, nk_style_window_combo_border_color);
  scm_c_define_gsubr("nk_style_window_set_popup_border_color", 2, 0, 0, nk_style_window_set_popup_border_color);
  scm_c_define_gsubr("nk_style_window_popup_border_color", 1, 0, 0, nk_style_window_popup_border_color);
  scm_c_define_gsubr("nk_style_window_set_border_color", 2, 0, 0, nk_style_window_set_border_color);
  scm_c_define_gsubr("nk_style_window_border_color", 1, 0, 0, nk_style_window_border_color);
  scm_c_define_gsubr("nk_style_window_set_background", 2, 0, 0, nk_style_window_set_background);
  scm_c_define_gsubr("nk_style_window_background", 1, 0, 0, nk_style_window_background);
  scm_c_define_gsubr("nk_style_window_set_fixed_background", 2, 0, 0, nk_style_window_set_fixed_background);
  scm_c_define_gsubr("nk_style_window_fixed_background", 1, 0, 0, nk_style_window_fixed_background);
  scm_c_define_gsubr("nk_style_window_set_header", 2, 0, 0, nk_style_window_set_header);
  scm_c_define_gsubr("nk_style_window_header", 1, 0, 0, nk_style_window_header);
  scm_c_define_gsubr("nk_style_window_header_set_spacing", 2, 0, 0, nk_style_window_header_set_spacing);
  scm_c_define_gsubr("nk_style_window_header_spacing", 1, 0, 0, nk_style_window_header_spacing);
  scm_c_define_gsubr("nk_style_window_header_set_label_padding", 2, 0, 0, nk_style_window_header_set_label_padding);
  scm_c_define_gsubr("nk_style_window_header_label_padding", 1, 0, 0, nk_style_window_header_label_padding);
  scm_c_define_gsubr("nk_style_window_header_set_padding", 2, 0, 0, nk_style_window_header_set_padding);
  scm_c_define_gsubr("nk_style_window_header_padding", 1, 0, 0, nk_style_window_header_padding);
  scm_c_define_gsubr("nk_style_window_header_set_align", 2, 0, 0, nk_style_window_header_set_align);
  scm_c_define_gsubr("nk_style_window_header_align", 1, 0, 0, nk_style_window_header_align);
  scm_c_define_gsubr("nk_style_window_header_set_label_active", 2, 0, 0, nk_style_window_header_set_label_active);
  scm_c_define_gsubr("nk_style_window_header_label_active", 1, 0, 0, nk_style_window_header_label_active);
  scm_c_define_gsubr("nk_style_window_header_set_label_hover", 2, 0, 0, nk_style_window_header_set_label_hover);
  scm_c_define_gsubr("nk_style_window_header_label_hover", 1, 0, 0, nk_style_window_header_label_hover);
  scm_c_define_gsubr("nk_style_window_header_set_label_normal", 2, 0, 0, nk_style_window_header_set_label_normal);
  scm_c_define_gsubr("nk_style_window_header_label_normal", 1, 0, 0, nk_style_window_header_label_normal);
  scm_c_define_gsubr("nk_style_window_header_set_maximize_symbol", 2, 0, 0, nk_style_window_header_set_maximize_symbol);
  scm_c_define_gsubr("nk_style_window_header_maximize_symbol", 1, 0, 0, nk_style_window_header_maximize_symbol);
  scm_c_define_gsubr("nk_style_window_header_set_minimize_symbol", 2, 0, 0, nk_style_window_header_set_minimize_symbol);
  scm_c_define_gsubr("nk_style_window_header_minimize_symbol", 1, 0, 0, nk_style_window_header_minimize_symbol);
  scm_c_define_gsubr("nk_style_window_header_set_close_symbol", 2, 0, 0, nk_style_window_header_set_close_symbol);
  scm_c_define_gsubr("nk_style_window_header_close_symbol", 1, 0, 0, nk_style_window_header_close_symbol);
  scm_c_define_gsubr("nk_style_window_header_set_minimize_button", 2, 0, 0, nk_style_window_header_set_minimize_button);
  scm_c_define_gsubr("nk_style_window_header_minimize_button", 1, 0, 0, nk_style_window_header_minimize_button);
  scm_c_define_gsubr("nk_style_window_header_set_close_button", 2, 0, 0, nk_style_window_header_set_close_button);
  scm_c_define_gsubr("nk_style_window_header_close_button", 1, 0, 0, nk_style_window_header_close_button);
  scm_c_define_gsubr("nk_style_window_header_set_active", 2, 0, 0, nk_style_window_header_set_active);
  scm_c_define_gsubr("nk_style_window_header_active", 1, 0, 0, nk_style_window_header_active);
  scm_c_define_gsubr("nk_style_window_header_set_hover", 2, 0, 0, nk_style_window_header_set_hover);
  scm_c_define_gsubr("nk_style_window_header_hover", 1, 0, 0, nk_style_window_header_hover);
  scm_c_define_gsubr("nk_style_window_header_set_normal", 2, 0, 0, nk_style_window_header_set_normal);
  scm_c_define_gsubr("nk_style_window_header_normal", 1, 0, 0, nk_style_window_header_normal);
  scm_c_define_gsubr("nk_style_tab_set_spacing", 2, 0, 0, nk_style_tab_set_spacing);
  scm_c_define_gsubr("nk_style_tab_spacing", 1, 0, 0, nk_style_tab_spacing);
  scm_c_define_gsubr("nk_style_tab_set_padding", 2, 0, 0, nk_style_tab_set_padding);
  scm_c_define_gsubr("nk_style_tab_padding", 1, 0, 0, nk_style_tab_padding);
  scm_c_define_gsubr("nk_style_tab_set_indent", 2, 0, 0, nk_style_tab_set_indent);
  scm_c_define_gsubr("nk_style_tab_indent", 1, 0, 0, nk_style_tab_indent);
  scm_c_define_gsubr("nk_style_tab_set_rounding", 2, 0, 0, nk_style_tab_set_rounding);
  scm_c_define_gsubr("nk_style_tab_rounding", 1, 0, 0, nk_style_tab_rounding);
  scm_c_define_gsubr("nk_style_tab_set_border", 2, 0, 0, nk_style_tab_set_border);
  scm_c_define_gsubr("nk_style_tab_border", 1, 0, 0, nk_style_tab_border);
  scm_c_define_gsubr("nk_style_tab_set_sym_maximize", 2, 0, 0, nk_style_tab_set_sym_maximize);
  scm_c_define_gsubr("nk_style_tab_sym_maximize", 1, 0, 0, nk_style_tab_sym_maximize);
  scm_c_define_gsubr("nk_style_tab_set_sym_minimize", 2, 0, 0, nk_style_tab_set_sym_minimize);
  scm_c_define_gsubr("nk_style_tab_sym_minimize", 1, 0, 0, nk_style_tab_sym_minimize);
  scm_c_define_gsubr("nk_style_tab_set_node_minimize_button", 2, 0, 0, nk_style_tab_set_node_minimize_button);
  scm_c_define_gsubr("nk_style_tab_node_minimize_button", 1, 0, 0, nk_style_tab_node_minimize_button);
  scm_c_define_gsubr("nk_style_tab_set_node_maximize_button", 2, 0, 0, nk_style_tab_set_node_maximize_button);
  scm_c_define_gsubr("nk_style_tab_node_maximize_button", 1, 0, 0, nk_style_tab_node_maximize_button);
  scm_c_define_gsubr("nk_style_tab_set_tab_minimize_button", 2, 0, 0, nk_style_tab_set_tab_minimize_button);
  scm_c_define_gsubr("nk_style_tab_tab_minimize_button", 1, 0, 0, nk_style_tab_tab_minimize_button);
  scm_c_define_gsubr("nk_style_tab_set_tab_maximize_button", 2, 0, 0, nk_style_tab_set_tab_maximize_button);
  scm_c_define_gsubr("nk_style_tab_tab_maximize_button", 1, 0, 0, nk_style_tab_tab_maximize_button);
  scm_c_define_gsubr("nk_style_tab_set_text", 2, 0, 0, nk_style_tab_set_text);
  scm_c_define_gsubr("nk_style_tab_text", 1, 0, 0, nk_style_tab_text);
  scm_c_define_gsubr("nk_style_tab_set_border_color", 2, 0, 0, nk_style_tab_set_border_color);
  scm_c_define_gsubr("nk_style_tab_border_color", 1, 0, 0, nk_style_tab_border_color);
  scm_c_define_gsubr("nk_style_tab_set_background", 2, 0, 0, nk_style_tab_set_background);
  scm_c_define_gsubr("nk_style_tab_background", 1, 0, 0, nk_style_tab_background);
  scm_c_define_gsubr("nk_style_combo_set_spacing", 2, 0, 0, nk_style_combo_set_spacing);
  scm_c_define_gsubr("nk_style_combo_spacing", 1, 0, 0, nk_style_combo_spacing);
  scm_c_define_gsubr("nk_style_combo_set_button_padding", 2, 0, 0, nk_style_combo_set_button_padding);
  scm_c_define_gsubr("nk_style_combo_button_padding", 1, 0, 0, nk_style_combo_button_padding);
  scm_c_define_gsubr("nk_style_combo_set_content_padding", 2, 0, 0, nk_style_combo_set_content_padding);
  scm_c_define_gsubr("nk_style_combo_content_padding", 1, 0, 0, nk_style_combo_content_padding);
  scm_c_define_gsubr("nk_style_combo_set_rounding", 2, 0, 0, nk_style_combo_set_rounding);
  scm_c_define_gsubr("nk_style_combo_rounding", 1, 0, 0, nk_style_combo_rounding);
  scm_c_define_gsubr("nk_style_combo_set_border", 2, 0, 0, nk_style_combo_set_border);
  scm_c_define_gsubr("nk_style_combo_border", 1, 0, 0, nk_style_combo_border);
  scm_c_define_gsubr("nk_style_combo_set_sym_active", 2, 0, 0, nk_style_combo_set_sym_active);
  scm_c_define_gsubr("nk_style_combo_sym_active", 1, 0, 0, nk_style_combo_sym_active);
  scm_c_define_gsubr("nk_style_combo_set_sym_hover", 2, 0, 0, nk_style_combo_set_sym_hover);
  scm_c_define_gsubr("nk_style_combo_sym_hover", 1, 0, 0, nk_style_combo_sym_hover);
  scm_c_define_gsubr("nk_style_combo_set_sym_normal", 2, 0, 0, nk_style_combo_set_sym_normal);
  scm_c_define_gsubr("nk_style_combo_sym_normal", 1, 0, 0, nk_style_combo_sym_normal);
  scm_c_define_gsubr("nk_style_combo_set_button", 2, 0, 0, nk_style_combo_set_button);
  scm_c_define_gsubr("nk_style_combo_button", 1, 0, 0, nk_style_combo_button);
  scm_c_define_gsubr("nk_style_combo_set_symbol_active", 2, 0, 0, nk_style_combo_set_symbol_active);
  scm_c_define_gsubr("nk_style_combo_symbol_active", 1, 0, 0, nk_style_combo_symbol_active);
  scm_c_define_gsubr("nk_style_combo_set_symbol_hover", 2, 0, 0, nk_style_combo_set_symbol_hover);
  scm_c_define_gsubr("nk_style_combo_symbol_hover", 1, 0, 0, nk_style_combo_symbol_hover);
  scm_c_define_gsubr("nk_style_combo_set_symbol_normal", 2, 0, 0, nk_style_combo_set_symbol_normal);
  scm_c_define_gsubr("nk_style_combo_symbol_normal", 1, 0, 0, nk_style_combo_symbol_normal);
  scm_c_define_gsubr("nk_style_combo_set_label_active", 2, 0, 0, nk_style_combo_set_label_active);
  scm_c_define_gsubr("nk_style_combo_label_active", 1, 0, 0, nk_style_combo_label_active);
  scm_c_define_gsubr("nk_style_combo_set_label_hover", 2, 0, 0, nk_style_combo_set_label_hover);
  scm_c_define_gsubr("nk_style_combo_label_hover", 1, 0, 0, nk_style_combo_label_hover);
  scm_c_define_gsubr("nk_style_combo_set_label_normal", 2, 0, 0, nk_style_combo_set_label_normal);
  scm_c_define_gsubr("nk_style_combo_label_normal", 1, 0, 0, nk_style_combo_label_normal);
  scm_c_define_gsubr("nk_style_combo_set_border_color", 2, 0, 0, nk_style_combo_set_border_color);
  scm_c_define_gsubr("nk_style_combo_border_color", 1, 0, 0, nk_style_combo_border_color);
  scm_c_define_gsubr("nk_style_combo_set_active", 2, 0, 0, nk_style_combo_set_active);
  scm_c_define_gsubr("nk_style_combo_active", 1, 0, 0, nk_style_combo_active);
  scm_c_define_gsubr("nk_style_combo_set_hover", 2, 0, 0, nk_style_combo_set_hover);
  scm_c_define_gsubr("nk_style_combo_hover", 1, 0, 0, nk_style_combo_hover);
  scm_c_define_gsubr("nk_style_combo_set_normal", 2, 0, 0, nk_style_combo_set_normal);
  scm_c_define_gsubr("nk_style_combo_normal", 1, 0, 0, nk_style_combo_normal);
  scm_c_define_gsubr("nk_style_chart_set_padding", 2, 0, 0, nk_style_chart_set_padding);
  scm_c_define_gsubr("nk_style_chart_padding", 1, 0, 0, nk_style_chart_padding);
  scm_c_define_gsubr("nk_style_chart_set_rounding", 2, 0, 0, nk_style_chart_set_rounding);
  scm_c_define_gsubr("nk_style_chart_rounding", 1, 0, 0, nk_style_chart_rounding);
  scm_c_define_gsubr("nk_style_chart_set_border", 2, 0, 0, nk_style_chart_set_border);
  scm_c_define_gsubr("nk_style_chart_border", 1, 0, 0, nk_style_chart_border);
  scm_c_define_gsubr("nk_style_chart_set_color", 2, 0, 0, nk_style_chart_set_color);
  scm_c_define_gsubr("nk_style_chart_color", 1, 0, 0, nk_style_chart_color);
  scm_c_define_gsubr("nk_style_chart_set_selected_color", 2, 0, 0, nk_style_chart_set_selected_color);
  scm_c_define_gsubr("nk_style_chart_selected_color", 1, 0, 0, nk_style_chart_selected_color);
  scm_c_define_gsubr("nk_style_chart_set_border_color", 2, 0, 0, nk_style_chart_set_border_color);
  scm_c_define_gsubr("nk_style_chart_border_color", 1, 0, 0, nk_style_chart_border_color);
  scm_c_define_gsubr("nk_style_chart_set_background", 2, 0, 0, nk_style_chart_set_background);
  scm_c_define_gsubr("nk_style_chart_background", 1, 0, 0, nk_style_chart_background);
  scm_c_define_gsubr("nk_style_property_set_draw_end", 2, 0, 0, nk_style_property_set_draw_end);
  scm_c_define_gsubr("nk_style_property_draw_end", 1, 0, 0, nk_style_property_draw_end);
  scm_c_define_gsubr("nk_style_property_set_draw_begin", 2, 0, 0, nk_style_property_set_draw_begin);
  scm_c_define_gsubr("nk_style_property_draw_begin", 1, 0, 0, nk_style_property_draw_begin);
  scm_c_define_gsubr("nk_style_property_set_userdata", 2, 0, 0, nk_style_property_set_userdata);
  scm_c_define_gsubr("nk_style_property_userdata", 1, 0, 0, nk_style_property_userdata);
  scm_c_define_gsubr("nk_style_property_set_dec_button", 2, 0, 0, nk_style_property_set_dec_button);
  scm_c_define_gsubr("nk_style_property_dec_button", 1, 0, 0, nk_style_property_dec_button);
  scm_c_define_gsubr("nk_style_property_set_inc_button", 2, 0, 0, nk_style_property_set_inc_button);
  scm_c_define_gsubr("nk_style_property_inc_button", 1, 0, 0, nk_style_property_inc_button);
  scm_c_define_gsubr("nk_style_property_set_edit", 2, 0, 0, nk_style_property_set_edit);
  scm_c_define_gsubr("nk_style_property_edit", 1, 0, 0, nk_style_property_edit);
  scm_c_define_gsubr("nk_style_property_set_padding", 2, 0, 0, nk_style_property_set_padding);
  scm_c_define_gsubr("nk_style_property_padding", 1, 0, 0, nk_style_property_padding);
  scm_c_define_gsubr("nk_style_property_set_rounding", 2, 0, 0, nk_style_property_set_rounding);
  scm_c_define_gsubr("nk_style_property_rounding", 1, 0, 0, nk_style_property_rounding);
  scm_c_define_gsubr("nk_style_property_set_border", 2, 0, 0, nk_style_property_set_border);
  scm_c_define_gsubr("nk_style_property_border", 1, 0, 0, nk_style_property_border);
  scm_c_define_gsubr("nk_style_property_set_sym_right", 2, 0, 0, nk_style_property_set_sym_right);
  scm_c_define_gsubr("nk_style_property_sym_right", 1, 0, 0, nk_style_property_sym_right);
  scm_c_define_gsubr("nk_style_property_set_sym_left", 2, 0, 0, nk_style_property_set_sym_left);
  scm_c_define_gsubr("nk_style_property_sym_left", 1, 0, 0, nk_style_property_sym_left);
  scm_c_define_gsubr("nk_style_property_set_label_active", 2, 0, 0, nk_style_property_set_label_active);
  scm_c_define_gsubr("nk_style_property_label_active", 1, 0, 0, nk_style_property_label_active);
  scm_c_define_gsubr("nk_style_property_set_label_hover", 2, 0, 0, nk_style_property_set_label_hover);
  scm_c_define_gsubr("nk_style_property_label_hover", 1, 0, 0, nk_style_property_label_hover);
  scm_c_define_gsubr("nk_style_property_set_label_normal", 2, 0, 0, nk_style_property_set_label_normal);
  scm_c_define_gsubr("nk_style_property_label_normal", 1, 0, 0, nk_style_property_label_normal);
  scm_c_define_gsubr("nk_style_property_set_border_color", 2, 0, 0, nk_style_property_set_border_color);
  scm_c_define_gsubr("nk_style_property_border_color", 1, 0, 0, nk_style_property_border_color);
  scm_c_define_gsubr("nk_style_property_set_active", 2, 0, 0, nk_style_property_set_active);
  scm_c_define_gsubr("nk_style_property_active", 1, 0, 0, nk_style_property_active);
  scm_c_define_gsubr("nk_style_property_set_hover", 2, 0, 0, nk_style_property_set_hover);
  scm_c_define_gsubr("nk_style_property_hover", 1, 0, 0, nk_style_property_hover);
  scm_c_define_gsubr("nk_style_property_set_normal", 2, 0, 0, nk_style_property_set_normal);
  scm_c_define_gsubr("nk_style_property_normal", 1, 0, 0, nk_style_property_normal);
  scm_c_define_gsubr("nk_style_edit_set_row_padding", 2, 0, 0, nk_style_edit_set_row_padding);
  scm_c_define_gsubr("nk_style_edit_row_padding", 1, 0, 0, nk_style_edit_row_padding);
  scm_c_define_gsubr("nk_style_edit_set_padding", 2, 0, 0, nk_style_edit_set_padding);
  scm_c_define_gsubr("nk_style_edit_padding", 1, 0, 0, nk_style_edit_padding);
  scm_c_define_gsubr("nk_style_edit_set_scrollbar_size", 2, 0, 0, nk_style_edit_set_scrollbar_size);
  scm_c_define_gsubr("nk_style_edit_scrollbar_size", 1, 0, 0, nk_style_edit_scrollbar_size);
  scm_c_define_gsubr("nk_style_edit_set_cursor_size", 2, 0, 0, nk_style_edit_set_cursor_size);
  scm_c_define_gsubr("nk_style_edit_cursor_size", 1, 0, 0, nk_style_edit_cursor_size);
  scm_c_define_gsubr("nk_style_edit_set_rounding", 2, 0, 0, nk_style_edit_set_rounding);
  scm_c_define_gsubr("nk_style_edit_rounding", 1, 0, 0, nk_style_edit_rounding);
  scm_c_define_gsubr("nk_style_edit_set_border", 2, 0, 0, nk_style_edit_set_border);
  scm_c_define_gsubr("nk_style_edit_border", 1, 0, 0, nk_style_edit_border);
  scm_c_define_gsubr("nk_style_edit_set_selected_text_hover", 2, 0, 0, nk_style_edit_set_selected_text_hover);
  scm_c_define_gsubr("nk_style_edit_selected_text_hover", 1, 0, 0, nk_style_edit_selected_text_hover);
  scm_c_define_gsubr("nk_style_edit_set_selected_text_normal", 2, 0, 0, nk_style_edit_set_selected_text_normal);
  scm_c_define_gsubr("nk_style_edit_selected_text_normal", 1, 0, 0, nk_style_edit_selected_text_normal);
  scm_c_define_gsubr("nk_style_edit_set_selected_hover", 2, 0, 0, nk_style_edit_set_selected_hover);
  scm_c_define_gsubr("nk_style_edit_selected_hover", 1, 0, 0, nk_style_edit_selected_hover);
  scm_c_define_gsubr("nk_style_edit_set_selected_normal", 2, 0, 0, nk_style_edit_set_selected_normal);
  scm_c_define_gsubr("nk_style_edit_selected_normal", 1, 0, 0, nk_style_edit_selected_normal);
  scm_c_define_gsubr("nk_style_edit_set_text_active", 2, 0, 0, nk_style_edit_set_text_active);
  scm_c_define_gsubr("nk_style_edit_text_active", 1, 0, 0, nk_style_edit_text_active);
  scm_c_define_gsubr("nk_style_edit_set_text_hover", 2, 0, 0, nk_style_edit_set_text_hover);
  scm_c_define_gsubr("nk_style_edit_text_hover", 1, 0, 0, nk_style_edit_text_hover);
  scm_c_define_gsubr("nk_style_edit_set_text_normal", 2, 0, 0, nk_style_edit_set_text_normal);
  scm_c_define_gsubr("nk_style_edit_text_normal", 1, 0, 0, nk_style_edit_text_normal);
  scm_c_define_gsubr("nk_style_edit_set_cursor_text_hover", 2, 0, 0, nk_style_edit_set_cursor_text_hover);
  scm_c_define_gsubr("nk_style_edit_cursor_text_hover", 1, 0, 0, nk_style_edit_cursor_text_hover);
  scm_c_define_gsubr("nk_style_edit_set_cursor_text_normal", 2, 0, 0, nk_style_edit_set_cursor_text_normal);
  scm_c_define_gsubr("nk_style_edit_cursor_text_normal", 1, 0, 0, nk_style_edit_cursor_text_normal);
  scm_c_define_gsubr("nk_style_edit_set_cursor_hover", 2, 0, 0, nk_style_edit_set_cursor_hover);
  scm_c_define_gsubr("nk_style_edit_cursor_hover", 1, 0, 0, nk_style_edit_cursor_hover);
  scm_c_define_gsubr("nk_style_edit_set_cursor_normal", 2, 0, 0, nk_style_edit_set_cursor_normal);
  scm_c_define_gsubr("nk_style_edit_cursor_normal", 1, 0, 0, nk_style_edit_cursor_normal);
  scm_c_define_gsubr("nk_style_edit_set_scrollbar", 2, 0, 0, nk_style_edit_set_scrollbar);
  scm_c_define_gsubr("nk_style_edit_scrollbar", 1, 0, 0, nk_style_edit_scrollbar);
  scm_c_define_gsubr("nk_style_edit_set_border_color", 2, 0, 0, nk_style_edit_set_border_color);
  scm_c_define_gsubr("nk_style_edit_border_color", 1, 0, 0, nk_style_edit_border_color);
  scm_c_define_gsubr("nk_style_edit_set_active", 2, 0, 0, nk_style_edit_set_active);
  scm_c_define_gsubr("nk_style_edit_active", 1, 0, 0, nk_style_edit_active);
  scm_c_define_gsubr("nk_style_edit_set_hover", 2, 0, 0, nk_style_edit_set_hover);
  scm_c_define_gsubr("nk_style_edit_hover", 1, 0, 0, nk_style_edit_hover);
  scm_c_define_gsubr("nk_style_edit_set_normal", 2, 0, 0, nk_style_edit_set_normal);
  scm_c_define_gsubr("nk_style_edit_normal", 1, 0, 0, nk_style_edit_normal);
  scm_c_define_gsubr("nk_style_scrollbar_set_draw_end", 2, 0, 0, nk_style_scrollbar_set_draw_end);
  scm_c_define_gsubr("nk_style_scrollbar_draw_end", 1, 0, 0, nk_style_scrollbar_draw_end);
  scm_c_define_gsubr("nk_style_scrollbar_set_draw_begin", 2, 0, 0, nk_style_scrollbar_set_draw_begin);
  scm_c_define_gsubr("nk_style_scrollbar_draw_begin", 1, 0, 0, nk_style_scrollbar_draw_begin);
  scm_c_define_gsubr("nk_style_scrollbar_set_userdata", 2, 0, 0, nk_style_scrollbar_set_userdata);
  scm_c_define_gsubr("nk_style_scrollbar_userdata", 1, 0, 0, nk_style_scrollbar_userdata);
  scm_c_define_gsubr("nk_style_scrollbar_set_dec_symbol", 2, 0, 0, nk_style_scrollbar_set_dec_symbol);
  scm_c_define_gsubr("nk_style_scrollbar_dec_symbol", 1, 0, 0, nk_style_scrollbar_dec_symbol);
  scm_c_define_gsubr("nk_style_scrollbar_set_inc_symbol", 2, 0, 0, nk_style_scrollbar_set_inc_symbol);
  scm_c_define_gsubr("nk_style_scrollbar_inc_symbol", 1, 0, 0, nk_style_scrollbar_inc_symbol);
  scm_c_define_gsubr("nk_style_scrollbar_set_dec_button", 2, 0, 0, nk_style_scrollbar_set_dec_button);
  scm_c_define_gsubr("nk_style_scrollbar_dec_button", 1, 0, 0, nk_style_scrollbar_dec_button);
  scm_c_define_gsubr("nk_style_scrollbar_set_inc_button", 2, 0, 0, nk_style_scrollbar_set_inc_button);
  scm_c_define_gsubr("nk_style_scrollbar_inc_button", 1, 0, 0, nk_style_scrollbar_inc_button);
  scm_c_define_gsubr("nk_style_scrollbar_set_show_buttons", 2, 0, 0, nk_style_scrollbar_set_show_buttons);
  scm_c_define_gsubr("nk_style_scrollbar_show_buttons", 1, 0, 0, nk_style_scrollbar_show_buttons);
  scm_c_define_gsubr("nk_style_scrollbar_set_padding", 2, 0, 0, nk_style_scrollbar_set_padding);
  scm_c_define_gsubr("nk_style_scrollbar_padding", 1, 0, 0, nk_style_scrollbar_padding);
  scm_c_define_gsubr("nk_style_scrollbar_set_rounding_cursor", 2, 0, 0, nk_style_scrollbar_set_rounding_cursor);
  scm_c_define_gsubr("nk_style_scrollbar_rounding_cursor", 1, 0, 0, nk_style_scrollbar_rounding_cursor);
  scm_c_define_gsubr("nk_style_scrollbar_set_border_cursor", 2, 0, 0, nk_style_scrollbar_set_border_cursor);
  scm_c_define_gsubr("nk_style_scrollbar_border_cursor", 1, 0, 0, nk_style_scrollbar_border_cursor);
  scm_c_define_gsubr("nk_style_scrollbar_set_rounding", 2, 0, 0, nk_style_scrollbar_set_rounding);
  scm_c_define_gsubr("nk_style_scrollbar_rounding", 1, 0, 0, nk_style_scrollbar_rounding);
  scm_c_define_gsubr("nk_style_scrollbar_set_border", 2, 0, 0, nk_style_scrollbar_set_border);
  scm_c_define_gsubr("nk_style_scrollbar_border", 1, 0, 0, nk_style_scrollbar_border);
  scm_c_define_gsubr("nk_style_scrollbar_set_cursor_border_color", 2, 0, 0, nk_style_scrollbar_set_cursor_border_color);
  scm_c_define_gsubr("nk_style_scrollbar_cursor_border_color", 1, 0, 0, nk_style_scrollbar_cursor_border_color);
  scm_c_define_gsubr("nk_style_scrollbar_set_cursor_active", 2, 0, 0, nk_style_scrollbar_set_cursor_active);
  scm_c_define_gsubr("nk_style_scrollbar_cursor_active", 1, 0, 0, nk_style_scrollbar_cursor_active);
  scm_c_define_gsubr("nk_style_scrollbar_set_cursor_hover", 2, 0, 0, nk_style_scrollbar_set_cursor_hover);
  scm_c_define_gsubr("nk_style_scrollbar_cursor_hover", 1, 0, 0, nk_style_scrollbar_cursor_hover);
  scm_c_define_gsubr("nk_style_scrollbar_set_cursor_normal", 2, 0, 0, nk_style_scrollbar_set_cursor_normal);
  scm_c_define_gsubr("nk_style_scrollbar_cursor_normal", 1, 0, 0, nk_style_scrollbar_cursor_normal);
  scm_c_define_gsubr("nk_style_scrollbar_set_border_color", 2, 0, 0, nk_style_scrollbar_set_border_color);
  scm_c_define_gsubr("nk_style_scrollbar_border_color", 1, 0, 0, nk_style_scrollbar_border_color);
  scm_c_define_gsubr("nk_style_scrollbar_set_active", 2, 0, 0, nk_style_scrollbar_set_active);
  scm_c_define_gsubr("nk_style_scrollbar_active", 1, 0, 0, nk_style_scrollbar_active);
  scm_c_define_gsubr("nk_style_scrollbar_set_hover", 2, 0, 0, nk_style_scrollbar_set_hover);
  scm_c_define_gsubr("nk_style_scrollbar_hover", 1, 0, 0, nk_style_scrollbar_hover);
  scm_c_define_gsubr("nk_style_scrollbar_set_normal", 2, 0, 0, nk_style_scrollbar_set_normal);
  scm_c_define_gsubr("nk_style_scrollbar_normal", 1, 0, 0, nk_style_scrollbar_normal);
  scm_c_define_gsubr("nk_style_progress_set_draw_end", 2, 0, 0, nk_style_progress_set_draw_end);
  scm_c_define_gsubr("nk_style_progress_draw_end", 1, 0, 0, nk_style_progress_draw_end);
  scm_c_define_gsubr("nk_style_progress_set_draw_begin", 2, 0, 0, nk_style_progress_set_draw_begin);
  scm_c_define_gsubr("nk_style_progress_draw_begin", 1, 0, 0, nk_style_progress_draw_begin);
  scm_c_define_gsubr("nk_style_progress_set_userdata", 2, 0, 0, nk_style_progress_set_userdata);
  scm_c_define_gsubr("nk_style_progress_userdata", 1, 0, 0, nk_style_progress_userdata);
  scm_c_define_gsubr("nk_style_progress_set_padding", 2, 0, 0, nk_style_progress_set_padding);
  scm_c_define_gsubr("nk_style_progress_padding", 1, 0, 0, nk_style_progress_padding);
  scm_c_define_gsubr("nk_style_progress_set_cursor_rounding", 2, 0, 0, nk_style_progress_set_cursor_rounding);
  scm_c_define_gsubr("nk_style_progress_cursor_rounding", 1, 0, 0, nk_style_progress_cursor_rounding);
  scm_c_define_gsubr("nk_style_progress_set_cursor_border", 2, 0, 0, nk_style_progress_set_cursor_border);
  scm_c_define_gsubr("nk_style_progress_cursor_border", 1, 0, 0, nk_style_progress_cursor_border);
  scm_c_define_gsubr("nk_style_progress_set_border", 2, 0, 0, nk_style_progress_set_border);
  scm_c_define_gsubr("nk_style_progress_border", 1, 0, 0, nk_style_progress_border);
  scm_c_define_gsubr("nk_style_progress_set_rounding", 2, 0, 0, nk_style_progress_set_rounding);
  scm_c_define_gsubr("nk_style_progress_rounding", 1, 0, 0, nk_style_progress_rounding);
  scm_c_define_gsubr("nk_style_progress_set_cursor_border_color", 2, 0, 0, nk_style_progress_set_cursor_border_color);
  scm_c_define_gsubr("nk_style_progress_cursor_border_color", 1, 0, 0, nk_style_progress_cursor_border_color);
  scm_c_define_gsubr("nk_style_progress_set_cursor_active", 2, 0, 0, nk_style_progress_set_cursor_active);
  scm_c_define_gsubr("nk_style_progress_cursor_active", 1, 0, 0, nk_style_progress_cursor_active);
  scm_c_define_gsubr("nk_style_progress_set_cursor_hover", 2, 0, 0, nk_style_progress_set_cursor_hover);
  scm_c_define_gsubr("nk_style_progress_cursor_hover", 1, 0, 0, nk_style_progress_cursor_hover);
  scm_c_define_gsubr("nk_style_progress_set_cursor_normal", 2, 0, 0, nk_style_progress_set_cursor_normal);
  scm_c_define_gsubr("nk_style_progress_cursor_normal", 1, 0, 0, nk_style_progress_cursor_normal);
  scm_c_define_gsubr("nk_style_progress_set_border_color", 2, 0, 0, nk_style_progress_set_border_color);
  scm_c_define_gsubr("nk_style_progress_border_color", 1, 0, 0, nk_style_progress_border_color);
  scm_c_define_gsubr("nk_style_progress_set_active", 2, 0, 0, nk_style_progress_set_active);
  scm_c_define_gsubr("nk_style_progress_active", 1, 0, 0, nk_style_progress_active);
  scm_c_define_gsubr("nk_style_progress_set_hover", 2, 0, 0, nk_style_progress_set_hover);
  scm_c_define_gsubr("nk_style_progress_hover", 1, 0, 0, nk_style_progress_hover);
  scm_c_define_gsubr("nk_style_progress_set_normal", 2, 0, 0, nk_style_progress_set_normal);
  scm_c_define_gsubr("nk_style_progress_normal", 1, 0, 0, nk_style_progress_normal);
  scm_c_define_gsubr("nk_style_slider_set_draw_end", 2, 0, 0, nk_style_slider_set_draw_end);
  scm_c_define_gsubr("nk_style_slider_draw_end", 1, 0, 0, nk_style_slider_draw_end);
  scm_c_define_gsubr("nk_style_slider_set_draw_begin", 2, 0, 0, nk_style_slider_set_draw_begin);
  scm_c_define_gsubr("nk_style_slider_draw_begin", 1, 0, 0, nk_style_slider_draw_begin);
  scm_c_define_gsubr("nk_style_slider_set_userdata", 2, 0, 0, nk_style_slider_set_userdata);
  scm_c_define_gsubr("nk_style_slider_userdata", 1, 0, 0, nk_style_slider_userdata);
  scm_c_define_gsubr("nk_style_slider_set_dec_symbol", 2, 0, 0, nk_style_slider_set_dec_symbol);
  scm_c_define_gsubr("nk_style_slider_dec_symbol", 1, 0, 0, nk_style_slider_dec_symbol);
  scm_c_define_gsubr("nk_style_slider_set_inc_symbol", 2, 0, 0, nk_style_slider_set_inc_symbol);
  scm_c_define_gsubr("nk_style_slider_inc_symbol", 1, 0, 0, nk_style_slider_inc_symbol);
  scm_c_define_gsubr("nk_style_slider_set_dec_button", 2, 0, 0, nk_style_slider_set_dec_button);
  scm_c_define_gsubr("nk_style_slider_dec_button", 1, 0, 0, nk_style_slider_dec_button);
  scm_c_define_gsubr("nk_style_slider_set_inc_button", 2, 0, 0, nk_style_slider_set_inc_button);
  scm_c_define_gsubr("nk_style_slider_inc_button", 1, 0, 0, nk_style_slider_inc_button);
  scm_c_define_gsubr("nk_style_slider_set_show_buttons", 2, 0, 0, nk_style_slider_set_show_buttons);
  scm_c_define_gsubr("nk_style_slider_show_buttons", 1, 0, 0, nk_style_slider_show_buttons);
  scm_c_define_gsubr("nk_style_slider_set_cursor_size", 2, 0, 0, nk_style_slider_set_cursor_size);
  scm_c_define_gsubr("nk_style_slider_cursor_size", 1, 0, 0, nk_style_slider_cursor_size);
  scm_c_define_gsubr("nk_style_slider_set_spacing", 2, 0, 0, nk_style_slider_set_spacing);
  scm_c_define_gsubr("nk_style_slider_spacing", 1, 0, 0, nk_style_slider_spacing);
  scm_c_define_gsubr("nk_style_slider_set_padding", 2, 0, 0, nk_style_slider_set_padding);
  scm_c_define_gsubr("nk_style_slider_padding", 1, 0, 0, nk_style_slider_padding);
  scm_c_define_gsubr("nk_style_slider_set_bar_height", 2, 0, 0, nk_style_slider_set_bar_height);
  scm_c_define_gsubr("nk_style_slider_bar_height", 1, 0, 0, nk_style_slider_bar_height);
  scm_c_define_gsubr("nk_style_slider_set_rounding", 2, 0, 0, nk_style_slider_set_rounding);
  scm_c_define_gsubr("nk_style_slider_rounding", 1, 0, 0, nk_style_slider_rounding);
  scm_c_define_gsubr("nk_style_slider_set_border", 2, 0, 0, nk_style_slider_set_border);
  scm_c_define_gsubr("nk_style_slider_border", 1, 0, 0, nk_style_slider_border);
  scm_c_define_gsubr("nk_style_slider_set_cursor_active", 2, 0, 0, nk_style_slider_set_cursor_active);
  scm_c_define_gsubr("nk_style_slider_cursor_active", 1, 0, 0, nk_style_slider_cursor_active);
  scm_c_define_gsubr("nk_style_slider_set_cursor_hover", 2, 0, 0, nk_style_slider_set_cursor_hover);
  scm_c_define_gsubr("nk_style_slider_cursor_hover", 1, 0, 0, nk_style_slider_cursor_hover);
  scm_c_define_gsubr("nk_style_slider_set_cursor_normal", 2, 0, 0, nk_style_slider_set_cursor_normal);
  scm_c_define_gsubr("nk_style_slider_cursor_normal", 1, 0, 0, nk_style_slider_cursor_normal);
  scm_c_define_gsubr("nk_style_slider_set_bar_filled", 2, 0, 0, nk_style_slider_set_bar_filled);
  scm_c_define_gsubr("nk_style_slider_bar_filled", 1, 0, 0, nk_style_slider_bar_filled);
  scm_c_define_gsubr("nk_style_slider_set_bar_active", 2, 0, 0, nk_style_slider_set_bar_active);
  scm_c_define_gsubr("nk_style_slider_bar_active", 1, 0, 0, nk_style_slider_bar_active);
  scm_c_define_gsubr("nk_style_slider_set_bar_hover", 2, 0, 0, nk_style_slider_set_bar_hover);
  scm_c_define_gsubr("nk_style_slider_bar_hover", 1, 0, 0, nk_style_slider_bar_hover);
  scm_c_define_gsubr("nk_style_slider_set_bar_normal", 2, 0, 0, nk_style_slider_set_bar_normal);
  scm_c_define_gsubr("nk_style_slider_bar_normal", 1, 0, 0, nk_style_slider_bar_normal);
  scm_c_define_gsubr("nk_style_slider_set_border_color", 2, 0, 0, nk_style_slider_set_border_color);
  scm_c_define_gsubr("nk_style_slider_border_color", 1, 0, 0, nk_style_slider_border_color);
  scm_c_define_gsubr("nk_style_slider_set_active", 2, 0, 0, nk_style_slider_set_active);
  scm_c_define_gsubr("nk_style_slider_active", 1, 0, 0, nk_style_slider_active);
  scm_c_define_gsubr("nk_style_slider_set_hover", 2, 0, 0, nk_style_slider_set_hover);
  scm_c_define_gsubr("nk_style_slider_hover", 1, 0, 0, nk_style_slider_hover);
  scm_c_define_gsubr("nk_style_slider_set_normal", 2, 0, 0, nk_style_slider_set_normal);
  scm_c_define_gsubr("nk_style_slider_normal", 1, 0, 0, nk_style_slider_normal);
  scm_c_define_gsubr("nk_style_selectable_set_draw_end", 2, 0, 0, nk_style_selectable_set_draw_end);
  scm_c_define_gsubr("nk_style_selectable_draw_end", 1, 0, 0, nk_style_selectable_draw_end);
  scm_c_define_gsubr("nk_style_selectable_set_draw_begin", 2, 0, 0, nk_style_selectable_set_draw_begin);
  scm_c_define_gsubr("nk_style_selectable_draw_begin", 1, 0, 0, nk_style_selectable_draw_begin);
  scm_c_define_gsubr("nk_style_selectable_set_userdata", 2, 0, 0, nk_style_selectable_set_userdata);
  scm_c_define_gsubr("nk_style_selectable_userdata", 1, 0, 0, nk_style_selectable_userdata);
  scm_c_define_gsubr("nk_style_selectable_set_image_padding", 2, 0, 0, nk_style_selectable_set_image_padding);
  scm_c_define_gsubr("nk_style_selectable_image_padding", 1, 0, 0, nk_style_selectable_image_padding);
  scm_c_define_gsubr("nk_style_selectable_set_touch_padding", 2, 0, 0, nk_style_selectable_set_touch_padding);
  scm_c_define_gsubr("nk_style_selectable_touch_padding", 1, 0, 0, nk_style_selectable_touch_padding);
  scm_c_define_gsubr("nk_style_selectable_set_padding", 2, 0, 0, nk_style_selectable_set_padding);
  scm_c_define_gsubr("nk_style_selectable_padding", 1, 0, 0, nk_style_selectable_padding);
  scm_c_define_gsubr("nk_style_selectable_set_rounding", 2, 0, 0, nk_style_selectable_set_rounding);
  scm_c_define_gsubr("nk_style_selectable_rounding", 1, 0, 0, nk_style_selectable_rounding);
  scm_c_define_gsubr("nk_style_selectable_set_text_alignment", 2, 0, 0, nk_style_selectable_set_text_alignment);
  scm_c_define_gsubr("nk_style_selectable_text_alignment", 1, 0, 0, nk_style_selectable_text_alignment);
  scm_c_define_gsubr("nk_style_selectable_set_text_background", 2, 0, 0, nk_style_selectable_set_text_background);
  scm_c_define_gsubr("nk_style_selectable_text_background", 1, 0, 0, nk_style_selectable_text_background);
  scm_c_define_gsubr("nk_style_selectable_set_text_pressed_active", 2, 0, 0, nk_style_selectable_set_text_pressed_active);
  scm_c_define_gsubr("nk_style_selectable_text_pressed_active", 1, 0, 0, nk_style_selectable_text_pressed_active);
  scm_c_define_gsubr("nk_style_selectable_set_text_hover_active", 2, 0, 0, nk_style_selectable_set_text_hover_active);
  scm_c_define_gsubr("nk_style_selectable_text_hover_active", 1, 0, 0, nk_style_selectable_text_hover_active);
  scm_c_define_gsubr("nk_style_selectable_set_text_normal_active", 2, 0, 0, nk_style_selectable_set_text_normal_active);
  scm_c_define_gsubr("nk_style_selectable_text_normal_active", 1, 0, 0, nk_style_selectable_text_normal_active);
  scm_c_define_gsubr("nk_style_selectable_set_text_pressed", 2, 0, 0, nk_style_selectable_set_text_pressed);
  scm_c_define_gsubr("nk_style_selectable_text_pressed", 1, 0, 0, nk_style_selectable_text_pressed);
  scm_c_define_gsubr("nk_style_selectable_set_text_hover", 2, 0, 0, nk_style_selectable_set_text_hover);
  scm_c_define_gsubr("nk_style_selectable_text_hover", 1, 0, 0, nk_style_selectable_text_hover);
  scm_c_define_gsubr("nk_style_selectable_set_text_normal", 2, 0, 0, nk_style_selectable_set_text_normal);
  scm_c_define_gsubr("nk_style_selectable_text_normal", 1, 0, 0, nk_style_selectable_text_normal);
  scm_c_define_gsubr("nk_style_selectable_set_pressed_active", 2, 0, 0, nk_style_selectable_set_pressed_active);
  scm_c_define_gsubr("nk_style_selectable_pressed_active", 1, 0, 0, nk_style_selectable_pressed_active);
  scm_c_define_gsubr("nk_style_selectable_set_hover_active", 2, 0, 0, nk_style_selectable_set_hover_active);
  scm_c_define_gsubr("nk_style_selectable_hover_active", 1, 0, 0, nk_style_selectable_hover_active);
  scm_c_define_gsubr("nk_style_selectable_set_normal_active", 2, 0, 0, nk_style_selectable_set_normal_active);
  scm_c_define_gsubr("nk_style_selectable_normal_active", 1, 0, 0, nk_style_selectable_normal_active);
  scm_c_define_gsubr("nk_style_selectable_set_pressed", 2, 0, 0, nk_style_selectable_set_pressed);
  scm_c_define_gsubr("nk_style_selectable_pressed", 1, 0, 0, nk_style_selectable_pressed);
  scm_c_define_gsubr("nk_style_selectable_set_hover", 2, 0, 0, nk_style_selectable_set_hover);
  scm_c_define_gsubr("nk_style_selectable_hover", 1, 0, 0, nk_style_selectable_hover);
  scm_c_define_gsubr("nk_style_selectable_set_normal", 2, 0, 0, nk_style_selectable_set_normal);
  scm_c_define_gsubr("nk_style_selectable_normal", 1, 0, 0, nk_style_selectable_normal);
  scm_c_define_gsubr("nk_style_toggle_set_draw_end", 2, 0, 0, nk_style_toggle_set_draw_end);
  scm_c_define_gsubr("nk_style_toggle_draw_end", 1, 0, 0, nk_style_toggle_draw_end);
  scm_c_define_gsubr("nk_style_toggle_set_draw_begin", 2, 0, 0, nk_style_toggle_set_draw_begin);
  scm_c_define_gsubr("nk_style_toggle_draw_begin", 1, 0, 0, nk_style_toggle_draw_begin);
  scm_c_define_gsubr("nk_style_toggle_set_userdata", 2, 0, 0, nk_style_toggle_set_userdata);
  scm_c_define_gsubr("nk_style_toggle_userdata", 1, 0, 0, nk_style_toggle_userdata);
  scm_c_define_gsubr("nk_style_toggle_set_border", 2, 0, 0, nk_style_toggle_set_border);
  scm_c_define_gsubr("nk_style_toggle_border", 1, 0, 0, nk_style_toggle_border);
  scm_c_define_gsubr("nk_style_toggle_set_spacing", 2, 0, 0, nk_style_toggle_set_spacing);
  scm_c_define_gsubr("nk_style_toggle_spacing", 1, 0, 0, nk_style_toggle_spacing);
  scm_c_define_gsubr("nk_style_toggle_set_touch_padding", 2, 0, 0, nk_style_toggle_set_touch_padding);
  scm_c_define_gsubr("nk_style_toggle_touch_padding", 1, 0, 0, nk_style_toggle_touch_padding);
  scm_c_define_gsubr("nk_style_toggle_set_padding", 2, 0, 0, nk_style_toggle_set_padding);
  scm_c_define_gsubr("nk_style_toggle_padding", 1, 0, 0, nk_style_toggle_padding);
  scm_c_define_gsubr("nk_style_toggle_set_text_alignment", 2, 0, 0, nk_style_toggle_set_text_alignment);
  scm_c_define_gsubr("nk_style_toggle_text_alignment", 1, 0, 0, nk_style_toggle_text_alignment);
  scm_c_define_gsubr("nk_style_toggle_set_text_background", 2, 0, 0, nk_style_toggle_set_text_background);
  scm_c_define_gsubr("nk_style_toggle_text_background", 1, 0, 0, nk_style_toggle_text_background);
  scm_c_define_gsubr("nk_style_toggle_set_text_active", 2, 0, 0, nk_style_toggle_set_text_active);
  scm_c_define_gsubr("nk_style_toggle_text_active", 1, 0, 0, nk_style_toggle_text_active);
  scm_c_define_gsubr("nk_style_toggle_set_text_hover", 2, 0, 0, nk_style_toggle_set_text_hover);
  scm_c_define_gsubr("nk_style_toggle_text_hover", 1, 0, 0, nk_style_toggle_text_hover);
  scm_c_define_gsubr("nk_style_toggle_set_text_normal", 2, 0, 0, nk_style_toggle_set_text_normal);
  scm_c_define_gsubr("nk_style_toggle_text_normal", 1, 0, 0, nk_style_toggle_text_normal);
  scm_c_define_gsubr("nk_style_toggle_set_cursor_hover", 2, 0, 0, nk_style_toggle_set_cursor_hover);
  scm_c_define_gsubr("nk_style_toggle_cursor_hover", 1, 0, 0, nk_style_toggle_cursor_hover);
  scm_c_define_gsubr("nk_style_toggle_set_cursor_normal", 2, 0, 0, nk_style_toggle_set_cursor_normal);
  scm_c_define_gsubr("nk_style_toggle_cursor_normal", 1, 0, 0, nk_style_toggle_cursor_normal);
  scm_c_define_gsubr("nk_style_toggle_set_border_color", 2, 0, 0, nk_style_toggle_set_border_color);
  scm_c_define_gsubr("nk_style_toggle_border_color", 1, 0, 0, nk_style_toggle_border_color);
  scm_c_define_gsubr("nk_style_toggle_set_active", 2, 0, 0, nk_style_toggle_set_active);
  scm_c_define_gsubr("nk_style_toggle_active", 1, 0, 0, nk_style_toggle_active);
  scm_c_define_gsubr("nk_style_toggle_set_hover", 2, 0, 0, nk_style_toggle_set_hover);
  scm_c_define_gsubr("nk_style_toggle_hover", 1, 0, 0, nk_style_toggle_hover);
  scm_c_define_gsubr("nk_style_toggle_set_normal", 2, 0, 0, nk_style_toggle_set_normal);
  scm_c_define_gsubr("nk_style_toggle_normal", 1, 0, 0, nk_style_toggle_normal);
  scm_c_define_gsubr("nk_style_button_set_draw_end", 2, 0, 0, nk_style_button_set_draw_end);
  scm_c_define_gsubr("nk_style_button_draw_end", 1, 0, 0, nk_style_button_draw_end);
  scm_c_define_gsubr("nk_style_button_set_draw_begin", 2, 0, 0, nk_style_button_set_draw_begin);
  scm_c_define_gsubr("nk_style_button_draw_begin", 1, 0, 0, nk_style_button_draw_begin);
  scm_c_define_gsubr("nk_style_button_set_userdata", 2, 0, 0, nk_style_button_set_userdata);
  scm_c_define_gsubr("nk_style_button_userdata", 1, 0, 0, nk_style_button_userdata);
  scm_c_define_gsubr("nk_style_button_set_touch_padding", 2, 0, 0, nk_style_button_set_touch_padding);
  scm_c_define_gsubr("nk_style_button_touch_padding", 1, 0, 0, nk_style_button_touch_padding);
  scm_c_define_gsubr("nk_style_button_set_image_padding", 2, 0, 0, nk_style_button_set_image_padding);
  scm_c_define_gsubr("nk_style_button_image_padding", 1, 0, 0, nk_style_button_image_padding);
  scm_c_define_gsubr("nk_style_button_set_padding", 2, 0, 0, nk_style_button_set_padding);
  scm_c_define_gsubr("nk_style_button_padding", 1, 0, 0, nk_style_button_padding);
  scm_c_define_gsubr("nk_style_button_set_rounding", 2, 0, 0, nk_style_button_set_rounding);
  scm_c_define_gsubr("nk_style_button_rounding", 1, 0, 0, nk_style_button_rounding);
  scm_c_define_gsubr("nk_style_button_set_border", 2, 0, 0, nk_style_button_set_border);
  scm_c_define_gsubr("nk_style_button_border", 1, 0, 0, nk_style_button_border);
  scm_c_define_gsubr("nk_style_button_set_text_alignment", 2, 0, 0, nk_style_button_set_text_alignment);
  scm_c_define_gsubr("nk_style_button_text_alignment", 1, 0, 0, nk_style_button_text_alignment);
  scm_c_define_gsubr("nk_style_button_set_text_active", 2, 0, 0, nk_style_button_set_text_active);
  scm_c_define_gsubr("nk_style_button_text_active", 1, 0, 0, nk_style_button_text_active);
  scm_c_define_gsubr("nk_style_button_set_text_hover", 2, 0, 0, nk_style_button_set_text_hover);
  scm_c_define_gsubr("nk_style_button_text_hover", 1, 0, 0, nk_style_button_text_hover);
  scm_c_define_gsubr("nk_style_button_set_text_normal", 2, 0, 0, nk_style_button_set_text_normal);
  scm_c_define_gsubr("nk_style_button_text_normal", 1, 0, 0, nk_style_button_text_normal);
  scm_c_define_gsubr("nk_style_button_set_text_background", 2, 0, 0, nk_style_button_set_text_background);
  scm_c_define_gsubr("nk_style_button_text_background", 1, 0, 0, nk_style_button_text_background);
  scm_c_define_gsubr("nk_style_button_set_border_color", 2, 0, 0, nk_style_button_set_border_color);
  scm_c_define_gsubr("nk_style_button_border_color", 1, 0, 0, nk_style_button_border_color);
  scm_c_define_gsubr("nk_style_button_set_active", 2, 0, 0, nk_style_button_set_active);
  scm_c_define_gsubr("nk_style_button_active", 1, 0, 0, nk_style_button_active);
  scm_c_define_gsubr("nk_style_button_set_hover", 2, 0, 0, nk_style_button_set_hover);
  scm_c_define_gsubr("nk_style_button_hover", 1, 0, 0, nk_style_button_hover);
  scm_c_define_gsubr("nk_style_button_set_normal", 2, 0, 0, nk_style_button_set_normal);
  scm_c_define_gsubr("nk_style_button_normal", 1, 0, 0, nk_style_button_normal);
  scm_c_define_gsubr("nk_style_text_set_padding", 2, 0, 0, nk_style_text_set_padding);
  scm_c_define_gsubr("nk_style_text_padding", 1, 0, 0, nk_style_text_padding);
  scm_c_define_gsubr("nk_style_text_set_color", 2, 0, 0, nk_style_text_set_color);
  scm_c_define_gsubr("nk_style_text_color", 1, 0, 0, nk_style_text_color);
  scm_c_define_gsubr("nk_style_item_set_data", 2, 0, 0, nk_style_item_set_data);
  scm_c_define_gsubr("nk_style_item_data", 1, 0, 0, nk_style_item_data);
  scm_c_define_gsubr("nk_style_item_set_type", 2, 0, 0, nk_style_item_set_type);
  scm_c_define_gsubr("nk_style_item_type", 1, 0, 0, nk_style_item_type);
  scm_c_define_gsubr("nk_draw_list_set_shape_AA", 2, 0, 0, nk_draw_list_set_shape_AA);
  scm_c_define_gsubr("nk_draw_list_shape_AA", 1, 0, 0, nk_draw_list_shape_AA);
  scm_c_define_gsubr("nk_draw_list_set_line_AA", 2, 0, 0, nk_draw_list_set_line_AA);
  scm_c_define_gsubr("nk_draw_list_line_AA", 1, 0, 0, nk_draw_list_line_AA);
  scm_c_define_gsubr("nk_draw_list_set_path_offset", 2, 0, 0, nk_draw_list_set_path_offset);
  scm_c_define_gsubr("nk_draw_list_path_offset", 1, 0, 0, nk_draw_list_path_offset);
  scm_c_define_gsubr("nk_draw_list_set_path_count", 2, 0, 0, nk_draw_list_set_path_count);
  scm_c_define_gsubr("nk_draw_list_path_count", 1, 0, 0, nk_draw_list_path_count);
  scm_c_define_gsubr("nk_draw_list_set_cmd_offset", 2, 0, 0, nk_draw_list_set_cmd_offset);
  scm_c_define_gsubr("nk_draw_list_cmd_offset", 1, 0, 0, nk_draw_list_cmd_offset);
  scm_c_define_gsubr("nk_draw_list_set_cmd_count", 2, 0, 0, nk_draw_list_set_cmd_count);
  scm_c_define_gsubr("nk_draw_list_cmd_count", 1, 0, 0, nk_draw_list_cmd_count);
  scm_c_define_gsubr("nk_draw_list_set_vertex_count", 2, 0, 0, nk_draw_list_set_vertex_count);
  scm_c_define_gsubr("nk_draw_list_vertex_count", 1, 0, 0, nk_draw_list_vertex_count);
  scm_c_define_gsubr("nk_draw_list_set_element_count", 2, 0, 0, nk_draw_list_set_element_count);
  scm_c_define_gsubr("nk_draw_list_element_count", 1, 0, 0, nk_draw_list_element_count);
  scm_c_define_gsubr("nk_draw_list_set_elements", 2, 0, 0, nk_draw_list_set_elements);
  scm_c_define_gsubr("nk_draw_list_elements", 1, 0, 0, nk_draw_list_elements);
  scm_c_define_gsubr("nk_draw_list_set_vertices", 2, 0, 0, nk_draw_list_set_vertices);
  scm_c_define_gsubr("nk_draw_list_vertices", 1, 0, 0, nk_draw_list_vertices);
  scm_c_define_gsubr("nk_draw_list_set_buffer", 2, 0, 0, nk_draw_list_set_buffer);
  scm_c_define_gsubr("nk_draw_list_buffer", 1, 0, 0, nk_draw_list_buffer);
  scm_c_define_gsubr("nk_draw_list_set_config", 2, 0, 0, nk_draw_list_set_config);
  scm_c_define_gsubr("nk_draw_list_config", 1, 0, 0, nk_draw_list_config);
  scm_c_define_gsubr("nk_draw_list_set_circle_vtx", 2, 0, 0, nk_draw_list_set_circle_vtx);
  scm_c_define_gsubr("nk_draw_list_circle_vtx", 1, 0, 0, nk_draw_list_circle_vtx);
  scm_c_define_gsubr("nk_draw_list_set_clip_rect", 2, 0, 0, nk_draw_list_set_clip_rect);
  scm_c_define_gsubr("nk_draw_list_clip_rect", 1, 0, 0, nk_draw_list_clip_rect);
  scm_c_define_gsubr("nk_draw_command_set_texture", 2, 0, 0, nk_draw_command_set_texture);
  scm_c_define_gsubr("nk_draw_command_texture", 1, 0, 0, nk_draw_command_texture);
  scm_c_define_gsubr("nk_draw_command_set_clip_rect", 2, 0, 0, nk_draw_command_set_clip_rect);
  scm_c_define_gsubr("nk_draw_command_clip_rect", 1, 0, 0, nk_draw_command_clip_rect);
  scm_c_define_gsubr("nk_draw_command_set_elem_count", 2, 0, 0, nk_draw_command_set_elem_count);
  scm_c_define_gsubr("nk_draw_command_elem_count", 1, 0, 0, nk_draw_command_elem_count);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_set_offset", 2, 0, 0, nk_draw_vertex_layout_element_set_offset);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_offset", 1, 0, 0, nk_draw_vertex_layout_element_offset);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_set_format", 2, 0, 0, nk_draw_vertex_layout_element_set_format);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_format", 1, 0, 0, nk_draw_vertex_layout_element_format);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_set_attribute", 2, 0, 0, nk_draw_vertex_layout_element_set_attribute);
  scm_c_define_gsubr("nk_draw_vertex_layout_element_attribute", 1, 0, 0, nk_draw_vertex_layout_element_attribute);
  scm_c_define_gsubr("nk_input_set_mouse", 2, 0, 0, nk_input_set_mouse);
  scm_c_define_gsubr("nk_input_mouse", 1, 0, 0, nk_input_mouse);
  scm_c_define_gsubr("nk_input_set_keyboard", 2, 0, 0, nk_input_set_keyboard);
  scm_c_define_gsubr("nk_input_keyboard", 1, 0, 0, nk_input_keyboard);
  scm_c_define_gsubr("nk_keyboard_set_text_len", 2, 0, 0, nk_keyboard_set_text_len);
  scm_c_define_gsubr("nk_keyboard_text_len", 1, 0, 0, nk_keyboard_text_len);
  scm_c_define_gsubr("nk_keyboard_set_text", 2, 0, 0, nk_keyboard_set_text);
  scm_c_define_gsubr("nk_keyboard_text", 1, 0, 0, nk_keyboard_text);
  scm_c_define_gsubr("nk_keyboard_set_keys", 2, 0, 0, nk_keyboard_set_keys);
  scm_c_define_gsubr("nk_keyboard_keys", 1, 0, 0, nk_keyboard_keys);
  scm_c_define_gsubr("nk_key_set_clicked", 2, 0, 0, nk_key_set_clicked);
  scm_c_define_gsubr("nk_key_clicked", 1, 0, 0, nk_key_clicked);
  scm_c_define_gsubr("nk_key_set_down", 2, 0, 0, nk_key_set_down);
  scm_c_define_gsubr("nk_key_down", 1, 0, 0, nk_key_down);
  scm_c_define_gsubr("nk_mouse_set_ungrab", 2, 0, 0, nk_mouse_set_ungrab);
  scm_c_define_gsubr("nk_mouse_ungrab", 1, 0, 0, nk_mouse_ungrab);
  scm_c_define_gsubr("nk_mouse_set_grabbed", 2, 0, 0, nk_mouse_set_grabbed);
  scm_c_define_gsubr("nk_mouse_grabbed", 1, 0, 0, nk_mouse_grabbed);
  scm_c_define_gsubr("nk_mouse_set_grab", 2, 0, 0, nk_mouse_set_grab);
  scm_c_define_gsubr("nk_mouse_grab", 1, 0, 0, nk_mouse_grab);
  scm_c_define_gsubr("nk_mouse_set_scroll_delta", 2, 0, 0, nk_mouse_set_scroll_delta);
  scm_c_define_gsubr("nk_mouse_scroll_delta", 1, 0, 0, nk_mouse_scroll_delta);
  scm_c_define_gsubr("nk_mouse_set_delta", 2, 0, 0, nk_mouse_set_delta);
  scm_c_define_gsubr("nk_mouse_delta", 1, 0, 0, nk_mouse_delta);
  scm_c_define_gsubr("nk_mouse_set_prev", 2, 0, 0, nk_mouse_set_prev);
  scm_c_define_gsubr("nk_mouse_prev", 1, 0, 0, nk_mouse_prev);
  scm_c_define_gsubr("nk_mouse_set_pos", 2, 0, 0, nk_mouse_set_pos);
  scm_c_define_gsubr("nk_mouse_pos", 1, 0, 0, nk_mouse_pos);
  scm_c_define_gsubr("nk_mouse_set_buttons", 2, 0, 0, nk_mouse_set_buttons);
  scm_c_define_gsubr("nk_mouse_buttons", 1, 0, 0, nk_mouse_buttons);
  scm_c_define_gsubr("nk_mouse_button_set_clicked_pos", 2, 0, 0, nk_mouse_button_set_clicked_pos);
  scm_c_define_gsubr("nk_mouse_button_clicked_pos", 1, 0, 0, nk_mouse_button_clicked_pos);
  scm_c_define_gsubr("nk_mouse_button_set_clicked", 2, 0, 0, nk_mouse_button_set_clicked);
  scm_c_define_gsubr("nk_mouse_button_clicked", 1, 0, 0, nk_mouse_button_clicked);
  scm_c_define_gsubr("nk_mouse_button_set_down", 2, 0, 0, nk_mouse_button_set_down);
  scm_c_define_gsubr("nk_mouse_button_down", 1, 0, 0, nk_mouse_button_down);
  scm_c_define_gsubr("nk_command_buffer_set_last", 2, 0, 0, nk_command_buffer_set_last);
  scm_c_define_gsubr("nk_command_buffer_last", 1, 0, 0, nk_command_buffer_last);
  scm_c_define_gsubr("nk_command_buffer_set_end", 2, 0, 0, nk_command_buffer_set_end);
  scm_c_define_gsubr("nk_command_buffer_end", 1, 0, 0, nk_command_buffer_end);
  scm_c_define_gsubr("nk_command_buffer_set_begin", 2, 0, 0, nk_command_buffer_set_begin);
  scm_c_define_gsubr("nk_command_buffer_begin", 1, 0, 0, nk_command_buffer_begin);
  scm_c_define_gsubr("nk_command_buffer_set_userdata", 2, 0, 0, nk_command_buffer_set_userdata);
  scm_c_define_gsubr("nk_command_buffer_userdata", 1, 0, 0, nk_command_buffer_userdata);
  scm_c_define_gsubr("nk_command_buffer_set_use_clipping", 2, 0, 0, nk_command_buffer_set_use_clipping);
  scm_c_define_gsubr("nk_command_buffer_use_clipping", 1, 0, 0, nk_command_buffer_use_clipping);
  scm_c_define_gsubr("nk_command_buffer_set_clip", 2, 0, 0, nk_command_buffer_set_clip);
  scm_c_define_gsubr("nk_command_buffer_clip", 1, 0, 0, nk_command_buffer_clip);
  scm_c_define_gsubr("nk_command_buffer_set_base", 2, 0, 0, nk_command_buffer_set_base);
  scm_c_define_gsubr("nk_command_buffer_base", 1, 0, 0, nk_command_buffer_base);
  scm_c_define_gsubr("nk_command_text_set_string", 2, 0, 0, nk_command_text_set_string);
  scm_c_define_gsubr("nk_command_text_string", 1, 0, 0, nk_command_text_string);
  scm_c_define_gsubr("nk_command_text_set_length", 2, 0, 0, nk_command_text_set_length);
  scm_c_define_gsubr("nk_command_text_length", 1, 0, 0, nk_command_text_length);
  scm_c_define_gsubr("nk_command_text_set_height", 2, 0, 0, nk_command_text_set_height);
  scm_c_define_gsubr("nk_command_text_height", 1, 0, 0, nk_command_text_height);
  scm_c_define_gsubr("nk_command_text_set_h", 2, 0, 0, nk_command_text_set_h);
  scm_c_define_gsubr("nk_command_text_h", 1, 0, 0, nk_command_text_h);
  scm_c_define_gsubr("nk_command_text_set_w", 2, 0, 0, nk_command_text_set_w);
  scm_c_define_gsubr("nk_command_text_w", 1, 0, 0, nk_command_text_w);
  scm_c_define_gsubr("nk_command_text_set_y", 2, 0, 0, nk_command_text_set_y);
  scm_c_define_gsubr("nk_command_text_y", 1, 0, 0, nk_command_text_y);
  scm_c_define_gsubr("nk_command_text_set_x", 2, 0, 0, nk_command_text_set_x);
  scm_c_define_gsubr("nk_command_text_x", 1, 0, 0, nk_command_text_x);
  scm_c_define_gsubr("nk_command_text_set_foreground", 2, 0, 0, nk_command_text_set_foreground);
  scm_c_define_gsubr("nk_command_text_foreground", 1, 0, 0, nk_command_text_foreground);
  scm_c_define_gsubr("nk_command_text_set_background", 2, 0, 0, nk_command_text_set_background);
  scm_c_define_gsubr("nk_command_text_background", 1, 0, 0, nk_command_text_background);
  scm_c_define_gsubr("nk_command_text_set_font", 2, 0, 0, nk_command_text_set_font);
  scm_c_define_gsubr("nk_command_text_font", 1, 0, 0, nk_command_text_font);
  scm_c_define_gsubr("nk_command_text_set_header", 2, 0, 0, nk_command_text_set_header);
  scm_c_define_gsubr("nk_command_text_header", 1, 0, 0, nk_command_text_header);
  scm_c_define_gsubr("nk_command_custom_set_callback", 2, 0, 0, nk_command_custom_set_callback);
  scm_c_define_gsubr("nk_command_custom_get_callback", 1, 0, 0, nk_command_custom_get_callback);
  scm_c_define_gsubr("nk_command_custom_set_callback_data", 2, 0, 0, nk_command_custom_set_callback_data);
  scm_c_define_gsubr("nk_command_custom_callback_data", 1, 0, 0, nk_command_custom_callback_data);
  scm_c_define_gsubr("nk_command_custom_set_h", 2, 0, 0, nk_command_custom_set_h);
  scm_c_define_gsubr("nk_command_custom_h", 1, 0, 0, nk_command_custom_h);
  scm_c_define_gsubr("nk_command_custom_set_w", 2, 0, 0, nk_command_custom_set_w);
  scm_c_define_gsubr("nk_command_custom_w", 1, 0, 0, nk_command_custom_w);
  scm_c_define_gsubr("nk_command_custom_set_y", 2, 0, 0, nk_command_custom_set_y);
  scm_c_define_gsubr("nk_command_custom_y", 1, 0, 0, nk_command_custom_y);
  scm_c_define_gsubr("nk_command_custom_set_x", 2, 0, 0, nk_command_custom_set_x);
  scm_c_define_gsubr("nk_command_custom_x", 1, 0, 0, nk_command_custom_x);
  scm_c_define_gsubr("nk_command_custom_set_header", 2, 0, 0, nk_command_custom_set_header);
  scm_c_define_gsubr("nk_command_custom_header", 1, 0, 0, nk_command_custom_header);
  scm_c_define_gsubr("nk_command_image_set_col", 2, 0, 0, nk_command_image_set_col);
  scm_c_define_gsubr("nk_command_image_col", 1, 0, 0, nk_command_image_col);
  scm_c_define_gsubr("nk_command_image_set_img", 2, 0, 0, nk_command_image_set_img);
  scm_c_define_gsubr("nk_command_image_img", 1, 0, 0, nk_command_image_img);
  scm_c_define_gsubr("nk_command_image_set_h", 2, 0, 0, nk_command_image_set_h);
  scm_c_define_gsubr("nk_command_image_h", 1, 0, 0, nk_command_image_h);
  scm_c_define_gsubr("nk_command_image_set_w", 2, 0, 0, nk_command_image_set_w);
  scm_c_define_gsubr("nk_command_image_w", 1, 0, 0, nk_command_image_w);
  scm_c_define_gsubr("nk_command_image_set_y", 2, 0, 0, nk_command_image_set_y);
  scm_c_define_gsubr("nk_command_image_y", 1, 0, 0, nk_command_image_y);
  scm_c_define_gsubr("nk_command_image_set_x", 2, 0, 0, nk_command_image_set_x);
  scm_c_define_gsubr("nk_command_image_x", 1, 0, 0, nk_command_image_x);
  scm_c_define_gsubr("nk_command_image_set_header", 2, 0, 0, nk_command_image_set_header);
  scm_c_define_gsubr("nk_command_image_header", 1, 0, 0, nk_command_image_header);
  scm_c_define_gsubr("nk_command_polyline_set_points", 2, 0, 0, nk_command_polyline_set_points);
  scm_c_define_gsubr("nk_command_polyline_points", 1, 0, 0, nk_command_polyline_points);
  scm_c_define_gsubr("nk_command_polyline_set_point_count", 2, 0, 0, nk_command_polyline_set_point_count);
  scm_c_define_gsubr("nk_command_polyline_point_count", 1, 0, 0, nk_command_polyline_point_count);
  scm_c_define_gsubr("nk_command_polyline_set_line_thickness", 2, 0, 0, nk_command_polyline_set_line_thickness);
  scm_c_define_gsubr("nk_command_polyline_line_thickness", 1, 0, 0, nk_command_polyline_line_thickness);
  scm_c_define_gsubr("nk_command_polyline_set_color", 2, 0, 0, nk_command_polyline_set_color);
  scm_c_define_gsubr("nk_command_polyline_color", 1, 0, 0, nk_command_polyline_color);
  scm_c_define_gsubr("nk_command_polyline_set_header", 2, 0, 0, nk_command_polyline_set_header);
  scm_c_define_gsubr("nk_command_polyline_header", 1, 0, 0, nk_command_polyline_header);
  scm_c_define_gsubr("nk_command_polygon_filled_set_points", 2, 0, 0, nk_command_polygon_filled_set_points);
  scm_c_define_gsubr("nk_command_polygon_filled_points", 1, 0, 0, nk_command_polygon_filled_points);
  scm_c_define_gsubr("nk_command_polygon_filled_set_point_count", 2, 0, 0, nk_command_polygon_filled_set_point_count);
  scm_c_define_gsubr("nk_command_polygon_filled_point_count", 1, 0, 0, nk_command_polygon_filled_point_count);
  scm_c_define_gsubr("nk_command_polygon_filled_set_color", 2, 0, 0, nk_command_polygon_filled_set_color);
  scm_c_define_gsubr("nk_command_polygon_filled_color", 1, 0, 0, nk_command_polygon_filled_color);
  scm_c_define_gsubr("nk_command_polygon_filled_set_header", 2, 0, 0, nk_command_polygon_filled_set_header);
  scm_c_define_gsubr("nk_command_polygon_filled_header", 1, 0, 0, nk_command_polygon_filled_header);
  scm_c_define_gsubr("nk_command_polygon_set_points", 2, 0, 0, nk_command_polygon_set_points);
  scm_c_define_gsubr("nk_command_polygon_points", 1, 0, 0, nk_command_polygon_points);
  scm_c_define_gsubr("nk_command_polygon_set_point_count", 2, 0, 0, nk_command_polygon_set_point_count);
  scm_c_define_gsubr("nk_command_polygon_point_count", 1, 0, 0, nk_command_polygon_point_count);
  scm_c_define_gsubr("nk_command_polygon_set_line_thickness", 2, 0, 0, nk_command_polygon_set_line_thickness);
  scm_c_define_gsubr("nk_command_polygon_line_thickness", 1, 0, 0, nk_command_polygon_line_thickness);
  scm_c_define_gsubr("nk_command_polygon_set_color", 2, 0, 0, nk_command_polygon_set_color);
  scm_c_define_gsubr("nk_command_polygon_color", 1, 0, 0, nk_command_polygon_color);
  scm_c_define_gsubr("nk_command_polygon_set_header", 2, 0, 0, nk_command_polygon_set_header);
  scm_c_define_gsubr("nk_command_polygon_header", 1, 0, 0, nk_command_polygon_header);
  scm_c_define_gsubr("nk_command_arc_filled_set_color", 2, 0, 0, nk_command_arc_filled_set_color);
  scm_c_define_gsubr("nk_command_arc_filled_color", 1, 0, 0, nk_command_arc_filled_color);
  scm_c_define_gsubr("nk_command_arc_filled_set_a", 2, 0, 0, nk_command_arc_filled_set_a);
  scm_c_define_gsubr("nk_command_arc_filled_a", 1, 0, 0, nk_command_arc_filled_a);
  scm_c_define_gsubr("nk_command_arc_filled_set_r", 2, 0, 0, nk_command_arc_filled_set_r);
  scm_c_define_gsubr("nk_command_arc_filled_r", 1, 0, 0, nk_command_arc_filled_r);
  scm_c_define_gsubr("nk_command_arc_filled_set_cy", 2, 0, 0, nk_command_arc_filled_set_cy);
  scm_c_define_gsubr("nk_command_arc_filled_cy", 1, 0, 0, nk_command_arc_filled_cy);
  scm_c_define_gsubr("nk_command_arc_filled_set_cx", 2, 0, 0, nk_command_arc_filled_set_cx);
  scm_c_define_gsubr("nk_command_arc_filled_cx", 1, 0, 0, nk_command_arc_filled_cx);
  scm_c_define_gsubr("nk_command_arc_filled_set_header", 2, 0, 0, nk_command_arc_filled_set_header);
  scm_c_define_gsubr("nk_command_arc_filled_header", 1, 0, 0, nk_command_arc_filled_header);
  scm_c_define_gsubr("nk_command_arc_set_color", 2, 0, 0, nk_command_arc_set_color);
  scm_c_define_gsubr("nk_command_arc_color", 1, 0, 0, nk_command_arc_color);
  scm_c_define_gsubr("nk_command_arc_set_a", 2, 0, 0, nk_command_arc_set_a);
  scm_c_define_gsubr("nk_command_arc_a", 1, 0, 0, nk_command_arc_a);
  scm_c_define_gsubr("nk_command_arc_set_line_thickness", 2, 0, 0, nk_command_arc_set_line_thickness);
  scm_c_define_gsubr("nk_command_arc_line_thickness", 1, 0, 0, nk_command_arc_line_thickness);
  scm_c_define_gsubr("nk_command_arc_set_r", 2, 0, 0, nk_command_arc_set_r);
  scm_c_define_gsubr("nk_command_arc_r", 1, 0, 0, nk_command_arc_r);
  scm_c_define_gsubr("nk_command_arc_set_cy", 2, 0, 0, nk_command_arc_set_cy);
  scm_c_define_gsubr("nk_command_arc_cy", 1, 0, 0, nk_command_arc_cy);
  scm_c_define_gsubr("nk_command_arc_set_cx", 2, 0, 0, nk_command_arc_set_cx);
  scm_c_define_gsubr("nk_command_arc_cx", 1, 0, 0, nk_command_arc_cx);
  scm_c_define_gsubr("nk_command_arc_set_header", 2, 0, 0, nk_command_arc_set_header);
  scm_c_define_gsubr("nk_command_arc_header", 1, 0, 0, nk_command_arc_header);
  scm_c_define_gsubr("nk_command_circle_filled_set_color", 2, 0, 0, nk_command_circle_filled_set_color);
  scm_c_define_gsubr("nk_command_circle_filled_color", 1, 0, 0, nk_command_circle_filled_color);
  scm_c_define_gsubr("nk_command_circle_filled_set_h", 2, 0, 0, nk_command_circle_filled_set_h);
  scm_c_define_gsubr("nk_command_circle_filled_h", 1, 0, 0, nk_command_circle_filled_h);
  scm_c_define_gsubr("nk_command_circle_filled_set_w", 2, 0, 0, nk_command_circle_filled_set_w);
  scm_c_define_gsubr("nk_command_circle_filled_w", 1, 0, 0, nk_command_circle_filled_w);
  scm_c_define_gsubr("nk_command_circle_filled_set_y", 2, 0, 0, nk_command_circle_filled_set_y);
  scm_c_define_gsubr("nk_command_circle_filled_y", 1, 0, 0, nk_command_circle_filled_y);
  scm_c_define_gsubr("nk_command_circle_filled_set_x", 2, 0, 0, nk_command_circle_filled_set_x);
  scm_c_define_gsubr("nk_command_circle_filled_x", 1, 0, 0, nk_command_circle_filled_x);
  scm_c_define_gsubr("nk_command_circle_filled_set_header", 2, 0, 0, nk_command_circle_filled_set_header);
  scm_c_define_gsubr("nk_command_circle_filled_header", 1, 0, 0, nk_command_circle_filled_header);
  scm_c_define_gsubr("nk_command_circle_set_color", 2, 0, 0, nk_command_circle_set_color);
  scm_c_define_gsubr("nk_command_circle_color", 1, 0, 0, nk_command_circle_color);
  scm_c_define_gsubr("nk_command_circle_set_h", 2, 0, 0, nk_command_circle_set_h);
  scm_c_define_gsubr("nk_command_circle_h", 1, 0, 0, nk_command_circle_h);
  scm_c_define_gsubr("nk_command_circle_set_w", 2, 0, 0, nk_command_circle_set_w);
  scm_c_define_gsubr("nk_command_circle_w", 1, 0, 0, nk_command_circle_w);
  scm_c_define_gsubr("nk_command_circle_set_line_thickness", 2, 0, 0, nk_command_circle_set_line_thickness);
  scm_c_define_gsubr("nk_command_circle_line_thickness", 1, 0, 0, nk_command_circle_line_thickness);
  scm_c_define_gsubr("nk_command_circle_set_y", 2, 0, 0, nk_command_circle_set_y);
  scm_c_define_gsubr("nk_command_circle_y", 1, 0, 0, nk_command_circle_y);
  scm_c_define_gsubr("nk_command_circle_set_x", 2, 0, 0, nk_command_circle_set_x);
  scm_c_define_gsubr("nk_command_circle_x", 1, 0, 0, nk_command_circle_x);
  scm_c_define_gsubr("nk_command_circle_set_header", 2, 0, 0, nk_command_circle_set_header);
  scm_c_define_gsubr("nk_command_circle_header", 1, 0, 0, nk_command_circle_header);
  scm_c_define_gsubr("nk_command_triangle_filled_set_color", 2, 0, 0, nk_command_triangle_filled_set_color);
  scm_c_define_gsubr("nk_command_triangle_filled_color", 1, 0, 0, nk_command_triangle_filled_color);
  scm_c_define_gsubr("nk_command_triangle_filled_set_c", 2, 0, 0, nk_command_triangle_filled_set_c);
  scm_c_define_gsubr("nk_command_triangle_filled_c", 1, 0, 0, nk_command_triangle_filled_c);
  scm_c_define_gsubr("nk_command_triangle_filled_set_b", 2, 0, 0, nk_command_triangle_filled_set_b);
  scm_c_define_gsubr("nk_command_triangle_filled_b", 1, 0, 0, nk_command_triangle_filled_b);
  scm_c_define_gsubr("nk_command_triangle_filled_set_a", 2, 0, 0, nk_command_triangle_filled_set_a);
  scm_c_define_gsubr("nk_command_triangle_filled_a", 1, 0, 0, nk_command_triangle_filled_a);
  scm_c_define_gsubr("nk_command_triangle_filled_set_header", 2, 0, 0, nk_command_triangle_filled_set_header);
  scm_c_define_gsubr("nk_command_triangle_filled_header", 1, 0, 0, nk_command_triangle_filled_header);
  scm_c_define_gsubr("nk_command_triangle_set_color", 2, 0, 0, nk_command_triangle_set_color);
  scm_c_define_gsubr("nk_command_triangle_color", 1, 0, 0, nk_command_triangle_color);
  scm_c_define_gsubr("nk_command_triangle_set_c", 2, 0, 0, nk_command_triangle_set_c);
  scm_c_define_gsubr("nk_command_triangle_c", 1, 0, 0, nk_command_triangle_c);
  scm_c_define_gsubr("nk_command_triangle_set_b", 2, 0, 0, nk_command_triangle_set_b);
  scm_c_define_gsubr("nk_command_triangle_b", 1, 0, 0, nk_command_triangle_b);
  scm_c_define_gsubr("nk_command_triangle_set_a", 2, 0, 0, nk_command_triangle_set_a);
  scm_c_define_gsubr("nk_command_triangle_a", 1, 0, 0, nk_command_triangle_a);
  scm_c_define_gsubr("nk_command_triangle_set_line_thickness", 2, 0, 0, nk_command_triangle_set_line_thickness);
  scm_c_define_gsubr("nk_command_triangle_line_thickness", 1, 0, 0, nk_command_triangle_line_thickness);
  scm_c_define_gsubr("nk_command_triangle_set_header", 2, 0, 0, nk_command_triangle_set_header);
  scm_c_define_gsubr("nk_command_triangle_header", 1, 0, 0, nk_command_triangle_header);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_right", 2, 0, 0, nk_command_rect_multi_color_set_right);
  scm_c_define_gsubr("nk_command_rect_multi_color_right", 1, 0, 0, nk_command_rect_multi_color_right);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_bottom", 2, 0, 0, nk_command_rect_multi_color_set_bottom);
  scm_c_define_gsubr("nk_command_rect_multi_color_bottom", 1, 0, 0, nk_command_rect_multi_color_bottom);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_top", 2, 0, 0, nk_command_rect_multi_color_set_top);
  scm_c_define_gsubr("nk_command_rect_multi_color_top", 1, 0, 0, nk_command_rect_multi_color_top);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_left", 2, 0, 0, nk_command_rect_multi_color_set_left);
  scm_c_define_gsubr("nk_command_rect_multi_color_left", 1, 0, 0, nk_command_rect_multi_color_left);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_h", 2, 0, 0, nk_command_rect_multi_color_set_h);
  scm_c_define_gsubr("nk_command_rect_multi_color_h", 1, 0, 0, nk_command_rect_multi_color_h);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_w", 2, 0, 0, nk_command_rect_multi_color_set_w);
  scm_c_define_gsubr("nk_command_rect_multi_color_w", 1, 0, 0, nk_command_rect_multi_color_w);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_y", 2, 0, 0, nk_command_rect_multi_color_set_y);
  scm_c_define_gsubr("nk_command_rect_multi_color_y", 1, 0, 0, nk_command_rect_multi_color_y);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_x", 2, 0, 0, nk_command_rect_multi_color_set_x);
  scm_c_define_gsubr("nk_command_rect_multi_color_x", 1, 0, 0, nk_command_rect_multi_color_x);
  scm_c_define_gsubr("nk_command_rect_multi_color_set_header", 2, 0, 0, nk_command_rect_multi_color_set_header);
  scm_c_define_gsubr("nk_command_rect_multi_color_header", 1, 0, 0, nk_command_rect_multi_color_header);
  scm_c_define_gsubr("nk_command_rect_filled_set_color", 2, 0, 0, nk_command_rect_filled_set_color);
  scm_c_define_gsubr("nk_command_rect_filled_color", 1, 0, 0, nk_command_rect_filled_color);
  scm_c_define_gsubr("nk_command_rect_filled_set_h", 2, 0, 0, nk_command_rect_filled_set_h);
  scm_c_define_gsubr("nk_command_rect_filled_h", 1, 0, 0, nk_command_rect_filled_h);
  scm_c_define_gsubr("nk_command_rect_filled_set_w", 2, 0, 0, nk_command_rect_filled_set_w);
  scm_c_define_gsubr("nk_command_rect_filled_w", 1, 0, 0, nk_command_rect_filled_w);
  scm_c_define_gsubr("nk_command_rect_filled_set_y", 2, 0, 0, nk_command_rect_filled_set_y);
  scm_c_define_gsubr("nk_command_rect_filled_y", 1, 0, 0, nk_command_rect_filled_y);
  scm_c_define_gsubr("nk_command_rect_filled_set_x", 2, 0, 0, nk_command_rect_filled_set_x);
  scm_c_define_gsubr("nk_command_rect_filled_x", 1, 0, 0, nk_command_rect_filled_x);
  scm_c_define_gsubr("nk_command_rect_filled_set_rounding", 2, 0, 0, nk_command_rect_filled_set_rounding);
  scm_c_define_gsubr("nk_command_rect_filled_rounding", 1, 0, 0, nk_command_rect_filled_rounding);
  scm_c_define_gsubr("nk_command_rect_filled_set_header", 2, 0, 0, nk_command_rect_filled_set_header);
  scm_c_define_gsubr("nk_command_rect_filled_header", 1, 0, 0, nk_command_rect_filled_header);
  scm_c_define_gsubr("nk_command_rect_set_color", 2, 0, 0, nk_command_rect_set_color);
  scm_c_define_gsubr("nk_command_rect_color", 1, 0, 0, nk_command_rect_color);
  scm_c_define_gsubr("nk_command_rect_set_h", 2, 0, 0, nk_command_rect_set_h);
  scm_c_define_gsubr("nk_command_rect_h", 1, 0, 0, nk_command_rect_h);
  scm_c_define_gsubr("nk_command_rect_set_w", 2, 0, 0, nk_command_rect_set_w);
  scm_c_define_gsubr("nk_command_rect_w", 1, 0, 0, nk_command_rect_w);
  scm_c_define_gsubr("nk_command_rect_set_y", 2, 0, 0, nk_command_rect_set_y);
  scm_c_define_gsubr("nk_command_rect_y", 1, 0, 0, nk_command_rect_y);
  scm_c_define_gsubr("nk_command_rect_set_x", 2, 0, 0, nk_command_rect_set_x);
  scm_c_define_gsubr("nk_command_rect_x", 1, 0, 0, nk_command_rect_x);
  scm_c_define_gsubr("nk_command_rect_set_line_thickness", 2, 0, 0, nk_command_rect_set_line_thickness);
  scm_c_define_gsubr("nk_command_rect_line_thickness", 1, 0, 0, nk_command_rect_line_thickness);
  scm_c_define_gsubr("nk_command_rect_set_rounding", 2, 0, 0, nk_command_rect_set_rounding);
  scm_c_define_gsubr("nk_command_rect_rounding", 1, 0, 0, nk_command_rect_rounding);
  scm_c_define_gsubr("nk_command_rect_set_header", 2, 0, 0, nk_command_rect_set_header);
  scm_c_define_gsubr("nk_command_rect_header", 1, 0, 0, nk_command_rect_header);
  scm_c_define_gsubr("nk_command_curve_set_color", 2, 0, 0, nk_command_curve_set_color);
  scm_c_define_gsubr("nk_command_curve_color", 1, 0, 0, nk_command_curve_color);
  scm_c_define_gsubr("nk_command_curve_set_ctrl", 2, 0, 0, nk_command_curve_set_ctrl);
  scm_c_define_gsubr("nk_command_curve_ctrl", 1, 0, 0, nk_command_curve_ctrl);
  scm_c_define_gsubr("nk_command_curve_set_end", 2, 0, 0, nk_command_curve_set_end);
  scm_c_define_gsubr("nk_command_curve_end", 1, 0, 0, nk_command_curve_end);
  scm_c_define_gsubr("nk_command_curve_set_begin", 2, 0, 0, nk_command_curve_set_begin);
  scm_c_define_gsubr("nk_command_curve_begin", 1, 0, 0, nk_command_curve_begin);
  scm_c_define_gsubr("nk_command_curve_set_line_thickness", 2, 0, 0, nk_command_curve_set_line_thickness);
  scm_c_define_gsubr("nk_command_curve_line_thickness", 1, 0, 0, nk_command_curve_line_thickness);
  scm_c_define_gsubr("nk_command_curve_set_header", 2, 0, 0, nk_command_curve_set_header);
  scm_c_define_gsubr("nk_command_curve_header", 1, 0, 0, nk_command_curve_header);
  scm_c_define_gsubr("nk_command_line_set_color", 2, 0, 0, nk_command_line_set_color);
  scm_c_define_gsubr("nk_command_line_color", 1, 0, 0, nk_command_line_color);
  scm_c_define_gsubr("nk_command_line_set_end", 2, 0, 0, nk_command_line_set_end);
  scm_c_define_gsubr("nk_command_line_end", 1, 0, 0, nk_command_line_end);
  scm_c_define_gsubr("nk_command_line_set_begin", 2, 0, 0, nk_command_line_set_begin);
  scm_c_define_gsubr("nk_command_line_begin", 1, 0, 0, nk_command_line_begin);
  scm_c_define_gsubr("nk_command_line_set_line_thickness", 2, 0, 0, nk_command_line_set_line_thickness);
  scm_c_define_gsubr("nk_command_line_line_thickness", 1, 0, 0, nk_command_line_line_thickness);
  scm_c_define_gsubr("nk_command_line_set_header", 2, 0, 0, nk_command_line_set_header);
  scm_c_define_gsubr("nk_command_line_header", 1, 0, 0, nk_command_line_header);
  scm_c_define_gsubr("nk_command_scissor_set_h", 2, 0, 0, nk_command_scissor_set_h);
  scm_c_define_gsubr("nk_command_scissor_h", 1, 0, 0, nk_command_scissor_h);
  scm_c_define_gsubr("nk_command_scissor_set_w", 2, 0, 0, nk_command_scissor_set_w);
  scm_c_define_gsubr("nk_command_scissor_w", 1, 0, 0, nk_command_scissor_w);
  scm_c_define_gsubr("nk_command_scissor_set_y", 2, 0, 0, nk_command_scissor_set_y);
  scm_c_define_gsubr("nk_command_scissor_y", 1, 0, 0, nk_command_scissor_y);
  scm_c_define_gsubr("nk_command_scissor_set_x", 2, 0, 0, nk_command_scissor_set_x);
  scm_c_define_gsubr("nk_command_scissor_x", 1, 0, 0, nk_command_scissor_x);
  scm_c_define_gsubr("nk_command_scissor_set_header", 2, 0, 0, nk_command_scissor_set_header);
  scm_c_define_gsubr("nk_command_scissor_header", 1, 0, 0, nk_command_scissor_header);
  scm_c_define_gsubr("nk_command_set_next", 2, 0, 0, nk_command_set_next);
  scm_c_define_gsubr("nk_command_next", 1, 0, 0, nk_command_next);
  scm_c_define_gsubr("nk_command_set_type", 2, 0, 0, nk_command_set_type);
  scm_c_define_gsubr("nk_command_type", 1, 0, 0, nk_command_type);
  scm_c_define_gsubr("nk_text_edit_set_undo", 2, 0, 0, nk_text_edit_set_undo);
  scm_c_define_gsubr("nk_text_edit_undo", 1, 0, 0, nk_text_edit_undo);
  scm_c_define_gsubr("nk_text_edit_set_preferred_x", 2, 0, 0, nk_text_edit_set_preferred_x);
  scm_c_define_gsubr("nk_text_edit_preferred_x", 1, 0, 0, nk_text_edit_preferred_x);
  scm_c_define_gsubr("nk_text_edit_set_padding1", 2, 0, 0, nk_text_edit_set_padding1);
  scm_c_define_gsubr("nk_text_edit_padding1", 1, 0, 0, nk_text_edit_padding1);
  scm_c_define_gsubr("nk_text_edit_set_active", 2, 0, 0, nk_text_edit_set_active);
  scm_c_define_gsubr("nk_text_edit_active", 1, 0, 0, nk_text_edit_active);
  scm_c_define_gsubr("nk_text_edit_set_single_line", 2, 0, 0, nk_text_edit_set_single_line);
  scm_c_define_gsubr("nk_text_edit_single_line", 1, 0, 0, nk_text_edit_single_line);
  scm_c_define_gsubr("nk_text_edit_set_has_preferred_x", 2, 0, 0, nk_text_edit_set_has_preferred_x);
  scm_c_define_gsubr("nk_text_edit_has_preferred_x", 1, 0, 0, nk_text_edit_has_preferred_x);
  scm_c_define_gsubr("nk_text_edit_set_initialized", 2, 0, 0, nk_text_edit_set_initialized);
  scm_c_define_gsubr("nk_text_edit_initialized", 1, 0, 0, nk_text_edit_initialized);
  scm_c_define_gsubr("nk_text_edit_set_cursor_at_end_of_line", 2, 0, 0, nk_text_edit_set_cursor_at_end_of_line);
  scm_c_define_gsubr("nk_text_edit_cursor_at_end_of_line", 1, 0, 0, nk_text_edit_cursor_at_end_of_line);
  scm_c_define_gsubr("nk_text_edit_set_mode", 2, 0, 0, nk_text_edit_set_mode);
  scm_c_define_gsubr("nk_text_edit_mode", 1, 0, 0, nk_text_edit_mode);
  scm_c_define_gsubr("nk_text_edit_set_select_end", 2, 0, 0, nk_text_edit_set_select_end);
  scm_c_define_gsubr("nk_text_edit_select_end", 1, 0, 0, nk_text_edit_select_end);
  scm_c_define_gsubr("nk_text_edit_set_select_start", 2, 0, 0, nk_text_edit_set_select_start);
  scm_c_define_gsubr("nk_text_edit_select_start", 1, 0, 0, nk_text_edit_select_start);
  scm_c_define_gsubr("nk_text_edit_set_cursor", 2, 0, 0, nk_text_edit_set_cursor);
  scm_c_define_gsubr("nk_text_edit_cursor", 1, 0, 0, nk_text_edit_cursor);
  scm_c_define_gsubr("nk_text_edit_set_scrollbar", 2, 0, 0, nk_text_edit_set_scrollbar);
  scm_c_define_gsubr("nk_text_edit_scrollbar", 1, 0, 0, nk_text_edit_scrollbar);
  scm_c_define_gsubr("nk_text_edit_set_filter", 2, 0, 0, nk_text_edit_set_filter);
  scm_c_define_gsubr("nk_text_edit_filter", 1, 0, 0, nk_text_edit_filter);
  scm_c_define_gsubr("nk_text_edit_set_string", 2, 0, 0, nk_text_edit_set_string);
  scm_c_define_gsubr("nk_text_edit_string", 1, 0, 0, nk_text_edit_string);
  scm_c_define_gsubr("nk_text_edit_set_clip", 2, 0, 0, nk_text_edit_set_clip);
  scm_c_define_gsubr("nk_text_edit_clip", 1, 0, 0, nk_text_edit_clip);
  scm_c_define_gsubr("nk_text_undo_state_set_redo_char_point", 2, 0, 0, nk_text_undo_state_set_redo_char_point);
  scm_c_define_gsubr("nk_text_undo_state_redo_char_point", 1, 0, 0, nk_text_undo_state_redo_char_point);
  scm_c_define_gsubr("nk_text_undo_state_set_undo_char_point", 2, 0, 0, nk_text_undo_state_set_undo_char_point);
  scm_c_define_gsubr("nk_text_undo_state_undo_char_point", 1, 0, 0, nk_text_undo_state_undo_char_point);
  scm_c_define_gsubr("nk_text_undo_state_set_redo_point", 2, 0, 0, nk_text_undo_state_set_redo_point);
  scm_c_define_gsubr("nk_text_undo_state_redo_point", 1, 0, 0, nk_text_undo_state_redo_point);
  scm_c_define_gsubr("nk_text_undo_state_set_undo_point", 2, 0, 0, nk_text_undo_state_set_undo_point);
  scm_c_define_gsubr("nk_text_undo_state_undo_point", 1, 0, 0, nk_text_undo_state_undo_point);
  scm_c_define_gsubr("nk_text_undo_state_set_undo_char", 2, 0, 0, nk_text_undo_state_set_undo_char);
  scm_c_define_gsubr("nk_text_undo_state_undo_char", 1, 0, 0, nk_text_undo_state_undo_char);
  scm_c_define_gsubr("nk_text_undo_state_set_undo_rec", 2, 0, 0, nk_text_undo_state_set_undo_rec);
  scm_c_define_gsubr("nk_text_undo_state_undo_rec", 1, 0, 0, nk_text_undo_state_undo_rec);
  scm_c_define_gsubr("nk_text_undo_record_set_char_storage", 2, 0, 0, nk_text_undo_record_set_char_storage);
  scm_c_define_gsubr("nk_text_undo_record_char_storage", 1, 0, 0, nk_text_undo_record_char_storage);
  scm_c_define_gsubr("nk_text_undo_record_set_delete_length", 2, 0, 0, nk_text_undo_record_set_delete_length);
  scm_c_define_gsubr("nk_text_undo_record_delete_length", 1, 0, 0, nk_text_undo_record_delete_length);
  scm_c_define_gsubr("nk_text_undo_record_set_insert_length", 2, 0, 0, nk_text_undo_record_set_insert_length);
  scm_c_define_gsubr("nk_text_undo_record_insert_length", 1, 0, 0, nk_text_undo_record_insert_length);
  scm_c_define_gsubr("nk_text_undo_record_set_where", 2, 0, 0, nk_text_undo_record_set_where);
  scm_c_define_gsubr("nk_text_undo_record_where", 1, 0, 0, nk_text_undo_record_where);
  scm_c_define_gsubr("nk_clipboard_set_copy", 2, 0, 0, nk_clipboard_set_copy);
  scm_c_define_gsubr("nk_clipboard_copy", 1, 0, 0, nk_clipboard_copy);
  scm_c_define_gsubr("nk_clipboard_set_paste", 2, 0, 0, nk_clipboard_set_paste);
  scm_c_define_gsubr("nk_clipboard_paste", 1, 0, 0, nk_clipboard_paste);
  scm_c_define_gsubr("nk_clipboard_set_userdata", 2, 0, 0, nk_clipboard_set_userdata);
  scm_c_define_gsubr("nk_clipboard_userdata", 1, 0, 0, nk_clipboard_userdata);
  scm_c_define_gsubr("nk_str_set_len", 2, 0, 0, nk_str_set_len);
  scm_c_define_gsubr("nk_str_get_len", 1, 0, 0, nk_str_get_len);
  scm_c_define_gsubr("nk_str_set_buffer", 2, 0, 0, nk_str_set_buffer);
  scm_c_define_gsubr("nk_str_buffer", 1, 0, 0, nk_str_buffer);
  scm_c_define_gsubr("nk_buffer_set_size", 2, 0, 0, nk_buffer_set_size);
  scm_c_define_gsubr("nk_buffer_size", 1, 0, 0, nk_buffer_size);
  scm_c_define_gsubr("nk_buffer_set_calls", 2, 0, 0, nk_buffer_set_calls);
  scm_c_define_gsubr("nk_buffer_calls", 1, 0, 0, nk_buffer_calls);
  scm_c_define_gsubr("nk_buffer_set_needed", 2, 0, 0, nk_buffer_set_needed);
  scm_c_define_gsubr("nk_buffer_needed", 1, 0, 0, nk_buffer_needed);
  scm_c_define_gsubr("nk_buffer_set_allocated", 2, 0, 0, nk_buffer_set_allocated);
  scm_c_define_gsubr("nk_buffer_allocated", 1, 0, 0, nk_buffer_allocated);
  scm_c_define_gsubr("nk_buffer_set_grow_factor", 2, 0, 0, nk_buffer_set_grow_factor);
  scm_c_define_gsubr("nk_buffer_grow_factor", 1, 0, 0, nk_buffer_grow_factor);
  scm_c_define_gsubr("nk_buffer_set_memory", 2, 0, 0, nk_buffer_set_memory);
  scm_c_define_gsubr("nk_buffer_get_memory", 1, 0, 0, nk_buffer_get_memory);
  scm_c_define_gsubr("nk_buffer_set_type", 2, 0, 0, nk_buffer_set_type);
  scm_c_define_gsubr("nk_buffer_type", 1, 0, 0, nk_buffer_type);
  scm_c_define_gsubr("nk_buffer_set_pool", 2, 0, 0, nk_buffer_set_pool);
  scm_c_define_gsubr("nk_buffer_pool", 1, 0, 0, nk_buffer_pool);
  scm_c_define_gsubr("nk_buffer_set_marker", 2, 0, 0, nk_buffer_set_marker);
  scm_c_define_gsubr("nk_buffer_marker", 1, 0, 0, nk_buffer_marker);
  scm_c_define_gsubr("nk_memory_set_size", 2, 0, 0, nk_memory_set_size);
  scm_c_define_gsubr("nk_memory_size", 1, 0, 0, nk_memory_size);
  scm_c_define_gsubr("nk_memory_set_ptr", 2, 0, 0, nk_memory_set_ptr);
  scm_c_define_gsubr("nk_memory_ptr", 1, 0, 0, nk_memory_ptr);
  scm_c_define_gsubr("nk_buffer_marker_set_offset", 2, 0, 0, nk_buffer_marker_set_offset);
  scm_c_define_gsubr("nk_buffer_marker_offset", 1, 0, 0, nk_buffer_marker_offset);
  scm_c_define_gsubr("nk_buffer_marker_set_active", 2, 0, 0, nk_buffer_marker_set_active);
  scm_c_define_gsubr("nk_buffer_marker_active", 1, 0, 0, nk_buffer_marker_active);
  scm_c_define_gsubr("nk_memory_status_set_calls", 2, 0, 0, nk_memory_status_set_calls);
  scm_c_define_gsubr("nk_memory_status_calls", 1, 0, 0, nk_memory_status_calls);
  scm_c_define_gsubr("nk_memory_status_set_needed", 2, 0, 0, nk_memory_status_set_needed);
  scm_c_define_gsubr("nk_memory_status_needed", 1, 0, 0, nk_memory_status_needed);
  scm_c_define_gsubr("nk_memory_status_set_allocated", 2, 0, 0, nk_memory_status_set_allocated);
  scm_c_define_gsubr("nk_memory_status_allocated", 1, 0, 0, nk_memory_status_allocated);
  scm_c_define_gsubr("nk_memory_status_set_size", 2, 0, 0, nk_memory_status_set_size);
  scm_c_define_gsubr("nk_memory_status_size", 1, 0, 0, nk_memory_status_size);
  scm_c_define_gsubr("nk_memory_status_set_type", 2, 0, 0, nk_memory_status_set_type);
  scm_c_define_gsubr("nk_memory_status_type", 1, 0, 0, nk_memory_status_type);
  scm_c_define_gsubr("nk_memory_status_set_memory", 2, 0, 0, nk_memory_status_set_memory);
  scm_c_define_gsubr("nk_memory_status_memory", 1, 0, 0, nk_memory_status_memory);
  scm_c_define_gsubr("nk_font_atlas_set_font_num", 2, 0, 0, nk_font_atlas_set_font_num);
  scm_c_define_gsubr("nk_font_atlas_font_num", 1, 0, 0, nk_font_atlas_font_num);
  scm_c_define_gsubr("nk_font_atlas_set_config", 2, 0, 0, nk_font_atlas_set_config);
  scm_c_define_gsubr("nk_font_atlas_config", 1, 0, 0, nk_font_atlas_config);
  scm_c_define_gsubr("nk_font_atlas_set_fonts", 2, 0, 0, nk_font_atlas_set_fonts);
  scm_c_define_gsubr("nk_font_atlas_fonts", 1, 0, 0, nk_font_atlas_fonts);
  scm_c_define_gsubr("nk_font_atlas_set_default_font", 2, 0, 0, nk_font_atlas_set_default_font);
  scm_c_define_gsubr("nk_font_atlas_default_font", 1, 0, 0, nk_font_atlas_default_font);
  scm_c_define_gsubr("nk_font_atlas_set_glyphs", 2, 0, 0, nk_font_atlas_set_glyphs);
  scm_c_define_gsubr("nk_font_atlas_glyphs", 1, 0, 0, nk_font_atlas_glyphs);
  scm_c_define_gsubr("nk_font_atlas_set_glyph_count", 2, 0, 0, nk_font_atlas_set_glyph_count);
  scm_c_define_gsubr("nk_font_atlas_glyph_count", 1, 0, 0, nk_font_atlas_glyph_count);
  scm_c_define_gsubr("nk_font_atlas_set_cursors", 2, 0, 0, nk_font_atlas_set_cursors);
  scm_c_define_gsubr("nk_font_atlas_cursors", 1, 0, 0, nk_font_atlas_cursors);
  scm_c_define_gsubr("nk_font_atlas_set_custom", 2, 0, 0, nk_font_atlas_set_custom);
  scm_c_define_gsubr("nk_font_atlas_custom", 1, 0, 0, nk_font_atlas_custom);
  scm_c_define_gsubr("nk_font_atlas_set_temporary", 2, 0, 0, nk_font_atlas_set_temporary);
  scm_c_define_gsubr("nk_font_atlas_temporary", 1, 0, 0, nk_font_atlas_temporary);
  scm_c_define_gsubr("nk_font_atlas_set_permanent", 2, 0, 0, nk_font_atlas_set_permanent);
  scm_c_define_gsubr("nk_font_atlas_permanent", 1, 0, 0, nk_font_atlas_permanent);
  scm_c_define_gsubr("nk_font_atlas_set_tex_height", 2, 0, 0, nk_font_atlas_set_tex_height);
  scm_c_define_gsubr("nk_font_atlas_tex_height", 1, 0, 0, nk_font_atlas_tex_height);
  scm_c_define_gsubr("nk_font_atlas_set_tex_width", 2, 0, 0, nk_font_atlas_set_tex_width);
  scm_c_define_gsubr("nk_font_atlas_tex_width", 1, 0, 0, nk_font_atlas_tex_width);
  scm_c_define_gsubr("nk_font_atlas_set_pixel", 2, 0, 0, nk_font_atlas_set_pixel);
  scm_c_define_gsubr("nk_font_atlas_pixel", 1, 0, 0, nk_font_atlas_pixel);
  scm_c_define_gsubr("nk_font_set_config", 2, 0, 0, nk_font_set_config);
  scm_c_define_gsubr("nk_font_get_config", 1, 0, 0, nk_font_get_config);
  scm_c_define_gsubr("nk_font_set_texture", 2, 0, 0, nk_font_set_texture);
  scm_c_define_gsubr("nk_font_texture", 1, 0, 0, nk_font_texture);
  scm_c_define_gsubr("nk_font_set_fallback_codepoint", 2, 0, 0, nk_font_set_fallback_codepoint);
  scm_c_define_gsubr("nk_font_fallback_codepoint", 1, 0, 0, nk_font_fallback_codepoint);
  scm_c_define_gsubr("nk_font_set_fallback", 2, 0, 0, nk_font_set_fallback);
  scm_c_define_gsubr("nk_font_fallback", 1, 0, 0, nk_font_fallback);
  scm_c_define_gsubr("nk_font_set_glyphs", 2, 0, 0, nk_font_set_glyphs);
  scm_c_define_gsubr("nk_font_glyphs", 1, 0, 0, nk_font_glyphs);
  scm_c_define_gsubr("nk_font_set_scale", 2, 0, 0, nk_font_set_scale);
  scm_c_define_gsubr("nk_font_scale", 1, 0, 0, nk_font_scale);
  scm_c_define_gsubr("nk_font_set_info", 2, 0, 0, nk_font_set_info);
  scm_c_define_gsubr("nk_font_info", 1, 0, 0, nk_font_info);
  scm_c_define_gsubr("nk_font_set_handle", 2, 0, 0, nk_font_set_handle);
  scm_c_define_gsubr("nk_font_handle", 1, 0, 0, nk_font_handle);
  scm_c_define_gsubr("nk_font_set_next", 2, 0, 0, nk_font_set_next);
  scm_c_define_gsubr("nk_font_next", 1, 0, 0, nk_font_next);
  scm_c_define_gsubr("nk_font_glyph_set_v1", 2, 0, 0, nk_font_glyph_set_v1);
  scm_c_define_gsubr("nk_font_glyph_v1", 1, 0, 0, nk_font_glyph_v1);
  scm_c_define_gsubr("nk_font_glyph_set_u1", 2, 0, 0, nk_font_glyph_set_u1);
  scm_c_define_gsubr("nk_font_glyph_u1", 1, 0, 0, nk_font_glyph_u1);
  scm_c_define_gsubr("nk_font_glyph_set_v0", 2, 0, 0, nk_font_glyph_set_v0);
  scm_c_define_gsubr("nk_font_glyph_v0", 1, 0, 0, nk_font_glyph_v0);
  scm_c_define_gsubr("nk_font_glyph_set_u0", 2, 0, 0, nk_font_glyph_set_u0);
  scm_c_define_gsubr("nk_font_glyph_u0", 1, 0, 0, nk_font_glyph_u0);
  scm_c_define_gsubr("nk_font_glyph_set_h", 2, 0, 0, nk_font_glyph_set_h);
  scm_c_define_gsubr("nk_font_glyph_h", 1, 0, 0, nk_font_glyph_h);
  scm_c_define_gsubr("nk_font_glyph_set_w", 2, 0, 0, nk_font_glyph_set_w);
  scm_c_define_gsubr("nk_font_glyph_w", 1, 0, 0, nk_font_glyph_w);
  scm_c_define_gsubr("nk_font_glyph_set_y1", 2, 0, 0, nk_font_glyph_set_y1);
  scm_c_define_gsubr("nk_font_glyph_y1", 1, 0, 0, nk_font_glyph_y1);
  scm_c_define_gsubr("nk_font_glyph_set_x1", 2, 0, 0, nk_font_glyph_set_x1);
  scm_c_define_gsubr("nk_font_glyph_x1", 1, 0, 0, nk_font_glyph_x1);
  scm_c_define_gsubr("nk_font_glyph_set_y0", 2, 0, 0, nk_font_glyph_set_y0);
  scm_c_define_gsubr("nk_font_glyph_y0", 1, 0, 0, nk_font_glyph_y0);
  scm_c_define_gsubr("nk_font_glyph_set_x0", 2, 0, 0, nk_font_glyph_set_x0);
  scm_c_define_gsubr("nk_font_glyph_x0", 1, 0, 0, nk_font_glyph_x0);
  scm_c_define_gsubr("nk_font_glyph_set_xadvance", 2, 0, 0, nk_font_glyph_set_xadvance);
  scm_c_define_gsubr("nk_font_glyph_xadvance", 1, 0, 0, nk_font_glyph_xadvance);
  scm_c_define_gsubr("nk_font_glyph_set_codepoint", 2, 0, 0, nk_font_glyph_set_codepoint);
  scm_c_define_gsubr("nk_font_glyph_codepoint", 1, 0, 0, nk_font_glyph_codepoint);
  scm_c_define_gsubr("nk_font_config_set_p", 2, 0, 0, nk_font_config_set_p);
  scm_c_define_gsubr("nk_font_config_p", 1, 0, 0, nk_font_config_p);
  scm_c_define_gsubr("nk_font_config_set_n", 2, 0, 0, nk_font_config_set_n);
  scm_c_define_gsubr("nk_font_config_n", 1, 0, 0, nk_font_config_n);
  scm_c_define_gsubr("nk_font_config_set_fallback_glyph", 2, 0, 0, nk_font_config_set_fallback_glyph);
  scm_c_define_gsubr("nk_font_config_fallback_glyph", 1, 0, 0, nk_font_config_fallback_glyph);
  scm_c_define_gsubr("nk_font_config_set_font", 2, 0, 0, nk_font_config_set_font);
  scm_c_define_gsubr("nk_font_config_font", 1, 0, 0, nk_font_config_font);
  scm_c_define_gsubr("nk_font_config_set_range", 2, 0, 0, nk_font_config_set_range);
  scm_c_define_gsubr("nk_font_config_range", 1, 0, 0, nk_font_config_range);
  scm_c_define_gsubr("nk_font_config_set_spacing", 2, 0, 0, nk_font_config_set_spacing);
  scm_c_define_gsubr("nk_font_config_spacing", 1, 0, 0, nk_font_config_spacing);
  scm_c_define_gsubr("nk_font_config_set_coord_type", 2, 0, 0, nk_font_config_set_coord_type);
  scm_c_define_gsubr("nk_font_config_coord_type", 1, 0, 0, nk_font_config_coord_type);
  scm_c_define_gsubr("nk_font_config_set_size", 2, 0, 0, nk_font_config_set_size);
  scm_c_define_gsubr("nk_font_config_size", 1, 0, 0, nk_font_config_size);
  scm_c_define_gsubr("nk_font_config_set_padding", 2, 0, 0, nk_font_config_set_padding);
  scm_c_define_gsubr("nk_font_config_padding", 1, 0, 0, nk_font_config_padding);
  scm_c_define_gsubr("nk_font_config_set_oversample_h", 2, 0, 0, nk_font_config_set_oversample_h);
  scm_c_define_gsubr("nk_font_config_oversample_h", 1, 0, 0, nk_font_config_oversample_h);
  scm_c_define_gsubr("nk_font_config_set_oversample_v", 2, 0, 0, nk_font_config_set_oversample_v);
  scm_c_define_gsubr("nk_font_config_oversample_v", 1, 0, 0, nk_font_config_oversample_v);
  scm_c_define_gsubr("nk_font_config_set_pixel_snap", 2, 0, 0, nk_font_config_set_pixel_snap);
  scm_c_define_gsubr("nk_font_config_pixel_snap", 1, 0, 0, nk_font_config_pixel_snap);
  scm_c_define_gsubr("nk_font_config_set_merge_mode", 2, 0, 0, nk_font_config_set_merge_mode);
  scm_c_define_gsubr("nk_font_config_merge_mode", 1, 0, 0, nk_font_config_merge_mode);
  scm_c_define_gsubr("nk_font_config_set_ttf_data_owned_by_atlas", 2, 0, 0, nk_font_config_set_ttf_data_owned_by_atlas);
  scm_c_define_gsubr("nk_font_config_ttf_data_owned_by_atlas", 1, 0, 0, nk_font_config_ttf_data_owned_by_atlas);
  scm_c_define_gsubr("nk_font_config_set_ttf_size", 2, 0, 0, nk_font_config_set_ttf_size);
  scm_c_define_gsubr("nk_font_config_ttf_size", 1, 0, 0, nk_font_config_ttf_size);
  scm_c_define_gsubr("nk_font_config_set_ttf_blob", 2, 0, 0, nk_font_config_set_ttf_blob);
  scm_c_define_gsubr("nk_font_config_ttf_blob", 1, 0, 0, nk_font_config_ttf_blob);
  scm_c_define_gsubr("nk_font_config_set_next", 2, 0, 0, nk_font_config_set_next);
  scm_c_define_gsubr("nk_font_config_next", 1, 0, 0, nk_font_config_next);
  scm_c_define_gsubr("nk_baked_font_set_ranges", 2, 0, 0, nk_baked_font_set_ranges);
  scm_c_define_gsubr("nk_baked_font_ranges", 1, 0, 0, nk_baked_font_ranges);
  scm_c_define_gsubr("nk_baked_font_set_glyph_count", 2, 0, 0, nk_baked_font_set_glyph_count);
  scm_c_define_gsubr("nk_baked_font_glyph_count", 1, 0, 0, nk_baked_font_glyph_count);
  scm_c_define_gsubr("nk_baked_font_set_glyph_offset", 2, 0, 0, nk_baked_font_set_glyph_offset);
  scm_c_define_gsubr("nk_baked_font_glyph_offset", 1, 0, 0, nk_baked_font_glyph_offset);
  scm_c_define_gsubr("nk_baked_font_set_descent", 2, 0, 0, nk_baked_font_set_descent);
  scm_c_define_gsubr("nk_baked_font_descent", 1, 0, 0, nk_baked_font_descent);
  scm_c_define_gsubr("nk_baked_font_set_ascent", 2, 0, 0, nk_baked_font_set_ascent);
  scm_c_define_gsubr("nk_baked_font_ascent", 1, 0, 0, nk_baked_font_ascent);
  scm_c_define_gsubr("nk_baked_font_set_height", 2, 0, 0, nk_baked_font_set_height);
  scm_c_define_gsubr("nk_baked_font_height", 1, 0, 0, nk_baked_font_height);
  scm_c_define_gsubr("nk_user_font_set_texture", 2, 0, 0, nk_user_font_set_texture);
  scm_c_define_gsubr("nk_user_font_texture", 1, 0, 0, nk_user_font_texture);
  scm_c_define_gsubr("nk_user_font_set_query", 2, 0, 0, nk_user_font_set_query);
  scm_c_define_gsubr("nk_user_font_query", 1, 0, 0, nk_user_font_query);
  scm_c_define_gsubr("nk_user_font_set_width", 2, 0, 0, nk_user_font_set_width);
  scm_c_define_gsubr("nk_user_font_width", 1, 0, 0, nk_user_font_width);
  scm_c_define_gsubr("nk_user_font_set_height", 2, 0, 0, nk_user_font_set_height);
  scm_c_define_gsubr("nk_user_font_height", 1, 0, 0, nk_user_font_height);
  scm_c_define_gsubr("nk_user_font_set_userdata", 2, 0, 0, nk_user_font_set_userdata);
  scm_c_define_gsubr("nk_user_font_userdata", 1, 0, 0, nk_user_font_userdata);
  scm_c_define_gsubr("nk_user_font_glyph_set_xadvance", 2, 0, 0, nk_user_font_glyph_set_xadvance);
  scm_c_define_gsubr("nk_user_font_glyph_xadvance", 1, 0, 0, nk_user_font_glyph_xadvance);
  scm_c_define_gsubr("nk_user_font_glyph_set_height", 2, 0, 0, nk_user_font_glyph_set_height);
  scm_c_define_gsubr("nk_user_font_glyph_height", 1, 0, 0, nk_user_font_glyph_height);
  scm_c_define_gsubr("nk_user_font_glyph_set_width", 2, 0, 0, nk_user_font_glyph_set_width);
  scm_c_define_gsubr("nk_user_font_glyph_width", 1, 0, 0, nk_user_font_glyph_width);
  scm_c_define_gsubr("nk_user_font_glyph_set_offset", 2, 0, 0, nk_user_font_glyph_set_offset);
  scm_c_define_gsubr("nk_user_font_glyph_offset", 1, 0, 0, nk_user_font_glyph_offset);
  scm_c_define_gsubr("nk_user_font_glyph_set_uv", 2, 0, 0, nk_user_font_glyph_set_uv);
  scm_c_define_gsubr("nk_user_font_glyph_uv", 1, 0, 0, nk_user_font_glyph_uv);
  scm_c_define_gsubr("nk_list_view_set_scroll_value", 2, 0, 0, nk_list_view_set_scroll_value);
  scm_c_define_gsubr("nk_list_view_scroll_value", 1, 0, 0, nk_list_view_scroll_value);
  scm_c_define_gsubr("nk_list_view_set_scroll_pointer", 2, 0, 0, nk_list_view_set_scroll_pointer);
  scm_c_define_gsubr("nk_list_view_scroll_pointer", 1, 0, 0, nk_list_view_scroll_pointer);
  scm_c_define_gsubr("nk_list_view_set_ctx", 2, 0, 0, nk_list_view_set_ctx);
  scm_c_define_gsubr("nk_list_view_ctx", 1, 0, 0, nk_list_view_ctx);
  scm_c_define_gsubr("nk_list_view_set_total_height", 2, 0, 0, nk_list_view_set_total_height);
  scm_c_define_gsubr("nk_list_view_total_height", 1, 0, 0, nk_list_view_total_height);
  scm_c_define_gsubr("nk_list_view_set_count", 2, 0, 0, nk_list_view_set_count);
  scm_c_define_gsubr("nk_list_view_count", 1, 0, 0, nk_list_view_count);
  scm_c_define_gsubr("nk_list_view_set_end", 2, 0, 0, nk_list_view_set_end);
  scm_c_define_gsubr("nk_list_view_get_end", 1, 0, 0, nk_list_view_get_end);
  scm_c_define_gsubr("nk_list_view_set_begin", 2, 0, 0, nk_list_view_set_begin);
  scm_c_define_gsubr("nk_list_view_get_begin", 1, 0, 0, nk_list_view_get_begin);
  scm_c_define_gsubr("nk_convert_config_set_vertex_alignment", 2, 0, 0, nk_convert_config_set_vertex_alignment);
  scm_c_define_gsubr("nk_convert_config_vertex_alignment", 1, 0, 0, nk_convert_config_vertex_alignment);
  scm_c_define_gsubr("nk_convert_config_set_vertex_size", 2, 0, 0, nk_convert_config_set_vertex_size);
  scm_c_define_gsubr("nk_convert_config_vertex_size", 1, 0, 0, nk_convert_config_vertex_size);
  scm_c_define_gsubr("nk_convert_config_set_vertex_layout", 2, 0, 0, nk_convert_config_set_vertex_layout);
  scm_c_define_gsubr("nk_convert_config_vertex_layout", 1, 0, 0, nk_convert_config_vertex_layout);
  scm_c_define_gsubr("nk_convert_config_set_null", 2, 0, 0, nk_convert_config_set_null);
  scm_c_define_gsubr("nk_convert_config_null", 1, 0, 0, nk_convert_config_null);
  scm_c_define_gsubr("nk_convert_config_set_curve_segment_count", 2, 0, 0, nk_convert_config_set_curve_segment_count);
  scm_c_define_gsubr("nk_convert_config_curve_segment_count", 1, 0, 0, nk_convert_config_curve_segment_count);
  scm_c_define_gsubr("nk_convert_config_set_arc_segment_count", 2, 0, 0, nk_convert_config_set_arc_segment_count);
  scm_c_define_gsubr("nk_convert_config_arc_segment_count", 1, 0, 0, nk_convert_config_arc_segment_count);
  scm_c_define_gsubr("nk_convert_config_set_circle_segment_count", 2, 0, 0, nk_convert_config_set_circle_segment_count);
  scm_c_define_gsubr("nk_convert_config_circle_segment_count", 1, 0, 0, nk_convert_config_circle_segment_count);
  scm_c_define_gsubr("nk_convert_config_set_shape_AA", 2, 0, 0, nk_convert_config_set_shape_AA);
  scm_c_define_gsubr("nk_convert_config_shape_AA", 1, 0, 0, nk_convert_config_shape_AA);
  scm_c_define_gsubr("nk_convert_config_set_line_AA", 2, 0, 0, nk_convert_config_set_line_AA);
  scm_c_define_gsubr("nk_convert_config_line_AA", 1, 0, 0, nk_convert_config_line_AA);
  scm_c_define_gsubr("nk_convert_config_set_global_alpha", 2, 0, 0, nk_convert_config_set_global_alpha);
  scm_c_define_gsubr("nk_convert_config_global_alpha", 1, 0, 0, nk_convert_config_global_alpha);
  scm_c_define_gsubr("nk_draw_null_texture_set_uv", 2, 0, 0, nk_draw_null_texture_set_uv);
  scm_c_define_gsubr("nk_draw_null_texture_uv", 1, 0, 0, nk_draw_null_texture_uv);
  scm_c_define_gsubr("nk_draw_null_texture_set_texture", 2, 0, 0, nk_draw_null_texture_set_texture);
  scm_c_define_gsubr("nk_draw_null_texture_texture", 1, 0, 0, nk_draw_null_texture_texture);
  scm_c_define_gsubr("nk_allocator_set_free", 2, 0, 0, nk_allocator_set_free);
  scm_c_define_gsubr("nk_allocator_free", 1, 0, 0, nk_allocator_free);
  scm_c_define_gsubr("nk_allocator_set_alloc", 2, 0, 0, nk_allocator_set_alloc);
  scm_c_define_gsubr("nk_allocator_alloc", 1, 0, 0, nk_allocator_alloc);
  scm_c_define_gsubr("nk_allocator_set_userdata", 2, 0, 0, nk_allocator_set_userdata);
  scm_c_define_gsubr("nk_allocator_userdata", 1, 0, 0, nk_allocator_userdata);
  scm_c_define_gsubr("nk_scroll_set_y", 2, 0, 0, nk_scroll_set_y);
  scm_c_define_gsubr("nk_scroll_y", 1, 0, 0, nk_scroll_y);
  scm_c_define_gsubr("nk_scroll_set_x", 2, 0, 0, nk_scroll_set_x);
  scm_c_define_gsubr("nk_scroll_x", 1, 0, 0, nk_scroll_x);
  scm_c_define_gsubr("nk_cursor_set_offset", 2, 0, 0, nk_cursor_set_offset);
  scm_c_define_gsubr("nk_cursor_offset", 1, 0, 0, nk_cursor_offset);
  scm_c_define_gsubr("nk_cursor_set_size", 2, 0, 0, nk_cursor_set_size);
  scm_c_define_gsubr("nk_cursor_size", 1, 0, 0, nk_cursor_size);
  scm_c_define_gsubr("nk_cursor_set_img", 2, 0, 0, nk_cursor_set_img);
  scm_c_define_gsubr("nk_cursor_img", 1, 0, 0, nk_cursor_img);
  scm_c_define_gsubr("nk_image_set_region", 2, 0, 0, nk_image_set_region);
  scm_c_define_gsubr("nk_image_region", 1, 0, 0, nk_image_region);
  scm_c_define_gsubr("nk_image_set_h", 2, 0, 0, nk_image_set_h);
  scm_c_define_gsubr("nk_image_h", 1, 0, 0, nk_image_h);
  scm_c_define_gsubr("nk_image_set_w", 2, 0, 0, nk_image_set_w);
  scm_c_define_gsubr("nk_image_w", 1, 0, 0, nk_image_w);
  scm_c_define_gsubr("nk_image_set_handle", 2, 0, 0, nk_image_set_handle);
  scm_c_define_gsubr("nk_recti_set_h", 2, 0, 0, nk_recti_set_h);
  scm_c_define_gsubr("nk_recti_h", 1, 0, 0, nk_recti_h);
  scm_c_define_gsubr("nk_recti_set_w", 2, 0, 0, nk_recti_set_w);
  scm_c_define_gsubr("nk_recti_w", 1, 0, 0, nk_recti_w);
  scm_c_define_gsubr("nk_recti_set_y", 2, 0, 0, nk_recti_set_y);
  scm_c_define_gsubr("nk_recti_y", 1, 0, 0, nk_recti_y);
  scm_c_define_gsubr("nk_recti_set_x", 2, 0, 0, nk_recti_set_x);
  scm_c_define_gsubr("nk_recti_x", 1, 0, 0, nk_recti_x);
  scm_c_define_gsubr("nk_rect_set_h", 2, 0, 0, nk_rect_set_h);
  scm_c_define_gsubr("nk_rect_h", 1, 0, 0, nk_rect_h);
  scm_c_define_gsubr("nk_rect_set_w", 2, 0, 0, nk_rect_set_w);
  scm_c_define_gsubr("nk_rect_w", 1, 0, 0, nk_rect_w);
  scm_c_define_gsubr("nk_rect_set_y", 2, 0, 0, nk_rect_set_y);
  scm_c_define_gsubr("nk_rect_y", 1, 0, 0, nk_rect_y);
  scm_c_define_gsubr("nk_rect_set_x", 2, 0, 0, nk_rect_set_x);
  scm_c_define_gsubr("nk_rect_x", 1, 0, 0, nk_rect_x);
  scm_c_define_gsubr("nk_vec2i_set_y", 2, 0, 0, nk_vec2i_set_y);
  scm_c_define_gsubr("nk_vec2i_y", 1, 0, 0, nk_vec2i_y);
  scm_c_define_gsubr("nk_vec2i_set_x", 2, 0, 0, nk_vec2i_set_x);
  scm_c_define_gsubr("nk_vec2i_x", 1, 0, 0, nk_vec2i_x);
  scm_c_define_gsubr("nk_vec2_set_y", 2, 0, 0, nk_vec2_set_y);
  scm_c_define_gsubr("nk_vec2_y", 1, 0, 0, nk_vec2_y);
  scm_c_define_gsubr("nk_vec2_set_x", 2, 0, 0, nk_vec2_set_x);
  scm_c_define_gsubr("nk_vec2_x", 1, 0, 0, nk_vec2_x);
  scm_c_define_gsubr("nk_colorf_set_a", 2, 0, 0, nk_colorf_set_a);
  scm_c_define_gsubr("nk_colorf_a", 1, 0, 0, nk_colorf_a);
  scm_c_define_gsubr("nk_colorf_set_b", 2, 0, 0, nk_colorf_set_b);
  scm_c_define_gsubr("nk_colorf_b", 1, 0, 0, nk_colorf_b);
  scm_c_define_gsubr("nk_colorf_set_g", 2, 0, 0, nk_colorf_set_g);
  scm_c_define_gsubr("nk_colorf_g", 1, 0, 0, nk_colorf_g);
  scm_c_define_gsubr("nk_colorf_set_r", 2, 0, 0, nk_colorf_set_r);
  scm_c_define_gsubr("nk_colorf_r", 1, 0, 0, nk_colorf_r);
  scm_c_define_gsubr("nk_color_set_a", 2, 0, 0, nk_color_set_a);
  scm_c_define_gsubr("nk_color_a", 1, 0, 0, nk_color_a);
  scm_c_define_gsubr("nk_color_set_b", 2, 0, 0, nk_color_set_b);
  scm_c_define_gsubr("nk_color_b", 1, 0, 0, nk_color_b);
  scm_c_define_gsubr("nk_color_set_g", 2, 0, 0, nk_color_set_g);
  scm_c_define_gsubr("nk_color_g", 1, 0, 0, nk_color_g);
  scm_c_define_gsubr("nk_color_set_r", 2, 0, 0, nk_color_set_r);
  scm_c_define_gsubr("nk_color_r", 1, 0, 0, nk_color_r);
  scm_c_define_gsubr("nk_init_default", 2, 0, 0, nk_init_default_wrapper);
  scm_c_define_gsubr("nk_init_fixed", 4, 0, 0, nk_init_fixed_wrapper);
  scm_c_define_gsubr("nk_init", 3, 0, 0, nk_init_wrapper);
  scm_c_define_gsubr("nk_init_custom", 4, 0, 0, nk_init_custom_wrapper);
  scm_c_define_gsubr("nk_clear", 1, 0, 0, nk_clear_wrapper);
  scm_c_define_gsubr("nk_free", 1, 0, 0, nk_free_wrapper);
  scm_c_define_gsubr("nk_input_begin", 1, 0, 0, nk_input_begin_wrapper);
  scm_c_define_gsubr("nk_input_motion", 3, 0, 0, nk_input_motion_wrapper);
  scm_c_define_gsubr("nk_input_key", 3, 0, 0, nk_input_key_wrapper);
  scm_c_define_gsubr("nk_input_button", 5, 0, 0, nk_input_button_wrapper);
  scm_c_define_gsubr("nk_input_scroll", 2, 0, 0, nk_input_scroll_wrapper);
  scm_c_define_gsubr("nk_input_char", 2, 0, 0, nk_input_char_wrapper);
  scm_c_define_gsubr("nk_input_glyph", 2, 0, 0, nk_input_glyph_wrapper);
  scm_c_define_gsubr("nk_input_unicode", 2, 0, 0, nk_input_unicode_wrapper);
  scm_c_define_gsubr("nk_input_end", 1, 0, 0, nk_input_end_wrapper);
  scm_c_define_gsubr("nk__begin", 1, 0, 0, nk__begin_wrapper);
  scm_c_define_gsubr("nk__next", 2, 0, 0, nk__next_wrapper);
  scm_c_define_gsubr("nk_convert", 5, 0, 0, nk_convert_wrapper);
  scm_c_define_gsubr("nk__draw_begin", 2, 0, 0, nk__draw_begin_wrapper);
  scm_c_define_gsubr("nk__draw_end", 2, 0, 0, nk__draw_end_wrapper);
  scm_c_define_gsubr("nk__draw_next", 3, 0, 0, nk__draw_next_wrapper);
  scm_c_define_gsubr("nk_begin", 4, 0, 0, nk_begin_wrapper);
  scm_c_define_gsubr("nk_begin_titled", 5, 0, 0, nk_begin_titled_wrapper);
  scm_c_define_gsubr("nk_end", 1, 0, 0, nk_end_wrapper);
  scm_c_define_gsubr("nk_window_find", 2, 0, 0, nk_window_find_wrapper);
  scm_c_define_gsubr("nk_window_get_bounds", 1, 0, 0, nk_window_get_bounds_wrapper);
  scm_c_define_gsubr("nk_window_get_position", 1, 0, 0, nk_window_get_position_wrapper);
  scm_c_define_gsubr("nk_window_get_size", 1, 0, 0, nk_window_get_size_wrapper);
  scm_c_define_gsubr("nk_window_get_width", 1, 0, 0, nk_window_get_width_wrapper);
  scm_c_define_gsubr("nk_window_get_height", 1, 0, 0, nk_window_get_height_wrapper);
  scm_c_define_gsubr("nk_window_get_panel", 1, 0, 0, nk_window_get_panel_wrapper);
  scm_c_define_gsubr("nk_window_get_content_region", 1, 0, 0, nk_window_get_content_region_wrapper);
  scm_c_define_gsubr("nk_window_get_content_region_min", 1, 0, 0, nk_window_get_content_region_min_wrapper);
  scm_c_define_gsubr("nk_window_get_content_region_max", 1, 0, 0, nk_window_get_content_region_max_wrapper);
  scm_c_define_gsubr("nk_window_get_content_region_size", 1, 0, 0, nk_window_get_content_region_size_wrapper);
  scm_c_define_gsubr("nk_window_get_canvas", 1, 0, 0, nk_window_get_canvas_wrapper);
  scm_c_define_gsubr("nk_window_get_scroll", 3, 0, 0, nk_window_get_scroll_wrapper);
  scm_c_define_gsubr("nk_window_has_focus", 1, 0, 0, nk_window_has_focus_wrapper);
  scm_c_define_gsubr("nk_window_is_hovered", 1, 0, 0, nk_window_is_hovered_wrapper);
  scm_c_define_gsubr("nk_window_is_collapsed", 2, 0, 0, nk_window_is_collapsed_wrapper);
  scm_c_define_gsubr("nk_window_is_closed", 2, 0, 0, nk_window_is_closed_wrapper);
  scm_c_define_gsubr("nk_window_is_hidden", 2, 0, 0, nk_window_is_hidden_wrapper);
  scm_c_define_gsubr("nk_window_is_active", 2, 0, 0, nk_window_is_active_wrapper);
  scm_c_define_gsubr("nk_window_is_any_hovered", 1, 0, 0, nk_window_is_any_hovered_wrapper);
  scm_c_define_gsubr("nk_item_is_any_active", 1, 0, 0, nk_item_is_any_active_wrapper);
  scm_c_define_gsubr("nk_window_set_position", 3, 0, 0, nk_window_set_position_wrapper);
  scm_c_define_gsubr("nk_window_set_size", 3, 0, 0, nk_window_set_size_wrapper);
  scm_c_define_gsubr("nk_window_set_focus", 2, 0, 0, nk_window_set_focus_wrapper);
  scm_c_define_gsubr("nk_window_set_scroll", 3, 0, 0, nk_window_set_scroll_wrapper);
  scm_c_define_gsubr("nk_window_close", 2, 0, 0, nk_window_close_wrapper);
  scm_c_define_gsubr("nk_window_collapse", 3, 0, 0, nk_window_collapse_wrapper);
  scm_c_define_gsubr("nk_window_collapse_if", 4, 0, 0, nk_window_collapse_if_wrapper);
  scm_c_define_gsubr("nk_window_show", 3, 0, 0, nk_window_show_wrapper);
  scm_c_define_gsubr("nk_window_show_if", 4, 0, 0, nk_window_show_if_wrapper);
  scm_c_define_gsubr("nk_layout_set_min_row_height", 2, 0, 0, nk_layout_set_min_row_height_wrapper);
  scm_c_define_gsubr("nk_layout_reset_min_row_height", 1, 0, 0, nk_layout_reset_min_row_height_wrapper);
  scm_c_define_gsubr("nk_layout_widget_bounds", 1, 0, 0, nk_layout_widget_bounds_wrapper);
  scm_c_define_gsubr("nk_layout_ratio_from_pixel", 2, 0, 0, nk_layout_ratio_from_pixel_wrapper);
  scm_c_define_gsubr("nk_layout_row_dynamic", 3, 0, 0, nk_layout_row_dynamic_wrapper);
  scm_c_define_gsubr("nk_layout_row_static", 4, 0, 0, nk_layout_row_static_wrapper);
  scm_c_define_gsubr("nk_layout_row_begin", 4, 0, 0, nk_layout_row_begin_wrapper);
  scm_c_define_gsubr("nk_layout_row_push", 2, 0, 0, nk_layout_row_push_wrapper);
  scm_c_define_gsubr("nk_layout_row_end", 1, 0, 0, nk_layout_row_end_wrapper);
  scm_c_define_gsubr("nk_layout_row", 5, 0, 0, nk_layout_row_wrapper);
  scm_c_define_gsubr("nk_layout_row_template_begin", 2, 0, 0, nk_layout_row_template_begin_wrapper);
  scm_c_define_gsubr("nk_layout_row_template_push_dynamic", 1, 0, 0, nk_layout_row_template_push_dynamic_wrapper);
  scm_c_define_gsubr("nk_layout_row_template_push_variable", 2, 0, 0, nk_layout_row_template_push_variable_wrapper);
  scm_c_define_gsubr("nk_layout_row_template_push_static", 2, 0, 0, nk_layout_row_template_push_static_wrapper);
  scm_c_define_gsubr("nk_layout_row_template_end", 1, 0, 0, nk_layout_row_template_end_wrapper);
  scm_c_define_gsubr("nk_layout_space_begin", 4, 0, 0, nk_layout_space_begin_wrapper);
  scm_c_define_gsubr("nk_layout_space_push", 2, 0, 0, nk_layout_space_push_wrapper);
  scm_c_define_gsubr("nk_layout_space_end", 1, 0, 0, nk_layout_space_end_wrapper);
  scm_c_define_gsubr("nk_layout_space_bounds", 1, 0, 0, nk_layout_space_bounds_wrapper);
  scm_c_define_gsubr("nk_layout_space_to_screen", 2, 0, 0, nk_layout_space_to_screen_wrapper);
  scm_c_define_gsubr("nk_layout_space_to_local", 2, 0, 0, nk_layout_space_to_local_wrapper);
  scm_c_define_gsubr("nk_layout_space_rect_to_screen", 2, 0, 0, nk_layout_space_rect_to_screen_wrapper);
  scm_c_define_gsubr("nk_layout_space_rect_to_local", 2, 0, 0, nk_layout_space_rect_to_local_wrapper);
  scm_c_define_gsubr("nk_group_begin", 3, 0, 0, nk_group_begin_wrapper);
  scm_c_define_gsubr("nk_group_begin_titled", 4, 0, 0, nk_group_begin_titled_wrapper);
  scm_c_define_gsubr("nk_group_end", 1, 0, 0, nk_group_end_wrapper);
  scm_c_define_gsubr("nk_group_scrolled_offset_begin", 5, 0, 0, nk_group_scrolled_offset_begin_wrapper);
  scm_c_define_gsubr("nk_group_scrolled_begin", 4, 0, 0, nk_group_scrolled_begin_wrapper);
  scm_c_define_gsubr("nk_group_scrolled_end", 1, 0, 0, nk_group_scrolled_end_wrapper);
  scm_c_define_gsubr("nk_group_get_scroll", 4, 0, 0, nk_group_get_scroll_wrapper);
  scm_c_define_gsubr("nk_group_set_scroll", 4, 0, 0, nk_group_set_scroll_wrapper);
  scm_c_define_gsubr("nk_tree_push_hashed", 7, 0, 0, nk_tree_push_hashed_wrapper);
  scm_c_define_gsubr("nk_tree_image_push_hashed", 8, 0, 0, nk_tree_image_push_hashed_wrapper);
  scm_c_define_gsubr("nk_tree_pop", 1, 0, 0, nk_tree_pop_wrapper);
  scm_c_define_gsubr("nk_tree_state_push", 4, 0, 0, nk_tree_state_push_wrapper);
  scm_c_define_gsubr("nk_tree_state_image_push", 5, 0, 0, nk_tree_state_image_push_wrapper);
  scm_c_define_gsubr("nk_tree_state_pop", 1, 0, 0, nk_tree_state_pop_wrapper);
  scm_c_define_gsubr("nk_tree_element_push_hashed", 8, 0, 0, nk_tree_element_push_hashed_wrapper);
  scm_c_define_gsubr("nk_tree_element_image_push_hashed", 9, 0, 0, nk_tree_element_image_push_hashed_wrapper);
  scm_c_define_gsubr("nk_tree_element_pop", 1, 0, 0, nk_tree_element_pop_wrapper);
  scm_c_define_gsubr("nk_list_view_begin", 6, 0, 0, nk_list_view_begin_wrapper);
  scm_c_define_gsubr("nk_list_view_end", 1, 0, 0, nk_list_view_end_wrapper);
  scm_c_define_gsubr("nk_widget", 2, 0, 0, nk_widget_wrapper);
  scm_c_define_gsubr("nk_widget_fitting", 3, 0, 0, nk_widget_fitting_wrapper);
  scm_c_define_gsubr("nk_widget_bounds", 1, 0, 0, nk_widget_bounds_wrapper);
  scm_c_define_gsubr("nk_widget_position", 1, 0, 0, nk_widget_position_wrapper);
  scm_c_define_gsubr("nk_widget_size", 1, 0, 0, nk_widget_size_wrapper);
  scm_c_define_gsubr("nk_widget_width", 1, 0, 0, nk_widget_width_wrapper);
  scm_c_define_gsubr("nk_widget_height", 1, 0, 0, nk_widget_height_wrapper);
  scm_c_define_gsubr("nk_widget_is_hovered", 1, 0, 0, nk_widget_is_hovered_wrapper);
  scm_c_define_gsubr("nk_widget_is_mouse_clicked", 2, 0, 0, nk_widget_is_mouse_clicked_wrapper);
  scm_c_define_gsubr("nk_widget_has_mouse_click_down", 3, 0, 0, nk_widget_has_mouse_click_down_wrapper);
  scm_c_define_gsubr("nk_spacing", 2, 0, 0, nk_spacing_wrapper);
  scm_c_define_gsubr("nk_text", 4, 0, 0, nk_text_wrapper);
  scm_c_define_gsubr("nk_text_colored", 5, 0, 0, nk_text_colored_wrapper);
  scm_c_define_gsubr("nk_text_wrap", 3, 0, 0, nk_text_wrap_wrapper);
  scm_c_define_gsubr("nk_text_wrap_colored", 4, 0, 0, nk_text_wrap_colored_wrapper);
  scm_c_define_gsubr("nk_label", 3, 0, 0, nk_label_wrapper);
  scm_c_define_gsubr("nk_label_colored", 4, 0, 0, nk_label_colored_wrapper);
  scm_c_define_gsubr("nk_label_wrap", 2, 0, 0, nk_label_wrap_wrapper);
  scm_c_define_gsubr("nk_label_colored_wrap", 3, 0, 0, nk_label_colored_wrap_wrapper);
  scm_c_define_gsubr("nk_image", 2, 0, 0, nk_image_wrapper);
  scm_c_define_gsubr("nk_image_color", 3, 0, 0, nk_image_color_wrapper);
  scm_c_define_gsubr("nk_button_text", 3, 0, 0, nk_button_text_wrapper);
  scm_c_define_gsubr("nk_button_label", 2, 0, 0, nk_button_label_wrapper);
  scm_c_define_gsubr("nk_button_color", 2, 0, 0, nk_button_color_wrapper);
  scm_c_define_gsubr("nk_button_symbol", 2, 0, 0, nk_button_symbol_wrapper);
  scm_c_define_gsubr("nk_button_image", 2, 0, 0, nk_button_image_wrapper);
  scm_c_define_gsubr("nk_button_symbol_label", 4, 0, 0, nk_button_symbol_label_wrapper);
  scm_c_define_gsubr("nk_button_symbol_text", 5, 0, 0, nk_button_symbol_text_wrapper);
  scm_c_define_gsubr("nk_button_image_label", 4, 0, 0, nk_button_image_label_wrapper);
  scm_c_define_gsubr("nk_button_image_text", 5, 0, 0, nk_button_image_text_wrapper);
  scm_c_define_gsubr("nk_button_text_styled", 4, 0, 0, nk_button_text_styled_wrapper);
  scm_c_define_gsubr("nk_button_label_styled", 3, 0, 0, nk_button_label_styled_wrapper);
  scm_c_define_gsubr("nk_button_symbol_styled", 3, 0, 0, nk_button_symbol_styled_wrapper);
  scm_c_define_gsubr("nk_button_image_styled", 3, 0, 0, nk_button_image_styled_wrapper);
  scm_c_define_gsubr("nk_button_symbol_text_styled", 6, 0, 0, nk_button_symbol_text_styled_wrapper);
  scm_c_define_gsubr("nk_button_symbol_label_styled", 5, 0, 0, nk_button_symbol_label_styled_wrapper);
  scm_c_define_gsubr("nk_button_image_label_styled", 5, 0, 0, nk_button_image_label_styled_wrapper);
  scm_c_define_gsubr("nk_button_image_text_styled", 6, 0, 0, nk_button_image_text_styled_wrapper);
  scm_c_define_gsubr("nk_button_set_behavior", 2, 0, 0, nk_button_set_behavior_wrapper);
  scm_c_define_gsubr("nk_button_push_behavior", 2, 0, 0, nk_button_push_behavior_wrapper);
  scm_c_define_gsubr("nk_button_pop_behavior", 1, 0, 0, nk_button_pop_behavior_wrapper);
  scm_c_define_gsubr("nk_check_label", 3, 0, 0, nk_check_label_wrapper);
  scm_c_define_gsubr("nk_check_text", 4, 0, 0, nk_check_text_wrapper);
  scm_c_define_gsubr("nk_check_flags_label", 4, 0, 0, nk_check_flags_label_wrapper);
  scm_c_define_gsubr("nk_check_flags_text", 5, 0, 0, nk_check_flags_text_wrapper);
  scm_c_define_gsubr("nk_checkbox_label", 3, 0, 0, nk_checkbox_label_wrapper);
  scm_c_define_gsubr("nk_checkbox_text", 4, 0, 0, nk_checkbox_text_wrapper);
  scm_c_define_gsubr("nk_checkbox_flags_label", 4, 0, 0, nk_checkbox_flags_label_wrapper);
  scm_c_define_gsubr("nk_checkbox_flags_text", 5, 0, 0, nk_checkbox_flags_text_wrapper);
  scm_c_define_gsubr("nk_radio_label", 3, 0, 0, nk_radio_label_wrapper);
  scm_c_define_gsubr("nk_radio_text", 4, 0, 0, nk_radio_text_wrapper);
  scm_c_define_gsubr("nk_option_label", 3, 0, 0, nk_option_label_wrapper);
  scm_c_define_gsubr("nk_option_text", 4, 0, 0, nk_option_text_wrapper);
  scm_c_define_gsubr("nk_selectable_label", 4, 0, 0, nk_selectable_label_wrapper);
  scm_c_define_gsubr("nk_selectable_text", 5, 0, 0, nk_selectable_text_wrapper);
  scm_c_define_gsubr("nk_selectable_image_label", 5, 0, 0, nk_selectable_image_label_wrapper);
  scm_c_define_gsubr("nk_selectable_image_text", 6, 0, 0, nk_selectable_image_text_wrapper);
  scm_c_define_gsubr("nk_selectable_symbol_label", 5, 0, 0, nk_selectable_symbol_label_wrapper);
  scm_c_define_gsubr("nk_selectable_symbol_text", 6, 0, 0, nk_selectable_symbol_text_wrapper);
  scm_c_define_gsubr("nk_select_label", 4, 0, 0, nk_select_label_wrapper);
  scm_c_define_gsubr("nk_select_text", 5, 0, 0, nk_select_text_wrapper);
  scm_c_define_gsubr("nk_select_image_label", 5, 0, 0, nk_select_image_label_wrapper);
  scm_c_define_gsubr("nk_select_image_text", 6, 0, 0, nk_select_image_text_wrapper);
  scm_c_define_gsubr("nk_select_symbol_label", 5, 0, 0, nk_select_symbol_label_wrapper);
  scm_c_define_gsubr("nk_select_symbol_text", 6, 0, 0, nk_select_symbol_text_wrapper);
  scm_c_define_gsubr("nk_slide_float", 5, 0, 0, nk_slide_float_wrapper);
  scm_c_define_gsubr("nk_slide_int", 5, 0, 0, nk_slide_int_wrapper);
  scm_c_define_gsubr("nk_slider_float", 5, 0, 0, nk_slider_float_wrapper);
  scm_c_define_gsubr("nk_slider_int", 5, 0, 0, nk_slider_int_wrapper);
  scm_c_define_gsubr("nk_progress", 4, 0, 0, nk_progress_wrapper);
  scm_c_define_gsubr("nk_prog", 4, 0, 0, nk_prog_wrapper);
  scm_c_define_gsubr("nk_color_picker", 3, 0, 0, nk_color_picker_wrapper);
  scm_c_define_gsubr("nk_color_pick", 3, 0, 0, nk_color_pick_wrapper);
  scm_c_define_gsubr("nk_property_int", 7, 0, 0, nk_property_int_wrapper);
  scm_c_define_gsubr("nk_property_float", 7, 0, 0, nk_property_float_wrapper);
  scm_c_define_gsubr("nk_property_double", 7, 0, 0, nk_property_double_wrapper);
  scm_c_define_gsubr("nk_propertyi", 7, 0, 0, nk_propertyi_wrapper);
  scm_c_define_gsubr("nk_propertyf", 7, 0, 0, nk_propertyf_wrapper);
  scm_c_define_gsubr("nk_propertyd", 7, 0, 0, nk_propertyd_wrapper);
  scm_c_define_gsubr("nk_edit_string", 6, 0, 0, nk_edit_string_wrapper);
  scm_c_define_gsubr("nk_edit_string_zero_terminated", 5, 0, 0, nk_edit_string_zero_terminated_wrapper);
  scm_c_define_gsubr("nk_edit_buffer", 4, 0, 0, nk_edit_buffer_wrapper);
  scm_c_define_gsubr("nk_edit_focus", 2, 0, 0, nk_edit_focus_wrapper);
  scm_c_define_gsubr("nk_edit_unfocus", 1, 0, 0, nk_edit_unfocus_wrapper);
  scm_c_define_gsubr("nk_chart_begin", 5, 0, 0, nk_chart_begin_wrapper);
  scm_c_define_gsubr("nk_chart_begin_colored", 7, 0, 0, nk_chart_begin_colored_wrapper);
  scm_c_define_gsubr("nk_chart_add_slot", 5, 0, 0, nk_chart_add_slot_wrapper);
  scm_c_define_gsubr("nk_chart_add_slot_colored", 7, 0, 0, nk_chart_add_slot_colored_wrapper);
  scm_c_define_gsubr("nk_chart_push", 2, 0, 0, nk_chart_push_wrapper);
  scm_c_define_gsubr("nk_chart_push_slot", 3, 0, 0, nk_chart_push_slot_wrapper);
  scm_c_define_gsubr("nk_chart_end", 1, 0, 0, nk_chart_end_wrapper);
  scm_c_define_gsubr("nk_plot", 5, 0, 0, nk_plot_wrapper);
  scm_c_define_gsubr("nk_plot_function", 6, 0, 0, nk_plot_function_wrapper);
  scm_c_define_gsubr("nk_popup_begin", 5, 0, 0, nk_popup_begin_wrapper);
  scm_c_define_gsubr("nk_popup_close", 1, 0, 0, nk_popup_close_wrapper);
  scm_c_define_gsubr("nk_popup_end", 1, 0, 0, nk_popup_end_wrapper);
  scm_c_define_gsubr("nk_popup_get_scroll", 3, 0, 0, nk_popup_get_scroll_wrapper);
  scm_c_define_gsubr("nk_popup_set_scroll", 3, 0, 0, nk_popup_set_scroll_wrapper);
  scm_c_define_gsubr("nk_combo", 6, 0, 0, nk_combo_wrapper);
  scm_c_define_gsubr("nk_combo_separator", 7, 0, 0, nk_combo_separator_wrapper);
  scm_c_define_gsubr("nk_combo_string", 6, 0, 0, nk_combo_string_wrapper);
  scm_c_define_gsubr("nk_combo_callback", 7, 0, 0, nk_combo_callback_wrapper);
  scm_c_define_gsubr("nk_combobox", 6, 0, 0, nk_combobox_wrapper);
  scm_c_define_gsubr("nk_combobox_string", 6, 0, 0, nk_combobox_string_wrapper);
  scm_c_define_gsubr("nk_combobox_separator", 7, 0, 0, nk_combobox_separator_wrapper);
  scm_c_define_gsubr("nk_combobox_callback", 7, 0, 0, nk_combobox_callback_wrapper);
  scm_c_define_gsubr("nk_combo_begin_text", 4, 0, 0, nk_combo_begin_text_wrapper);
  scm_c_define_gsubr("nk_combo_begin_label", 3, 0, 0, nk_combo_begin_label_wrapper);
  scm_c_define_gsubr("nk_combo_begin_color", 3, 0, 0, nk_combo_begin_color_wrapper);
  scm_c_define_gsubr("nk_combo_begin_symbol", 3, 0, 0, nk_combo_begin_symbol_wrapper);
  scm_c_define_gsubr("nk_combo_begin_symbol_label", 4, 0, 0, nk_combo_begin_symbol_label_wrapper);
  scm_c_define_gsubr("nk_combo_begin_symbol_text", 5, 0, 0, nk_combo_begin_symbol_text_wrapper);
  scm_c_define_gsubr("nk_combo_begin_image", 3, 0, 0, nk_combo_begin_image_wrapper);
  scm_c_define_gsubr("nk_combo_begin_image_label", 4, 0, 0, nk_combo_begin_image_label_wrapper);
  scm_c_define_gsubr("nk_combo_begin_image_text", 5, 0, 0, nk_combo_begin_image_text_wrapper);
  scm_c_define_gsubr("nk_combo_item_label", 3, 0, 0, nk_combo_item_label_wrapper);
  scm_c_define_gsubr("nk_combo_item_text", 4, 0, 0, nk_combo_item_text_wrapper);
  scm_c_define_gsubr("nk_combo_item_image_label", 4, 0, 0, nk_combo_item_image_label_wrapper);
  scm_c_define_gsubr("nk_combo_item_image_text", 5, 0, 0, nk_combo_item_image_text_wrapper);
  scm_c_define_gsubr("nk_combo_item_symbol_label", 4, 0, 0, nk_combo_item_symbol_label_wrapper);
  scm_c_define_gsubr("nk_combo_item_symbol_text", 5, 0, 0, nk_combo_item_symbol_text_wrapper);
  scm_c_define_gsubr("nk_combo_close", 1, 0, 0, nk_combo_close_wrapper);
  scm_c_define_gsubr("nk_combo_end", 1, 0, 0, nk_combo_end_wrapper);
  scm_c_define_gsubr("nk_contextual_begin", 4, 0, 0, nk_contextual_begin_wrapper);
  scm_c_define_gsubr("nk_contextual_item_text", 4, 0, 0, nk_contextual_item_text_wrapper);
  scm_c_define_gsubr("nk_contextual_item_label", 3, 0, 0, nk_contextual_item_label_wrapper);
  scm_c_define_gsubr("nk_contextual_item_image_label", 4, 0, 0, nk_contextual_item_image_label_wrapper);
  scm_c_define_gsubr("nk_contextual_item_image_text", 5, 0, 0, nk_contextual_item_image_text_wrapper);
  scm_c_define_gsubr("nk_contextual_item_symbol_label", 4, 0, 0, nk_contextual_item_symbol_label_wrapper);
  scm_c_define_gsubr("nk_contextual_item_symbol_text", 5, 0, 0, nk_contextual_item_symbol_text_wrapper);
  scm_c_define_gsubr("nk_contextual_close", 1, 0, 0, nk_contextual_close_wrapper);
  scm_c_define_gsubr("nk_contextual_end", 1, 0, 0, nk_contextual_end_wrapper);
  scm_c_define_gsubr("nk_tooltip", 2, 0, 0, nk_tooltip_wrapper);
  scm_c_define_gsubr("nk_tooltip_begin", 2, 0, 0, nk_tooltip_begin_wrapper);
  scm_c_define_gsubr("nk_tooltip_end", 1, 0, 0, nk_tooltip_end_wrapper);
  scm_c_define_gsubr("nk_menubar_begin", 1, 0, 0, nk_menubar_begin_wrapper);
  scm_c_define_gsubr("nk_menubar_end", 1, 0, 0, nk_menubar_end_wrapper);
  scm_c_define_gsubr("nk_menu_begin_text", 5, 0, 0, nk_menu_begin_text_wrapper);
  scm_c_define_gsubr("nk_menu_begin_label", 4, 0, 0, nk_menu_begin_label_wrapper);
  scm_c_define_gsubr("nk_menu_begin_image", 4, 0, 0, nk_menu_begin_image_wrapper);
  scm_c_define_gsubr("nk_menu_begin_image_text", 6, 0, 0, nk_menu_begin_image_text_wrapper);
  scm_c_define_gsubr("nk_menu_begin_image_label", 5, 0, 0, nk_menu_begin_image_label_wrapper);
  scm_c_define_gsubr("nk_menu_begin_symbol", 4, 0, 0, nk_menu_begin_symbol_wrapper);
  scm_c_define_gsubr("nk_menu_begin_symbol_text", 6, 0, 0, nk_menu_begin_symbol_text_wrapper);
  scm_c_define_gsubr("nk_menu_begin_symbol_label", 5, 0, 0, nk_menu_begin_symbol_label_wrapper);
  scm_c_define_gsubr("nk_menu_item_text", 4, 0, 0, nk_menu_item_text_wrapper);
  scm_c_define_gsubr("nk_menu_item_label", 3, 0, 0, nk_menu_item_label_wrapper);
  scm_c_define_gsubr("nk_menu_item_image_label", 4, 0, 0, nk_menu_item_image_label_wrapper);
  scm_c_define_gsubr("nk_menu_item_image_text", 5, 0, 0, nk_menu_item_image_text_wrapper);
  scm_c_define_gsubr("nk_menu_item_symbol_text", 5, 0, 0, nk_menu_item_symbol_text_wrapper);
  scm_c_define_gsubr("nk_menu_item_symbol_label", 4, 0, 0, nk_menu_item_symbol_label_wrapper);
  scm_c_define_gsubr("nk_menu_close", 1, 0, 0, nk_menu_close_wrapper);
  scm_c_define_gsubr("nk_menu_end", 1, 0, 0, nk_menu_end_wrapper);
  scm_c_define_gsubr("nk_style_default", 1, 0, 0, nk_style_default_wrapper);
  scm_c_define_gsubr("nk_style_from_table", 2, 0, 0, nk_style_from_table_wrapper);
  scm_c_define_gsubr("nk_style_load_cursor", 3, 0, 0, nk_style_load_cursor_wrapper);
  scm_c_define_gsubr("nk_style_load_all_cursors", 2, 0, 0, nk_style_load_all_cursors_wrapper);
  scm_c_define_gsubr("nk_style_get_color_by_name", 1, 0, 0, nk_style_get_color_by_name_wrapper);
  scm_c_define_gsubr("nk_style_set_cursor", 2, 0, 0, nk_style_set_cursor_wrapper);
  scm_c_define_gsubr("nk_style_show_cursor", 1, 0, 0, nk_style_show_cursor_wrapper);
  scm_c_define_gsubr("nk_style_hide_cursor", 1, 0, 0, nk_style_hide_cursor_wrapper);
  scm_c_define_gsubr("nk_style_push_font", 2, 0, 0, nk_style_push_font_wrapper);
  scm_c_define_gsubr("nk_style_push_float", 3, 0, 0, nk_style_push_float_wrapper);
  scm_c_define_gsubr("nk_style_push_vec2", 3, 0, 0, nk_style_push_vec2_wrapper);
  scm_c_define_gsubr("nk_style_push_style_item", 3, 0, 0, nk_style_push_style_item_wrapper);
  scm_c_define_gsubr("nk_style_push_flags", 3, 0, 0, nk_style_push_flags_wrapper);
  scm_c_define_gsubr("nk_style_push_color", 3, 0, 0, nk_style_push_color_wrapper);
  scm_c_define_gsubr("nk_style_pop_font", 1, 0, 0, nk_style_pop_font_wrapper);
  scm_c_define_gsubr("nk_style_pop_float", 1, 0, 0, nk_style_pop_float_wrapper);
  scm_c_define_gsubr("nk_style_pop_vec2", 1, 0, 0, nk_style_pop_vec2_wrapper);
  scm_c_define_gsubr("nk_style_pop_style_item", 1, 0, 0, nk_style_pop_style_item_wrapper);
  scm_c_define_gsubr("nk_style_pop_flags", 1, 0, 0, nk_style_pop_flags_wrapper);
  scm_c_define_gsubr("nk_style_pop_color", 1, 0, 0, nk_style_pop_color_wrapper);
  scm_c_define_gsubr("nk_rgb", 3, 0, 0, nk_rgb_wrapper);
  scm_c_define_gsubr("nk_rgb_iv", 1, 0, 0, nk_rgb_iv_wrapper);
  scm_c_define_gsubr("nk_rgb_bv", 1, 0, 0, nk_rgb_bv_wrapper);
  scm_c_define_gsubr("nk_rgb_f", 3, 0, 0, nk_rgb_f_wrapper);
  scm_c_define_gsubr("nk_rgb_fv", 1, 0, 0, nk_rgb_fv_wrapper);
  scm_c_define_gsubr("nk_rgb_cf", 1, 0, 0, nk_rgb_cf_wrapper);
  scm_c_define_gsubr("nk_rgb_hex", 1, 0, 0, nk_rgb_hex_wrapper);
  scm_c_define_gsubr("nk_rgba", 4, 0, 0, nk_rgba_wrapper);
  scm_c_define_gsubr("nk_rgba_u32", 1, 0, 0, nk_rgba_u32_wrapper);
  scm_c_define_gsubr("nk_rgba_iv", 1, 0, 0, nk_rgba_iv_wrapper);
  scm_c_define_gsubr("nk_rgba_bv", 1, 0, 0, nk_rgba_bv_wrapper);
  scm_c_define_gsubr("nk_rgba_f", 4, 0, 0, nk_rgba_f_wrapper);
  scm_c_define_gsubr("nk_rgba_fv", 1, 0, 0, nk_rgba_fv_wrapper);
  scm_c_define_gsubr("nk_rgba_cf", 1, 0, 0, nk_rgba_cf_wrapper);
  scm_c_define_gsubr("nk_rgba_hex", 1, 0, 0, nk_rgba_hex_wrapper);
  scm_c_define_gsubr("nk_hsva_colorf", 4, 0, 0, nk_hsva_colorf_wrapper);
  scm_c_define_gsubr("nk_hsva_colorfv", 1, 0, 0, nk_hsva_colorfv_wrapper);
  scm_c_define_gsubr("nk_colorf_hsva_f", 5, 0, 0, nk_colorf_hsva_f_wrapper);
  scm_c_define_gsubr("nk_colorf_hsva_fv", 2, 0, 0, nk_colorf_hsva_fv_wrapper);
  scm_c_define_gsubr("nk_hsv", 3, 0, 0, nk_hsv_wrapper);
  scm_c_define_gsubr("nk_hsv_iv", 1, 0, 0, nk_hsv_iv_wrapper);
  scm_c_define_gsubr("nk_hsv_bv", 1, 0, 0, nk_hsv_bv_wrapper);
  scm_c_define_gsubr("nk_hsv_f", 3, 0, 0, nk_hsv_f_wrapper);
  scm_c_define_gsubr("nk_hsv_fv", 1, 0, 0, nk_hsv_fv_wrapper);
  scm_c_define_gsubr("nk_hsva", 4, 0, 0, nk_hsva_wrapper);
  scm_c_define_gsubr("nk_hsva_iv", 1, 0, 0, nk_hsva_iv_wrapper);
  scm_c_define_gsubr("nk_hsva_bv", 1, 0, 0, nk_hsva_bv_wrapper);
  scm_c_define_gsubr("nk_hsva_f", 4, 0, 0, nk_hsva_f_wrapper);
  scm_c_define_gsubr("nk_hsva_fv", 1, 0, 0, nk_hsva_fv_wrapper);
  scm_c_define_gsubr("nk_color_f", 5, 0, 0, nk_color_f_wrapper);
  scm_c_define_gsubr("nk_color_fv", 2, 0, 0, nk_color_fv_wrapper);
  scm_c_define_gsubr("nk_color_cf", 1, 0, 0, nk_color_cf_wrapper);
  scm_c_define_gsubr("nk_color_d", 5, 0, 0, nk_color_d_wrapper);
  scm_c_define_gsubr("nk_color_dv", 2, 0, 0, nk_color_dv_wrapper);
  scm_c_define_gsubr("nk_color_u32", 1, 0, 0, nk_color_u32_wrapper);
  scm_c_define_gsubr("nk_color_hex_rgba", 2, 0, 0, nk_color_hex_rgba_wrapper);
  scm_c_define_gsubr("nk_color_hex_rgb", 2, 0, 0, nk_color_hex_rgb_wrapper);
  scm_c_define_gsubr("nk_color_hsv_i", 4, 0, 0, nk_color_hsv_i_wrapper);
  scm_c_define_gsubr("nk_color_hsv_b", 4, 0, 0, nk_color_hsv_b_wrapper);
  scm_c_define_gsubr("nk_color_hsv_iv", 2, 0, 0, nk_color_hsv_iv_wrapper);
  scm_c_define_gsubr("nk_color_hsv_bv", 2, 0, 0, nk_color_hsv_bv_wrapper);
  scm_c_define_gsubr("nk_color_hsv_f", 4, 0, 0, nk_color_hsv_f_wrapper);
  scm_c_define_gsubr("nk_color_hsv_fv", 2, 0, 0, nk_color_hsv_fv_wrapper);
  scm_c_define_gsubr("nk_color_hsva_i", 5, 0, 0, nk_color_hsva_i_wrapper);
  scm_c_define_gsubr("nk_color_hsva_b", 5, 0, 0, nk_color_hsva_b_wrapper);
  scm_c_define_gsubr("nk_color_hsva_iv", 2, 0, 0, nk_color_hsva_iv_wrapper);
  scm_c_define_gsubr("nk_color_hsva_bv", 2, 0, 0, nk_color_hsva_bv_wrapper);
  scm_c_define_gsubr("nk_color_hsva_f", 5, 0, 0, nk_color_hsva_f_wrapper);
  scm_c_define_gsubr("nk_color_hsva_fv", 2, 0, 0, nk_color_hsva_fv_wrapper);
  scm_c_define_gsubr("nk_handle_ptr", 1, 0, 0, nk_handle_ptr_wrapper);
  scm_c_define_gsubr("nk_handle_id", 1, 0, 0, nk_handle_id_wrapper);
  scm_c_define_gsubr("nk_image_ptr", 1, 0, 0, nk_image_ptr_wrapper);
  scm_c_define_gsubr("nk_image_id", 1, 0, 0, nk_image_id_wrapper);
  scm_c_define_gsubr("nk_image_is_subimage", 1, 0, 0, nk_image_is_subimage_wrapper);
  scm_c_define_gsubr("nk_subimage_ptr", 4, 0, 0, nk_subimage_ptr_wrapper);
  scm_c_define_gsubr("nk_subimage_id", 4, 0, 0, nk_subimage_id_wrapper);
  scm_c_define_gsubr("nk_subimage_handle", 4, 0, 0, nk_subimage_handle_wrapper);
  scm_c_define_gsubr("nk_murmur_hash", 3, 0, 0, nk_murmur_hash_wrapper);
  scm_c_define_gsubr("nk_triangle_from_direction", 5, 0, 0, nk_triangle_from_direction_wrapper);
  scm_c_define_gsubr("nk_vec2", 2, 0, 0, nk_vec2_wrapper);
  scm_c_define_gsubr("nk_vec2i", 2, 0, 0, nk_vec2i_wrapper);
  scm_c_define_gsubr("nk_vec2v", 1, 0, 0, nk_vec2v_wrapper);
  scm_c_define_gsubr("nk_vec2iv", 1, 0, 0, nk_vec2iv_wrapper);
  scm_c_define_gsubr("nk_get_null_rect", 0, 0, 0, nk_get_null_rect_wrapper);
  scm_c_define_gsubr("nk_rect", 4, 0, 0, nk_rect_wrapper);
  scm_c_define_gsubr("nk_recti", 4, 0, 0, nk_recti_wrapper);
  scm_c_define_gsubr("nk_recta", 2, 0, 0, nk_recta_wrapper);
  scm_c_define_gsubr("nk_rectv", 1, 0, 0, nk_rectv_wrapper);
  scm_c_define_gsubr("nk_rectiv", 1, 0, 0, nk_rectiv_wrapper);
  scm_c_define_gsubr("nk_rect_pos", 1, 0, 0, nk_rect_pos_wrapper);
  scm_c_define_gsubr("nk_rect_size", 1, 0, 0, nk_rect_size_wrapper);
  scm_c_define_gsubr("nk_strlen", 1, 0, 0, nk_strlen_wrapper);
  scm_c_define_gsubr("nk_stricmp", 2, 0, 0, nk_stricmp_wrapper);
  scm_c_define_gsubr("nk_stricmpn", 3, 0, 0, nk_stricmpn_wrapper);
  scm_c_define_gsubr("nk_strtoi", 2, 0, 0, nk_strtoi_wrapper);
  scm_c_define_gsubr("nk_strtof", 2, 0, 0, nk_strtof_wrapper);
  scm_c_define_gsubr("nk_strtod", 2, 0, 0, nk_strtod_wrapper);
  scm_c_define_gsubr("nk_strfilter", 2, 0, 0, nk_strfilter_wrapper);
  scm_c_define_gsubr("nk_strmatch_fuzzy_string", 3, 0, 0, nk_strmatch_fuzzy_string_wrapper);
  scm_c_define_gsubr("nk_strmatch_fuzzy_text", 4, 0, 0, nk_strmatch_fuzzy_text_wrapper);
  scm_c_define_gsubr("nk_utf_decode", 3, 0, 0, nk_utf_decode_wrapper);
  scm_c_define_gsubr("nk_utf_encode", 3, 0, 0, nk_utf_encode_wrapper);
  scm_c_define_gsubr("nk_utf_len", 2, 0, 0, nk_utf_len_wrapper);
  scm_c_define_gsubr("nk_utf_at", 5, 0, 0, nk_utf_at_wrapper);
  scm_c_define_gsubr("nk_font_default_glyph_ranges", 0, 0, 0, nk_font_default_glyph_ranges_wrapper);
  scm_c_define_gsubr("nk_font_chinese_glyph_ranges", 0, 0, 0, nk_font_chinese_glyph_ranges_wrapper);
  scm_c_define_gsubr("nk_font_cyrillic_glyph_ranges", 0, 0, 0, nk_font_cyrillic_glyph_ranges_wrapper);
  scm_c_define_gsubr("nk_font_korean_glyph_ranges", 0, 0, 0, nk_font_korean_glyph_ranges_wrapper);
  scm_c_define_gsubr("nk_font_atlas_init_default", 1, 0, 0, nk_font_atlas_init_default_wrapper);
  scm_c_define_gsubr("nk_font_atlas_init", 2, 0, 0, nk_font_atlas_init_wrapper);
  scm_c_define_gsubr("nk_font_atlas_init_custom", 3, 0, 0, nk_font_atlas_init_custom_wrapper);
  scm_c_define_gsubr("nk_font_atlas_begin", 1, 0, 0, nk_font_atlas_begin_wrapper);
  scm_c_define_gsubr("nk_font_config", 1, 0, 0, nk_font_config_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add", 2, 0, 0, nk_font_atlas_add_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add_default", 3, 0, 0, nk_font_atlas_add_default_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add_from_memory", 5, 0, 0, nk_font_atlas_add_from_memory_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add_from_file", 4, 0, 0, nk_font_atlas_add_from_file_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add_compressed", 5, 0, 0, nk_font_atlas_add_compressed_wrapper);
  scm_c_define_gsubr("nk_font_atlas_add_compressed_base85", 4, 0, 0, nk_font_atlas_add_compressed_base85_wrapper);
  scm_c_define_gsubr("nk_font_atlas_bake", 4, 0, 0, nk_font_atlas_bake_wrapper);
  scm_c_define_gsubr("nk_font_atlas_end", 3, 0, 0, nk_font_atlas_end_wrapper);
  scm_c_define_gsubr("nk_font_find_glyph", 2, 0, 0, nk_font_find_glyph_wrapper);
  scm_c_define_gsubr("nk_font_atlas_cleanup", 1, 0, 0, nk_font_atlas_cleanup_wrapper);
  scm_c_define_gsubr("nk_font_atlas_clear", 1, 0, 0, nk_font_atlas_clear_wrapper);
  scm_c_define_gsubr("nk_buffer_init_default", 1, 0, 0, nk_buffer_init_default_wrapper);
  scm_c_define_gsubr("nk_buffer_init", 3, 0, 0, nk_buffer_init_wrapper);
  scm_c_define_gsubr("nk_buffer_init_fixed", 3, 0, 0, nk_buffer_init_fixed_wrapper);
  scm_c_define_gsubr("nk_buffer_info", 2, 0, 0, nk_buffer_info_wrapper);
  scm_c_define_gsubr("nk_buffer_push", 5, 0, 0, nk_buffer_push_wrapper);
  scm_c_define_gsubr("nk_buffer_mark", 2, 0, 0, nk_buffer_mark_wrapper);
  scm_c_define_gsubr("nk_buffer_reset", 2, 0, 0, nk_buffer_reset_wrapper);
  scm_c_define_gsubr("nk_buffer_clear", 1, 0, 0, nk_buffer_clear_wrapper);
  scm_c_define_gsubr("nk_buffer_free", 1, 0, 0, nk_buffer_free_wrapper);
  scm_c_define_gsubr("nk_buffer_memory", 1, 0, 0, nk_buffer_memory_wrapper);
  scm_c_define_gsubr("nk_buffer_memory_const", 1, 0, 0, nk_buffer_memory_const_wrapper);
  scm_c_define_gsubr("nk_buffer_total", 1, 0, 0, nk_buffer_total_wrapper);
  scm_c_define_gsubr("nk_str_init_default", 1, 0, 0, nk_str_init_default_wrapper);
  scm_c_define_gsubr("nk_str_init", 3, 0, 0, nk_str_init_wrapper);
  scm_c_define_gsubr("nk_str_init_fixed", 3, 0, 0, nk_str_init_fixed_wrapper);
  scm_c_define_gsubr("nk_str_clear", 1, 0, 0, nk_str_clear_wrapper);
  scm_c_define_gsubr("nk_str_free", 1, 0, 0, nk_str_free_wrapper);
  scm_c_define_gsubr("nk_str_append_text_char", 3, 0, 0, nk_str_append_text_char_wrapper);
  scm_c_define_gsubr("nk_str_append_str_char", 2, 0, 0, nk_str_append_str_char_wrapper);
  scm_c_define_gsubr("nk_str_append_text_utf8", 3, 0, 0, nk_str_append_text_utf8_wrapper);
  scm_c_define_gsubr("nk_str_append_str_utf8", 2, 0, 0, nk_str_append_str_utf8_wrapper);
  scm_c_define_gsubr("nk_str_append_text_runes", 3, 0, 0, nk_str_append_text_runes_wrapper);
  scm_c_define_gsubr("nk_str_append_str_runes", 2, 0, 0, nk_str_append_str_runes_wrapper);
  scm_c_define_gsubr("nk_str_insert_at_char", 4, 0, 0, nk_str_insert_at_char_wrapper);
  scm_c_define_gsubr("nk_str_insert_at_rune", 4, 0, 0, nk_str_insert_at_rune_wrapper);
  scm_c_define_gsubr("nk_str_insert_text_char", 4, 0, 0, nk_str_insert_text_char_wrapper);
  scm_c_define_gsubr("nk_str_insert_str_char", 3, 0, 0, nk_str_insert_str_char_wrapper);
  scm_c_define_gsubr("nk_str_insert_text_utf8", 4, 0, 0, nk_str_insert_text_utf8_wrapper);
  scm_c_define_gsubr("nk_str_insert_str_utf8", 3, 0, 0, nk_str_insert_str_utf8_wrapper);
  scm_c_define_gsubr("nk_str_insert_text_runes", 4, 0, 0, nk_str_insert_text_runes_wrapper);
  scm_c_define_gsubr("nk_str_insert_str_runes", 3, 0, 0, nk_str_insert_str_runes_wrapper);
  scm_c_define_gsubr("nk_str_remove_chars", 2, 0, 0, nk_str_remove_chars_wrapper);
  scm_c_define_gsubr("nk_str_remove_runes", 2, 0, 0, nk_str_remove_runes_wrapper);
  scm_c_define_gsubr("nk_str_delete_chars", 3, 0, 0, nk_str_delete_chars_wrapper);
  scm_c_define_gsubr("nk_str_delete_runes", 3, 0, 0, nk_str_delete_runes_wrapper);
  scm_c_define_gsubr("nk_str_at_char", 2, 0, 0, nk_str_at_char_wrapper);
  scm_c_define_gsubr("nk_str_at_rune", 4, 0, 0, nk_str_at_rune_wrapper);
  scm_c_define_gsubr("nk_str_rune_at", 2, 0, 0, nk_str_rune_at_wrapper);
  scm_c_define_gsubr("nk_str_at_char_const", 2, 0, 0, nk_str_at_char_const_wrapper);
  scm_c_define_gsubr("nk_str_at_const", 4, 0, 0, nk_str_at_const_wrapper);
  scm_c_define_gsubr("nk_str_get", 1, 0, 0, nk_str_get_wrapper);
  scm_c_define_gsubr("nk_str_get_const", 1, 0, 0, nk_str_get_const_wrapper);
  scm_c_define_gsubr("nk_str_len", 1, 0, 0, nk_str_len_wrapper);
  scm_c_define_gsubr("nk_str_len_char", 1, 0, 0, nk_str_len_char_wrapper);
  scm_c_define_gsubr("nk_filter_default", 2, 0, 0, nk_filter_default_wrapper);
  scm_c_define_gsubr("nk_filter_ascii", 2, 0, 0, nk_filter_ascii_wrapper);
  scm_c_define_gsubr("nk_filter_float", 2, 0, 0, nk_filter_float_wrapper);
  scm_c_define_gsubr("nk_filter_decimal", 2, 0, 0, nk_filter_decimal_wrapper);
  scm_c_define_gsubr("nk_filter_hex", 2, 0, 0, nk_filter_hex_wrapper);
  scm_c_define_gsubr("nk_filter_oct", 2, 0, 0, nk_filter_oct_wrapper);
  scm_c_define_gsubr("nk_filter_binary", 2, 0, 0, nk_filter_binary_wrapper);
  scm_c_define_gsubr("nk_textedit_init_default", 1, 0, 0, nk_textedit_init_default_wrapper);
  scm_c_define_gsubr("nk_textedit_init", 3, 0, 0, nk_textedit_init_wrapper);
  scm_c_define_gsubr("nk_textedit_init_fixed", 3, 0, 0, nk_textedit_init_fixed_wrapper);
  scm_c_define_gsubr("nk_textedit_free", 1, 0, 0, nk_textedit_free_wrapper);
  scm_c_define_gsubr("nk_textedit_text", 3, 0, 0, nk_textedit_text_wrapper);
  scm_c_define_gsubr("nk_textedit_delete", 3, 0, 0, nk_textedit_delete_wrapper);
  scm_c_define_gsubr("nk_textedit_delete_selection", 1, 0, 0, nk_textedit_delete_selection_wrapper);
  scm_c_define_gsubr("nk_textedit_select_all", 1, 0, 0, nk_textedit_select_all_wrapper);
  scm_c_define_gsubr("nk_textedit_cut", 1, 0, 0, nk_textedit_cut_wrapper);
  scm_c_define_gsubr("nk_textedit_paste", 3, 0, 0, nk_textedit_paste_wrapper);
  scm_c_define_gsubr("nk_textedit_undo", 1, 0, 0, nk_textedit_undo_wrapper);
  scm_c_define_gsubr("nk_textedit_redo", 1, 0, 0, nk_textedit_redo_wrapper);
  scm_c_define_gsubr("nk_stroke_line", 7, 0, 0, nk_stroke_line_wrapper);
  scm_c_define_gsubr("nk_stroke_curve", 3, 0, 0, nk_stroke_curve_wrapper);
  scm_c_define_gsubr("nk_stroke_rect", 5, 0, 0, nk_stroke_rect_wrapper);
  scm_c_define_gsubr("nk_stroke_circle", 4, 0, 0, nk_stroke_circle_wrapper);
  scm_c_define_gsubr("nk_stroke_arc", 8, 0, 0, nk_stroke_arc_wrapper);
  scm_c_define_gsubr("nk_stroke_triangle", 9, 0, 0, nk_stroke_triangle_wrapper);
  scm_c_define_gsubr("nk_stroke_polyline", 5, 0, 0, nk_stroke_polyline_wrapper);
  scm_c_define_gsubr("nk_stroke_polygon", 5, 0, 0, nk_stroke_polygon_wrapper);
  scm_c_define_gsubr("nk_fill_rect", 4, 0, 0, nk_fill_rect_wrapper);
  scm_c_define_gsubr("nk_fill_rect_multi_color", 6, 0, 0, nk_fill_rect_multi_color_wrapper);
  scm_c_define_gsubr("nk_fill_circle", 3, 0, 0, nk_fill_circle_wrapper);
  scm_c_define_gsubr("nk_fill_arc", 7, 0, 0, nk_fill_arc_wrapper);
  scm_c_define_gsubr("nk_fill_triangle", 8, 0, 0, nk_fill_triangle_wrapper);
  scm_c_define_gsubr("nk_fill_polygon", 4, 0, 0, nk_fill_polygon_wrapper);
  scm_c_define_gsubr("nk_draw_image", 4, 0, 0, nk_draw_image_wrapper);
  scm_c_define_gsubr("nk_draw_text", 7, 0, 0, nk_draw_text_wrapper);
  scm_c_define_gsubr("nk_push_scissor", 2, 0, 0, nk_push_scissor_wrapper);
  scm_c_define_gsubr("nk_push_custom", 4, 0, 0, nk_push_custom_wrapper);
  scm_c_define_gsubr("nk_input_has_mouse_click", 2, 0, 0, nk_input_has_mouse_click_wrapper);
  scm_c_define_gsubr("nk_input_has_mouse_click_in_rect", 3, 0, 0, nk_input_has_mouse_click_in_rect_wrapper);
  scm_c_define_gsubr("nk_input_has_mouse_click_down_in_rect", 4, 0, 0, nk_input_has_mouse_click_down_in_rect_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_click_in_rect", 3, 0, 0, nk_input_is_mouse_click_in_rect_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_click_down_in_rect", 4, 0, 0, nk_input_is_mouse_click_down_in_rect_wrapper);
  scm_c_define_gsubr("nk_input_any_mouse_click_in_rect", 2, 0, 0, nk_input_any_mouse_click_in_rect_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_prev_hovering_rect", 2, 0, 0, nk_input_is_mouse_prev_hovering_rect_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_hovering_rect", 2, 0, 0, nk_input_is_mouse_hovering_rect_wrapper);
  scm_c_define_gsubr("nk_input_mouse_clicked", 3, 0, 0, nk_input_mouse_clicked_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_down", 2, 0, 0, nk_input_is_mouse_down_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_pressed", 2, 0, 0, nk_input_is_mouse_pressed_wrapper);
  scm_c_define_gsubr("nk_input_is_mouse_released", 2, 0, 0, nk_input_is_mouse_released_wrapper);
  scm_c_define_gsubr("nk_input_is_key_pressed", 2, 0, 0, nk_input_is_key_pressed_wrapper);
  scm_c_define_gsubr("nk_input_is_key_released", 2, 0, 0, nk_input_is_key_released_wrapper);
  scm_c_define_gsubr("nk_input_is_key_down", 2, 0, 0, nk_input_is_key_down_wrapper);
  scm_c_define_gsubr("nk_draw_list_init", 1, 0, 0, nk_draw_list_init_wrapper);
  scm_c_define_gsubr("nk_draw_list_setup", 7, 0, 0, nk_draw_list_setup_wrapper);
  scm_c_define_gsubr("nk__draw_list_begin", 2, 0, 0, nk__draw_list_begin_wrapper);
  scm_c_define_gsubr("nk__draw_list_next", 3, 0, 0, nk__draw_list_next_wrapper);
  scm_c_define_gsubr("nk__draw_list_end", 2, 0, 0, nk__draw_list_end_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_clear", 1, 0, 0, nk_draw_list_path_clear_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_line_to", 2, 0, 0, nk_draw_list_path_line_to_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_arc_to_fast", 5, 0, 0, nk_draw_list_path_arc_to_fast_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_arc_to", 6, 0, 0, nk_draw_list_path_arc_to_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_rect_to", 4, 0, 0, nk_draw_list_path_rect_to_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_curve_to", 5, 0, 0, nk_draw_list_path_curve_to_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_fill", 2, 0, 0, nk_draw_list_path_fill_wrapper);
  scm_c_define_gsubr("nk_draw_list_path_stroke", 4, 0, 0, nk_draw_list_path_stroke_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_line", 5, 0, 0, nk_draw_list_stroke_line_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_rect", 5, 0, 0, nk_draw_list_stroke_rect_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_triangle", 6, 0, 0, nk_draw_list_stroke_triangle_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_circle", 6, 0, 0, nk_draw_list_stroke_circle_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_curve", 8, 0, 0, nk_draw_list_stroke_curve_wrapper);
  scm_c_define_gsubr("nk_draw_list_stroke_poly_line", 7, 0, 0, nk_draw_list_stroke_poly_line_wrapper);
  scm_c_define_gsubr("nk_draw_list_fill_rect", 4, 0, 0, nk_draw_list_fill_rect_wrapper);
  scm_c_define_gsubr("nk_draw_list_fill_rect_multi_color", 6, 0, 0, nk_draw_list_fill_rect_multi_color_wrapper);
  scm_c_define_gsubr("nk_draw_list_fill_triangle", 5, 0, 0, nk_draw_list_fill_triangle_wrapper);
  scm_c_define_gsubr("nk_draw_list_fill_circle", 5, 0, 0, nk_draw_list_fill_circle_wrapper);
  scm_c_define_gsubr("nk_draw_list_fill_poly_convex", 5, 0, 0, nk_draw_list_fill_poly_convex_wrapper);
  scm_c_define_gsubr("nk_draw_list_add_image", 4, 0, 0, nk_draw_list_add_image_wrapper);
  scm_c_define_gsubr("nk_draw_list_add_text", 7, 0, 0, nk_draw_list_add_text_wrapper);
  scm_c_define_gsubr("nk_style_item_image", 1, 0, 0, nk_style_item_image_wrapper);
  scm_c_define_gsubr("nk_style_item_color", 1, 0, 0, nk_style_item_color_wrapper);
  scm_c_define_gsubr("nk_style_item_hide", 0, 0, 0, nk_style_item_hide_wrapper);
}
